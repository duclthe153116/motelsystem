﻿Create database Motel_System

Create table Account(
	AccountID int IDENTITY(1,1) primary key,
	userName nvarchar(50),
	[passWord] nvarchar(50),
)

Create table [Role](
	RoleID int IDENTITY(1,1) primary key,
	roleName nvarchar(50),
)

Create table RoleOfAccount(
	RoleOfAccountID int IDENTITY(1,1) primary key,
	AccountID int references Account(AccountID),
	RoleID int references [Role](RoleID),
)

Create table [Profile](
	profileID int IDENTITY(1,1) primary key,
	AccountID int unique references Account(AccountID),
	fullName nvarchar(50),
	phone varchar(10) check(phone like '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	[address] nvarchar(100),
	email varchar(100),
	DOB date,
	Gender nvarchar(20),
	imageProfile nvarchar(1000)
)


Create table Motel(
	motelID int IDENTITY(1,1) primary key,
	AccountID int references Account(AccountID),
	motelName nvarchar(50),
	motelAddress nvarchar(100),
	motelPhone varchar(10) check(motelPhone like '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	motelStatus bit
)



Create table Post(
	postID int IDENTITY(1,1) primary key,
	motelID int references Motel(motelID),
	postContent nvarchar(1000),
	postDate date,
	postTitle nvarchar(1000),
	postStatus bit
)


Create table PostImage(
	postImageID int IDENTITY(1,1) primary key,
	postID int references Post(postID),
	postImageLink varchar(1000),
)

Create table [Services](
	servicesID int IDENTITY(1,1) primary key,
	servicesName nvarchar(100),
	price float,
)

Create table ServicesOfMotel(
	servicesOfMotelID int IDENTITY(1,1) primary key,
	motelID int references Motel(motelID),
	serviceID int references [Services](servicesID),
)

Create table Room (
	roomID int IDENTITY(1,1) primary key,
	motelID int references Motel(motelID),
	roomName nvarchar(50),
	numberOfPeopleMax int,
	numberOfPeopleCurrent int,
	price float,
	area float,
	noteRoom nvarchar(1000),
)

Create table Request (
	requestID int IDENTITY(1,1) primary key,
	roomID int references Room(roomID),
	content nvarchar(1000),
	[status] varchar(10),
	noteRequest nvarchar(1000),
	createdAt datetime,
	updateAt datetime
)

Create table RoomOfAccount(
	roomOfAccountID int IDENTITY(1,1) primary key,
	AccountID int references Account(AccountID),
	roomID int references Room(roomID),
	startAt datetime,
	endAt datetime
)

Create table ServicesOfRoom(
	servicesOfRoomID int IDENTITY(1,1) primary key,
	roomID int references Room(roomID),
	servicesID int references [Services](servicesID),	
)

Create table Bill(
	billID int IDENTITY(1,1) primary key,
	roomID int references Room(roomID),
	dateOfPayment datetime,
	totalBill float,
	statusBill varchar(10),
)

Create table BillDetail(
	billDetailID int IDENTITY(1,1) primary key,
	billID int references Bill(billID),
	servicesOfRoomID int unique references ServicesOfRoom(servicesOfRoomID),
	dateCreate datetime,
	quantity int,
	totalBillDetail float,
)

