/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    // Active tooltips
    $('[data-toggle="tooltip"]').tooltip();

    // Filter table rows based on search
    $("#search").on("keyup", function () {
        var term = $(this).val().toLowerCase();
        $("table tbody tr").each(function () {
            $row = $(this);
            var keyword = $row.find("td").text().toLowerCase();
            console.log(keyword);
            if (keyword.search(term) < 0) {
                $row.hide();
            } else {
                $row.show();
            }
        });
    });
});

$(document).ready(function () {
    $('#myTable').dataTable({
        "bPaginate": true,
        "bFilter": false,
        "bInfo": false
    });
});

var table = $("#datatable").DataTable({
    "paging": true,
    "ordering": false,
    "searching": false
});

function UpdateRequestPost(id, id_select) {
    var conf = confirm("Bạn có muốn chấp nhận bài này?");
    if(!conf){
        return false;
    }
    var option = document.getElementById(id_select);
//        var selected = option.options[option.selectedIndex].text;
    var selected = option.value;
    console.log(selected);
    var id_post = id.split("_")[1];
    if (selected === '1') {
        document.getElementById(id).href = "../admin/UpdatePostStatus?id=" + id_post + "&accept=true&request=true";
    } else {
        document.getElementById(id).href = "../admin/UpdatePostStatus?id=" + id_post + "&accept=false&request=true";
    }
}
;

function UpdateRequestHost(id, id_select) {
    var conf = confirm("Bạn có muốn chấp nhận trọ này?");
    if(!conf){
        return false;
    }
    var option = document.getElementById(id_select);
//        var selected = option.options[option.selectedIndex].text;
    var selected = option.value;
    console.log(selected);
    var id_post = id.split("_")[1];
    if (selected === '1') {
        document.getElementById(id).href = "../admin/UpdateMotelStatus?id=" + id_post + "&accept=true&request=true";
    } else {
        document.getElementById(id).href = "../admin/UpdateMotelStatus?id=" + id_post + "&accept=false&request=true";
    }
}
;


