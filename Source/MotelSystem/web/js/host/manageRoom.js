$(document).ready(function () {
    // Active tooltips
    $('[data-toggle="tooltip"]').tooltip();

    // Filter table rows based on search
    $("#search").on("keyup", function () {
        var term = $(this).val().toLowerCase();
        $("table tbody tr").each(function() {
            $row = $(this);
            var keyword = $row.find("td").text().toLowerCase();
            console.log(keyword);
            if(keyword.search(term) < 0) {
                $row.hide();
            }else {
                $row.show();
            }
        });
    });
});

$(document).ready(function(){
    $('#myTable').dataTable({
        "bPaginate": true,
        "bFilter": false,
        "bInfo": false
                 });
});

var table = $("#datatable").DataTable({
   "paging": true,
   "ordering": false,
   "searching": false
});

