/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
    // Active tooltips
    $('[data-toggle="tooltip"]').tooltip();

    // Filter table rows based on search
    $("#search").on("keyup", function () {
        var term = $(this).val().toLowerCase();
        $("table tbody tr").each(function () {
            $row = $(this);
            var keyword = $row.find("td").text().toLowerCase();
            console.log(keyword);
            if (keyword.search(term) < 0) {
                $row.hide();
            } else {
                $row.show();
            }
        });
    });
});

$(document).ready(function () {
    $('#myTable').dataTable({
        "bPaginate": true,
        "bFilter": false,
        "bInfo": false
    });
});

var table = $("#datatable").DataTable({
    "paging": true,
    "ordering": false,
    "searching": false
});

function UpdateRoleAccount(id, id_select) {
    var conf = confirm("Bạn có muốn thay đổi vai trò?");
    if(!conf){
        return false;
    }
    var id_account = id.split("_")[1];
    var cboxes = document.getElementsByName(id_select);
    var isAdmin = (cboxes[0].checked?'true':'false');
    var isHost = (cboxes[1].checked?'true':'false');
    var isCustomer = (cboxes[2].checked?'true':'false');

    // In ra kết quả
    document.getElementById(id).href = "../admin/UpdateRole?id="+id_account+"&isAdmin="+isAdmin+"&isHost="+isHost+"&isCustomer="+isCustomer;
}
;

//function UpdateRoleAccount(id, id_select) {
//    var cboxes = document.getElementsByName(id_select);
//    var len = cboxes.length;
//    var isAdmin = (cboxes[0].checked?'true':'false');
//    var isHost = (cboxes[1].checked?'true':'false');
//    var isCustomer = (cboxes[2].checked?'true':'false');
//
//    var link = isAdmin=${isAdmin}&isHost=${isHost}&isCustomer=${isCustomer};
//    console.log(link);
//    document.getElementById(id).href = "../admin/UpdateRole?id="+id+"&isAdmin="+isAdmin+"&isHost="+isHost+"&isCustomer="+isCustomer;
//};