/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function validateForm(listRoom) {
//    console.log('listOfRoom', list);
    var motelID = document.getElementById("motelID").value;
    var roomName = document.forms["myForm"]["roomName"].value;
    for (var i = 0; i < listRoom.length; i++){
        if (listRoom[i].roomName == roomName && listRoom[i].motelID == motelID){
            alert("Tên phòng đã tồn tại!");
            return (false);
        }
    }
    var numberMax = document.forms["myForm"]["numberMax"].value;
    var numberMaxI;
    try {
        numberMaxI = parseInt(numberMax);
        if (numberMaxI == numberMax) {
            if (numberMax < 1) {
                alert("Số lượng người tối đa phải lớn hơn hoặc bằng 1!");
                return false;
            }
        } else {
            alert("Số lượng người tối đa phải là số nguyên!");
            return false;
        }
    } catch (ex) {
        alert("Số lượng người tối đa phải là số nguyên!");
        return(false);
    }
//    var numberCurrent = document.forms["myForm"]["numberCurrent"].value;
//    try {
//        numberCurrentI = parseInt(numberCurrent);
//        if (numberCurrentI == numberCurrent) {
//            if (numberCurrentI > numberMaxI ) {
//                alert("Số lượng người hiện tại phải bé hơn hoặc bằng số người tối đa!");
//                return false;
//            }
//        } else {
//            alert("Số lượng người hiện tại phải là số nguyên!");
//            return false;
//        }
//    } catch (ex) {
//        alert("Số lượng người hiện tại phải là số nguyên!");
//        return(false);
//    }
    var area = document.forms["myForm"]["area"].value;
    try {
        areaF = parseFloat(area);
        if (areaF == area) {
            if (areaF <= 0) {
                alert("Diện tích phải lớn hơn 0!");
                return false;
            }
        } else {
            alert("Diện tích phải là số thực hoặc số nguyên!");
            return false;
        }
    } catch (ex) {
        alert("Diện tích phải là số thực hoặc số nguyên!");
        return(false);
    }
    var price = document.forms["myForm"]["price"].value;
    try {
        priceF = parseFloat(price);
        if (priceF == price) {
            if (priceF < 0) {
                alert("Giá phòng phải lớn hơn 0!");
                return false;
            }
        } else {
            alert("Giá phòng phải là số thực hoặc số nguyên!");
            return false;
        }
    } catch (ex) {
        alert("Giá phòng phải là số thực hoặc số nguyên!");
        return(false);
    }
}