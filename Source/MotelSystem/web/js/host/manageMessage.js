/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    // Active tooltips
    $('[data-toggle="tooltip"]').tooltip();

    // Filter table rows based on search
    $("#search").on("keyup", function () {
        var term = $(this).val().toLowerCase();
        $("table tbody tr").each(function () {
            $row = $(this);
            var keyword = $row.find("td").text().toLowerCase();
            console.log(keyword);
            if (keyword.search(term) < 0) {
                $row.hide();
            } else {
                $row.show();
            }
        });
    });
});

$(document).ready(function () {
    $('#myTable').dataTable({
        "bPaginate": true,
        "bFilter": false,
        "bInfo": false
    });
});

var table = $("#datatable").DataTable({
    "paging": true,
    "ordering": false,
    "searching": false
});



function openFormAdd() {
    document.getElementById("myFormAdd").style.display = "block";
}

function closeFormAdd() {
    document.getElementById("myFormAdd").style.display = "none";
}

function validateFormAdd() {
//    console.log('listOfRoom', list);
    var price = document.forms["myFormAdd"]["servicesPrice"].value;
    var priceF;
    try {
        priceF = parseFloat(price);
        if (priceF == price) {
            if (priceF < 0) {
                throw ""
            }
        } else {
            throw "";
        }
    } catch (ex) {
        alert("Giá phải là số thực lớn hơn hoặc bằng 0");
        return(false);
    }
}

function openFormRep(id, from, content) {
    document.getElementById("messageID").value = id;
    document.getElementById("from").value = from;
    document.getElementById("content_id").placeholder = content;
    document.getElementById("myFormRep").style.display = "block";
}

function closeFormRep() {
    document.getElementById("myFormRep").style.display = "none";
}

function validateFormRep() {
//    console.log('listOfRoom', list);
    var price = document.forms["myFormRep"]["servicesPrice"].value;
    var priceF;
    try {
        priceF = parseFloat(price);
        if (priceF == price) {
            if (priceF < 0) {
                throw ""
            }
        } else {
            throw "";
        }
    } catch (ex) {
        alert("Giá phải là số thực lớn hơn hoặc bằng 0");
        return(false);
    }
}