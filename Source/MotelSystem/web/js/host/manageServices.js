$(document).ready(function () {
    // Active tooltips
    $('[data-toggle="tooltip"]').tooltip();

    // Filter table rows based on search
    $("#search").on("keyup", function () {
        var term = $(this).val().toLowerCase();
        $("table tbody tr").each(function() {
            $row = $(this);
            var keyword = $row.find("td").text().toLowerCase();
            console.log(keyword);
            if(keyword.search(term) < 0) {
                $row.hide();
            }else {
                $row.show();
            }
        });
    });
});

$(document).ready(function(){
    $('#myTable').dataTable({
        "bPaginate": true,
        "bFilter": false,
        "bInfo": false
                 });
});

var table = $("#datatable").DataTable({
   "paging": true,
   "ordering": false,
   "searching": false
});

function openForm() {
    document.getElementById("myForm").style.display = "block";
}

function closeForm() {
    document.getElementById("myForm").style.display = "none";
}

function validateForm() {
//    console.log('listOfRoom', list);
    var price = document.forms["myForm"]["servicesPrice"].value;
    var priceF;
    try {
        priceF = parseFloat(price);
        if (priceF == price) {
            if (priceF < 0) {
                throw ""
            }
        } else {
            throw "";
        }
    } catch (ex) {
        alert("Giá phải là số thực lớn hơn hoặc bằng 0");
        return(false);
    }
}