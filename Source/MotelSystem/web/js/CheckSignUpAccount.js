/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
window.onload = function () {
    var pwd = document.getElementById('user_password');
    var eye = document.getElementById('eye');
    eye.addEventListener('click', togglePass);
}

function togglePass() {

    eye.classList.toggle('active');

    (pwd.type == 'password') ? pwd.type = 'text' : pwd.type = 'password';
}

$(document).ready(function () {
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Nhập tên của bạn'
                    }
                }
            },
            address: {
                validators: {
                    stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Nhập địa chỉ'
                    }
                }
            },
            user_name: {
                validators: {
                    stringLength: {
                        min: 5,
                    },
                    notEmpty: {
                        message: 'Nhập tên tài khoản có ít nhất 5 ký tự'
                    }
                }
            },
            user_password: {
                validators: {
                    stringLength: {
                        min: 8,
                    },
                    notEmpty: {
                        message: 'Nhập mật khẩu có ít nhất 8 ký tự'
                    }
                }
            },
            confirm_password: {
                validators: {
                    stringLength: {
                        min: 8,
                    },
                    notEmpty: {
                        message: 'Xác nhận lại mật khẩu'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Nhập địa chỉ Email'
                    },
                    emailAddress: {
                        message: 'Nhập địa chỉ Email thích hợp'
                    }
                }
            },
            contact_no: {
                validators: {
                    stringLength: {
                        min: 10,
                        max: 10,
                        notEmpty: {
                            message: 'Nhập số điện thoại của bạn'
                        }
                    }
                },
            }
        }
    })

            $('#user_password, #confirm_password').on('keyup', function () {
        if ($('#user_password').val() === $('#confirm_password').val()) {
            $('#message').html('Mật khẩu tương xứng').css('color', 'green');
        } else
            $('#message').html('Mật khẩu không tương xứng').css('color', 'red');
    });
});

