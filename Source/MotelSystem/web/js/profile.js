/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function onChange() {
    const password = document.querySelector('input[name=newPassword]');
    const confirm = document.querySelector('input[name=newPasswordAgain]');
    if (confirm.value === password.value) {
        $('#message').html('Matching').css('color', 'green');
    } else {
        $('#message').html('Not Matching').css('color', 'green');
    }
}