function validateForm(){
    if (!confirm('Bạn chắc chắn muốn gửi yêu cầu này')) {
            return false;
        }
        
    if($('#re-submit').val()=="re-submit"){
        return true;
    }
    var input_motelImg = document.forms["form-add-motel"]["motel_Image"].value;
    var input_motelImg_verification = document.forms["form-add-motel"]["motel_Image_Verification"].value;
    var input_motelName = document.forms["form-add-motel"]["motelName"].value;
    var input_motelAddress = document.forms["form-add-motel"]["motelAddress"].value;
    var input_motelPhone = document.forms["form-add-motel"]["motelPhone"].value;
    var input_motelDescription = document.forms["form-add-motel"]["motelDescription"].value;
    if(input_motelImg==""){
        alert("Vui lòng thêm ảnh đại diện trọ.");
        return false;
    }
    if(input_motelImg_verification==""){
        alert("Vui lòng thêm ảnh xác minh trọ.");
        return false;
    }
    if (input_motelName == "" || input_motelAddress=="" || input_motelPhone=="" || input_motelDescription=="") {
        alert("Vui lòng điền đầy đủ thông tin.");
        return false;
    }
    if(input_motelDescription.length>=1000){
        alert("Mô tả thông tin vượt quá giới hạn cho phép " +input_motelDescription.length + "/1000" );
        return false;
    }
};


