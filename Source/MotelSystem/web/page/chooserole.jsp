<%-- 
    Document   : chooserole
    Created on : May 13, 2022, 10:29:40 AM
    Author     : coder
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Chọn vai trò</title>
        <link rel="stylesheet" href="css/chooseRole.css">
    </head>
    <body>
        <main>
            <h1>Chọn Vai Trò</h1>
            <button onclick="location.href='http://localhost:8080/MotelSystem/admin/login'"><span>Admin</span></button>
            <button onclick="location.href='http://localhost:8080/MotelSystem/host/login'">Chủ Trọ</button>
            <button onclick="location.href='http://localhost:8080/MotelSystem/customer/login'">Người Thuê</button>
        </main>
    </body>
</html>
