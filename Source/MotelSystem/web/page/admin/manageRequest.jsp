<%-- 
    Document   : ManageRequest
    Created on : May 27, 2022, 12:34:26 PM
    Author     : Admin
--%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý yêu cầu</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/admin/manageRequest.css">
        <script language="javascript" src="../js/admin/manageRequest.js"></script>
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-admin.jsp">
            <jsp:param name="nav" value="manageReq"></jsp:param>
        </jsp:include>

        <div class="container-lg">
            <!-- Body -->
            <div id="table-account" class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <b>Quản lý yêu cầu</b></h2>
                            </div>
                            <div class="col-sm-6">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm yêu cầu">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <td style="width:20% ;">Ngày yêu cầu</td>
                                <td style="width:20% ;">Tên chủ trọ</td>
                                <td style="width:20% ;">Loại yêu cầu</td>
                                <td style="width:20% ;">Trạng thái</td>
                                <td style="width:20% ;">Hành động</td>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- list of host request to post -->
                            <c:forEach items="${requestScope.postOfMotels}" varStatus="loop" var="post">
                                <tr class="row-hover">
                                    <fmt:parseDate pattern="yyyy-MM-dd" value="${post.postDate}" var="parsedDate" />
                                    <td><fmt:formatDate value="${parsedDate}" pattern="dd/MM/yyyy" /></td>
                                    <td>${post.getFullName()}</td>
                                    <td>Yêu cầu đăng bài</td>
                                    <td>
                                        <c:if test="${post.postStatus == 'pending'}">
                                            Đang chờ duyệt
                                        </c:if>
                                        <c:if test="${post.postStatus == 'reject'}">
                                            Bị từ chối
                                        </c:if>
                                    </td>
                                    <td>
                                        <a href="ViewPost?id=${post.getPostID()}" class="view" title="View detail" data-toggle="tooltip"><i 
                                                class="material-icons">visibility</i></a>
                                        <a onclick="if (!confirm('Bạn chắc chắn muốn xóa yêu cầu này')) {
                                                    return false;
                                                }"
                                           href="DeletePost?id=${post.getPostID()}&request=true" class="delete" title="Delete" data-toggle="tooltip"><i
                                                class="material-icons">&#xE872;</i></a>


                                    </td>
                                </tr>
                            </c:forEach>
                            <!-- list of customer request to become host -->
                            <c:forEach items="${requestScope.motelHasHosts}" varStatus="loop" var="motel">
                                <tr class="row-hover">
                                    <fmt:parseDate pattern="yyyy-MM-dd" value="${motel.motelCreateAt}" var="parsedDateMotel" />
                                    <td><fmt:formatDate value="${parsedDateMotel}" pattern="dd/MM/yyyy" /></td>
                                    <td>${motel.getFullName()}</td>
                                    <td>
                                        <c:if test="${motel.isRoleOfAccount==true}">
                                            Yêu cầu mở trọ
                                        </c:if>
                                        <c:if test="${motel.isRoleOfAccount==false}">
                                            Yêu cầu trở thành chủ trọ
                                        </c:if>    
                                    </td>
                                    <td>
                                        <c:if test="${motel.motelStatus == 'pending'}">
                                            Đang chờ duyệt
                                        </c:if>
                                        <c:if test="${motel.motelStatus == 'reject'}">
                                            Bị từ chối
                                        </c:if>
                                    </td>
                                    <td>
                                        <a href="ViewMotel?id=${motel.getMotelID()}" class="view" title="View detail" data-toggle="tooltip"><i 
                                                class="material-icons">visibility</i></a>
                                        <a onclick="if (!confirm('Bạn chắc chắn muốn xóa yêu cầu này')) {
                                                        return false;
                                                    }"
                                           href="DeleteMotel?id=${motel.getMotelID()}&request=true" class="delete" title="Delete" data-toggle="tooltip"><i
                                                class="material-icons">&#xE872;</i></a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>    

                    </table>
                    <h4 class="${requestScope.bg} text-white text-center rounded w-25 p-2 mb-2 ">${requestScope.error}</h4>    
                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
