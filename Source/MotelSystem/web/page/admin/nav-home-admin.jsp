<%-- 
    Document   : nav-home-normal
    Created on : May 14, 2022, 4:29:33 AM
    Author     : royal
--%>

<%@page import="model.AccountAllInfor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    AccountAllInfor loginedUser = (AccountAllInfor) session.getAttribute("accountAllInfor");
%>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <div class="container px-4 px-lg-5">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="w3-container collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 bg-light">
                <li class="nav-item bg-dark" style="padding-right: 5px"><div class="divSmt <c:if test="${param.nav == 'home'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'home'}">active</c:if>" aria-current="page" href="<%= request.getContextPath()%>/admin/home">Trang Chủ</a></div></li>

                        <li class="nav-item bg-dark" style="padding-right: 5px"><div class="divSmt <c:if test="${param.nav == 'manageAcc'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'manageAcc'}">active</c:if>" href="<%=request.getContextPath()%>/admin/ManageAccount">Tài khoản</a></div></li>
                <li class="nav-item bg-dark" style="padding-right: 5px"><div class="divSmt <c:if test="${param.nav == 'manageMotel'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'manageMotel'}">active</c:if>" href="<%=request.getContextPath()%>/admin/ManageMotel">Nhà trọ</a></div></li>
                <li class="nav-item bg-dark" style="padding-right: 5px"><div class="divSmt <c:if test="${param.nav == 'managePost'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'managePost'}">active</c:if>" href="<%=request.getContextPath()%>/admin/ManagePost">Bài đăng</a></div></li>
                <li class="nav-item bg-dark" style="padding-right: 5px"><div class="divSmt <c:if test="${param.nav == 'manageReq'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'manageReq'}">active</c:if>" href="<%=request.getContextPath()%>/admin/ManageRequest">Yêu cầu</a></div></li>

                </ul>

                <form class="d-flex">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                        <li class="dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="data:image/jpg;base64,<%= loginedUser.getImageProfile()%>" width="50px" height="50px">
                                &nbsp;
                                    Hello, <%= loginedUser.getFullName()%>!</a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="profile">Thông tin cá nhân</a></li>
                            <li><a class="dropdown-item" href="#!">Đổi mật khẩu</a></li>
                            <li><hr class="dropdown-divider" /></li>
                            <li><a class="dropdown-item" href="../logout">Đăng xuất</a></li>
                        </ul>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</nav>
<script>
    $('.divSmt').hover(
            function () {
                if ($(this).attr('class').indexOf('w3-animate-zoom') === -1) {
                    $(this).toggleClass("bg-light");
                    $(this).children("a").toggleClass("text-dark-when-hover");
                }
            });
</script>