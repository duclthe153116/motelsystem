<%-- 
    Document   : manageMotel
    Created on : May 16, 2022, 2:08:53 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý trọ</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/admin/manageMotel.css">
        <script language="javascript" src="../js/admin/manageMotel.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-admin.jsp">
            <jsp:param name="nav" value="manageMotel"></jsp:param>
        </jsp:include>
        <!-- Body -->
        <div class="container-lg">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <b>Quản lý trọ</b></h2>
                            </div>
                            <div class="col-sm-6">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm trọ">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="myTable"  class="table table-striped">
                        <thead>
                            <tr>
                                <td style="width:2% ;">ID</td>
                                <td style="width:12% ;">Tên trọ</td>
                                <td style="width:10% ;">Tên tài khoản</td>
                                <td style="width:10% ;">Số điện thoại</td>
                                <td>Địa chỉ</td>
                                <td style="width:7% ;">Số phòng</td>
                                <td style="width:7% ;">Trạng thái</td>
                                <td style="width:7% ;">Hành động</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.motelHasHosts}" varStatus="loop" var="motel">
                                <tr class="row:hover">
                                    <td>${motel.getMotelID()}</td>
                                    <td>${motel.getMotelName()}</td>
                                    <td>${motel.getUserName()}</td>
                                    <td>${motel.getMotelPhone()}</td>
                                    <td>${motel.getMotelAddress()}</td>
                                    <td>${motel.getMotelRoomNumber()}</td>
                                    <td>
                                        <c:if test="${motel.getMotelStatus()=='publish'}">
                                            Đã duyệt
                                        </c:if>
                                        <c:if test="${motel.getMotelStatus()=='pending' || motel.motelStatus=='re-submit'}">
                                            Chưa duyệt
                                        </c:if>
                                        <c:if test="${motel.getMotelStatus()=='reject'}">
                                            Đã từ chối
                                        </c:if>    
                                    </td>

                                    <td>
                                        <c:if test="${!motel.isIsDelete()}">
                                            <a href="ViewMotel?id=${motel.getMotelID()}" class="view" title="View detail" data-toggle="tooltip"><i 
                                                    class="material-icons">visibility</i></a>
                                            <a onclick="if (!confirm('Bạn chắc chắn muốn khóa trọ này')) {
                                                            return false;
                                                        }"
                                               href="DeleteMotel?id=${motel.getMotelID()}&request=false" class="delete" title="Block" data-toggle="tooltip"><i
                                                    class="material-icons">lock_open</i></a>
                                            </c:if>
                                            <c:if test="${motel.isIsDelete()}">
                                                <a onclick="if (!confirm('Bạn chắc chắn muốn mở khóa trọ này')) {
                                                            return false;
                                                        }" href="OpenLockMotel?id=${motel.getMotelID()}" class="delete" title="Block" data-toggle="tooltip"><i
                                                    class="material-icons">lock</i></a>
                                            </c:if>

                                    </td>
                                </tr>
                            </c:forEach>

                        </tbody>
                    </table>
                    <h4 class="${requestScope.bg} text-white text-center rounded w-25 p-2 mb-2 ">${requestScope.error}</h4>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>  
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
