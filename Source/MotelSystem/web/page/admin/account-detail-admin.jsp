<%-- 
    Document   : ViewAccount
    Created on : May 29, 2022, 11:09:01 PM
    Author     : Admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Thông tin tài khoản</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
         
        <link rel="stylesheet" href="../css/admin/viewAccount.css">
    </head>
    <body>
        <jsp:include page="nav-home-admin.jsp"/>
        <!-- Profile -->
        <div class="container" style="margin-bottom: 100px;margin-top:40px;">
            <div class="row">
            <div class="col-sm-7">
                <h2>Thông tin chi tiết tài khoản</h2>
                    <table>
                        <tr>
                            <td>Họ và tên</td>
                            <td>${account.getFullName()}</td>
                        </tr>
                        <tr>
                            <td>Tên đăng nhập</td>
                            <td>${account.getUsername()}</td>
                        </tr>
                        <tr>
                            <td>Ngày sinh</td>
                            <td>
                                <fmt:parseDate pattern="yyyy-MM-dd" value="${account.getdOB()}" var="parsedDate" />
                                <fmt:formatDate value="${parsedDate}" pattern="dd/MM/yyyy" />
                            </td>
                        </tr>
                        <tr>
                            <td>Giới tính</td>
                            <td>
                                <c:if test="${account.getGender()=='Male'}">Nam</c:if>
                                <c:if test="${account.getGender()!='Male'}">Nữ</c:if>
                            </td>
                        </tr>
                        <tr>
                            <td>Số điện thoại</td>
                            <td>${account.getPhone()}</td>
                        </tr>
                        <tr>
                            <td>Địa chỉ</td>
                            <td>${account.getAddress()}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>${account.getEmail()}</td>
                        </tr>
                        <tr>
                            <td>Vai trò</td>
                            <td>
                                <c:set var="roleAdmin" value="0"></c:set>
                                <c:set var="roleHost" value="0"></c:set>
                                <c:set var="roleCustomer" value="0"></c:set>
                                <c:forEach items="${account.getRoleName()}" varStatus="loop" var="roleName">
                                    <c:if test="${roleName == 'admin'}">
                                        <input type="checkbox" checked="true" disabled="true"> Admin
                                        <c:set var="roleAdmin" value="1"></c:set>
                                    </c:if>
                                </c:forEach>
                                <c:if test="${roleAdmin==0}">
                                    <input type="checkbox" disabled="true"> Admin
                                </c:if>        
                                <c:forEach items="${account.getRoleName()}" varStatus="loop" var="roleName">
                                    <c:if test="${roleName == 'host'}">
                                        <input type="checkbox" checked="true" disabled="true"> Chủ trọ
                                        <c:set var="roleHost" value="1"></c:set>
                                    </c:if>
                                </c:forEach>
                                <c:if test="${roleHost==0}">
                                    <input type="checkbox" disabled="true"> Chủ trọ
                                </c:if>  
                                <c:forEach items="${account.getRoleName()}" varStatus="loop" var="roleName">
                                    <c:if test="${roleName == 'customer'}">
                                        <input type="checkbox" checked="true" disabled="true"> Người thuê
                                        <c:set var="roleCustomer" value="1"></c:set>
                                     </c:if>
                                </c:forEach>
                                <c:if test="${roleCustomer==0}">
                                    <input type="checkbox" disabled="true"> Người thuê
                                </c:if>
                        </tr>
                    </table>
                                <a href="ManageAccount" class="sub-btn">Trở về</a>
            </div>
            <div class="col-sm-5">
                <img class="account-img" src="data:image/jpg;base64,${account.imageProfile}" alt="">

            </div>
            
        </div>
        </div> <!-- End profile -->
        <jsp:include page="../footer.html"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
