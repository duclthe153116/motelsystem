<%-- 
    Document   : managePost
    Created on : May 16, 2022, 2:09:05 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý bài đăng</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/admin/managePost.css">
        <script language="javascript" src="../js/admin/managePost.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-admin.jsp">
            <jsp:param name="nav" value="managePost"></jsp:param>
        </jsp:include>
        <!-- Body -->
        <div class="container-lg">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <b>Quản lý bài viết</b></h2>
                            </div>
                            <div class="col-sm-6">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm bài viết">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <td style="width:2% ;">ID</td>
                                <td>Tiêu đề</td>
                                <td style="width:15% ;">Tên trọ</td>
                                <td style="width:10% ;">Tên tài khoản</td>
                                <td style="width:10% ;">Ngày đăng bài</td>
                                <td style="width:8% ;">Trạng thái</td>
                                <td style="width:10% ;">Hành động</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.postOfMotels}" varStatus="loop" var="post">
                                <tr class="row:hover">
                                    <td>${post.getPostID()}</td>
                                    <td>${post.getPostTitle()}</td>
                                    <td>${post.getMotelName()}</td>
                                    <td>${post.getUserName()}</td>
                                    <td>
                                        <fmt:parseDate pattern="yyyy-MM-dd" value="${post.getPostDate()}" var="parsedDate" />
                                        <fmt:formatDate value="${parsedDate}" pattern="dd/MM/yyyy" />
                                    <td>
                                        <c:if test="${post.postStatus=='publish'}">Đã duyệt</c:if>
                                        <c:if test="${post.postStatus=='pending'||postOfMotel.postStatus=='re-submit'}">Chưa duyệt</c:if>
                                    </td>
                                    <td>
                                        <a href="ViewPost?id=${post.getPostID()}" class="view" title="View detail" data-toggle="tooltip"><i 
                                                    class="material-icons">visibility</i></a>
                                        <a onclick="if (!confirm('Bạn chắc chắn muốn xóa bài đăng này')) {return false;}"
                                                    href="DeletePost?id=${post.getPostID()}&request=false" class="delete" title="Delete" data-toggle="tooltip"><i
                                                    class="material-icons">&#xE872;</i></a>
                                    </td>
                                </tr>
                            </c:forEach>

                        </tbody>
                    </table>
                    <h4 class="${requestScope.bg} text-white text-center rounded w-25 p-2 mb-2 ">${requestScope.error}</h4>    
                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>

</html>
