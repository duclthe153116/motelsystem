<%-- 
    Document   : homepage-normal
    Created on : May 14, 2022, 1:43:58 AM
    Author     : royal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en" style="overflow: auto;">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Chi tiết trọ</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <!-- Favicon-->
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<%= request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="<%= request.getContextPath()%>/css/swalert.css" rel="stylesheet" />
        <style>
            img {
                vertical-align: middle;
            }


            /* Hide the images by default */
            .mySlides {
                display: block;
                width: 100%;
                height: 500px;
            }

            /* Add a pointer when hovering over the thumbnail images */
            .cursor {
                cursor: pointer;
            }

            /* Next & previous buttons */
            .prev,
            .next {
                cursor: pointer;
                position: absolute;
                top: 50%;
                width: auto;
                padding: 16px;
                margin-top: -50px;
                color: #FFF;
                font-weight: bold;
                font-size: 20px;
                border-radius: 0 3px 3px 0;
                user-select: none;
                text-decoration: none;
                -webkit-user-select: none;
            }

            /* Position the "next button" to the right */
            .next {
                right:0;
                border-radius: 3px 0 0 3px;
            }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover,
            .next:hover {
                background-color: rgba(0, 0, 0, 0.8);
            }

            /* Add a transparency effect for thumnbail images */
            .demo {
                opacity: 0.6;
            }

            .active,
            .demo:hover {
                opacity: 1;
            }

            i{
                color: #8080CC;
            }

            h5{
                margin: 0px!important;
            }

            .submit-request{
                background-color: #8080CC;
                color:#fff;
                text-decoration: none;
                font-size: 20px;
                padding: 6px;
                border:1px solid #8080CC;
            }
            .submit-request:hover{
                background-color: #fff;
                color: #8080CC;
            }
        </style>
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-admin.jsp"/>
        <!-- Header-->
        <!-- Section-->

        <section style="padding-bottom: 5em">
            <div class="container mt-5" style="border: 3px solid #8080CC;padding-top: 20px; border-radius: 10px;box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;">
                <div class="d-flex justify-content-between">
                    <div>
                        <a href="#" style="font-size: 25px;text-decoration: none;color:#8080CC"><b><i>&emsp;&emsp;${motel.motelName}</i></b></a>
                    </div>
                </div><br/>
                <div class="row px-4" style="padding-bottom: 20px;padding-top: 20px">
                    <div class="col px-4" style="border-right: 2px solid #8080CC">
                        <div class="slideshow" style="width: 100%;height: 80%;position: relative;padding-right: 0px;">
                            <c:forEach items="${motel.getMotelImgList()}" var="i" >
                                <div style="height: 500px;" class="mySlides">
                                    <img src="data:image/jpg;base64,${i}" style="width:100%;height: 100%;">
                                </div>
                            </c:forEach>
                            <a class="prev" onclick="plusSlides(-1)">❮</a>
                            <a class="next" onclick="plusSlides(1)">❯</a>
                            <br/>
                            <div class="row">
                                <c:forEach items="${motel.getMotelImgList()}" var="i">
                                    <div class="column">
                                        <img class="demo cursor" src="data:image/jpg;base64,${i}" style="width:100%;height: 80px;" onclick="currentSlide(${loop.index+1})" alt="The Woods">
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row" style="padding-left: 50px;">
                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Chủ trọ:</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.ownerName}</h5>
                                </div>
                            </div><br/><br/>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Số điện thoại:</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.motelPhone}</h5>
                                </div>
                            </div><br/><br/>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Số phòng:</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.roomNumber} (phòng)</h5>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Số phòng trống:</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.numberEmptyRoom} (phòng)</h5>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Giá:</i></b></p>
                                </div>
                                <div class="col-8">
                                    <fmt:setLocale value="vi-VN"/>
                                    <h5>
                                        <fmt:formatNumber type = "currency" value = "${motel.minPrice}" /> - <fmt:formatNumber type = "currency" value = "${motel.maxPrice}" /> (VNĐ)</h5>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Địa chỉ:</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.motelAddress}</h5>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Mô tả:</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.motelDescription}</h5>
                                </div>
                            </div><br/><br/>  
                            <c:if test="${motel.motelStatus=='pending'}" >
                                <div class="row" style="margin-top: 30px">
                                    <div class="col-12" style="text-align: center;">
                                        <a onclick="if (!confirm('Bạn có muốn cấp quyền cho trọ này hoạt động?')) {
                                                    return false;
                                                }" href="UpdateMotelStatus?id=${motel.motelID}&status=publish&request=false" class="submit-request">Duyệt bài</a>
                                        <a onclick="if (!confirm('Bạn có muốn từ chối cấp quyền cho trọ này hoạt động?')) {
                                                    return false;
                                                }" href="UpdateMotelStatus?id=${motel.motelID}&status=reject&request=false" class="submit-request">Từ chối</a>
                                    </div>
                                </div><br/><br/>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    </body>

    <script>
                                            let slideIndex = 1;
                                            showSlides(slideIndex);

                                            function plusSlides(n) {
                                                showSlides(slideIndex += n);
                                            }

                                            function currentSlide(n) {
                                                showSlides(slideIndex = n);
                                            }

                                            function showSlides(n) {
                                                let i;
                                                let slides = document.getElementsByClassName("mySlides");
                                                let dots = document.getElementsByClassName("demo");
                                                let captionText = document.getElementById("caption");
                                                if (n > slides.length) {
                                                    slideIndex = 1
                                                }
                                                if (n < 1) {
                                                    slideIndex = slides.length
                                                }
                                                for (i = 0; i < slides.length; i++) {
                                                    slides[i].style.display = "none";
                                                }
                                                for (i = 0; i < dots.length; i++) {
                                                    dots[i].className = dots[i].className.replace(" active", "");
                                                }
                                                slides[slideIndex - 1].style.display = "block";
                                                dots[slideIndex - 1].className += " active";
                                            }
    </script>
    <script>
        var w = 100 / ($(".mySlides").length);
        $(".column").css("width", w + "%");
        
        var flag = '${error}';
        if (flag.localeCompare('Success') === 0) {
            swal({
                title: "Cập nhật thành công!",
                text: "${message}",
                icon: "success",
                buttons: {
                    ok: "OK!"
                }
            });
        } else if (flag.localeCompare('Fail') === 0) {
            swal({
                title: "Cập nhật thất bại!",
                text: "${message}",
                icon: "error",
                buttons: {
                    ok: "OK!"
                }
            });
        }
    </script>

</html>
