<%-- 
    Document   : manageAccount.jsp
    Created on : May 16, 2022, 1:33:09 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý tài khoản</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/admin/manageAccount.css">
        <script language="javascript" src="../js/admin/manageAccount.js"></script>
        <style>
            .active{
                color: #8080CC;
            }
        </style>
    </head>

    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-admin.jsp">
            <jsp:param name="nav" value="manageAcc"></jsp:param>
        </jsp:include>
        <div class="container-lg">
            <!-- Body -->
            <div id="table-account" class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2><b>Quản lý tài khoản</b></h2>
                            </div>
                            <div class="col-sm-6">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm tài khoản">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <td style="width:2% ;">ID</td>
                                <td >Tên tài khoản</td>
                                <td>Số điện thoại</td>
                                <td style="width:15% ;">Email</td>
                                <td >Quản trị viên</td>
                                <td >Chủ trọ</td>
                                <td >Khách hàng</td>
                                <td >Hành động</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.accountAllInfors}" varStatus="loop" var="accountAllInfor">
                                <tr class="row-hover">
                                    <td>${accountAllInfor.getAccountID()}</td>
                                    <td>${accountAllInfor.getUsername()}</td>
                                    <td>${accountAllInfor.getPhone()}</td>
                                    <td>${accountAllInfor.getEmail()}</td>
                                    <c:set var="roleAdmin" value="0"></c:set>
                                    <c:set var="roleHost" value="0"></c:set>
                                    <c:set var="roleCustomer" value="0"></c:set>
                                        <!-- role admin -->
                                    <c:forEach items="${accountAllInfor.getRoleName()}" varStatus="loop" var="roleName">
                                        <c:if test="${roleName == 'admin'}">
                                            <td><input name="role_name${accountAllInfor.getAccountID()}" type="checkbox" checked="true"></td>
                                                <c:set var="roleAdmin" value="1"></c:set>
                                            </c:if>
                                        </c:forEach>
                                        <c:if test="${roleAdmin==0}">
                                        <td><input name="role_name${accountAllInfor.getAccountID()}" type="checkbox"></td>
                                        </c:if>
                                    <!-- role host -->    
                                    <c:forEach items="${accountAllInfor.getRoleName()}" varStatus="loop" var="roleName">
                                        <c:if test="${roleName == 'host' && roleAdmin == 1}">
                                            <td><input name="role_name${accountAllInfor.getAccountID()}" type="checkbox" checked="true" disabled="true"></td>
                                                <c:set var="roleHost" value="1"></c:set>
                                            </c:if>
                                            <c:if test="${roleName == 'host'&&!(roleAdmin==1)}">
                                            <td><input name="role_name${accountAllInfor.getAccountID()}" type="checkbox" checked="true"></td>
                                                <c:set var="roleHost" value="1"></c:set>
                                            </c:if>
                                    </c:forEach>
                                        <c:if test="${roleHost==0&&(roleAdmin==1)}">
                                            <td><input name="role_name${accountAllInfor.getAccountID()}" type="checkbox" disabled="true"></td>
                                        </c:if> 
                                        <c:if test="${roleHost==0&&!(roleAdmin==1)}">
                                            <td><input name="role_name${accountAllInfor.getAccountID()}" type="checkbox"></td>
                                        </c:if>
                                    <!-- role customer -->    
                                    <c:forEach items="${accountAllInfor.getRoleName()}" varStatus="loop" var="roleName">
                                        <c:if test="${roleName == 'customer'&&(roleAdmin==1)}">
                                            <td><input name="role_name${accountAllInfor.getAccountID()}" type="checkbox" checked="true" disabled="true"></td>
                                            <c:set var="roleCustomer" value="1"></c:set>
                                        </c:if>
                                        <c:if test="${roleName == 'customer'&&roleAdmin!=1}">
                                            <td><input name="role_name${accountAllInfor.getAccountID()}" type="checkbox" checked="true" disabled="true"></td>
                                            <c:set var="roleCustomer" value="1"></c:set>
                                        </c:if>    
                                    </c:forEach>
                                        <c:if test="${roleCustomer==0&&roleAdmin==1}">
                                            <td><input name="role_name${accountAllInfor.getAccountID()}" type="checkbox" disabled="true"></td>
                                        </c:if>
                                        <c:if test="${roleCustomer==0&&roleAdmin!=1}">
                                            <td><input name="role_name${accountAllInfor.getAccountID()}" type="checkbox" disabled="true"></td>
                                        </c:if>  
                                    <td>
                                    <!-- Action -->    
                                    <c:if test="${accountAllInfor.isIsdelete()}">
                                        <a onclick="if (!confirm('Bạn chắc chắn muốn mở khóa tài khoản này')) {
                                                            return false;
                                                        }" href="OpenLockAccount?id=${accountAllInfor.getAccountID()}" class="delete" title="Block" data-toggle="tooltip"><i class="material-icons">lock</i></a>
                                    </c:if>
                                    <c:if test="${!accountAllInfor.isIsdelete()}">
                                        <a href="ViewAccount?id=${accountAllInfor.getAccountID()}" class="view" title="View detail" data-toggle="tooltip"><i 
                                                class="material-icons">visibility</i></a>
                                        <c:if test="${sessionScope.accountAllInfor.getAccountID()!=accountAllInfor.getAccountID()}">
                                        <a onclick="if (!confirm('Bạn chắc chắn muốn khóa tài khoản này')) { return false; }"
                                            href="DeleteAccount?id=${accountAllInfor.getAccountID()}" class="delete" title="Block" data-toggle="tooltip"><i
                                                    class="material-icons">lock_open</i></a>
                                        <a id ="save-role_${accountAllInfor.getAccountID()}"
                                            onclick="UpdateRoleAccount(this.id, 'role_name${accountAllInfor.getAccountID()}')"
                                            href="#" class="save" title="Save" data-toggle="tooltip"><i class="material-icons">done</i></a>
                                        </c:if>
                                    </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>    

                    </table>
                    <h4 class="${requestScope.bg} text-white text-center rounded w-25 p-2 mb-2 ">${requestScope.error}</h4>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>

</html>
