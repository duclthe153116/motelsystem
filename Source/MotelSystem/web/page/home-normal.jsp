<%-- 
    Document   : homepage-normal
    Created on : May 14, 2022, 1:43:58 AM
    Author     : royal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en" style="overflow: auto;">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Trang chủ</title>
        <!-- Favicon-->
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<%= request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <script src="<%= request.getContextPath()%>/js/paging-home-1.js" ></script>
        <!-- JS tạo nút bấm di chuyển trang start -->
        <script src="<%= request.getContextPath()%>/js/pageing-home-2.js" type="text/javascript"></script>
        <!-- JS tạo nút bấm di chuyển trang end -->

        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    </head>
    <body>
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-dark">
            <div class="container px-4 px-lg-5">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                        <div class="bg-light" style="border-radius: 5px">                     
                            <li class="nav-item"><a class="nav-link active" aria-current="page" href="<%= request.getContextPath()%>/home">Trang chủ</a></li>
                        </div>
                    </ul>
                    <button class="d-flex btn btn-outline-light" onclick="location.href = 'http://localhost:8080/MotelSystem/customer/login'">Đăng Nhập</button>
                </div>
            </div>
        </nav>
        <!-- Header-->
    <center>

        <div class="w3-content w3-section" style="max-width:100%;height: 400px;padding-top: 0px;margin-top: 0px!important;">
            <img class="mySlides w3-animate-left" src="<%= request.getContextPath()%>/assets/img/pfp.jpg" style="width:65%;height: 400px">
            <img class="mySlides w3-animate-left" src="<%= request.getContextPath()%>/assets/img/spaceCat.jpg" style="width:65%;height: 400px">
            <img class="mySlides w3-animate-left" src="<%= request.getContextPath()%>/assets/img/mona-finden-a-forgotten-love-story.jpg" style="width:65%;height: 400px">
        </div>
    </center>
    <!-- Section-->

    <section class="py-5">
        <div class="container px-4 px-lg-5">
            <div class="d-flex justify-content-between">
                <div style="padding-right: 25px">
                    <button id="flip" class="btn btn-dark align-items-center"><b>Chọn Tỉnh/thành &#8595;</b></button>
                </div>
                <div style="padding-right: 25px">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="inputGroupSelect01" style="background-color: #8080CC;color: #FFF;"><b>Ngày đăng</b></label>
                        </div>
                        <select class="form-select" id="inputGroupSelect01">
                            <option>Chọn thứ tự...</option>
                            <option value="1" ${param.order == 'desc' ? 'selected' : ''}>Mới &#8594; Cũ</option>
                            <option value="2" ${param.order == 'asc' ? 'selected' : ''}>Cũ &#8594; Mới</option>
                        </select>
                    </div>
                </div>
                <div class="flex-grow-1">
                    <form action="search" method="get">
                        <div class="input-group mb-3">
                            <input type="search" id="searchTxt" name="motel_name" value="${param.motel_name}" class="form-control" required=""  oninvalid="setCustomValidity('Không được để trống!')" oninput="setCustomValidity('')" placeholder="Tìm theo tên trọ...">
                            <div class="input-group-append">
                                <input type="submit" class="btn btn-dark" value="Tìm kiếm">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- panelFilterCity-->
        <jsp:include page="panel-filter-city.jsp"></jsp:include>

            <div class="container px-4 px-lg-5 mt-5"> 
            <c:if test="${listPost!=null}">
                <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-3 justify-content-center">

                    <c:forEach var="post" items="${listPost}">
                        <div class="contentPage">
                            <div class="col mb-5">
                                <div class="card h-100" style="border-radius: 10px;box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;">
                                    <!-- Product image-->
                                    <img class="card-img-top" src="data:image/jpg;base64,${post.specificImage}" width="450px" height="300px" alt="image of post" />
                                    <!-- Product details-->
                                    <div class="card-body p-4">
                                        <div class="text-left">
                                            <!-- Product name-->
                                            <h3 class="fw-bolder">${post.motelName}</h3>
                                            <!-- Product price-->
                                            <h5>${post.title}</h5>
                                            <fmt:setLocale value="vi-VN"/>
                                            <h6>Giá: <fmt:formatNumber type = "currency" value = "${post.minPrice}" /> - <fmt:formatNumber type = "currency" value = "${post.maxPrice}" /></h6><br/>
                                            <fmt:parseDate pattern="yyyy-MM-dd" value="${post.pDate}" var="parsedDate" />
                                            <i>Ngày đăng: <fmt:formatDate value="${parsedDate}" pattern="dd/MM/yyyy" /></i>
                                        </div>
                                    </div>
                                    <!-- Product actions-->
                                    <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                        <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="PostDetail?postID=${post.id}">Xem chi tiết</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>


                </div>  
            </c:if>
            <c:if test="${listPost==null}">
                <h2 style="color: lightgrey"><i>Không có bài đăng nào hiện tại...</i></h2>
                <h2 style="color: lightgrey"><i>.</i></h2>
                <h2 style="color: lightgrey"><i>.</i></h2>
                <h2 style="color: lightgrey"><i>.</i></h2>
                <h2 style="color: lightgrey"><i>.</i></h2>
            </c:if>
            <br/><ul id="pagination" style="float: right;"></ul></div><br/><br/>
    </div>
</section>
<!-- Footer-->
<jsp:include page="footer.html"/>
<!-- Bootstrap core JS-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<!-- Core theme JS-->
<!--<script src="js/normal-homepage-js.js"></script>-->
<script type="text/javascript">
                        $(function () {
                            var pageSize = 6; // Hiển thị 6 sản phẩm trên 1 trang
                            showPage = function (page) {
                                $(".contentPage").hide();
                                $(".contentPage").each(function (n) {
                                    if (n >= pageSize * (page - 1) && n < pageSize * page)
                                        $(this).show();
                                });
                            }
                            showPage(1);
                            ///** Cần truyền giá trị vào đây **///
                            var totalRows = ${listPost.size()}; // Tổng số sản phẩm hiển thị
                            var btnPage = 3; // Số nút bấm hiển thị di chuyển trang
                            var iTotalPages = Math.ceil(totalRows / pageSize);

                            var obj = $('#pagination').twbsPagination({
                                totalPages: iTotalPages,
                                visiblePages: btnPage,
                                first: '<<',
                                last: '>>',
                                prev: '<',
                                next: '>',
                                onPageClick: function (event, page) {
                                    console.info(page);
                                    showPage(page);
                                }
                            });
                            $('#pagination')
                            console.info(obj.data());
                        });
</script>
<script>
    var myIndex = 0;
    carousel();

    function carousel() {
        var i;
        var x = document.getElementsByClassName("mySlides");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        myIndex++;
        if (myIndex > x.length) {
            myIndex = 1
        }
        x[myIndex - 1].style.display = "block";
        setTimeout(carousel, 2000); // Change image every 2 seconds
    }
</script>
<script>
    function filterCity(city) {
        var order = $("#inputGroupSelect01").val();

        if (order === '1') {
            location.href = "<%=request.getContextPath()%>/filter?city=" + city + "&order=desc";
        } else if (order === '2') {
            location.href = "<%=request.getContextPath()%>/filter?city=" + city + "&order=asc";
        } else {
            location.href = "<%=request.getContextPath()%>/filter?city=" + city;
        }
    }
</script>
</body>
</html>
