<%-- 
    Document   : login
    Created on : May 13, 2022, 10:16:29 AM
    Author     : coder
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Đăng Nhập</title>
        <link rel="stylesheet" href="../css/customer/login.css">
        <script language="javascript" src="../js/customer/login.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/typicons/2.0.9/typicons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
        <script language="javascript" src="https://cldup.com/S6Ptkwu_qA.js"></script>
    </head>
    <body id="particles-js"></body>
    <div class="animated bounceInDown">
        <div class="container">
            <span class="error animated tada" id="msg"></span>
            <form method="POST" action="login" name="form1" class="box" onsubmit="return checkStuff()">
                <h4>Người Thuê</h4>
                <h5>Sign in to your account.</h5>
                <input type="text" name="username" placeholder="Username" autocomplete="off">
                <i class="typcn typcn-eye" id="eye"></i>
                <input type="password" name="password" placeholder="Passsword" id="pwd" autocomplete="off" >
                <%
                    boolean error = false;
                    if (request.getAttribute("error") != null){
                        error = Boolean.parseBoolean(request.getAttribute("error").toString());
                    }
                    if (error){%>
                    <p style="color: red">Sai tên đăng nhập hoặc mật khẩu!</p>
                    <%}
                %>
                <a href="../host/login" class="changerole">Đổi vai trò</a>
                <a href="../forget" class="forgetpass">Quên mật khẩu</a>
                <input type="submit" value="Sign in" class="btn1">
            </form>
            <a href="../customer/SignUp" class="dnthave">Đăng ký</a>
        </div> 
    </div>
</html>
