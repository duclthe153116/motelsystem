<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Danh sách yêu cầu</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/host/manageBill.css">
        <script language="javascript" src="../js/host/manageBill.js"></script>
    </head>

    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-customer.jsp">
            <jsp:param name="nav" value="hostManageRequest"></jsp:param>
        </jsp:include>
        <div class="container-lg">
            <!-- Body -->
            <div id="table-account" class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Danh sách yêu cầu</b></h2>
                            </div>
                            <div class="col-sm-4" style="text-align: center; ">
                                <button style="box-shadow: 3px 2px 10px 4px" onclick="location.href = 'http://localhost:8080/MotelSystem/customer/choosemotel'" type="button" class="open-button">Thêm Yêu Cầu</button>
                            </div>
                            <div class="col-sm-4">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm phòng">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <td style="width:10% ;">Tên trọ</td>
                                <td style="width:10% ;">Tên phòng</td>
                                <td style="width:10% ;">Nội dung yêu cầu</td>
                                <td style="width:10% ;">Trạng thái</td>
                                <td style="width:10% ;">Ngày tạo</td>
                                <td style="width:10% ;">Ngày cập nhật</td>
                                <td style="width:10% ;">Ghi chú của trọ</td>
                                <td style="width:10% ;">Ghi chú của chủ trọ</td>
                                <td style="width:10% ;">Hành động</td>

                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.listRequest}" varStatus="loop" var="listRequestOfRoom">
                                <tr class="row-hover">
                                    <td><c:out value="${listRequestOfRoom.getMotelName()}"></c:out></td>
                                    <td><c:out value="${listRequestOfRoom.getRoomName()}"></c:out></td>
                                    <td><c:out value="${listRequestOfRoom.getContent()}"></c:out></td>
                                    <td><c:if test="${listRequestOfRoom.getStatus() eq 'done'}">
                                            Đã hoàn thành
                                        </c:if>
                                        <c:if test="${listRequestOfRoom.getStatus()== '0'}">
                                            Chưa xem
                                        </c:if>
                                        <c:if test="${listRequestOfRoom.getStatus() eq 'seen'}">

                                            Đã xem
                                        </c:if>
                                        <c:if test="${listRequestOfRoom.getStatus() eq 'do'}">
                                            Đang trong quá trình hoàn thành
                                        </c:if>
                                        <c:if test="${listRequestOfRoom.getStatus() eq 'reject'}">
                                            Từ chối yêu cầu
                                        </c:if></td>
                                    <td><fmt:formatDate value="${listRequestOfRoom.getCreateAt()}" pattern="dd/MM/yyyy" /></td>
                                    <td><fmt:formatDate value="${listRequestOfRoom.getUpdateAt()}" pattern="dd/MM/yyyy" /></td>
                                    <td><c:out value="${listRequestOfRoom.getNoteRequestOfRoom()}"></c:out></td>
                                    <td><c:out value="${listRequestOfRoom.getNoteRequestOfHost()}"></c:out></td>
                                        <td>
                                            <a href="requestdetail?idRequest=${listRequestOfRoom.getRequestID()}" class="view" title="Xem chi tiết" data-toggle="tooltip"><i 
                                                class="material-icons">visibility</i></a>
                                            <c:if test="${listRequestOfRoom.getStatus()=='0'}">
                                            <a href="updaterequest?idRequest=${listRequestOfRoom.getRequestID()}&idRoom=${listRequestOfRoom.getRoomID()}" class="edit" title="Sửa yêu cầu" data-toggle="tooltip"><i
                                                    class="material-icons">&#xE254;</i></a>
                                            <a href="#" onclick="doDelete('${listRequestOfRoom.getRequestID()}', '${listRequestOfRoom.getContent()}', '${listRequestOfRoom.getRoomName()}')" class="delete" title="Xóa yêu cầu" data-toggle="tooltip"><i
                                                    class="material-icons">&#xE872;</i></a>
                                            </c:if>     

                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>    
                    </table>
                    <h4 class="${requestScope.bg} text-white text-center rounded w-25 p-2 mb-2 ">${requestScope.error}</h4>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>

</html>
<script>
    function doDelete(idRequest, nameContent, nameRoom) {
                                                    if (confirm("Bạn có chắc chắn muốn xóa yêu cầu " + nameContent + " của phòng " + nameRoom + " ?")) {
                                                        window.location = "deleterequest?id=" + idRequest;
                                                    }
                                                }
    </script>