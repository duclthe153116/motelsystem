<%-- 
    Document   : homepage-normal
    Created on : May 14, 2022, 1:43:58 AM
    Author     : royal
--%>

<%@page import="model.AccountAllInfor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="loginedUser" value="${sessionScope.accountAllInfor}"></c:set>

    <!DOCTYPE html>
    <html lang="en" style="overflow: auto;">
        <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            <meta name="description" content="" />
            <meta name="author" content="" />
            <title>Đăng kí trọ mới</title>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
            <!-- Favicon-->
            <!-- Bootstrap icons-->
            <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
            <!-- Core theme CSS (includes Bootstrap)-->
            <link href="<%= request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
        <link href="../css/customer/register-motel-customer.css" rel="stylesheet"/>
        <script language="javascript" src="../js/customer/register-motel-customer.js"></script>

    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-customer.jsp"/>
        <!-- Header-->
        <!-- Section-->

        <section style="padding-bottom: 5em">
            <div class="container mt-5" style="border: 3px solid #8080CC;padding-top: 20px; border-radius: 10px;box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;">
                <div class="d-flex justify-content-between">
                    <div>
                        <h4 style="font-size: 25px;text-decoration: none;color:#8080CC"><b><i>&emsp;&emsp;Đăng kí trọ mới</i></b></h4>
                    </div>
                </div><br/>
                <c:choose>
                    <c:when test="${motel.motelStatus == 'reject'}">
                        <form action="ResubmitRequestMotel" id="add-form" method="post" enctype="multipart/form-data" name="form-add-motel" onsubmit="return validateForm()">
                    </c:when>
                    <c:otherwise>
                        <form action="RegisterMotel" id="add-form" method="post" enctype="multipart/form-data" name="form-add-motel" onsubmit="return validateForm()">
                    </c:otherwise>
                </c:choose>
                    <div class="row px-4" style="padding-bottom: 20px;padding-top: 20px">
                        <div class="col px-4" style="border-right: 2px solid #8080CC">
                            <div class="slideshow" style="width: 100%;height: 80%;position: relative;padding-right: 0px;">
                                <h5 style="text-align: center;">
                                    <label for="motelImage-verification" class="btn btn-dark" style="margin-bottom:10px; margin-top: 36px;">Ảnh xác minh</label>
                                    <c:if test="${motel.motelName!=null}">
                                        <input onchange="check_input_image_verification()" id="motelImage-verification" name="motel_Image_Verification" type="file" multiple="multiple" class="form-control" style="display:none;" disabled />
                                    </c:if>
                                    <c:if test="${motel.motelName==null}">
                                        <input id="motelImage-verification" name="motel_Image_Verification" type="file" multiple="multiple" class="form-control" style="display:none;" />
                                    </c:if>
                                </h5>
                                <div class="mySlides">
                                    <div id="image-holder">
                                        <c:forEach items="${motel.motelImgList}" var="image">
                                            <img src="data:image/jpg;base64,${image}" class="thumb-image">
                                        </c:forEach>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row" style="padding-left: 50px;">
                                <div class="row" style="margin-top: 30px; margin-left: 110px;">
                                    <c:if test="${motel.motelName!=null}">
                                        <div id="image-motel">
                                            <img src="data:image/jpg;base64,${motel.motelImage}" class="thumb-image">
                                        </div>
                                    </c:if>
                                    <c:if test="${motel.motelName==null}">
                                        <div id="image-motel">
                                        </div>
                                    </c:if>    

                                    <label for="motelImage" class="btn btn-dark" style="margin:10px 0 10px 95px; width: 130px;">Ảnh đại diện</label>
                                    <c:if test="${motel.motelName!=null}">
                                        <input onchange="check_input_image()" id="motelImage" name="motel_Image" type="file" class="form-control" style="display:none;"  disabled />
                                    </c:if>
                                    <c:if test="${motel.motelName==null}">
                                        <input id="motelImage" name="motel_Image" type="file" class="form-control" style="display:none;"/>
                                    </c:if>
                                </div><br/><br/>
                                <div class="row" style="margin-top: 30px">
                                    <div class="col-3">
                                        <p><b><i>Tên trọ:</i></b></p>
                                    </div>
                                    <div class="col-9">
                                        <h5>
                                            <c:if test="${motel.motelName!=null}">
                                                <input class="input-motel" value="${motel.motelName}" type="text" name="motelName" disabled/>
                                            </c:if>
                                            <c:if test="${motel.motelName==null}">
                                                <input class="input-motel" value="" type="text" name="motelName"/>
                                            </c:if>
                                        </h5>
                                    </div>
                                </div><br/><br/>
                                <div class="row" style="margin-top: 30px">
                                    <div class="col-3">
                                        <p><b><i>Số điện thoại:</i></b></p>
                                    </div>
                                    <div class="col-9">
                                        <h5>
                                            <c:if test="${motel.motelPhone!=null}">
                                                <input class="input-motel" value="${motel.motelPhone}" type="text" name="motelPhone" disabled/>
                                            </c:if>
                                            <c:if test="${motel.motelPhone==null}">
                                                <input class="input-motel" value="" type="text" name="motelPhone"/>
                                            </c:if>
                                        </h5>
                                    </div>
                                </div><br/><br/>
                                <div class="row" style="margin-top: 30px">
                                    <div class="col-3">
                                        <p><b><i>Địa chỉ:</i></b></p>
                                    </div>
                                    <div class="col-9">
                                        <h5>
                                            <c:if test="${motel.motelAddress!=null}">
                                                <input class="input-motel" value="${motel.motelAddress}" type="text" name="motelAddress" disabled/>
                                            </c:if>
                                            <c:if test="${motel.motelAddress==null}">
                                                <input class="input-motel" value="" type="text" name="motelAddress"/>
                                            </c:if>
                                        </h5>
                                    </div>
                                </div><br/><br/>
                                <div class="row" style="margin-top: 30px">
                                    <div class="col-3">
                                        <p><b><i>Mô tả chi tiết:</i></b></p>
                                    </div>
                                    <div class="col-9">
                                        <h5>
                                            <c:if test="${motel.motelDescription!=null}">
                                                <textarea id="motelDescription" placeholder="${motel.motelDescription}" name="motelDescription" rows="4"style="width:100%;"  disabled>${motel.motelDescription}</textarea>
                                            </c:if>
                                            <c:if test="${motel.motelDescription==null}">
                                                <textarea id="motelDescription" name="motelDescription" rows="4"style="width:100%;"></textarea>
                                            </c:if>
                                        </h5>
                                    </div>
                                </div><br/><br/>
                            </div>
                            <c:if test="${motel.motelDescription==null}">
                                <div class="section-action">
                                    <a href="home" class="cancel-btn">Hủy</a>
                                    <input id="modal-btn" type="submit" class="btn btn-dark" style="height: 40px;min-width:120px;" value="Gửi yêu cầu"/>
                                </div>
                            </c:if>
                            <c:if test="${motel.motelStatus == 'reject'}">
                                <div class="section-action">
                                    <a href="home" class="cancel-btn">Hủy</a>
                                    <a href="#" id="edit-btn" class="cancel-btn">Chỉnh sửa</a>
                                    <input id="modal-btn" type="submit" class="btn btn-dark" style="height: 40px;min-width:120px;display:none;" value="Gửi yêu cầu"/>
                                    <input type="hidden" value="${motel.motelID}" name="motelID"/>
                                    <input type="hidden" value="0" id="re-submit-image-verification" name="input-resubmit-motelImage-verification"/>
                                    <input type="hidden" value="0" id="re-submit-image-motel" name="input-resubmit-motelImage" />
                                     <input type="hidden" value="re-submit" id="re-submit"/>
                                </div>
                            </c:if>
                        </div>        
                    </div>
                </form>
            </div>
        </section>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script>
                                        $("#motelImage-verification").on('change', function () {
                                            var countFiles = $(this)[0].files.length;
                                            var imgPath = $(this)[0].value;
                                            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
                                            var image_holder = $("#image-holder");
                                            image_holder.empty();
                                            if (extn === "gif" || extn === "png" || extn === "jpg" || extn === "jpeg") {
                                                if (typeof (FileReader) !== "undefined") {

//loop for each file selected for uploaded.
                                                    for (var i = 0; i < countFiles; i++) {
                                                        if (i == 4) {
                                                            alert("Bạn chỉ được thêm tối đa 4 ảnh");
                                                            break;
                                                        }
                                                        var reader = new FileReader();
                                                        reader.onload = function (e) {
                                                            $("<img />", {
                                                                "src": e.target.result,
                                                                "class": "thumb-image"
                                                            }).appendTo(image_holder);
                                                        }

                                                        image_holder.show();
                                                        reader.readAsDataURL($(this)[0].files[i]);
                                                    }

                                                } else {
                                                    alert("This browser does not support FileReader.");
                                                }
                                            } else {
                                                alert("Chỉ được thêm ảnh");
                                            }
                                        });
                                        $("#motelImage").on('change', function () {
                                            var imgPath = $(this)[0].value;
                                            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
                                            var image_holder = $("#image-motel");
                                            image_holder.empty();
                                            if (extn === "gif" || extn === "png" || extn === "jpg" || extn === "jpeg") {
                                                if (typeof (FileReader) !== "undefined") {
                                                    var reader = new FileReader();
                                                    reader.onload = function (e) {
                                                        $("<img />", {
                                                            "src": e.target.result,
                                                            "class": "thumb-image"
                                                        }).appendTo(image_holder);
                                                    }

                                                    image_holder.show();
                                                    reader.readAsDataURL($(this)[0].files[0]);
                                                } else {
                                                    alert("This browser does not support FileReader.");
                                                }
                                            } else {
                                                alert("Chỉ được thêm ảnh");
                                            }
                                        });
                                        $("#edit-btn").on('click', function () {
                                            $('.input-motel').removeAttr("disabled");
                                            $('#motelDescription').removeAttr("disabled");
                                            $('#motelImage-verification').removeAttr("disabled");
                                            $('#motelImage').removeAttr("disabled");
                                            $('#edit-btn').css("display", "none");
                                            $('#modal-btn').css("display", "");
                                        });
                                        
                                        function check_input_image_verification(){
                                            document.getElementById('re-submit-image-verification').value=1;
                                        }
                                        function check_input_image(){
                                            document.getElementById('re-submit-image-motel').value=1;
                                        }
        </script>
    </body>

</html>
