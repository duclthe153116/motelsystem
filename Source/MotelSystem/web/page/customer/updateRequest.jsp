<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cập nhật yêu cầu phòng</title>
        <link rel="stylesheet" href="../css/host/updateRoom.css">
        <link href="../../css/host/updateRoom.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" src="../js/host/updateRoom.js"></script>
    </head>
    <body>
        <form name="myForm" id="myForm" class="form" action="updaterequest" method="post">
            <h2>Cập Nhật Thông Tin Yêu Cầu</h2>
            <c:set value="${requestScope.requestOfRoomById}" var="request"/>
            <input type="hidden" value="${request.requestID}" name="idRequest">
            <p type="Tên Trọ:">
                
                <input name="roomName" value="${request.motelName}" readonly="">
            </p>
            <p type="Tên Phòng:">
                
                <input name="roomName" value="${request.roomName}" readonly="">
            </p>
            <p type="Nội dung yêu cầu">
                <input name="content" value="${request.content}">
            </p>
            <p type="Ngày tạo">
                <input name="numberCurrent" value="<fmt:formatDate value="${request.createAt}" pattern="dd/MM/yyyy" />" readonly="">
            </p>
            <p type="Ghi chú từ trọ">
                <input name="note" value="${request.noteRequestOfRoom}">
            </p>
            <p type="Ghi chú từ chủ trọ">
                <input name="notefromHost" value="${request.noteRequestOfHost}" readonly="">
            </p>
            <p type="Trạng thái">
                <c:if test="${request.status == '0'}">
                    <input value="Chưa xem"readonly="">
                </c:if>
                <c:if test="${request.status eq 'do'}">
                    <input value="Đang hoàn thành" readonly="">
                </c:if>
                <c:if test="${request.status eq 'done'}">
                    <input value="Đã hoàn thành" readonly="">
                </c:if> 
                <c:if test="${request.status eq 'reject'}">
                    <input value="Hủy yêu cầu" readonly="">
                </c:if> 
                <c:if test="${request.status eq 'seen'}">
                    <input value="Đã xem" readonly="">
                </c:if> 

            </p>
            <a href="../customer/request" style="display: flex;float: left;">Quay lại danh sách yêu cầu</a>
            <input value="Cập nhật" type="submit"> 
            </form>
    </body>
</html>