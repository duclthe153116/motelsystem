<%@page import="model.Room"%>
<%@page import="model.Services"%>
<%@page import="java.util.ArrayList"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : createRoom
    Created on : May 27, 2022, 7:54:01 PM
    Author     : coder
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tạo Yêu Cầu Mới</title>
        <link rel="stylesheet" href="../css/customer/createRoom.css">
        <link href="../../css/customer/createRoom.css" rel="stylesheet" type="text/css"/>
        <style>
            .form#f {
    width: 70ch;
    height: fit-content;
    background: #e6e6e6;
    border-radius: 8px;
    box-shadow: 0 0 40px -10px #000;
    margin: auto;
    padding: 20px 30px;
    margin-top: 3ch;
    max-width: calc(100vw - 40px);
    font-family: 'Montserrat',sans-serif;
    position: relative;
}
        </style>
    </head>
    <body>
        
        
        <div>
            <form id="f" class="form" action="createrequest" method="get">
                <select name="motel" style="color: black" onchange="document.getElementById('f').submit();">
                            <option style="color: black" value="0">Chọn trọ</option>
                            <c:forEach var="listMotel" items="${requestScope.listMotelByAccountID}">
                                <option style="color: black" value="${listMotel.motelID}"<c:if test="${listMotel.motelID == motelID}">selected=""</c:if> >${listMotel.motelName}</option>
                            </c:forEach>
                        </select>
            </form>
            <form id ="a" name="myForm" class="form" action="createrequest" method="post">
                            <h2>Tạo Yêu Cầu Mới</h2>
            <select name="room">
                            <option value="0">Chọn phòng</option>
                            <c:forEach var="listRoom" items="${requestScope.listRoom}">
                                <option value="${listRoom.roomID}">${listRoom.roomName}</option>
                            </c:forEach>
            </select>
            <p type="Nội dung yêu cầu">
                <input name="content" placeholder="Nhập nội dung yêu cầu" type="text">
            </p>
            <a href="http://localhost:8080/MotelSystem/customer/request" style="display: flex;float: left;">Hủy Tạo Yêu Cầu</a>
            
            <button type="submit">Tạo Yêu Cầu</button>
            <p style="color: red">${requestScope.err}</p>
            <p style="color: red">${err}</p>
        </form>
        </div>
            
    </body>
    
</html>
