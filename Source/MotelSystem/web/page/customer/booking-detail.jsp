<%-- 
    Document   : homepage-normal
    Created on : May 14, 2022, 1:43:58 AM
    Author     : royal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en" style="overflow: auto;">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Chi tiết đặt phòng</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <!-- Favicon-->
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<%= request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <style>
            i{
                color: #8080CC;
            }
        </style>
    </head>
    <body>
        <!-- Navigation-->
        <!--        <nav class="navbar navbar-expand-lg navbar-light bg-light" style="display: none">
                    <div class="container px-4 px-lg-5">
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                                <li class="nav-item"><a class="nav-link active" aria-current="page" href="<%= request.getContextPath()%>/home">Trang chủ</a></li>
        
                            </ul>
                            <form class="d-flex" action="chooserole">
                                <input type="submit" class="btn btn-dark" value="Đăng nhập">
                            </form>
                        </div>
                    </div>
                </nav>-->


        <jsp:include page="nav-home-customer.jsp">
            <jsp:param name="nav" value="ManageBooking"></jsp:param>
        </jsp:include>

        <!-- Header-->
        <!-- Section-->

        <section style="padding-bottom: 5em;">
            <div class="container mt-5" style="border: 2px solid #8080CC;padding-top: 20px; border-radius: 10px;box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;">
                <div class="row px-4" style="padding-bottom: 20px;padding-top: 20px">
                    <div class="col px-4" style="border-right: 2px solid #8080CC">
                        <div style="width: 100%;height: 80%;position: relative;padding-right: 0px;">
                            <c:forEach var="profile" items="${bookingDetails.listProfile}" varStatus="theCount">
                                <p><b><i>Người Thuê: ${theCount.count}</i></b></p>

                                <div class="row">
                                    <div class="col-4">
                                        <img src="data:image/jpg;base64,${profile.imageProfile}" style="width:100%;height: auto;"><br>
                                    </div>
                                    <div class="col-8">
                                        <b><i>Họ Tên:</i> ${profile.fullName}</b> <b></b> <br>
                                        <b><i>Số Điện Thoại:</i> ${profile.phone}</b> <b></b> <br>
                                        <b><i>Địa Chỉ:</i> ${profile.address}</b> <b></b> <br>
                                        <fmt:parseDate pattern="yyyy-MM-dd" value="${profile.dOB}" var="parsedDOB" />
                                        <b><i>Ngày Sinh:</i> <fmt:formatDate value="${parsedDOB}" pattern="dd/MM/yyyy" /></b> <b></b> <br>
                                        <b><i>Giới Tính:</i> ${profile.gender}</b> <b></b> <br>
                                    </div>
                                </div>
                                <br>
                            </c:forEach>
                        </div>
                    </div>
                    <div class="col" style="padding-left:50px">
                        <div class="row" style="margin-top: 30px">
                            <div class="col-3">
                                <p><b><i>Tên Trọ:</i></b></p>
                            </div>
                            <div class="col-9">
                                <h4>${bookingDetails.motelName}</h4>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 30px">
                            <div class="col-3">
                                <p><b><i>Tên Phòng:</i></b></p>
                            </div>
                            <div class="col-9">
                                <h5>${bookingDetails.roomName}</h5>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 30px">
                            <div class="col-3">
                                <p><b><i>Ngày Bắt Đầu</i></b></p>
                            </div>
                            <div class="col-9">
                                <fmt:parseDate pattern="yyyy-MM-dd" value="${bookingDetails.startAt}" var="parsedDateS" />
                                <h5><fmt:formatDate value="${parsedDateS}" pattern="dd/MM/yyyy" /></h5>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 30px">
                            <div class="col-3">
                                <p><b><i>Ngày Kết Thúc</i></b></p>
                            </div>
                            <div class="col-9">
                                <fmt:parseDate pattern="yyyy-MM-dd" value="${bookingDetails.endAt}" var="parsedDateE" />
                                <h5><fmt:formatDate value="${parsedDateE}" pattern="dd/MM/yyyy" /></h5>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 30px">
                            <div class="col-3">
                                <p><b><i>Giá:</i></b></p>
                            </div>
                            <div class="col-9">
                                <fmt:setLocale value="vi-VN"/>

                                <h5>
                                    <fmt:formatNumber type = "currency" value = "${bookingDetails.price}" />
                                </h5>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 30px">
                            <div class="col-12">
                                <p><b><i>Các Dịch Vụ Sử Dụng</i></b></p>
                            </div>
                            <div class="col-9">
                                <table border="1">
                                    <tbody >
                                        <tr style="border-width: 2px; width: 200px;">
                                            <td style="border-width: 2px; width: 200px;">Tên Dịch Vụ</td>
                                            <td style="border-width: 2px; width: 200px;">Giá</td>
                                        </tr>
                                        <c:forEach var="services" items="${bookingDetails.listServices}">
                                            <tr style="border-width: 2px; width: 200px;">
                                                <td style="border-width: 2px; width: 200px;">${services.servicesName}</td>
                                                <td style="border-width: 2px; width: 200px;">${services.price}</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>
                </div><br/>
            </div>
        </section>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    </body>


</html>
