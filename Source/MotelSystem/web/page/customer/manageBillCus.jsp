
<%@page import="model.BillByHost"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="model.BillDetail"%>
<%@page import="model.BillDetail"%>
<%@page import="model.BillByHost"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Xem hóa đơn</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/host/manageBill.css">
        <script language="javascript" src="../js/host/manageBill.js"></script>
    </head>

    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-customer.jsp">
            <jsp:param name="nav" value="hostManageBill"></jsp:param>
        </jsp:include>
        <div class="container-lg">
            <!-- Body -->
            <div id="table-account" class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <b>Quản lý hóa đơn</b></h2>
                            </div>
                            <div class="col-sm-6">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm phòng">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <td style="width:20%;">Tên Trọ</td>
                                <td style="width:10%;">Tên Phòng</td>
                                <td style="width:10%;">Tổng chi phí (VND)</td>
                                <td style="width:15%;">Xác nhận thanh toán</td>
                                <td style="width:15%;">Ngày Tạo</td>
                                <td style="width:15%;">Ngày Thanh Toán</td>
                                <td style="width:15%;">Hành Động</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:set var="billId" value=""></c:set>
                            <form action="viewBillDetail?id=" method="GET">
                            <c:forEach var="bill" varStatus="loop" items="${listBill}">
                                <tr class="row-hover">
                                    <td><c:out value="${bill.getMotelName()}"></c:out></td>
                                    <td><c:out value="${bill.getRoomName()}"></c:out></td>
                                    <td><c:out value="${bill.getTotalBill()}"></c:out></td>
                                    <td>
                                        <c:if test="${bill.isConfirmByHost()}">
                                            <c:out value="Đã xác nhận"></c:out>
                                        </c:if>

                                        <c:if test="${!bill.isConfirmByHost()}">
                                            <c:out value="Chưa xác nhận"></c:out>
                                        </c:if>
                                    </td>
                                    <fmt:parseDate pattern="yyyy-MM-dd" value="${bill.getDateCreate()}" var="parsedDate" />
                                    <td><fmt:formatDate value="${parsedDate}" pattern="dd/MM/yyyy" /></td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${bill.getDateOfPayment() != null}">
                                                <c:out value="${bill.getDateOfPayment()}"></c:out>
                                            </c:when>    
                                            <c:otherwise>
                                                <c:out value="Chưa có"></c:out>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>
                                        <a href="viewBillDetail?billId=${bill.getBillID()}" class="view" title="View detail" data-toggle="tooltip"><i 
                                            class="material-icons">visibility</i></a> 
                                    </td>
                                </tr>
                            </c:forEach>
                            </form>
                        </tbody>    
                    </table>
                    <h4 class="${requestScope.bg} text-white text-center rounded w-25 p-2 mb-2 ">${requestScope.error}</h4>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>

</html>
