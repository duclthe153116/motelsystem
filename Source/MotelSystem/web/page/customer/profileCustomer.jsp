<%-- 
    Document   : profileAdmin
    Created on : May 19, 2022, 2:54:13 PM
    Author     : doson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Thông tin cá nhân</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<%= request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <script src="https://code.jquery.com/jquery-3.2.1.js" ></script>
        <!-- JS tạo nút bấm di chuyển trang start -->
        <script src="http://1892.yn.lt/blogger/JQuery/Pagging/js/jquery.twbsPagination.js" type="text/javascript"></script>
        <!-- JS tạo nút bấm di chuyển trang end -->
    </head>
    <body>
        <c:set var="profile" value="${sessionScope.accountAllInfor}" />

        <jsp:include page="nav-home-customer.jsp"/>
        <div class="container rounded bg-white mt-5 mb-5">
            <div class="row">
                <div class="col-md-8">
                    <form action="updateprofile" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                                    <img class="rounded-circle mt-5" id="imageprofile" width="150px" src="data:image/jpg;base64,${profile.imageProfile}">
                                    <span class="font-weight-bold">${profile.fullName}</span>
                                    <span class="text-black-50">
                                        <c:forEach var="roleName" items="${profile.roleName}">
                                            <c:if test="${roleName eq 'admin'}">Admin</c:if>
                                            <c:if test="${roleName eq 'host'}">Chủ trọ</c:if>
                                            <c:if test="${roleName eq 'customer'}">Thuê phòng</c:if><br>
                                        </c:forEach>
                                    </span>
                                    <label for="image-profile" class="btn btn-dark" style="">Ảnh đại diện</label>
                                    <input id="image-profile" name="profile_image" type="file" class="form-control" style="display:none;"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="p-3 py-5">
                                    <div class="d-flex justify-content-between align-items-center mb-3">
                                        <h4 class="text-right">Cài đặt thông tin cá nhân</h4>
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col-md-12"><label class="labels">Họ và tên</label><input type="text" class="form-control" placeholder="${profile.fullName}" value="${profile.fullName}" name="fullName"></div>
                                        <div class="col-md-12"><label class="labels">Ngày sinh</label><input type="date" class="form-control" placeholder="${profile.dOB}" value="${profile.dOB}" name="dob"></div>
                                        <div class="col-md-12"><label class="labels">Giới tính</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="gender" value="Male" <c:if test="${profile.gender eq 'Male'}">checked=""</c:if>> Nam &nbsp;&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="gender" value="Female" <c:if test="${profile.gender eq 'Female'}">checked=""</c:if>> Nữ</div>
                                        <div class="col-md-12"><label class="labels">Số điện thoại</label><input type="text" class="form-control" placeholder="${profile.phone}" value="${profile.phone}" name="phone"></div>
                                        <div class="col-md-12"><label class="labels">Địa chỉ</label><input type="text" class="form-control" placeholder="${profile.address}" value="${profile.address}" name="address"></div>
                                        <div class="col-md-12"><label class="labels">Email</label><input type="text" class="form-control" placeholder="${profile.email}" value="${profile.email}" name="email"></div>
                                    </div>
                                    <c:set var="errChangePassword" value="${requestScope.errorPhone}"/>
                                    <p style="color: red">${errChangePassword}</p>
                                    <div class="mt-5 text-center"><input class="btn btn-primary profile-button" type="submit" value="Lưu thông tin cá nhân"></div>

                                </div>
                            </div>
                        </div>
                    </form>   
                </div>
                <div class="col-md-4">

                    <div class="p-3 py-5">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h4 class="text-right">Đổi mật khẩu</h4>
                        </div>
                        <form action="changepassword" method="get">
                            <div class="row mt-3">
                                <div class="col-md-12"><label class="labels">Mật khẩu cũ</label><input type="password" class="form-control" placeholder="Mật khẩu cũ" value="" name="oldPassword" ></div> <br>
                                <div class="col-md-12"><label class="labels">Mật khẩu mới</label><input class="form-control" type="password" class="form-control" placeholder="Mật khẩu mới" value="" name="newPassword"></div>
                                <div class="col-md-12"><label class="labels">Nhập lại mật khẩu mới</label><input class="form-control" type="password" class="form-control" placeholder="Nhập lại mật khẩu mới" value="" name="newPasswordAgain">
                                </div>
                            </div>
                            <c:set var="errChangePassword" value="${requestScope.errorOldpass}"/>
                            <p style="color: red">${errChangePassword}</p>
                            <div class="mt-5 text-center"><input class="btn btn-primary profile-button" type="submit" value="Đổi mật khẩu"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        $("#image-profile").on('change', function () {
            var imgPath = $(this)[0].value;
            var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var image_holder = $("#imageprofile");
            image_holder.empty();
            if (extn === "gif" || extn === "png" || extn === "jpg" || extn === "jpeg") {
                if (typeof (FileReader) !== "undefined") {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imageprofile").attr("src", e.target.result);
                    }



                    image_holder.show();
                    reader.readAsDataURL($(this)[0].files[0]);
                } else {
                    alert("This browser does not support FileReader.");
                }
            } else {
                alert("Chỉ được thêm ảnh");
            }
        });
    </script>
</html>

