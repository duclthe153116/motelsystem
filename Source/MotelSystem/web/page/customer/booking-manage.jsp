<%-- 
    Document   : post-management
    Created on : Jun 11, 2022, 4:09:33 AM
    Author     : royal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý đặt phòng</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">   
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/host/manageServices.css">
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
    </head>
    <body>
        <jsp:include page="nav-home-customer.jsp">
            <jsp:param name="nav" value="ManageBooking"></jsp:param>
        </jsp:include>
        <div class="container">
            <div class="table-responsive" style="border-radius: 10px;
                 box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;
                 margin-bottom: 100px;
                 margin-top: 90px">
                <div class="table-wrapper" style="border: 2px solid #8080CC;border-radius: 10px;">
                    <div class="table-title bg-dark" style="margin-bottom: 30px;">
                        <div class="row">
                            <div class="col-sm-8">
                                <h2><b>Quản lý đặt phòng</b></h2>
                            </div>
                            <div class="col-sm-4">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm với từ khóa..." style="background-color: white;border: 1px solid white;height: auto;float: end">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="table-manage-post" class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width: 11%;text-align: center;">Tên trọ</th>
                                <th style="width: 10%;text-align: center;">Tên phòng</th>
                                <th style="width: 10%;text-align: center;">Giá phòng</th>
                                <th style="width: 14%;text-align: center;">Ngày bắt đầu</th>
                                <th style="width: 14%;text-align: center;">Ngày kết thúc</th>
                                <th style="width: 1%;text-align: center;"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="b" items="${listBooking}" varStatus="loop">
                                <tr>
                                    <td style="text-align: center;">${b.motelName}<br/>
                                        <c:if test="${b.isDeleteMotel == true}"><i style="color: red;font-size: 12px">(Trọ đã bị khóa)</i></c:if>
                                    </td>
                                    <td style="text-align: center;">${b.roomName}</td>
                                    <td style="text-align: center;">${b.price}</td>
                                    <fmt:parseDate pattern="yyyy-MM-dd" value="${b.startAt}" var="parsedDateStart" />
                                    <td style="text-align: center;"><fmt:formatDate value="${parsedDateStart}" pattern="dd/MM/yyyy" /></td>
                                    <fmt:parseDate pattern="yyyy-MM-dd" value="${b.endAt}" var="parsedDateEnd" />
                                    <td style="text-align: center;"><fmt:formatDate value="${parsedDateEnd}" pattern="dd/MM/yyyy" /></td>
                                    <td><a href="CustomerBookingDetails?id=${b.bookingID}" class="view" title="View detail" data-toggle="tooltip"><i 
                                                class="material-icons">visibility</i></a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>  

    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script>
        $(document).ready(function () {
            // Active tooltips
            $('[data-toggle="tooltip"]').tooltip();

            // Filter table rows based on search
            $("#search").on("keyup", function () {
                var term = $(this).val().toLowerCase();
                $("table tbody tr").each(function () {
                    $row = $(this);
                    var keyword = $row.find("td").text().toLowerCase();
                    console.log(keyword);
                    if (keyword.search(term) < 0) {
                        $row.hide();
                    } else {
                        $row.show();
                    }
                });
            });
        });

        $(document).ready(function () {
            $('#table-manage-post').dataTable({
                "bPaginate": true,
                "oLanguage": {
                    "oPaginate": {
                        "sPrevious": "<",
                        "sNext": ">"
                    },
                    "sEmptyTable": "Không có dữ liệu",
                    "sLengthMenu": "Hiển thị _MENU_ kết quả"
                },
                "bFilter": false,
                "bInfo": false,
            });
        });

        function openForm() {
            document.getElementById("myForm").style.display = "block";
        }

        function closeForm() {
            document.getElementById("myForm").style.display = "none";
        }
    </script>
</html>
