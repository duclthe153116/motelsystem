<%-- 
    Document   : SignUpAccount
    Created on : May 15, 2022, 9:23:23 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Đăng ký tài khoản</title>
        <link rel="stylesheet" href="../css/SignUp.css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-validator/0.5.3/js/bootstrapValidator.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/typicons/2.0.9/typicons.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
        <script language="javascript" src="https://cldup.com/S6Ptkwu_qA.js"></script>
        <script language="javascript" src="../js/CheckSignUpAccount.js"></script>
    </head>

    <body id="particles-js">
        <div class="animated bounceInDown">
            <div class="container">

                <form class="well form-horizontal" action="SignUp" method="post" id="contact_form">
                    <fieldset>

                        <!-- Form Name -->
                        <legend>
                            <center>
                                <h2><b>Đăng ký tài khoản</b></h2>
                            </center>
                        </legend><br>

                        <!-- Text input-->

                        <div class="form-group">
                            <label class="col-md-4 control-label">Họ tên</label>
                            <div class="col-md-4 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input name="name" placeholder="Họ tên" class="form-control" type="text">
                                </div>
                            </div>
                        </div>

                        <!-- Text input-->

                        <div class="form-group">
                            <label class="col-md-4 control-label">Giới tính</label>
                            <div class="col-md-4 inputGroupContainer">
                                <div class="input-group"">
                                    <i class="bi bi-gender-female"></i>
                                    <input name="gender" value="Male" type="radio" > Nam     
                                    <input name="gender" value="Female" type="radio" > Nữ    
                                    <input name="gender" value="Other" type="radio"> Khác                                   
                                </div>
                            </div>
                        </div>

                        <!-- Text input-->

                        <div class="form-group">
                            <label class="col-md-4 control-label">Ngày sinh</label>
                            <div class="col-md-4 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input type="date" name="DOB" class="form-control">
                                </div>
                            </div>
                        </div>

                        <!-- Text input-->

                        <div class="form-group">
                            <label class="col-md-4 control-label">Địa chỉ</label>
                            <div class="col-md-4 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input name="address" placeholder="Địa chỉ" class="form-control" type="text">
                                </div>
                            </div>
                        </div>

                        <!-- Text input-->

                        <div class="form-group">
                            <label class="col-md-4 control-label">Tên tài khoản</label>
                            <div class="col-md-4 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input name="user_name" placeholder="Tên tài khoản" class="form-control" type="text">
                                </div>
                            </div>
                        </div>

                        <!-- Text input-->

                        <div class="form-group">
                            <label class="col-md-4 control-label">Mật khẩu</label>
                            <div class="col-md-4 inputGroupContainer">
                                <div class="input-group"> 
                                    <span class="input-group-addon"><i class="typcn typcn-eye" id="eye"></i></i></span>                               
                                    <input id="user_password" name="user_password" placeholder="Mật khẩu" class="form-control" type="password">
                                    <div></div>
                                </div>
                            </div>
                        </div>

                        <!-- Text input-->

                        <div class="form-group">
                            <label class="col-md-4 control-label">Xác nhận mật khẩu</label>
                            <div class="col-md-4 inputGroupContainer">
                                <div class="input-group">  
                                    <span class="input-group-addon"><i class="typcn typcn-eye" id="eye"></i></i></span>                              
                                    <input id="confirm_password" name="confirm_password" placeholder="Xác nhận mật khẩu" class="form-control"
                                           type="password">                                
                                </div>
                                <span id='message'></span>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label">E-Mail</label>
                            <div class="col-md-4 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                    <input name="email" placeholder="Địa chỉ E-Mail" class="form-control" type="text">
                                </div>
                            </div>
                        </div>


                        <!-- Text input-->

                        <div class="form-group">
                            <label class="col-md-4 control-label">Số điện thoại</label>
                            <div class="col-md-4 inputGroupContainer">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                    <input name="contact_no" placeholder="(+84)" class="form-control" type="text">
                                </div>
                            </div>
                        </div>


                        <!--                    <div class="form-group">
                                                <label class="col-md-4 control-label">Địa chỉ ảnh</label>
                                                <div class="col-md-4 inputGroupContainer">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                                        <input name="image" placeholder="" class="form-control" type="text">
                                                    </div>
                                                </div>
                                            </div>-->

                        <!-- Select Basic -->

                        <!-- Success message -->
                        <!--                    <div class="alert alert-success" role="alert" id="success_message">Success <i
                                                    class="glyphicon glyphicon-thumbs-up"></i> Success!.</div>-->
                        <div  class="text-center"><span style="color: red;text-align: center;">${error}</span></div>

                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-4"><br>                            
                                <input type="submit" class="btn btn-warning" value="Đăng ký">                            
                            </div>
                        </div>
                    </fieldset>
                    <!--                <input type="submit" class="btn btn-warning" value="Đăng ký">  -->
                </form>
            </div>
        </div><!-- /.container -->
    </body>
    <!--<script>
        
        $('#user_password, #confirm_password').on('keyup', function () {
            if ($('#user_password').val() === $('#confirm_password').val()) {
                $('#message').html('Mật khẩu tương xứng').css('color', 'green');
            } else
                $('#message').html('Mật khẩu không tương xứng').css('color', 'red');
        });
        
        $('#user_password').on(function () {
    
        $("input[type=text]").each(function () {
            var validated =  true;
            if(this.value.length < 8)
                validated = false;
            if(!/\d/.test(this.value))
                validated = false;
            if(!/[a-z]/.test(this.value))
                validated = false;
            if(!/[A-Z]/.test(this.value))
                validated = false;
            if(/[^0-9a-zA-Z]/.test(this.value))
                validated = false;
            $('div').text(validated ? "pass" : "fail");
            // use DOM traversal to select the correct div for this input above
        });
    });
    
    });
    </script>-->

</html>