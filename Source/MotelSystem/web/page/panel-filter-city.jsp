<%-- 
    Document   : panel-filter-city
    Created on : May 29, 2022, 7:17:45 AM
    Author     : royal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<script>
    var checkAreaToggle = false;
    $(document).ready(function () {
        $("#flip").click(function () {
            if (checkAreaToggle) {
                $("#panel").slideUp("slow");
                $("#flip").html("<b>Chọn Tỉnh/thành &#8595;</b>");
                checkAreaToggle = false;
            } else {
                $("#panel").slideDown("slow");
                $("#flip").html("<b>Chọn Tỉnh/thành &#8593;</b>");
                checkAreaToggle = true;
            }
        });
    });
</script>
<div class="container px-4 px-lg-5">            
    <div id="panel" class="table-responsive" style="padding: 5px;border: 2px solid #8080CC;border-radius: 5px;display: none;">
        <table class="table" style="border: 0px solid white;margin-bottom: 0px;">
            <tr>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${(param.city == 'Tất cả' || param.city==null) ? 'dark' : 'light'}">Tất cả</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Vũng Tàu' ? 'dark' : 'light'}">Vũng Tàu</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Bắc Giang' ? 'dark' : 'light'}">Bắc Giang</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Bắc Kạn' ? 'dark' : 'light'}">Bắc Kạn</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Bạc Liêu' ? 'dark' : 'light'}">Bạc Liêu</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Bắc Ninh' ? 'dark' : 'light'}">Bắc Ninh</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Bến Tre' ? 'dark' : 'light'}">Bến Tre</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Bình Định' ? 'dark' : 'light'}">Bình Định</a></td>
            </tr>
            <tr>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Bình Dương' ? 'dark' : 'light'}">Bình Dương</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Bình Phước' ? 'dark' : 'light'}">Bình Phước</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Bình Thuận' ? 'dark' : 'light'}">Bình Thuận</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Cà Mau' ? 'dark' : 'light'}">Cà Mau</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Cần Thơ' ? 'dark' : 'light'}">Cần Thơ</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Cao Bằng' ? 'dark' : 'light'}">Cao Bằng</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Đà Nẵng' ? 'dark' : 'light'}">Đà Nẵng</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Đắk Lắk' ? 'dark' : 'light'}">Đắk Lắk</a></td>
            </tr>
            <tr>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Đắk Nông' ? 'dark' : 'light'}">Đắk Nông</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Điện Biên' ? 'dark' : 'light'}">Điện Biên</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Đồng Nai' ? 'dark' : 'light'}">Đồng Nai</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Đồng Tháp' ? 'dark' : 'light'}">Đồng Tháp</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Gia Lai' ? 'dark' : 'light'}">Gia Lai</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Hà Giang' ? 'dark' : 'light'}">Hà Giang</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Hà Nam' ? 'dark' : 'light'}">Hà Nam</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Hà Nội' ? 'dark' : 'light'}">Hà Nội</a></td>
            </tr>
            <tr>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Hà Tĩnh' ? 'dark' : 'light'}">Hà Tĩnh</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Hải Dương' ? 'dark' : 'light'}">Hải Dương</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Hải Phòng' ? 'dark' : 'light'}">Hải Phòng</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Hậu Giang' ? 'dark' : 'light'}">Hậu Giang</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Hòa Bình' ? 'dark' : 'light'}">Hòa Bình</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Hưng Yên' ? 'dark' : 'light'}">Hưng Yên</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Khánh Hòa' ? 'dark' : 'light'}">Khánh Hòa</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Kiên Giang' ? 'dark' : 'light'}">Kiên Giang</a></td>
            </tr>
            <tr>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Kon Tum' ? 'dark' : 'light'}">Kon Tum</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Lai Châu' ? 'dark' : 'light'}">Lai Châu</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Lâm Đồng' ? 'dark' : 'light'}">Lâm Đồng</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Lạng Sơn' ? 'dark' : 'light'}">Lạng Sơn</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Lào Cai' ? 'dark' : 'light'}">Lào Cai</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Long An' ? 'dark' : 'light'}">Long An</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Nam Định' ? 'dark' : 'light'}">Nam Định</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Nghệ An' ? 'dark' : 'light'}">Nghệ An</a></td>
            </tr>
            <tr>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Ninh Bình' ? 'dark' : 'light'}">Ninh Bình</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Ninh Thuận' ? 'dark' : 'light'}">Ninh Thuận</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Phú Thọ' ? 'dark' : 'light'}">Phú Thọ</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Phú Yên' ? 'dark' : 'light'}">Phú Yên</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Quảng Bình' ? 'dark' : 'light'}">Quảng Bình</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Quảng Nam' ? 'dark' : 'light'}">Quảng Nam</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Quảng Ngãi' ? 'dark' : 'light'}">Quảng Ngãi</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Quảnh Ninh' ? 'dark' : 'light'}">Quảng Ninh</a></td>
            </tr>
            <tr>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Quảng Trị' ? 'dark' : 'light'}">Quảng Trị</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Sóc Trăng' ? 'dark' : 'light'}">Sóc Trăng</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Sơn La' ? 'dark' : 'light'}">Sơn La</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Tây Ninh' ? 'dark' : 'light'}">Tây Ninh</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Thái Bình' ? 'dark' : 'light'}">Thái Bình</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Thái Nguyên' ? 'dark' : 'light'}">Thái Nguyên</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Thanh Hóa' ? 'dark' : 'light'}">Thanh Hóa</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Thừa Thiên Huế' ? 'dark' : 'light'}">Thừa Thiên Huế</a></td>
            </tr>
            <tr>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Tiền Giang' ? 'dark' : 'light'}">Tiền Giang</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'TP Hồ Chí Minh' ? 'dark' : 'light'}">TP Hồ Chí Minh</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Trà Vinh' ? 'dark' : 'light'}">Trà Vinh</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Tuyên Quang' ? 'dark' : 'light'}">Tuyên Quang</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Vĩnh Long' ? 'dark' : 'light'}">Vĩnh Long</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Vĩnh Phúc' ? 'dark' : 'light'}">Vĩnh Phúc</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'Yên Bái' ? 'dark' : 'light'}">Yên Bái</a></td>
                <td><a style="width: 100%" href="#" onclick="return filterCity(this.innerHTML)" class="btn btn-${param.city == 'An Giang' ? 'dark' : 'light'}">An Giang</a></td>
            </tr>
        </table>
    </div>
</div>
