<%-- 
    Document   : post-management
    Created on : Jun 11, 2022, 4:09:33 AM
    Author     : royal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý bài đăng</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">   
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="<%=request.getContextPath()%>/css/host/manageServices.css">
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="<%= request.getContextPath()%>/css/swalert.css" rel="stylesheet" />
    </head>
    <body>
        <jsp:include page="nav-home-host.jsp">
            <jsp:param name="nav" value="hostManagePost"></jsp:param>
        </jsp:include>
        <div class="form-popup" id="myForm" style="border-radius: 10px;border:2px solid #8080CC">
            <form class="form-container bg-dark" name="myForm" action="<%=request.getContextPath()%>/host/PostAddNew" onsubmit="return validateForm()" method="get" style="box-shadow: 9px 12px 20px 13px;border-radius: 10px" >
                <h1 style="text-align: center;color:white">Soạn bài đăng</h1>
                <hr/>
                <label style="margin-top: 10px;color:white;"><b>Chọn Nhà Trọ:</b></label>
                <div style="height: auto;margin-bottom: 20px;margin-top: 20px" >
                    <select class="form-control selectpicker droplist" name="motelID" data-live-search="true">
                        <c:forEach var="motel" items="${listMotel}">
                            <option value="${motel.motelID}">${motel.motelName}</option>
                        </c:forEach>
                    </select>
                </div>
                <input type="submit" class="btn" value="Tới trang tạo mới bài đăng"/>
                <button type="button" class="btn cancel" onclick="closeForm()">Hủy</button>
            </form>
        </div>
        <div class="container">
            <div class="table-responsive" style="border-radius: 10px;
                 box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;
                 margin-bottom: 100px;
                 margin-top: 90px">
                <div class="table-wrapper" style="border: 2px solid #8080CC;border-radius: 10px;">
                    <div class="table-title bg-dark" style="margin-bottom: 30px;">
                        <div class="row">
                            <div class="col-sm-4">
                                <h2><b>Quản lý bài đăng</b></h2>
                            </div>
                            <div class="col-sm-4" style="text-align: center;">
                                <button style="box-shadow: 3px 2px 10px 4px;
                                        background-color:#a6cc80;
                                        color: black;
                                        border-radius: 10px;
                                        padding-left: 10px;
                                        padding-right: 10px;
                                        padding-top: 10px;
                                        padding-bottom: 10px; " type="button" class="open-button" onclick="openForm()"><b>Soạn bài đăng mới</b></button>
                            </div>
                            <div class="col-sm-4">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm với từ khóa..." style="background-color: white;border: 1px solid white;height: auto">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="table-manage-post" class="table table-striped">
                        <thead>
                            <tr>
                                <th style="width: 35%;text-align: center;">Tiêu đề</th>
                                <th style="width: 40%;text-align: center;">Nội dung</th>
                                <th style="width: 15%;text-align: center;">Ngày đăng</th>
                                <th style="width: 10%;text-align: center;">Trạng thái</th>
                                <th style="width: 10%;text-align: center;">Thao tác</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="post" items="${listPost}" varStatus="loop">
                                <tr>
                                    <td>${post.title}</td>
                                    <td>${post.content}</td>
                                    <fmt:parseDate pattern="yyyy-MM-dd" value="${post.pDate}" var="parsedDate" />
                                    <td style="text-align: center;"> <fmt:formatDate value="${parsedDate}" pattern="dd/MM/yyyy" /></td>
                                    <td style="text-align: center;">
                                        <c:choose>
                                            <c:when test = "${post.pStatus == 'publish'}">
                                                Đã duyệt
                                            </c:when>
                                            <c:when test = "${post.pStatus == 'pending'}">
                                                Chờ duyệt
                                            </c:when>
                                            <c:when test = "${post.pStatus == 'reject'}">
                                                Bị từ chối
                                            </c:when>
                                            <c:otherwise>
                                                Không có dữ liệu
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td style="text-align: center;">
                                        <a href="<%=request.getContextPath()%>/host/PostEditHost?postID=${post.id}" data-toggle="tooltip" title="Sửa bài đăng">
                                            <img src="<%=request.getContextPath()%>/assets/img/edit.png" width="25px" height="25px">
                                        </a>&nbsp;
                                        <a href="#" onclick="return showConfirmDelete(${post.id})" data-toggle="tooltip" title="Xóa bài đăng">
                                            <img src="<%=request.getContextPath()%>/assets/img/delete.png" width="25px" height="25px">
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>  

    </body>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script>
                                            $(document).ready(function () {
                                                // Active tooltips
                                                $('[data-toggle="tooltip"]').tooltip();

                                                // Filter table rows based on search
                                                $("#search").on("keyup", function () {
                                                    var term = $(this).val().toLowerCase();
                                                    $("table tbody tr").each(function () {
                                                        $row = $(this);
                                                        var keyword = $row.find("td").text().toLowerCase();
                                                        console.log(keyword);
                                                        if (keyword.search(term) < 0) {
                                                            $row.hide();
                                                        } else {
                                                            $row.show();
                                                        }
                                                    });
                                                });
                                            });

                                            $(document).ready(function () {
                                                $('#table-manage-post').dataTable({
                                                    "bPaginate": true,
                                                    "oLanguage": {
                                                        "oPaginate": {
                                                            "sPrevious": "<",
                                                            "sNext": ">"
                                                        },
                                                        "sEmptyTable": "Không có dữ liệu",
                                                        "sLengthMenu": "Hiển thị _MENU_ kết quả"
                                                    },
                                                    "bFilter": false,
                                                    "bInfo": false,
                                                });
                                            });

                                            function openForm() {
                                                document.getElementById("myForm").style.display = "block";
                                            }

                                            function closeForm() {
                                                document.getElementById("myForm").style.display = "none";
                                            }
    </script>
    <script>
        function showConfirmDelete(id) {
            swal({
                title: 'Hành động không thể hoàn tác!',
                text: 'Bạn muốn tiếp tục?',
                icon: "warning",
                buttons: ["Hủy bỏ", "Xóa"],
                dangerMode: true,
            })
                    .then((willDelete) => {
                        if (willDelete) {
                            window.location.href = "<%=request.getContextPath()%>/host/PostDeleteHost?postID=" + id;
                        }
                    });
        }

        var flag = '${action}';
        if (flag.localeCompare('insert') === 0) {
            swal({
                title: "Xóa thành công!",
                icon: "success",
                buttons: {
                    ok: "OK!"
                }
            });
        } else if (flag.localeCompare('failtoadd') === 0) {
            swal({
                title: "Có lỗi xảy ra!",
                text: "Vui lòng thực hiện lại thao tác",
                icon: "danger",
                buttons: {
                    ok: "OK!"
                }
            });
        }
    </script>
</html>
