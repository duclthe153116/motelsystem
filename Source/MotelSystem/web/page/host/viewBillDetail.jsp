
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="model.BillDetail"%>
<%@page import="model.BillDetail"%>
<%@page import="model.BillByHost"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Xem hóa đơn chi tiết</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/host/viewBillDetail.css">
        <script language="javascript" src="../js/host/viewBillDetail.js"></script>
    </head>

    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp"/>
        <div class="container-lg">
            <!-- Body -->
            <div id="table-account" class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Xem hóa đơn chi tiết </b></h2>
                            </div>
                            <div class="col-sm-4">
                                <!--<h2 class="add-service"><a href="screenAddService?motelId=${motelId}&roomId=${roomId}&billId=${billId}&isFirst=1">Thêm Dịch Vụ</a></h2>-->
                            </div>

                            <div class="col-sm-4">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm theo dịch vụ">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%
                        Object o = request.getAttribute("bill");
                        BillByHost bill = null;
                        if (o == null) {
                            bill = new BillByHost();
                        } else {
                            bill = (BillByHost) o;
                        }

                        int billID = bill.getBillID();
                        boolean confirmHost = bill.isConfirmByHost();
                        boolean confirmCus = bill.isConfirmByCustomer();
                        int motelID = bill.getMotelID();
                        int roomID = bill.getRoomID();
                        boolean isSent = bill.isIsSent();
                    %>

                    <div class="row">
                        <div class="col-4">
                            <p class="above-table">Người Tạo: ${hostName}</p>
                        </div>
                        <div class="col-4">
                            <p class="above-table">Tổng Chi Phí Hóa Đơn: ${total}</p>
                        </div>
                        <div class="col-4">
                            <p style="display: inline-block">Chủ Xác Nhận Thanh Toán: </p>
                            <input type="checkbox" name="confirmHost" 
                                   <% if (confirmHost == true) {%> checked="" <%}%>>
                            <a onclick="if (!window.confirm('Bạn chắc chắn muốn xác nhận thanh toán từ phía chủ trọ?')) {
                                        return false;
                                    }" 
                               href="confirmHost?billID=<%= billID%>&confirmCus=<%= confirmCus%>
                               &motelID=<%= motelID%>&roomID=<%= roomID%>">Lưu Thay Đổi</a>
                        </div>
                    </div>
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <td style="width:10% ;">Tên dịch vụ</td>
                                <td style="width:10% ;">Số lượng</td>
                                <td style="width:10% ;">Giá (VND)</td>
                                <td style="width:10%;">Tổng tiền (VND)</td>
                                <td style="width:20%;">Chủ Trọ Xác Nhận Thanh Toán</td>
                                <td style="width:20%;">Khách Xác Nhận Thanh Toán</td>

                                <% if (isSent == false) {%>
                                <td style="width:20%;">Hành Động</td>
                                <%}%>
                            </tr>
                        </thead>
                        <tbody>

                            <c:forEach items="${requestScope.billDetailList}" var="billDetail">
                                <tr class="row-hover">
                                    <td><c:out value="${billDetail.getServicesName()}"></c:out></td>
                                        <td>
                                        <c:out value="${billDetail.getQuantity()}"></c:out>
                                        </td>
                                        <td>
                                        <fmt:formatNumber type = "number" value = "${billDetail.getServicesPrice()}" />
                                        <%--<c:out value="${billDetail.getServicesPrice()}"></c:out>--%>
                                        </td>
                                        <td>
                                        <fmt:formatNumber type = "number" value = "${billDetail.getTotalBillDetail()}" />
                                        <%--<c:out value="${billDetail.getTotalBillDetail()}"></c:out>--%>
                                    </td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${billDetail.isHostConfirm() == true}">
                                                <c:out value="Đã xác nhận"></c:out>
                                            </c:when>
                                            <c:otherwise>
                                                <c:out value="Chưa xác nhận"></c:out>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>

                                    <td>
                                        <c:choose>
                                            <c:when test="${billDetail.isCusConfirm() == true}">
                                                <c:out value="Đã xác nhận"></c:out>
                                            </c:when>
                                            <c:otherwise>
                                                <c:out value="Chưa xác nhận"></c:out>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <% if (isSent == false) {%>
                                    <td>
                                        <a onclick="if (!confirm('Bạn chắc chắn muốn xóa dịch vụ này?')) {
                                                    return false;
                                                }"
                                           href="deleteBillDetail?billId=${billId}&billDetailId=${billDetail.getBillDetailID()}&motelId=${motelId}&roomId=${roomId}" class="delete" title="Delete" data-toggle="tooltip"><i
                                                class="material-icons">&#xE872;</i></a>
                                        <a id="updateService" 
                                           href="editBillDetail?billDetailId=${billDetail.getBillDetailID()}" class="edit" title="Edit" data-toggle="tooltip"><i
                                                class="material-icons">edit</i></a>
                                    </td>
                                    <%}%>
                                </tr>
                            </c:forEach>
                        </tbody>    

                    </table>
                    <p style="color: green" class="text-center">${success}</p>
                    <a href="manageBill">Quay lại</a>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>

</html>
