<%@page import="model.Room"%>
<%@page import="model.Services"%>
<%@page import="java.util.ArrayList"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : createRoom
    Created on : May 27, 2022, 7:54:01 PM
    Author     : coder
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tạo Phòng Mới</title>
        <link rel="stylesheet" href="../css/host/createRoom.css">
        <link href="../../css/host/createRoom.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" src="../js/host/createRoom.js"></script>
    </head>
    <body>
        <script>
            var listRoomJS = [];
            <%
                ArrayList<Room> listRoom = (ArrayList<Room>) request.getAttribute("listRoom");
                for (int i = 0; i < listRoom.size(); i++) {%>
            var roomJS = {roomName: '<%=listRoom.get(i).getRoomName()%>', motelID:<%=listRoom.get(i).getMotelID()%>};
            listRoomJS.push(roomJS);
            <%}%>
        </script>
        <form name="myForm" id="myForm" class="form" action="CreateRoom" method="post" onsubmit="return validateForm(listRoomJS)">
            <h2>Tạo Phòng Mới</h2>
            <p type="Chọn Trọ:">
                <select name="motelID" id="motelID" required="">
                    <c:forEach var="motel" items="${listMotel}">
                        <option value="${motel.motelID}">${motel.motelName}</option>
                    </c:forEach>
                </select>
            </p>
            <p type="Tên Phòng:">
                <input name="roomName" placeholder="H101" required="">
            </p>
            <p type="Số Lượng Người Tối Đa:">
                <input name="numberMax" placeholder="số lượng người tối đa phải là số nguyên" required="">
            </p>
            <p type="Diện Tích:">
                <input name="area" placeholder="diện tích có thể nhập số thực" required="">
            </p>
            <p type="Giá Phòng:">
                <input name="price" placeholder="giá phòng có thể nhập số thực" required="">
            </p>
            <p type="Ghi Chú:">
                <textarea name="note" placeholder=""></textarea>
            </p>
            <a href="http://localhost:8080/MotelSystem/host/ManageRoom" style="display: flex;float: left;">Hủy Tạo Phòng</a>
            <button type="submit">Tạo Phòng</button>
        </form>
    </body>
    
</html>
