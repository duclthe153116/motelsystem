<%-- 
    Document   : homepage-normal
    Created on : May 14, 2022, 1:43:58 AM
    Author     : royal
--%>

<%@page import="model.AccountAllInfor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%
    AccountAllInfor loginedUser = (AccountAllInfor) session.getAttribute("accountAllInfor");
%>
<!DOCTYPE html>
<html lang="en" style="overflow: auto;">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Chi tiết bài đăng</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <!-- Favicon-->
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<%= request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="<%= request.getContextPath()%>/css/swalert.css" rel="stylesheet" />
        <style>
            img {
                vertical-align: middle;
            }


            /* Hide the images by default */
            .mySlides {
                display: block;
                width: 100%;
                height: 500px;
            }

            /* Add a pointer when hovering over the thumbnail images */
            .cursor {
                cursor: pointer;
            }

            /* Next & previous buttons */
            .prev,
            .next {
                cursor: pointer;
                position: absolute;
                top: 50%;
                width: auto;
                padding: 16px;
                margin-top: -50px;
                color: #FFF;
                font-weight: bold;
                font-size: 20px;
                border-radius: 0 3px 3px 0;
                user-select: none;
                text-decoration: none;
                -webkit-user-select: none;
            }

            /* Position the "next button" to the right */
            .next {
                right:0;
                border-radius: 3px 0 0 3px;
            }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover,
            .next:hover {
                background-color: rgba(0, 0, 0, 0.8);
            }

            /* Add a transparency effect for thumnbail images */
            .demo {
                opacity: 0.6;
            }

            .active,
            .demo:hover {
                opacity: 1;
            }

            i{
                color: #8080CC;
            }
        </style>
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp"/>
        <!-- Header-->
        <!-- Section-->


        <section style="padding-bottom: 5em;">
            <div class="container mt-5" style="border: 3px solid #8080CC;padding-top: 20px; border-radius: 10px;box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;">
                <div class="d-flex justify-content-between">
                    <div>
                        <a href="ViewMotelDetail?id=${post.getModelID()}" style="font-size: 25px;text-decoration: none;color:#8080CC"><b><i>&emsp;&emsp;${post.motelName}</i></b></a>
                    </div>
                    <div>
                        <a href="<%=request.getContextPath()%>/host/PostEditHost?postID=${post.id}" id="editButton" class="btn btn-dark" style="display:none;">Sửa</a>
                    </div>
                </div>
                <div class="row px-4" style="padding-bottom: 20px;padding-top: 20px">
                    <div class="col px-4" style="border-right: 2px solid #8080CC">
                        <div class="slideshow" style="width: 100%;height: 80%;position: relative;padding-right: 0px;">
                            <c:forEach var="i" items="${post.listImages}">
                                <div class="mySlides">
                                    <img src="data:image/jpg;base64,${i.base64String}" style="width:100%;height: 100%;">
                                </div>
                            </c:forEach>
                            <a class="prev" onclick="plusSlides(-1)">❮</a>
                            <a class="next" onclick="plusSlides(1)">❯</a>
                            <br/>
                            <div class="row">
                                <c:forEach var="i" varStatus="loop" items="${post.listImages}">
                                    <div class="column">
                                        <img class="demo cursor" src="data:image/jpg;base64,${i.base64String}" style="width:100%;height: 80px" onclick="currentSlide(${loop.index+1})" alt="The Woods">
                                    </div>
                                </c:forEach>
                            </div>
                        </div>
                    </div>
                    <div class="col" style="padding-left:50px">
                        <div class="row" style="margin-top: 30px">
                            <div class="col-3">
                                <p><b><i>Tiêu Đề:</i></b></p>
                            </div>
                            <div class="col-9">
                                <h4>${post.title}</h4>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 30px">
                            <div class="col-3">
                                <p><b><i>Nội dung:</i></b></p>
                            </div>
                            <div class="col-9">
                                <h5>${post.content}</h5>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 30px">
                            <div class="col-3">
                                <p><b><i>Giá:</i></b></p>
                            </div>
                            <div class="col-9">
                                <fmt:setLocale value="vi-VN"/>
                                <h5>
                                    <fmt:formatNumber type = "currency" value = "${post.minPrice}" /> - <fmt:formatNumber type = "currency" value = "${post.maxPrice}" />
                                </h5>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 30px">
                            <div class="col-3">
                                <p><b><i>Số điện thoại:</i></b></p>
                            </div>
                            <div class="col-9">
                                <h5>${post.phone}</h5>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 30px">
                            <div class="col-3">
                                <p><b><i>Địa chỉ:</i></b></p>
                            </div>
                            <div class="col-9">
                                <h5>${post.address}</h5>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 30px">
                            <div class="col-3">
                                <p><b><i>Ngày đăng:</i></b></p>
                            </div>
                            <div class="col-9">
                                <fmt:parseDate pattern="yyyy-MM-dd" value="${post.pDate}" var="parsedDate" />
                                <h5><fmt:formatDate value="${parsedDate}" pattern="dd/MM/yyyy" /></h5>
                            </div>
                        </div>
                    </div>
                </div><br/>
            </div>
        </section>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    </body>

    <script>
                                            let slideIndex = 1;
                                            showSlides(slideIndex);

                                            function plusSlides(n) {
                                                showSlides(slideIndex += n);
                                            }

                                            function currentSlide(n) {
                                                showSlides(slideIndex = n);
                                            }

                                            function showSlides(n) {
                                                let i;
                                                let slides = document.getElementsByClassName("mySlides");
                                                let dots = document.getElementsByClassName("demo");
                                                if (n > slides.length) {
                                                    slideIndex = 1
                                                }
                                                if (n < 1) {
                                                    slideIndex = slides.length
                                                }
                                                for (i = 0; i < slides.length; i++) {
                                                    slides[i].style.display = "none";
                                                }
                                                for (i = 0; i < dots.length; i++) {
                                                    dots[i].className = dots[i].className.replace(" active", "");
                                                }
                                                slides[slideIndex - 1].style.display = "block";
                                                dots[slideIndex - 1].className += " active";
                                            }
    </script>
    <script>
        var w = 100 / ($(".mySlides").length);
        $(".column").css("width", w + "%");
    </script>
    <script>
        var text = '<%= loginedUser.getAccountID()%>';
        var text1 = '${post.ownerID}';
        if (text.localeCompare(text1) === 0) {
            document.getElementById("editButton").style.display = 'block';
        }

        var flag = '${action}';
        if (flag.localeCompare('insert') === 0) {
            swal({
                title: "Đăng bài thành công!",
                text: "Đang chờ duyệt",
                icon: "success",
                buttons: {
                    ok: "OK!"
                }
            });
        } else if (flag.localeCompare('update') === 0) {
            swal({
                title: "Cập nhật thành công!",
                text: "Đang chờ duyệt",
                icon: "success",
                buttons: {
                    ok: "OK!"
                }
            });
        } else if (flag.localeCompare('failtoupdate') === 0) {
            swal({
                title: "Có lỗi xảy ra!",
                text: "Vui lòng thực hiện lại thao tác",
                icon: "danger",
                buttons: {
                    ok: "OK!"
                }
            });
        }
    </script>

</html>
