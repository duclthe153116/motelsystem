<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Thêm người vào trọ</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/admin/manageRequest.css">
        <script language="javascript" src="../js/admin/manageRequest.js"></script>
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp">
            <jsp:param name="nav" value="hostManageBooking"></jsp:param>
        </jsp:include>
        <div class="container-lg">
            <!-- Body -->
            <div id="table-account" class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <b>Thêm hợp đồng và danh sách các phòng đã có hợp đồng</b></h2>
                            </div>
                            <div class="col-sm-6">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm phòng trọ">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <form action="choosemotel" method="get" id="f">
                            <c:set var="motelID" value="${requestScope.motelID}" />
                            <select name="chooseMotel" onchange="document.getElementById('f').submit();">
                                <option value="0">Chọn trọ</option>
                                <c:forEach items="${requestScope.listMotel}" var="motel">
                                    <option value="${motel.motelID}" <c:if test="${motel.motelID == motelID}">selected=""</c:if>>${motel.motelName}</option>
                                </c:forEach>
                            </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </form>
                        <form action="createbooking" method="post" >
                            
                            <select name="chooseRoom">
                                <option>Chọn phòng</option>
                                <c:forEach var="room" items="${requestScope.listRoomInMotel}">
                                    <option value="${room.roomID}">${room.roomName}</option>
                                    
                                </c:forEach>
                            </select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <br>
                            Ngày thuê trọ
                            <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Từ ngày
                            <input type="date" name="startAt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Đến ngày
                            <input type="date"name="endAt" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <br>
                            <input type="submit" value="Thêm booking"><br>
                            <c:set var="err" value="${requestScope.err}"/>
                            <p style="color: red">${err}</p>
                            <br>
                        </form>
                    </div>
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <td style="width:8% ;">Tên trọ</td>
                                <td style="width:5% ;">Tên phòng</td>
                                <td style="width:5% ;">Số người tối đa</td>
                                <td style="width:5% ;">Số người đang ở</td>
                                <td style="width:15% ;">Những người đang ở</td>
                                <td style="width:5% ;">Giá</td>
                                <td style="width:9% ;">Ngày bắt đầu thuê</td>
                                <td style="width:9% ;">Ngày kết thúc thuê</td>
                                <td style="width:5% ;">Hành động</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listRoomOfAccount}" var="listRoomOfAccount">
                                <tr>
                                    <td>${listRoomOfAccount.motelName}</td>
                                    <td>${listRoomOfAccount.roomName}</td>
                                    <td>${listRoomOfAccount.numberOfMax}</td>
                                    <td>${listRoomOfAccount.numberOfCurrent}</td>
                                    <td>
                                        <c:forEach var="fullName" items="${listRoomOfAccount.listAccountName}">
                                            ${fullName}<br>
                                        </c:forEach>
                                    </td>                                   
                                    <td>${listRoomOfAccount.price}</td>

                                    <td><fmt:formatDate value="${listRoomOfAccount.startAt}" pattern="dd/MM/yyyy" /></td>
                                    <td><fmt:formatDate value="${listRoomOfAccount.endAt}" pattern="dd/MM/yyyy" /></td>
                                    <td>
                                        <c:set value="${listRoomOfAccount.numberOfMax}" var="n"/>
                                        <c:if test="${listRoomOfAccount.numberOfCurrent != n}">
                                            <a href="addcustomerforroom?<c:forEach var="accID" items="${listRoomOfAccount.listAccountID}">accID=${accID}&</c:forEach>roomID=${listRoomOfAccount.roomID}" class="edit" title="Thêm người vào trọ" data-toggle="tooltip"><i 
                                                class="material-icons">edit</i></a>
                                        </c:if>
                                        
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>    

                    </table>

                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>