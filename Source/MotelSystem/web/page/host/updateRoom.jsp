<%@page import="model.Room"%>
<%@page import="model.Services"%>
<%@page import="java.util.ArrayList"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : createRoom
    Created on : May 27, 2022, 7:54:01 PM
    Author     : coder
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cập nhật phòng</title>
        <link rel="stylesheet" href="../css/host/updateRoom.css">
        <link href="../../css/host/updateRoom.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" src="../js/host/updateRoom.js"></script>
    </head>
    <body>
        <script>
            var listRoomJS = [];
            <%
                ArrayList<Room> listRoom = (ArrayList<Room>) request.getAttribute("listRoom");
                for (int i = 0; i < listRoom.size(); i++) {%>
            var roomJS = {roomName: '<%=listRoom.get(i).getRoomName()%>',roomID: '<%=listRoom.get(i).getRoomID()%>', motelID:<%=listRoom.get(i).getMotelID()%>};
            listRoomJS.push(roomJS);
            <%}%>
        </script>
        <form name="myForm" id="myForm" class="form" action="UpdateRoom" method="post" onsubmit="return validateForm(listRoomJS, ${room.roomID})">
            <input name="roomID" value="${room.roomID}" style="display: none">
            <h2>Cập Nhật Thông Tin Phòng</h2>
            <p type="Chọn Trọ:">
                <select name="motelID" id="motelID" required>
                    <c:forEach var="motel" items="${listMotel}">
                        <option value="${motel.motelID}" ${motel.motelID==room.motelID?" selected":""}>${motel.motelName}</option>
                    </c:forEach>
                </select>
            </p>
            <p type="Tên Phòng:">
                
                <input name="roomName" value="${room.roomName}" required>
            </p>
            <p type="Số Lượng Người Tối Đa:">
                <input name="numberMax" value="${room.numberOfPeopleMax}" required>
            </p>
            <p type="Số Lượng Người Hiện Tại:">
                <input name="numberCurrent" value="${room.numberOfPeopleCurrent}" readonly="">
            </p>
            <p type="Diện Tích:">
                <input name="area" value="${room.area}" required>
            </p>
            <p type="Giá Phòng:">
                <input name="price" value="${room.price}" required>
            </p>
            <p type="Ghi Chú:">
                <textarea name="note" value="${room.note}"></textarea>
            </p>
            <a href="http://localhost:8080/MotelSystem/host/ManageRoom" style="display: flex;float: left;">Hủy Cập Nhật</a>
            <button type="submit">Cập nhật</button>
        </form>
    </body>
</html>
