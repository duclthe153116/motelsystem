<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Thêm người vào trọ</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/admin/manageRequest.css">
        <script language="javascript" src="../js/admin/manageRequest.js"></script>
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp">
            <jsp:param name="nav" value="hostManageBooking"></jsp:param>
        </jsp:include>
        <div class="container-lg">
            <!-- Body -->
            <div id="table-account" class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <c:set value="${requestScope.room}" var="room"/>
                                <c:set value="${requestScope.nameMotel}" var="nameMotel"/>
                                <b>Thêm Thành Viên Mới Vào Phòng ${room.roomName} Của Trọ ${nameMotel}</b></h2>
                            </div>
                            <div class="col-sm-6">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm người dùng">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            
                            <form action="addcustomerforroom" method="post">
                                <div>
                                    Ngày thuê trọ
                            <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Từ ngày
                            <input type="date" name="startAt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Đến ngày
                            <input type="date"name="endAt" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <br>
                                </div>    
                                <table id="myTable"  class="table table-striped">
                        <thead>
                            <tr>
                                <td style="width:12% ;">Tên tài khoản</td>
                                <td style="width:20% ;">Họ và tên</td>
                                <td style="width:15% ;">Số điện thoại</td>
                                <td style="width:10% ;">Nơi ở</td>
                                <td style="width:20% ;">Email</td>
                                <td style="width:10% ;">Ngày sinh</td>
                                <td style="width:7% ;">Giới tính</td>
                                <td style="width:9% ;">Hành động</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listProfile}" var="listProfile">
                                <tr>
                                    <td>${listProfile.userName}</td>
                                    <td>${listProfile.fullName}</td>
                                    <td>${listProfile.phone}
                                    </td>                              

                                    <td>${listProfile.address}</td>
                                    <td>${listProfile.email}</td>

                                    <td><fmt:formatDate value="${listProfile.dob}" pattern="dd/MM/yyyy" /></td>
                                    <td>
                                        <c:if test="${listProfile.gender eq 'Male'}">Nam</c:if>
                                        <c:if test="${listProfile.gender eq 'FeMale'}">Nữ</c:if>
                                    </td>
                                    <td>
                                        <input type="checkbox" name="idAccount" value="${listProfile.accountID}"> 
                                    </td>
                            <input type="hidden" value="${room.roomID}" name="roomID" >
                            <c:forEach items="${requestScope.listAcc}" var="acc">
                                <input type="hidden" value="${acc}" name="accID">
                            </c:forEach>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                                <input type="submit" value="Thêm vào phòng">
                            </form>
                                <p style="color: red">${requestScope.err}</p>

                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>