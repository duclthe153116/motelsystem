

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sửa Hóa Đơn</title>
        <!-- boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link href="../css/host/editBillDetail.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>

        <div class="h-100 d-flex align-items-center justify-content-center">
            <div id="form-body">

                <div>
                    <h2 id="header-bill" class="text-center">Tạo Hóa Đơn</h2>
                </div>

                <div class="row" id="body-bill">
                    <form action="createBill" method="POST">

                        <!--Ten dich vu, so luong va gia tuong ung-->
                        <div class="row col-12 input" id="input-row2">

                            <!--Ten dich vu-->
                            <div class="col-6">
                                <h5>Tên Dịch Vụ: </h5>                               
                                <input type="text" value="${bdt.getServicesName()}" id="service" readonly="">
                            </div>

                            <!--So luong-->
                            <div class="col-6">
                                <h5>Số lượng</h5>
                                <input id="quantity" type="number" min="1" name="quantity" value="${bdt.getQuantity()}">
                            </div>
                        </div>

                        <p style="color: red" class="text-center">${error}</p>
                        <p style="color: green" class="text-center">${success}</p>

                        <div class="d-flex justify-content-around" id="button">
                            <a class="mouse-pointer" id="btn-cancle">Hủy</a>
                            <a class="mouse-pointer" id="btn-save">Lưu Thay Đổi</a>
                        </div>
                        <p class="text-center" style="color: red;margin-top: 57px;" id="message"></p>
                    </form>
                </div>

            </div>
        </div>

        <script>

            document.getElementById("btn-save").addEventListener("click", saveChange);
            function saveChange() {
                var quantity = document.getElementById("quantity").value;

                if (document.getElementById("quantity").value <= 0) {
                    document.getElementById("message").innerHTML = 'Số lượng phải lớn hơn 0'
                    location.href = "#";
                } else {
                    document.getElementById("message").innerHTML = ''

                    if (window.confirm("Bạn có chắc lưu thay đổi?")) {
                        location.href = "updateBillDetail?billDTId=" + ${bdt.getBillDetailID()} +
                                "&quantity=" + quantity;
                    } else {
                        location.href = "#";
                    }
                }
            }

            document.getElementById("btn-cancle").addEventListener("click", cancleBill);
            function cancleBill() {
                if (confirm("Bạn có chắc muốn hủy?")) {
                    location.href = "cancleEditBDT?billDTId=" + ${bdt.getBillDetailID()};
                } else {
                    location.href = "#";
                }
            }

        </script>        

    </body>
</html>