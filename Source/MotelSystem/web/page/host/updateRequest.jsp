<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cập nhật yêu cầu phòng</title>
        <link rel="stylesheet" href="../css/host/updateRoom.css">
        <link href="../../css/host/updateRoom.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" src="../js/host/updateRoom.js"></script>
    </head>
    <body>
        <form name="myForm" id="myForm" class="form" action="updaterequest" method="post">
            <h2>Cập Nhật Yêu Cầu</h2>
            <c:set value="${requestScope.requestOfRoomById}" var="request"/>
            <input type="hidden" value="${request.requestID}" name="idRequest">
            <p type="Tên Trọ:">
                
                <input name="roomName" value="${request.motelName}" readonly="">
            </p>
            <p type="Tên Phòng:">
                
                <input name="roomName" value="${request.roomName}" readonly="">
            </p>
            <p type="Nội dung yêu cầu">
                <input name="content" value="${request.content}">
            </p>
            <p type="Ghi chú từ trọ">
                <input name="notefromRoom" value="${request.noteRequestOfRoom}" readonly="">
            </p>
            <p type="Ghi chú từ chủ trọ">
                <input name="note" value="${request.noteRequestOfHost}">
            </p>
            <p type="Giá sửa">
                <input name="price" value="${request.priceRequest}">
            </p>
            <p type="Trạng thái">
                <select name="status">
                    <option value="do" <c:if test="${request.status eq 'do'}">selected=""</c:if>>Đang trong quá trình xử lý</option>
                    <option value="done" <c:if test="${request.status eq 'done'}">selected=""</c:if>>Đã hoàn thành</option>
                    <option value="reject" <c:if test="${request.status eq 'reject'}">selected=""</c:if>>Từ chối yêu cầu (Phải ghi rõ lý do từ chối)</option>
                </select>
            </p>
            <a href="../host/managerequest" style="display: flex;float: left;">Hủy Cập Nhật</a>
            <p style="color: red">${requestScope.err}</p>
            <input type="submit" value="Cập nhật">
        </form>
    </body>
</html>
