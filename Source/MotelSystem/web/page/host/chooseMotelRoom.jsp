

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tạo Hóa Đơn</title>
        <!-- boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link href="../css/host/chooseMotelRoom.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>

        <div class="h-100 d-flex align-items-center justify-content-center">
            <div id="form-body">

                <div>
                    <h2 id="header-bill" class="text-center">Chọn Phòng</h2>
                </div>

                <div class="row" id="body-bill">
                    <form action="createBill" method="POST">

                        <!-- ten tro va ten phong -->
                        <div class="row col-12 input">
                            <!--Ten tro-->
                            <div class="col-6">
                                <h5>Tên Trọ: </h5>
                                <select class="mouse-pointer" name="motel" id="motel">
                                    <c:forEach items="${motelList}" var="motel">
                                        <c:if test="${motelId == motel.getMotelID()}">
                                            <option value="${motel.getMotelID()}" selected="">${motel.getMotelName()}</option>
                                        </c:if>

                                        <c:if test="${motelId != motel.getMotelID()}">
                                            <option value="${motel.getMotelID()}">${motel.getMotelName()}</option>
                                        </c:if>
                                    </c:forEach>
                                </select><br>
                                <a class="detail-view mouse-pointer" id="view-room">Xem Phòng</a>
                            </div>

                            <!--Ten phong-->
                            <div class="col-6">
                                <h5>Tên Phòng: </h5>
                                <select class="mouse-pointer" name="room" id="room">
                                    <c:forEach items="${roomList}" var="room">
                                        <c:if test="${roomId == room.getRoomID()}">
                                            <option value="${room.getRoomID()}" selected="">${room.getRoomName()}</option>
                                        </c:if>

                                        <c:if test="${roomId != room.getRoomID()}">
                                            <option value="${room.getRoomID()}">${room.getRoomName()}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <!--<p style="color: red" class="text-center">ffhhhhhhhhhhhh</p>-->
                        <p style="color: red" class="text-center">${error}</p>
                        <p style="color: green" class="text-center">${success}</p>

                        <div class="d-flex justify-content-around" id="button">
                            <a href="manageBill" class="mouse-pointer" id="btn-cancle">Hủy</a>
                            <a class="mouse-pointer" id="btn-continue">Tiếp Tục</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>

        <script>
            
            if(document.getElementById("room").value === ''){
                document.getElementById("btn-continue").style.visibility = 'hidden';
            }else{
                document.getElementById("btn-continue").style.visibility = 'visible';
            }
            
            document.getElementById("view-room").addEventListener("click", viewRoom);
            function viewRoom() {
                var motelId = document.getElementById("motel").value;
                location.href = "chooseMotelRoom?motelId=" + motelId;
            }

            document.getElementById("btn-continue").addEventListener("click", myContinue);
            function myContinue() {
                var motelId = document.getElementById("motel").value;
                var roomId = document.getElementById("room").value;

                location.href = "createBill?motelId=" + motelId + "&roomId=" + roomId + 
                                "&isFirst=1";
            }

        </script>      

    </body>
</html>