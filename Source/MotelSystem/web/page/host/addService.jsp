
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tạo Hóa Đơn</title>
        <!-- boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link href="../css/host/addService.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>

        <div class="h-100 d-flex align-items-center justify-content-center">
            <div id="form-body">

                <div>
                    <h2 id="header-bill" class="text-center">Thêm Dịch Vụ</h2>
                </div>

                <div class="row" id="body-bill">
                    <form action="createBill" method="POST">

                        <!--Ten dich vu, so luong va gia tuong ung-->
                        <div class="row col-12 input" id="input-row2">

                            <!--Ten dich vu-->
                            <div class="col-6">
                                <h5>Chọn Dịch Vụ: </h5>
                                <select class="mouse-pointer" name="service" id="service">
                                    <c:forEach items="${serviceList}" var="service">
                                        <c:if test="${serviceId == service.getServicesID()}">
                                            <option value="${service.getServicesID()}" selected="">${service.getServicesName()}</option>
                                        </c:if>

                                        <c:if test="${serviceId != service.getServicesID()}">
                                            <option value="${service.getServicesID()}">${service.getServicesName()}</option>
                                        </c:if>
                                    </c:forEach>
                                </select><br>
                                <a class="detail-view mouse-pointer" id="view-price">Xem Giá</a>
                            </div>

                            <!--So luong-->
                            <div class="col-6">
                                <h5>Số lượng</h5>
                                <input id="quantity" type="number" min="1" name="quantity" value="${quantity == null ? 1 : quantity}">
                            </div>
                        </div>

                        <!--Tong tien hoa don-->
                        <div class="d-flex justify-content-center">
                            <div>
                                <h4 class="text-center">Tổng Tiền</h4>
                                <input id="total" type="text" value="${total}" readonly>
                            </div>
                        </div>

                        <p style="color: red" class="text-center">${error}</p>
                        <p style="color: green" class="text-center">${success}</p>

                        <div class="d-flex justify-content-around" id="button">
                            <a class="mouse-pointer" id="btn-cancle">Quay Lại</a>
                            <a class="mouse-pointer" id="btn-add-service">Thêm Dịch Vụ</a>
                        </div>
                        <p class="text-center" style="color: red;margin-top: 57px;" id="message"></p>
                    </form>
                </div>

            </div>
        </div>

        <script type="text/javascript">

            if (document.getElementById("total").value === '') {
                document.getElementById("btn-add-service").style.visibility = 'hidden';
            } else {
                document.getElementById("btn-add-service").style.visibility = 'visible';
            }

            document.getElementById("view-price").addEventListener("click", viewPrice);
            function viewPrice() {
                var serviceId = document.getElementById("service").value;
                var quantity = document.getElementById("quantity").value;

                if (document.getElementById("quantity").value <= 0) {
                    document.getElementById("message").innerHTML = 'Số lượng phải lớn hơn 0';
                    location.href = "#";
                } else {
                    document.getElementById("message").innerHTML = '';
                    location.href = "screenAddService?motelId=" + ${motelId} + "&roomId=" + ${roomId} +
                            "&billId=" + ${billId} +
                            "&serviceId=" + serviceId +
                            "&quantity=" + quantity;
                }
            }

            document.getElementById("btn-add-service").addEventListener("click", addService);
            function addService() {
                var serviceId = document.getElementById("service").value;
                var quantity = document.getElementById("quantity").value;
                var total = document.getElementById("total").value;

                if (document.getElementById("quantity").value <= 0) {
                    document.getElementById("message").innerHTML = 'Số lượng phải lớn hơn 0';
                    location.href = "#";
                } else {
                    document.getElementById("message").innerHTML = '';
                    location.href = "addService?motelId=" + ${motelId} +
                            "&roomId=" + ${roomId} + "&billId=" + ${billId} +
                            "&serviceId=" + serviceId +
                            "&quantity=" + quantity + "&total=" + total;
                }
            }

            document.getElementById("btn-cancle").addEventListener("click", cancleBill);
            function cancleBill() {
                location.href = "cancleAddService?billId=" + ${billId} + "&motelId=" + ${motelId} +
                        "&roomId=" + ${roomId};
            }

        </script>        

    </body>
</html>