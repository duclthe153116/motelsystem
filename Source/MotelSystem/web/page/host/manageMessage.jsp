<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
    <head>
        <title>Chat</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>
        <link href="../css/Message.css" rel="stylesheet" />
        <script language="javascript" src="../js/host/Message.js"></script>
        <link href="<%= request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
        
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp"/>
        <div class="container-fluid h-100">
            <div class="row justify-content-center h-100" style="margin-top:100px;">
                <div class="col-md-4 col-xl-3 chat"><div class="card mb-sm-3 mb-md-0 contacts_card">
<!--                        <div class="card-header">
                            <div class="input-group">
                                <input type="text" placeholder="Search..." name="" class="form-control search">
                                <div class="input-group-prepend">
                                    <span class="input-group-text search_btn"><i class="fas fa-search"></i></span>
                                </div>
                            </div>
                        </div>-->
                        <div class="card-body contacts_body">
                            <ui class="contacts">
                                <c:forEach items="${accountList}" var="account">
                                    <c:if test="${account.getAccountID() != idAccountTo}">
                                        <a href="../host/ManageMessage?Top=${account.getAccountID()}" style="text-decoration: none;">
                                            <li>
                                                <div class="d-flex bd-highlight">
                                                    <div class="img_cont">
                                                        <img src="data:image/jpg;base64,${account.imageProfile}" class="rounded-circle user_img">
                                                        <!--									<span class="online_icon offline"></span>-->
                                                    </div>
                                                    <div class="user_info">
                                                        <span>${account.getUsername()}</span>
                                                    </div>
                                                </div>
                                            </li>
                                        </a>
                                    </c:if>
                                    <c:if test="${account.getAccountID() == idAccountTo}">
                                        <a href="../host/ManageMessage?Top=${account.getAccountID()}" style="text-decoration: none;">
                                            <li class="active">
                                                <div class="d-flex bd-highlight">
                                                    <div class="img_cont">
                                                        <img src="data:image/jpg;base64,${account.imageProfile}" class="rounded-circle user_img">
                                                    </div>
                                                    <div class="user_info">
                                                        <span>${account.getUsername()}</span>
                                                    </div>
                                                </div>    
                                            </li>
                                        </a>
                                    </c:if>      
                                </c:forEach>
                            </ui>
                        </div>
                        <div class="card-footer"></div>
                    </div></div>
                <div class="col-md-8 col-xl-6 chat">
                    <div class="card">
                        <div class="card-header msg_head">                                                    
                            <div class="d-flex bd-highlight">
                                <div class="img_cont">
                                    <img src="data:image/jpg;base64,${accountList.get(0).imageProfile}" class="rounded-circle user_img">
                                    <span class="online_icon"></span>
                                </div>
                                <div class="user_info">
                                    <span>Chat with ${accountList.get(0).getUsername()}</span>
                                    <p>${messList.size()} Messages</p>
                                </div>
                            </div>  
                            
                            <div class="action_menu">
                                <ul>
                                    <li><i class="fas fa-user-circle"></i> View profile</li>
                                    <li><i class="fas fa-users"></i> Add to close friends</li>
                                    <li><i class="fas fa-plus"></i> Add to group</li>
                                    <li><i class="fas fa-ban"></i> Block</li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body msg_card_body">
                            <c:forEach items="${messList}" var="mess">
                                <c:if test="${mess.getTo() == accountAllInfor.accountID}">
                                    <div class="d-flex justify-content-start mb-4">
                                        <div class="img_cont_msg">
                                            <img src="data:image/jpg;base64,${accountList.get(0).imageProfile}" class="rounded-circle user_img_msg">
                                        </div>
                                        <div class="msg_cotainer">
                                            ${mess.getContent()}
                                            <fmt:parseDate pattern="yyyy-MM-dd" value="${mess.getCreateAt()}" var="parsedDate" />
                                            <span class="msg_time"><fmt:formatDate value="${parsedDate}" pattern="dd/MM/yyyy" /></span>
                                        </div>
                                    </div>
                                </c:if>
                                <c:if test="${mess.getTo() != accountAllInfor.accountID}">
                                    <div class="d-flex justify-content-end mb-4">
                                        <div class="msg_cotainer">
                                            ${mess.getContent()}
                                            <fmt:parseDate pattern="yyyy-MM-dd" value="${mess.getCreateAt()}" var="parsedDate" />
                                            <span class="msg_time"><fmt:formatDate value="${parsedDate}" pattern="dd/MM/yyyy" /></span>
                                        </div>
                                        <div class="img_cont_msg">
                                            <img src="data:image/jpeg;base64,${accountAllInfor.imageProfile}" class="rounded-circle user_img_msg">
                                        </div>
                                    </div>
                                </c:if>							

                            </c:forEach>
                        </div>
                        <div class="card-footer">
                            <div class="input-group">
                                <textarea name="" id="content_${idAccountTo}" class="form-control type_msg" placeholder=${error}></textarea>
                                <div class="input-group-append">
                                    <a id="create" onclick="SendMessage(this.id, 'content_${idAccountTo}')"
                                       href="#" class="input-group-text send_btn">
                                        <span ><i class="fas fa-location-arrow"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../footer.html"/>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
