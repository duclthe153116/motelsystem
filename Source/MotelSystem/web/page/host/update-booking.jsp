<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- 
    Document   : createRoom
    Created on : May 27, 2022, 7:54:01 PM
    Author     : coder
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <title>Cập nhật Thông Tin Booking</title>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <!-- Favicon-->
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<%= request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
        <style>
            i{
                color: #8080CC;
            }
            .img-wrap {
                position: relative;
            }
            .img-wrap .close {
                position: absolute;
                right: 2px;
                z-index: 100;
                padding: 5px 2px 2px;
                color: red;
                opacity: .5;
                line-height: 15px;
                font-weight: bold;
                cursor: pointer;
                text-align: center;
                font-size: 40px;
                height: fit-content;
                top:10px;
            }


            .close:hover{
                opacity: 1;
            }
        </style>
    </head>
    <body>

        <jsp:include page="nav-home-host.jsp">
            <jsp:param name="nav" value="hostManageBooking"></jsp:param>
        </jsp:include>

        <!-- Header-->
        <!-- Section-->
        <form action="UpdateBooking" method="post">
            <input type="hidden" id="listAccountDelete" name="listAccountDelete" value="nothing">
            <input type="hidden" id="listServiceDelete" name="listServiceDelete" value="nothing">
            <input type="hidden" name="bookingID" value="${param.bookingID}">
            <input type="hidden" name="start" value="${bookingDetails.startAt}">
            <input type="hidden" name="end" value="${bookingDetails.endAt}">
            <section style="padding-bottom: 5em;">
                <div class="container mt-5" style="border: 2px solid #8080CC;padding-top: 20px; border-radius: 10px;box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;">
                    <div class="row px-4" style="padding-bottom: 20px;padding-top: 20px">
                        <div class="col px-4" style="border-right: 2px solid #8080CC">
                            <div style="width: 100%;height: 80%;position: relative;padding-right: 0px;">
                                <c:forEach var="profile" items="${bookingDetails.listProfile}" varStatus="theCount">
                                    <div class="img-wrap" id="account${profile.accountID}" style="border: 1px solid #8080CC;padding: 10px;border-radius: 5px; margin-bottom: 25px">
                                        <p><b><i>Người Thuê: ${theCount.count}</i></b></p>
                                        <span class="close" onclick="return deleteOneUser(${profile.accountID}, '${profile.fullName}');">&times;</span>

                                        <div class="row">
                                            <div class="col-4">
                                                <img src="data:image/jpg;base64,${profile.imageProfile}" style="width:100%;height: auto;"><br>
                                            </div>
                                            <div class="col-8">
                                                <b><i>Họ Tên:</i> ${profile.fullName}</b> <b></b> <br>
                                                <b><i>Số Điện Thoại:</i> ${profile.phone}</b> <b></b> <br>
                                                <b><i>Địa Chỉ:</i> ${profile.address}</b> <b></b> <br>
                                                <fmt:parseDate pattern="yyyy-MM-dd" value="${profile.dOB}" var="parsedDOB" />
                                                <b><i>Ngày Sinh:</i> <fmt:formatDate value="${parsedDOB}" pattern="dd/MM/yyyy" /></b> <b></b> <br>
                                                <b><i>Giới Tính:</i> ${profile.gender}</b> <b></b> <br>
                                            </div>
                                        </div>
                                    </div>
                                </c:forEach>
                                <div style="border: 1px solid #8080CC;padding: 10px;border-radius: 5px;background-color: #8080CC">
                                    <select name="slUser" id="slUser" multiple="" style="width: 100%;">
                                        <c:forEach var="cus" items="${listCus}">
                                            <option value="${cus.accountID}">${cus.fullName}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col" style="padding-left:50px">
                            <div class="row" style="margin-top: 30px">
                                <div class="col-3">
                                    <p><b><i>Tên Trọ:</i></b></p>
                                </div>
                                <div class="col-9">
                                    <h4>${bookingDetails.motelName}</h4>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-3">
                                    <p><b><i>Tên Phòng:</i></b></p>
                                </div>
                                <div class="col-9">
                                    <h5>${bookingDetails.roomName}</h5>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-3">
                                    <p><b><i>Ngày Bắt Đầu</i></b></p>
                                </div>
                                <div class="col-9">
                                    <fmt:parseDate pattern="yyyy-MM-dd" value="${bookingDetails.startAt}" var="parsedDateS" />
                                    <h5><fmt:formatDate value="${parsedDateS}" pattern="dd/MM/yyyy" /></h5>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-3">
                                    <p><b><i>Ngày Kết Thúc</i></b></p>
                                </div>
                                <div class="col-9">
                                    <fmt:parseDate pattern="yyyy-MM-dd" value="${bookingDetails.endAt}" var="parsedDateE" />
                                    <h5><fmt:formatDate value="${parsedDateE}" pattern="dd/MM/yyyy" /></h5>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-3">
                                    <p><b><i>Giá:</i></b></p>
                                </div>
                                <div class="col-9">
                                    <fmt:setLocale value="vi-VN"/>

                                    <h5>
                                        <fmt:formatNumber type = "currency" value = "${bookingDetails.price}" />
                                    </h5>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-12">
                                    <p><b><i>Các Dịch Vụ Sử Dụng</i></b></p>
                                </div>
                                <div class="col-9">
                                    <table border="1">
                                        <tbody >
                                            <tr style="border-width: 2px; width: 200px;">
                                                <td style="border-width: 2px; width: 200px;">Tên Dịch Vụ</td>
                                                <td style="border-width: 2px; width: 200px;">Giá</td>
                                            </tr>
                                            <c:forEach var="services" items="${bookingDetails.listServices}">
                                                <tr id="sv${services.servicesID}" style="border-width: 2px; width: 200px;">
                                                    <td style="border-width: 2px; width: 200px;">${services.servicesName}</td>
                                                    <td style="border-width: 2px; width: 200px;">${services.price}</td>
                                                    <td style="border-width: 2px; width: 50px;"><a href="#" onclick="return deleteOneService(${services.servicesID}, '${services.servicesName}', '${services.price}');" class="delete" title="Delete" data-toggle="tooltip"><i 
                                                                class="material-icons">delete</i></a></td>
                                                </tr>
                                            </c:forEach>
                                            <tr>
                                                <td colspan="3" style="border-width: 2px; width: 200px;">
                                                    <select name="slService" id="slService" multiple="" style="width: 100%;">
                                                        <c:forEach var="sv" items="${listService}">
                                                            <option value="${sv.servicesID}">${sv.servicesName} - ${sv.price}</option>
                                                        </c:forEach>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>


                                </div>
                            </div>
                        </div><input type="submit" class="btn btn-dark" style="height: 70px;margin-top: 30px;" value="Lưu chỉnh sửa"/>
                    </div><br/>                                

                </div>
            </section>
        </form>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    </body>
    <script>
                                                        var max = ${max};
                                                        $('#slUser').select2({
                                                            maximumSelectionLength: ${max},
                                                            language: {
                                                                maximumSelected: function (e) {
                                                                    return "Phòng chỉ còn trống ${max} chỗ. Không thể thêm nữa!";
                                                                }
                                                            },
                                                            placeholder: 'Thêm Người thuê',
                                                            allowClear: true
                                                        });
                                                        $('#slService').select2({
                                                            placeholder: 'Thêm Dịch vụ',
                                                            allowClear: true});

                                                        function deleteOneUser(accountid, fullname) {
                                                            max = max + 1;
                                                            $('#slUser').select2({
                                                                maximumSelectionLength: max,
                                                                language: {
                                                                    maximumSelected: function (e) {
                                                                        return "Phòng chỉ còn trống " + max + " chỗ. Không thể thêm nữa!";
                                                                    }
                                                                },
                                                                placeholder: 'Thêm Người thuê',
                                                                allowClear: true
                                                            });
                                                            $('#account' + accountid).hide();

                                                            let opt = $('<option value="' + accountid + '">' + fullname + '</option>');
                                                            $('#slUser').append(opt);
                                                            let text = $('#listAccountDelete').val();
                                                            if (text.localeCompare('nothing') !== 0) {
                                                                $('#listAccountDelete').val($('#listAccountDelete').val() + accountid + '-');
                                                                $('#account' + accountid).hide();
                                                            } else {
                                                                $('#listAccountDelete').val(accountid + '-');
                                                                $('#account' + accountid).hide();
                                                            }
                                                        }
                                                        function deleteOneService(svID, svName, svPrice) {
                                                            $('#sv' + svID).hide();
                                                            let opt = $('<option value="' + svID + '">' + svName + ' - ' + svPrice + '</option>');
                                                            $('#slService').append(opt);
                                                            let text = $('#listServiceDelete').val();
                                                            if (text.localeCompare('nothing') !== 0) {
                                                                $('#listServiceDelete').val($('#listServiceDelete').val() + svID + '-');
                                                                $('#sv' + svID).hide();
                                                            } else {
                                                                $('#listServiceDelete').val(svID + '-');
                                                                $('#sv' + svID).hide();
                                                            }
                                                        }
    </script>

</html>
