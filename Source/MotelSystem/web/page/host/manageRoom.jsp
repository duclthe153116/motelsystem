<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý phòng</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/host/manageRoom.css">
        <script language="javascript" src="../js/host/manageRoom.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
        <script>
            const deleteRoomFunction = (id) => {
                if (confirm("Bấm ok để xóa dịch vụ đã chọn, cancel để hủy") == true) {
                    window.location.href = "DeleteRoom?id=" + id;
                }
            }
            var statusDelete = '<%=request.getAttribute("statusDelete")%>';
            var statusCreate = '<%=request.getAttribute("statusCreate")%>';
            var statusUpdate = '<%=request.getAttribute("statusUpdate")%>';
            if (statusDelete != "null") {
                function alertStatusAcction() {
                    if (statusDelete == "true") {
                        alert("Xóa phòng thành công!");
                    } else {
                        alert("Xóa phòng thất bại, do đang có người book!");
                    }
                }
            }
            if (statusCreate != "null") {
                function alertStatusAcction() {
                    if (statusCreate == "true") {
                        alert("Thêm phòng thành công!");
                    } else {
                        alert("Thêm phòng vụ thất bại!");
                    }
                }
            }
            if (statusUpdate != "null") {
                function alertStatusAcction() {
                    if (statusUpdate == "true") {
                        alert("Cập nhật phòng thành công!");
                    } else {
                        alert("Cập nhật phòng vụ thất bại!");
                    }
                }
            }
        </script>
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp">
            <jsp:param name="nav" value="hostManageRoom"></jsp:param>
        </jsp:include>
        <!-- Body -->
        <div class="container-lg">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Quản lý phòng</b></h2>
                            </div>
                            <div class="col-sm-4" style="text-align: center; ">
                                <button style="box-shadow: 3px 2px 10px 4px" onclick="location.href = 'http://localhost:8080/MotelSystem/host/CreateRoom'" type="button" class="open-button">Thêm Phòng Mới</button>
                            </div>
                            <div class="col-sm-4">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm phòng">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="myTable"  class="table table-striped">
                        <thead>
                            <tr style="text-align: center">
                                <td style="width:10% ;">Tên Trọ</td>
                                <td style="width:6% ;">Tên Phòng</td>
                                <td style="width:11% ;">Số Người Hiện Tại</td>
                                <td style="width:10% ;">Số Người Tối Đa</td>
                                <td style="width:6% ;">Diện Tích</td>
                                <td style="width:7% ;">Giá</td>
                                <td style="width:auto ;">Ghi Chú</td>
                                <td style="width:7% ;">Hành động</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="room" items="${listRoom}">
                                <tr>
                                    <c:forEach var="motel" items="${listMotel}">
                                        <c:if test="${motel.motelID == room.motelID}">
                                            <td>${motel.motelName}</td>
                                        </c:if>
                                    </c:forEach>
                                    <td>${room.roomName}</td>
                                    <td>${room.numberOfPeopleCurrent}</td>
                                    <td>${room.numberOfPeopleMax}</td>
                                    <td>${room.area}</td>
                                    <td>${room.price}</td>
                                    <td>${room.note}</td>
                                    <td>
                                        <a href="UpdateRoom?id=${room.roomID}" class="edit" title="Sửa/Xem Chi Tiết" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                                        <a href="#" class="delete" title="Xóa" data-toggle="tooltip"  onclick="deleteRoomFunction(${room.roomID})"><i class="material-icons">&#xE872;</i></a>
                                    </td>
                                </tr>
                            </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>  
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript"> window.onload = alertStatusAcction;</script>
    </body>
</html>
