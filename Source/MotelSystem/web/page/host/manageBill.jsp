
<%@page import="model.BillByHost"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="model.BillDetail"%>
<%@page import="model.BillDetail"%>
<%@page import="model.BillByHost"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý hóa đơn</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/host/manageBill.css">
        <!--<script language="javascript" src="../js/host/manageBill.js"></script>-->
    </head>

    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp">
            <jsp:param name="nav" value="hostManageBill"></jsp:param>
        </jsp:include>
        <div class="container-lg">
            <!-- Body -->
            <div id="table-account" class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Quản lý hóa đơn</b></h2>
                            </div>

                            <div class="col-sm-4">
                                <h2 class="add-bill"><a href="createBill">Thêm Hóa Đơn</a></h2>
                            </div>

                            <div class="col-sm-4">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm phòng">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <td style="width:20% ;">Tên Trọ</td>
                                <td style="width:10% ;">Tên Phòng</td>
                                <td style="width:10% ;">Tổng chi phí (VND)</td>
                                <td style="width:15%;">Ngày Tạo</td>
                                <td style="width:15%;">Ngày Thanh Toán</td>
                                <td style="width:15%;">Trạng Thái Gửi Đi</td>
                                <td style="width:15%;">Hành Động</td>
                            </tr>
                        </thead>
                        <tbody>

                            <%
                                Object o = request.getAttribute("listBills");
//                                Map<BillByHost, ArrayList<BillDetail>> billBillDetails = null;
                                ArrayList<BillByHost> listBills = null;
                                if (o == null) {
                                    listBills = new ArrayList<BillByHost>();
                                } else {
                                    listBills = (ArrayList<BillByHost>) o;
                                }
//                                for (Map.Entry<BillByHost, ArrayList<BillDetail>> en : listBills.entrySet()) {
//                                    BillByHost bill = en.getKey();
//                                    ArrayList<BillDetail> billDT = en.getValue();
                                for (int idx = 0; idx < listBills.size(); idx++) {
                                    BillByHost bill = listBills.get(idx);
                            %>
                            <c:set var="billId" value="<%= bill.getBillID()%>"></c:set>
                        <form action="viewBillDetail?id=${billId}" method="GET">
                            <tr class="row-hover">
                                <c:set var="motelId" value="<%= bill.getMotelID()%>"></c:set>
                                <c:set var="roomId" value="<%= bill.getRoomID()%>"></c:set>
                                <c:set var="motelName" value="<%= bill.getMotelName()%>"></c:set>
                                <c:set var="roomName" value="<%= bill.getRoomName()%>"></c:set>
                                <c:set var="totalPriceBll" value="<%= bill.getTotalPriceBill()%>"></c:set>
                                <c:set var="dateCreate" value="<%= bill.getDateCreate()%>"></c:set>
                                <c:set var="datePayment" value="<%= bill.getDateOfPayment()%>"></c:set>
                                <c:set var="isSent" value="<%= bill.isIsSent()%>"></c:set>
                                <c:set var="billId" value="<%= bill.getBillID()%>"></c:set>

                                    <td><c:out value="${motelName}"></c:out></td>
                                <td><c:out value="${roomName}"></c:out></td>
                                    <td>
                                    <fmt:formatNumber type = "number" value = "${totalPriceBll}" />
                                    <%--<c:out value="${totalPriceBll}"></c:out>--%>
                                    </td>

                                    <td>
                                    <fmt:formatDate pattern = "dd/MM/yyyy" value = "${dateCreate}" />
                                    <%--<c:out value="${dateCreate}"></c:out>--%>
                                </td>
                                </td>

                                <td>
                                    <c:choose>
                                        <c:when test="${datePayment == null}">
                                            <c:out value="Chưa Thanh Toán"></c:out>
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:formatDate pattern = "dd/MM/yyyy" value = "${datePayment}" />
                                            <%--<c:out value="${datePayment}"></c:out>--%>
                                        </c:otherwise>
                                    </c:choose>
                                </td>

                                <td>

                                    <c:choose>
                                        <c:when test="${isSent == true}">
                                            <c:out value="Đã gửi"></c:out>
                                        </c:when>    
                                        <c:otherwise>
                                            <c:out value="Chưa gửi"></c:out>
                                        </c:otherwise>
                                    </c:choose>

                                </td>

                                <td>
                                    <a href="viewBillDetail?billId=${billId}&motelId=${motelId}&roomId=${roomId}" class="view" title="View detail" data-toggle="tooltip"><i 
                                            class="material-icons">visibility</i></a>
                                        <c:choose>
                                            <c:when test="${confirmByHost == 'paid' && confirmByCustomer == 'paid'}">
                                            </c:when>    
                                            <c:otherwise>
                                                <c:if test="${isSent == false}">

                                                <a onclick="if (!confirm('Bạn chắc chắn muốn xóa hóa đơn này')) {
                                                            return false;
                                                        }"
                                                   href="deleteBill?billId=${billId}" class="delete" title="Delete" data-toggle="tooltip"><i
                                                        class="material-icons">&#xE872;</i></a>

                                                <a onclick="if (!confirm('Bạn chắc chắn muốn gửi hóa đơn này cho khách hàng?')) {
                                                            return false;
                                                        }"
                                                   href="sendBill?billId=${billId}" id="sendBill"  class="save" title="Send To Customer" data-toggle="tooltip"><i class="material-icons">
                                                        send</i></a> 
                                                    </c:if>
                                                </c:otherwise>
                                            </c:choose>
                                </td>
                            </tr>
                        </form>
                        <%}%>
                        </tbody>    

                    </table>
                    <p style="color: green" class="text-center">${message}</p>
                </div>
            </div>
        </div>

        <script>


            $(document).ready(function () {
                // Active tooltips
                $('[data-toggle="tooltip"]').tooltip();

                // Filter table rows based on search
                $("#search").on("keyup", function () {
                    var term = $(this).val().toLowerCase();
                    $("table tbody tr").each(function () {
                        $row = $(this);
                        var keyword = $row.find("td").text().toLowerCase();
                        console.log(keyword);
                        if (keyword.search(term) < 0) {
                            $row.hide();
                        } else {
                            $row.show();
                        }
                    });
                });
            });

            $(document).ready(function () {
                $('#myTable').dataTable({
                    "bPaginate": true,
                    "bFilter": false,
                    "bInfo": false
                });
            });

            var table = $("#datatable").DataTable({
                "paging": true,
                "ordering": false,
                "searching": false
            });

            function UpdateRoleAccount(id, id_select) {
                var conf = confirm("Bạn có muốn thay đổi vai trò?");
                if (!conf) {
                    return false;
                }
                var id_account = id.split("_")[1];
                var cboxes = document.getElementsByName(id_select);
                var isAdmin = (cboxes[0].checked ? 'true' : 'false');
                var isHost = (cboxes[1].checked ? 'true' : 'false');
                var isCustomer = (cboxes[2].checked ? 'true' : 'false');

                // In ra kết quả
                document.getElementById(id).href = "../admin/UpdateRole?id=" + id_account + "&isAdmin=" + isAdmin + "&isHost=" + isHost + "&isCustomer=" + isCustomer;
            }
            ;
        </script>

        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>

</html>
