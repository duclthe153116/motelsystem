
<%@page import="java.util.HashMap"%>
<%@page import="model.Services"%>
<%@page import="model.BillService"%>
<%@page import="model.Motel"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tạo Hóa Đơn</title>
        <!-- boostrap -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link href="../css/host/createBill.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>

        <div class="h-100 d-flex align-items-center justify-content-center">
            <div id="form-body">

                <div>
                    <h2 id="header-bill" class="text-center">Tạo Hóa Đơn</h2>
                </div>

                <div class="row" id="body-bill">
                    <!--Ten tro va phong-->
                    <div class="row col-12 input" id="input-row1">

                        <!--Ten tro-->
                        <div class="col-6">
                            <h5>Tên Trọ: </h5>
                            <select class="mouse-pointer" name="motel" id="motel">
                                <c:forEach items="${motelList}" var="mt">
                                    <c:choose>
                                        <c:when test="${mt.getMotelID() == motelId}">
                                            <option selected="" value="${mt.getMotelID()}">${mt.getMotelName()}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${mt.getMotelID()}">${mt.getMotelName()}</option>
                                        </c:otherwise>
                                    </c:choose>

                                </c:forEach>
                            </select><br>
                            <!--<a class="detail-view mouse-pointer" id="view-price">Xem Giá</a>-->
                        </div>


                        <!--Ten phong-->
                        <div class="col-6">
                            <h5>Tên Phòng</h5>
                            <!--<input id="quantity" type="number" min="1" name="quantity" value="${quantity == null ? 1 : quantity}">-->
                            <select class="mouse-pointer" name="room" id="room">
                                <c:forEach items="${roomList}" var="r">

                                    <c:choose>
                                        <c:when test="${r.getRoomID() == roomId}">
                                            <option selected="" value="${r.getRoomID()}">${r.getRoomName()}</option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${r.getRoomID()}">${r.getRoomName()}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </select><br>
                        </div>
                    </div>

                    <!--Dich vu o SOB-->
                    <div class="row col-12 input" id="input-row2">
                        <h4 class="text-center">Dịch vụ hàng tháng</h4>
                        <table>
                            <tr>
                                <th style="width: 20%"></th>
                                <th style="width: 20%">Tên Dịch Vụ</th>
                                <th style="width: 20%"></th>
                                <th style="width: 20%">Giá</th>
                                <th style="width: 20%"></th>

                            </tr>
                            <%
                                Object o1 = request.getAttribute("sobServiceList");
                                ArrayList<BillService> sobServiceList = new ArrayList<BillService>();

                                if (o1 != null) {
                                    sobServiceList = (ArrayList<BillService>) o1;
                                }

                                for (int i = 0; i < sobServiceList.size(); i++) {
                            %>
                            <tr>
                                <td><%= i + 1%></td>
                                <td><%= sobServiceList.get(i).getServiceName()%></td>
                                <td></td>

                                <td>
                                    <p id="<%= i%>sob" class="totalPrice vnd" ><%= sobServiceList.get(i).getPrice()%></p> (VND)
                                </td>
                                <td></td>
                            </tr>
                            <%}%>
                        </table>
                    </div>

                    <!--Dich vu o request-->
                    <div class="row col-12 input" id="input-row2">
                        <h4 class="text-center">Phụ Phí</h4>
                        <table>
                            <tr>
                                <th></th>
                                <th>Tên Dịch Vụ</th>
                                <th>Số Lượng</th>
                                <th>Giá</th>
                                <th>Tổng Giá</th>
                            </tr>

                            <%
                                Object o2 = request.getAttribute("serviceList");
                                ArrayList<Services> serviceList = new ArrayList<Services>();

                                if (o2 != null) {
                                    serviceList = (ArrayList<Services>) o2;
                                    double p = -1;
                                    for (int i = 0; i < serviceList.size(); i++) {
                                        if ("Điện".equalsIgnoreCase(serviceList.get(i).getServicesName())) {
                                            p = serviceList.get(i).getPrice();
                                        }
                                    }
                            %>
                            <tr>
                                <td>1</td>
                                <td>Điện</td>
                                <td><input class="input_af" type="number" min="0" id="quantityElec" 
                                           onchange="validateQuantity();updateTotalAll();" required=""></td>
                                <td> <p class="vnd" id="priceElec"><%= p%></p> (VND) </td>
                                <td ><p class="totalPrice vnd" id="totalElec"></p> (VND)</td>
                            </tr>
                            <%}%>

                            <%
                                Object o3 = request.getAttribute("rqServiceList");
                                ArrayList<BillService> rqServiceList = new ArrayList<BillService>();

                                if (o3 != null) {
                                    rqServiceList = (ArrayList<BillService>) o3;
                                }

                                for (int i = 0; i < rqServiceList.size(); i++) {
                            %>
                            <tr>
                                <td><%= i + 2%></td>
                                <td><%= rqServiceList.get(i).getServiceName()%></td>
                                <td> <p id="<%= rqServiceList.get(i).getRequestId()%>q" ><%= rqServiceList.get(i).getQuantity()%></p> </td>
                                <td>
                                    <input class="input_af" id="<%= rqServiceList.get(i).getRequestId()%>p" name="priceRq" type="number" min="0" 
                                           onchange="validatePrice('<%=rqServiceList.get(i).getRequestId()%>');showTotalRq('<%=rqServiceList.get(i).getRequestId()%>');updateTotalAll()" required=""> (VND)

                                </td>
                                <td>
                                    <p id="<%= rqServiceList.get(i).getRequestId()%>t" class="totalPrice totalRq vnd"></p> (VND)
                                </td>
                            </tr>
                            <%}%>
                        </table>
                    </div>

                    <!--Tong tien hoa don-->
                    <div class="d-flex justify-content-center">
                        <div>
                            <h4 class="text-center">Tổng Tiền</h4>
                            <p class="text-center vnd" id="totalAll">0</p> (VND)
                        </div>

                    </div>

                    <p style="color: red" class="text-center">${error}</p>
                    <p style="color: green" class="text-center">${success}</p>
                    <p class="text-center" style="color: red;margin-top: 57px;" id="message"></p>

                    <div class="d-flex justify-content-around" id="button">
                        <a onclick="if (!window.confirm('Bạn có chắc muốn hủy tạo hóa đơn?')) {
                                    return false
                                }"
                           href="manageBill"
                           class="mouse-pointer" id="btn-cancle">Hủy</a>
                        <c:if test="${displayBtn == 'on'}">
                            <a class="mouse-pointer" id="btn-add-bill" onclick="clickTo()">Tạo Hóa Đơn</a>
                        </c:if>
                    </div>
                </div>

            </div>
        </div>

        <script>


            document.getElementById("motel").addEventListener('change', showRoom);
            function showRoom() {
                var motelId = document.getElementById("motel").value;
                location.href = "createBill?motelId=" + motelId;
            }


            document.getElementById("room").addEventListener('change', showServices);
            function showServices() {
                var motelId = document.getElementById("motel").value;
                var roomId = document.getElementById("room").value;
                location.href = "createBill?motelId=" + motelId + "&roomId=" + roomId;
            }


            //            document.getElementById("quantityElec").addEventListener('change', validateQuantity);
            function validateQuantity() {
                var quantity = document.getElementById("quantityElec").value;
                if (quantity < 0) {
                    document.getElementById("message").innerHTML = "Số điện không được nhỏ hơn 0!!"
                } else {

                    document.getElementById("message").innerHTML = ""
                    var price = document.getElementById("priceElec").innerHTML;
                    var total = price * quantity;
                    document.getElementById("totalElec").innerHTML = total;
                }
            }


            function validatePrice(requestId) {
                var indexp = requestId.toString() + 'p';
                var price = document.getElementById(indexp).value;
                if (price < 0) {
                    document.getElementById("message").innerHTML = "Giá không được nhỏ hơn 0!"
                } else {

                    document.getElementById("message").innerHTML = ""
                }
            }


            function showTotalRq(requestId) {
                var indexp = requestId.toString() + 'p';
                var indexq = requestId.toString() + 'q';
                var indext = requestId.toString() + 't';
                var priceRq = document.getElementById(indexp).value;
                var quantityRq = document.getElementById(indexq).innerHTML;
                var total = priceRq * quantityRq;
                document.getElementById(indext).innerHTML = total;
            }


            function updateTotalAll() {
                var tt = 0;
            <%
                for (int i = 0; i < sobServiceList.size(); i++) {
            %>
                if (document.getElementById("<%= i%>sob").innerHTML != '') {
                    tt += parseInt(document.getElementById("<%= i%>sob").innerHTML);
                }

            <%}%>

            <%
                for (int i = 0; i < rqServiceList.size(); i++) {
            %>
                if (document.getElementById("<%= rqServiceList.get(i).getRequestId()%>t").innerHTML != '') {
                    tt += parseInt(document.getElementById("<%= rqServiceList.get(i).getRequestId()%>t").innerHTML);
                }
            <%}%>
                if (document.getElementById("totalElec").innerHTML != '') {
                    tt += parseInt(document.getElementById("totalElec").innerHTML);
                }
                document.getElementById("totalAll").innerHTML = tt;
            }


            function clickTo() {

                if (window.confirm("Bạn chắc chắn muốn tạo hóa đơn này?")) {
                    var elecQ = document.getElementById("quantityElec").value;
                    var priceRqService = [];
                    var urlPriceRqService = "";
            <%
                for (int i = 0; i < rqServiceList.size(); i++) {
            %>
                    urlPriceRqService += "&<%= rqServiceList.get(i).getRequestId()%>q=" +
                            document.getElementById("<%= rqServiceList.get(i).getRequestId()%>q").innerHTML +
                            "&<%= rqServiceList.get(i).getRequestId()%>p=" +
                            document.getElementById("<%= rqServiceList.get(i).getRequestId()%>p").value +
                            "&<%= rqServiceList.get(i).getRequestId()%>t=" +
                            document.getElementById("<%= rqServiceList.get(i).getRequestId()%>t").innerHTML;

            <% }%>
                    var totalBill = document.getElementById("totalAll").innerHTML;
                    var totalElec = document.getElementById("totalElec").innerHTML;
                    var priceElec = parseInt(document.getElementById("priceElec").innerHTML).toString();

                    location.href = "addBill?motelId=${motelId}&roomId=${roomId}" + "&totalBill=" +
                            totalBill + "&elecQ=" + elecQ + "&elecT=" +
                            totalElec + "&elecP=" + priceElec + urlPriceRqService;
                } else {
                    location.href = "#";
                }

            }

        </script>

    </body>
</html>