<%-- 
    Document   : nav-home-normal
    Created on : May 14, 2022, 4:29:33 AM
    Author     : royal
--%>
<%@page import="model.AccountAllInfor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    AccountAllInfor loginedUser = (AccountAllInfor) session.getAttribute("accountAllInfor");
%>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <div class="container px-4 px-lg-5">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 bg-light">
                <li class="nav-item bg-dark"><div class="divSmt <c:if test="${param.nav == 'home'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'home'}">active</c:if>" aria-current="page" href="<%= request.getContextPath()%>/host/home">Trang Chủ</a></div></li>
                <li class="nav-item bg-dark"><div class="divSmt <c:if test="${param.nav == 'hostManageRoom'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'hostManageRoom'}">active</c:if>" href="<%= request.getContextPath()%>/host/ManageRoom">Phòng</a></div></li>
                <li class="nav-item bg-dark"><div class="divSmt <c:if test="${param.nav == 'hostManageServices'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'hostManageServices'}">active</c:if>" href="<%= request.getContextPath()%>/host/ManageSevices">Dịch Vụ</a></div></li>
                <li class="nav-item bg-dark"><div class="divSmt <c:if test="${param.nav == 'hostManageBill'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'hostManageBill'}">active</c:if>" href="<%= request.getContextPath()%>/host/manageBill">Hóa Đơn</a></div></li>
                <li class="nav-item bg-dark"><div class="divSmt <c:if test="${param.nav == 'hostManagePost'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'hostManagePost'}">active</c:if>" href="<%= request.getContextPath()%>/host/PostManagement">Bài Đăng</a></div></li>
                <li class="nav-item bg-dark"><div class="divSmt <c:if test="${param.nav == 'hostManageRequest'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'hostManageRequest'}">active</c:if>" href="<%= request.getContextPath()%>/host/managerequest">Yêu Cầu</a></div></li>
                <li class="nav-item bg-dark"><div class="divSmt <c:if test="${param.nav == 'hostManageMessage'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'hostManageMessage'}">active</c:if>" href="<%= request.getContextPath()%>/host/ManageMessage?Top=0">Hòm thư</a></div></li>
                <li class="nav-item bg-dark"><div class="divSmt <c:if test="${param.nav == 'hostManageMotel'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'hostManageMotel'}">active</c:if>" href="<%= request.getContextPath()%>/host/ManageMotel">Trọ</a></div></li>
                <li class="nav-item bg-dark"><div class="divSmt <c:if test="${param.nav == 'hostManageBooking'}">w3-animate-zoom bg-light</c:if>" style="border-radius: 5px"><a class="nav-link <c:if test="${param.nav == 'hostManageBooking'}">active</c:if>" href="<%= request.getContextPath()%>/host/ManageBooking">Booking</a></div></li>
            </ul>

            <form class="d-flex">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                    <li class="dropdown">
                        <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <img src="data:image/jpg;base64,<%= loginedUser.getImageProfile()%>" width="50px" height="50px">
                            &nbsp;
                            Hello, <%= loginedUser.getFullName()%>!</a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a class="dropdown-item" href="profile">Thông tin cá nhân</a></li>
                            <li><a class="dropdown-item" href="#!">Đổi mật khẩu</a></li>
                            <li><hr class="dropdown-divider" /></li>
                            <li><a class="dropdown-item" href="../customer/home">Đổi vai trò</a></li>
                            <li><a class="dropdown-item" href="<%=request.getContextPath()%>/logout">Đăng xuất</a></li>
                        </ul>
                    </li>
                </ul>
            </form>
        </div>
    </div>
</nav>
<script>
    $('.divSmt').hover(
            function () {
                if ($(this).attr('class').indexOf('w3-animate-zoom') === -1) {
                    $(this).toggleClass("bg-light");
                    $(this).children("a").toggleClass("text-dark-when-hover");
                }
            });
</script>