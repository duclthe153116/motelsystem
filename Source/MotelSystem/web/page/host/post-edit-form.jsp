<%-- 
    Document   : post-edit-form
    Created on : Jun 8, 2022, 4:51:03 AM
    Author     : royal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <c:if test="${post.id == 0}"><title>Đăng bài</title></c:if>
        <c:if test="${post.id != 0}"><title>Chỉnh sửa bài đăng</title></c:if>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            <meta name="description" content="" />
            <meta name="author" content="" />
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
            <!-- Favicon-->
            <!-- Bootstrap icons-->
            <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
            <!-- Core theme CSS (includes Bootstrap)-->
            <link href="<%= request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="<%= request.getContextPath()%>/css/swalert.css" rel="stylesheet" />
        <style>
            .img-wrap {
                position: relative;
                display: inline-block;
                border: 1px black solid;
                font-size: 0;
            }
            .img-wrap .close {
                position: absolute;
                right: 2px;
                z-index: 100;
                padding: 5px 2px 2px;
                color: red;
                opacity: .5;
                line-height: 15px;
                font-weight: bold;
                cursor: pointer;
                text-align: center;
                font-size: 40px;
                height: fit-content;
            }


            .close:hover{
                opacity: 1;
            }

            @keyframes sizeBounce{
                0%   {fontsize:60px;}
                25%  {fontsize:55px;}
                50%  {fontsize:50px;}
                75%  {fontsize:45px;}
                100% {fontsize:40px;}
            }
            .motel-picture{
                width: 100px;
                height: 100px;
            }
            p{color:#8080CC}
        </style>
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp"/>
        <!-- Header-->
        <!-- Section-->

        <section style="padding-bottom: 5em">
            <div class="container px-4 px-lg-5 mt-5">
                <c:if test="${post.id==0}"><form action="<%=request.getContextPath()%>/host/PostAddNew" method="post" enctype="multipart/form-data"> </c:if>
                    <c:if test="${post.id!=0}"><form action="<%=request.getContextPath()%>/host/PostEditHost?postID=${post.id}" method="post" enctype="multipart/form-data"> </c:if>
                            <input type="hidden" id="listImageIDToDelete" name="listImageIDToDelete" value="nothing">
                            <input type="hidden" name="motelID" value="${post.modelID}">
                        <div class="row" style="border: 2px solid #8080CC;padding-top: 20px; border-radius: 10px;box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;">
                            <div class="col" style="border-right: 2px solid #8080CC">
                                <div class="container" style="width: 100%;height: 100%">
                                    <label for="gallery-photo-add" class="btn btn-dark">Thêm ảnh <c:if test="${post.id==0}">*</c:if></label>
                                    <input type="file" multiple <c:if test="${post.id==0}">required="true"</c:if> name="image"  id="gallery-photo-add" class="form-control" style="display: none;">
                                        <div id="gallery" style="border:2px solid #8080CC; border-radius: 10px;margin-top: 10px;padding-bottom: 10px" class="d-flex flex-wrap justify-content-center"><br/><br/><br/>
                                        <c:forEach items="${post.listImages}" var="img" varStatus="loop">
                                            <div class="img-wrap" id="existedImage${img.id}" style="border: 1px solid white;margin-right:15px;margin-top:15px;">
                                                <span class="close" onclick="return genListImageIDToDelete(${img.id});">&times;</span>
                                                <img class="motel-picture" src="data:image/jpg;base64,${img.base64String}">
                                            </div>
                                        </c:forEach>
                                    </div>
                                </div>

                            </div>
                            <div class="col" >
                                <div class="row" style="padding-left: 50px;">
                                    <center><h2><b>${post.motelName}</b></h2></center>
                                    <div class="row" style="margin-top: 30px">
                                        <div class="col-3">
                                            <p><b><i>*Tiêu Đề: </i></b></p>
                                        </div>
                                        <div class="col-9">
                                            <textarea id="title" name="title" required="true" maxlength="1000" class="form-control" oninvalid="setCustomValidity('Xin nhập đúng định dạng')" oninput="setCustomValidity('')" style="width:100%"><c:out value="${post.title}"/></textarea>
                                        </div>
                                    </div><br/><br/>
                                    <div class="row" style="margin-top: 30px">
                                        <div class="col-3">
                                            <p><b><i>*Nội dung: </i></b></p>
                                        </div>
                                        <div class="col-9">
                                            <textarea id="content" name="content" required="true" maxlength="1000" class="form-control" oninvalid="setCustomValidity('Xin nhập đúng định dạng')" oninput="setCustomValidity('')" style="width:100%"><c:out value="${post.content}"/></textarea>
                                        </div>
                                    </div><br/><br/>
                                    <div class="row" style="margin-top: 30px">
                                        <div class="col-3">
                                            <p><b><i>Giá:</i></b></p>
                                        </div>
                                        <div class="col-9">
                                            <fmt:setLocale value="vi-VN"/>

                                            <h5>
                                                <fmt:formatNumber type = "currency" value = "${post.minPrice}" /> - <fmt:formatNumber type = "currency" value = "${post.maxPrice}" />
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 30px">
                                        <div class="col-3">
                                            <p><b><i>Số điện thoại:</i></b></p>
                                        </div>
                                        <div class="col-9">
                                            <h5>${post.phone}</h5>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 30px">
                                        <div class="col-3">
                                            <p><b><i>Địa chỉ:</i></b></p>
                                        </div>
                                        <div class="col-9">
                                            <h5>${post.address}</h5>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 30px">
                                        <div class="col-3">
                                            <p><b><i>Ngày đăng:</i></b></p>
                                        </div>
                                        <div class="col-9">
                                            <h5><fmt:formatDate value="${post.pDate}" pattern="dd-MM-yyyy" /></h5>
                                        </div>
                                    </div>
                                </div>
                            </div><br/><br/>
                            <c:if test="${post.id == 0}">
                                <input type="submit" class="btn btn-dark" id="newpost" style="height: 70px;margin-top: 30px;" onclick="return isValidForm()" value="Đăng bài"/>
                            </c:if>
                            <c:if test="${post.id != 0}">
                                <input type="submit" class="btn btn-dark" id="updatepost" style="height: 70px;margin-top: 30px;" value="Lưu chỉnh sửa" disabled=""/>
                            </c:if>
                        </div>
                        </div>

                    </form>
                    </section>
                    <!-- Footer-->
                    <jsp:include page="../footer.html"/>
                    <!-- Bootstrap core JS-->
                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

                    </body>
                    <script>
                                    let indexOfUploadedImage = 0;
                                    function removeFileFromFileList(index) {
                                        const dt = new DataTransfer()
                                        const input = document.getElementById('gallery-photo-add');
                                        const {files} = input;

                                        for (let i = 0; i < files.length; i++) {
                                            const file = files[i];
                                            if (index !== i)
                                                dt.items.add(file); // here you exclude the file. thus removing it.
                                        }
                                        $('#' + index + '_div').hide();
                                        input.files = dt.files; // Assign the updates list
                                    }
                                    $(function () {
                                        // Multiple images preview in browser
                                        var imagesPreview = function (input, placeToInsertImagePreview) {
                                            var reader = new FileReader();
                                            reader.onload = function (file) {
                                                placeToInsertImagePreview
                                                        .attr('src', file.target.result);
                                            }
                                            reader.readAsDataURL(input);
                                        };
                                        $('#gallery-photo-add').on('change', function () {
                                            if (this.files) {
                                                for (var i = 0; i < this.files.length; i++) {
                                                    let divIter = $('<div class="img-wrap" style="margin-right:15px;margin-top:15px;" id="' + indexOfUploadedImage + '_div" ></div>');
                                                    divIter.append('<span id="' + indexOfUploadedImage + '_span" class="close" onclick="return removeFileFromFileList(' + indexOfUploadedImage + ');">&times;</span>');
                                                    divIter.append('<img id="' + indexOfUploadedImage + '_img" class="motel-picture">');
                                                    $('#gallery').append(divIter);
                                                    imagesPreview(this.files[i], $('#' + indexOfUploadedImage + '_img'));
                                                    indexOfUploadedImage++;
                                                }
                                            }
                                            $('#updatepost').attr('disabled', false);
                                        });
                                    }
                                    );
                                    function genListImageIDToDelete(id) {
                                        let text = $('#listImageIDToDelete').val();
                                        if (text.localeCompare('nothing') !== 0) {
                                            $('#listImageIDToDelete').val($('#listImageIDToDelete').val() + id + '-');
                                            $('#existedImage' + id).hide();
                                        } else {
                                            $('#listImageIDToDelete').val(id + '-');
                                            $('#existedImage' + id).hide();
                                        }
                                        $('#updatepost').attr('disabled', false);
                                    }
                                    function isValidForm() {
                                        if (document.getElementById("gallery-photo-add").files.length === 0) {
                                            swal({
                                                title: "Tải lên ít nhất 1 ảnh!",
                                                icon: "warning",
                                                buttons: {
                                                    ok: "OK!"
                                                }
                                            });
                                            return false;
                                        }
                                    }
                                    $(function () {
                                        $('form').on('change input', function () {
                                            $('#updatepost').attr('disabled', false);
                                        });
                                    }
                                    );
                    </script>
                    </html>
