<%@page import="model.Services"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý dịch vụ</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>


        <link rel="stylesheet" href="../css/host/manageServices.css">
        <script language="javascript" src="../js/host/manageServices.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
        <script>
            const deleteServicesFunction = (id) => {
                if (confirm("Bấm ok để xóa dịch vụ đã chọn, cancel để hủy") == true) {
                    window.location.href = "DeleteServices?id=" + id;
                }
            }
            var statusDelete = '<%=request.getAttribute("statusDelete")%>';
            var statusCreate = '<%=request.getAttribute("statusCreate")%>';
            if (statusDelete != "null") {
                function alertStatusDelete() {
                    if (statusDelete == "true") {
                        alert("Xóa dịch vụ thành công");
                    } else {
                        alert("Xóa dịch vụ thất bại, do có phòng đang sử dụng");
                    }
                }
            }
            if (statusCreate != "null") {
                function alertStatusDelete() {
                    if (statusCreate == "true") {
                        alert("Thêm dịch vụ thành công");
                    } else {
                        alert("Thêm dịch vụ thất bại");
                    }
                }
            }

        </script>
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp">
            <jsp:param name="nav" value="hostManageServices"></jsp:param>
        </jsp:include>
        <!--form thêm services-->
        <div class="form-popup" id="myForm">
            <form class="form-container" name="myForm" action="CreateServices" onsubmit="return validateForm()" method="post" style="box-shadow: 9px 12px 20px 13px;" >
                <h1 style="text-align: center;">Thêm Dịch Vụ</h1>
                <label><b>Chọn Nhà Trọ</b></label>
                <div style="height: auto" >
                    <select class="form-control selectpicker droplist" name="motelID" data-live-search="true">
                        <c:forEach var="motel" items="${listMotel}">
                            <option data-tokens="${motel.motelName}" value="${motel.motelID}">${motel.motelName}</option>
                        </c:forEach>
                    </select>
                </div>
                <label><b>Tên Dịch Vụ</b></label>
                <input type="text" placeholder="VD: điện" name="servicesName" required>
                <label><b>Giá(VNĐ)</b></label>
                <input type="text" placeholder="VD: 2300" name="servicesPrice" required="">
                <input type="submit" class="btn" value="Thêm"/>
                <button type="button" class="btn cancel" onclick="closeForm()">Hủy</button>
            </form>
        </div>
        <!-- Body -->
        <div class="container-lg">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Quản dịch vụ</b>
                            </div>
                            <div class="col-sm-4" style="text-align: center;">
                                <button style="box-shadow: 3px 2px 10px 4px" type="button" class="open-button" onclick="openForm()">Thêm Dịch Vụ</button>
                            </div>
                            <div class="col-sm-4">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm phòng">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="myTable"  class="table table-striped">
                        <thead>
                            <tr style="text-align: center">
                                <td style="width:20% ;">Tên Trọ</td>
                                <td style="width:40% ;">Tên Dịch Vụ</td>
                                <td style="width:25% ;">Giá/(Đơn Vị)</td>
                                <td  style="width:15%;">Hành động</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="services" items="${listServices}">
                                <tr>
                            <input form="${services.servicesID}"  type="text" name="servicesID" value="${services.servicesID}" style="display: none">
                            <c:forEach var="motel" items="${listMotel}">
                                <c:if test="${motel.motelID == services.motelID}">
                                    <td>
                                        <input form="${services.servicesID}"  type="text" name="motelID" value="${motel.motelID}" style="display: none">
                                        <form method="POST" id="${services.servicesID}" name="${services.servicesID}" action="UpdateServices" onsubmit="return validateForm(${services.servicesID})"></form>
                                        ${motel.motelName}
                                    </td>
                                </c:if>
                            </c:forEach>
                            <td><input form="${services.servicesID}" type="text" id="servicesName" name="servicesName" value="${services.servicesName}" required></td>
                            <td><input form="${services.servicesID}" type="text" id="servicesPrice" name="servicesPrice" value="${services.price}"></td>
                            <td>
                                <a href="#" class="delete" title="Xóa" data-toggle="tooltip"  onclick="deleteServicesFunction(${services.servicesID})"><i class="material-icons">&#xE872;</i></a>
                                <button form="${services.servicesID}" type="submit" title="Lưu"  data-toggle="tooltip"><i class="material-icons">done</i></button>
                            </td>
                            </tr>
                        </c:forEach>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>  
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

        <!--<script type="text/javascript"> window.onload = alertStatusCreate;</script>-->
        <script type="text/javascript"> window.onload = alertStatusDelete;</script>
    </body>
</html>
