<%-- 
    Document   : homepage-normal
    Created on : May 14, 2022, 1:43:58 AM
    Author     : royal
--%>

<%@page import="model.AccountAllInfor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="loginedUser" value="${sessionScope.accountAllInfor}"></c:set>

    <!DOCTYPE html>
    <html lang="en" style="overflow: auto;">
        <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            <meta name="description" content="" />
            <meta name="author" content="" />
            <title>Chi tiết trọ</title>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
            <!-- Favicon-->
            <!-- Bootstrap icons-->
            <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
            <!-- Core theme CSS (includes Bootstrap)-->
            <link href="<%= request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
        <link language="javascript" href="../js/host/manageMotel.js"/>
        <style>
            img {
                vertical-align: middle;
            }


            /* Hide the images by default */
            .mySlides {
                display: block;
                width: 100%;
                height: 500px;
            }

            /* Add a pointer when hovering over the thumbnail images */
            .cursor {
                cursor: pointer;
            }

            /* Next & previous buttons */
            .prev,
            .next-slide {
                cursor: pointer;
                position: absolute;
                top: 50%;
                width: auto;
                padding: 16px;
                margin-top: -50px;
                color: #FFF;
                font-weight: bold;
                font-size: 20px;
                border-radius: 0 3px 3px 0;
                user-select: none;
                text-decoration: none;
                -webkit-user-select: none;
            }

            /* Position the "next button" to the right */
            .next-slide {
                right:0;
                border-radius: 3px 0 0 3px;
            }

            /* On hover, add a black background color with a little bit see-through */
            .prev:hover,
            .next-slide:hover {
                background-color: rgba(0, 0, 0, 0.8);
            }

            /* Add a transparency effect for thumnbail images */
            .demo {
                opacity: 0.6;
            }

            .active,
            .demo:hover {
                opacity: 1;
            }

            i{
                color: #8080CC;
            }

            :root {
                --modal-duration: 1s;
                --modal-color: #428bca;
            }

            .button {
                background: #428bca;
                padding: 1em 2em;
                color: #fff;
                border: 0;
                border-radius: 5px;
                cursor: pointer;
            }

            .button:hover {
                background: #3876ac;
            }

            .modals {
                text-align:center!important;
                display: none;
                position: fixed;
                z-index: 1;
                left: 0;
                top: 0;
                height: 100%;
                width: 100%;
                overflow: auto;
                background-color: rgba(0, 0, 0, 0.5);
            }

            .modal-content {
                margin: 10% auto;
                width: 30%;
                box-shadow: 0 5px 8px 0 rgba(0, 0, 0, 0.2), 0 7px 20px 0 rgba(0, 0, 0, 0.17);
                animation-name: modalopen;
                animation-duration: var(--modal-duration);
            }

            .modal-header h2,
            .modal-footer h3 {
                margin: 0;
            }

            .modal-header {
                background: var(--modal-color);
                padding: 15px;
                color: #fff;
                border-top-left-radius: 5px;
                border-top-right-radius: 5px;
            }

            .modal-body {
                padding: 10px 20px;
                background: #fff;
            }

            .modal-footer {
                background: var(--modal-color);
                padding: 10px;
                color: #fff;
                text-align: center;
                border-bottom-left-radius: 5px;
                border-bottom-right-radius: 5px;
                cursor: pointer;
            }

            .close {
                color: #ccc;
                float: right;
                font-size: 30px;
                color: #fff;
            }

            .close:hover,
            .close:focus {
                color: #000;
                text-decoration: none;
                cursor: pointer;
            }

            @keyframes modalopen {
                from {
                    opacity: 0;
                }
                to {
                    opacity: 1;
                }
            }
            h5{
                margin: 0px!important;
            }
        </style>
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp"/>
        <!-- Header-->
        <!-- Section-->
        <div id="my-modal" class="modals">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close">&times;</span>
                    <h2 id="error">Cập nhật thành công </h2>
                </div>
                <div class="modal-body">
                    <image id="image-action" style ="height:45%;width:45%;" src=""></image>
                </div>
                <div class="modal-footer" onclick="closeModal()">
                    <h3>Đóng</h3>
                </div>
            </div>
        </div>
        <section style="padding-bottom: 5em">
            <div class="container mt-5" style="border: 3px solid #8080CC;padding-top: 20px; border-radius: 10px;box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;">
                <div class="d-flex justify-content-between">
                    
                    <c:if   test="${motel.getOwnerID() == loginedUser.getAccountID()}">
                        <div>
                            <h4 style="font-size: 25px;text-decoration: none;color:#8080CC"><b><i>&emsp;&emsp;Trọ của tôi</i></b></h4>
                        </div>
                        <div>
                            <a href="EditMotel?id=${motel.motelID}" id="editButton" class="btn btn-dark" style="display: block; margin-right: 20px;">Sửa</a>
                        </div>
                    </c:if>
                </div><br/>
                <div class="row px-4" style="padding-bottom: 20px;padding-top: 20px">
                    <div class="col px-4" style="border-right: 2px solid #8080CC">
                        <c:if   test="${motel.getOwnerID() == loginedUser.getAccountID()}">
                            <div class="slideshow" style="width: 100%;height: 80%;position: relative;padding-right: 0px;">
                                <c:forEach items="${motel.getMotelImgList()}" var="i" >
                                    <div style="height: 500px;" class="mySlides">
                                        <img src="data:image/jpg;base64,${i}" style="width:100%;height: 100%;">
                                    </div>
                                </c:forEach>
                                <a class="prev" onclick="plusSlides(-1)">❮</a>
                                <a class="next-slide" onclick="plusSlides(1)">❯</a>
                                <br/>
                                <div class="row">
                                    <c:forEach items="${motel.getMotelImgList()}" var="i">
                                        <div class="column">
                                            <img class="demo cursor" src="data:image/jpg;base64,${i}" style="width:100%;height: 80px;" onclick="currentSlide(${loop.index+1})" alt="The Woods">
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </c:if>
                        <c:if   test="${motel.getOwnerID() != loginedUser.getAccountID()}">
                            <div class="slideshow" style="width: 100%;height: 80%;position: relative;padding-right: 0px;">
                                <div style="height: 500px;" class="mySlides">
                                    <img src="data:image/jpg;base64,${motel.motelImage}" style="width:100%;height: 100%;">
                                </div>
                            </div>
                        </c:if>
                        
                            
                    </div>
                    <div class="col">
                        <div class="row" style="padding-left: 50px;">
                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Chủ trọ</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.ownerName}</h5>
                                </div>
                            </div><br/><br/>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Tên trọ:</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.motelName}</h5>
                                </div>
                            </div><br/><br/>

                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Số phòng:</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.roomNumber}</h5>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Giá:</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.minPrice} - ${motel.maxPrice} (VNĐ)</h5>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Số điện thoại:</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.motelPhone}</h5>
                                </div>
                            </div><br/><br/>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Địa chỉ:</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.motelAddress}</h5>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Mô tả chi tiết:</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.motelDescription}</h5>
                                </div>
                            </div><br/><br/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    </body>
    <script>
                                            let slideIndex = 1;
                                            showSlides(slideIndex);

                                            function plusSlides(n) {
                                                showSlides(slideIndex += n);
                                            }
                                            ;

                                            function currentSlide(n) {
                                                showSlides(slideIndex = n);
                                            }
                                            ;

                                            function showSlides(n) {
                                                let i;
                                                let slides = document.getElementsByClassName("mySlides");
                                                let dots = document.getElementsByClassName("demo");
                                                if (n > slides.length) {
                                                    slideIndex = 1
                                                }
                                                if (n < 1) {
                                                    slideIndex = slides.length
                                                }
                                                for (i = 0; i < slides.length; i++) {
                                                    slides[i].style.display = "none";
                                                }
                                                for (i = 0; i < dots.length; i++) {
                                                    dots[i].className = dots[i].className.replace(" active", "");
                                                }
                                                slides[slideIndex - 1].style.display = "block";
                                                dots[slideIndex - 1].className += " active";
                                            }
                                            ;
    </script>
    <script>
        var w = 100 / ($(".mySlides").length);
        $(".column").css("width", w + "%");
    </script>
    <script>

        // Get DOM Elements
        const modal = document.querySelector('#my-modal');
        const closeBtn = document.querySelector('.close');

// Events
        closeBtn.addEventListener('click', closeModal);
        window.addEventListener('click', outsideClick);

// Open
        function openModal() {
            modal.style.display = 'block';
        }

// Close
        function closeModal() {
            modal.style.display = 'none';
        }

// Close If Outside Click
        function outsideClick(e) {
            if (e.target == modal) {
                modal.style.display = 'none';
            }
        }
        var flag = '${action}';
        if (flag.localeCompare('success') === 0) {
            modal.style.display = 'block';
            document.getElementById("error").innerHTML = "Cập nhật thành công";
            document.getElementById('image-action').src = 'https://i.pinimg.com/originals/7b/dd/1b/7bdd1bc7db7fd48025d4e39a0e2f0fd8.jpg';
        }
        if (flag.localeCompare('fail') === 0) {
            modal.style.display = 'block';
            document.getElementById("error").innerHTML = "Cập nhật thất bại";
            document.getElementById('image-action').src = 'https://previews.123rf.com/images/alonastep/alonastep1605/alonastep160500131/57080070-cross-sign-element-red-x-icon-isolated-on-white-background-simple-mark-graphic-design-round-shape-bu.jpg';
        }
        if (flag.localeCompare('normal') === 0) {
            modal.style.display = 'block'
            document.getElementById("error").innerHTML = "Không thay đổi";
            document.getElementById('image-action').src = 'https://st.depositphotos.com/1842549/2251/v/950/depositphotos_22513947-stock-illustration-no-change.jpg';
        }
    </script>

</html>
