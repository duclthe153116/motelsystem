<%-- 
    Document   : ManageRequest
    Created on : May 27, 2022, 12:34:26 PM
    Author     : Admin
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý yêu cầu</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/admin/manageRequest.css">
        <script language="javascript" src="../js/admin/manageRequest.js"></script>
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp">
            <jsp:param name="nav" value="hostManageRequest"></jsp:param>
        </jsp:include>
        <div class="container-lg">
            <!-- Body -->
            <div id="table-account" class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <b>Quản lý yêu cầu</b></h2>
                            </div>
                            <div class="col-sm-6">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm yêu cầu">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form id ="f" action="managerequest" method="post">
                        <select name="motel" onchange="document.getElementById('f').submit();">
                            <option value="0">Tất cả các trọ</option>
                            <c:forEach var="listMotel" items="${requestScope.listMotelByAccountID}">
                                <option value="${listMotel.motelID}" <c:if test="${listMotel.motelID == motelID}"> selected=""</c:if>>${listMotel.motelName}</option>
                            </c:forEach>
                        </select>
                    </form>
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr>
                                <td style="width:10% ;">Tên phòng</td>
                                <td style="width:10% ;">Nội dung yêu cầu</td>
                                <td style="width:10% ;">Trạng thái</td>
                                <td style="width:10% ;">Ngày tạo</td>
                                <td style="width:10% ;">Ngày cập nhật</td>
                                <td style="width:10% ;">Ghi chú từ trọ</td>
                                <td style="width:10% ;">Ghi chú chủ trọ</td>
                                <td style="width:5%">Giá sửa</td>
                                <td style="width:10% ;">Hành động</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.listRequestOfRoom}" varStatus="loop" var="listRequestOfRoom">
                                <tr class="row-hover">
                                    <td><c:out value="${listRequestOfRoom.getRoomName()}"></c:out></td>
                                    <td><c:out value="${listRequestOfRoom.getContent()}"></c:out></td>
                                    <c:if test="${listRequestOfRoom.getStatus() eq 'done'}">
                                        <td>Đã hoàn thành</td>
                                    </c:if>
                                    <c:if test="${listRequestOfRoom.getStatus()== '0'}">
                                        <td>
                                            Chưa xem</td>
                                        </c:if>
                                        <c:if test="${listRequestOfRoom.getStatus() eq 'seen'}">
                                        <td>
                                            Đã xem</td>
                                        </c:if>
                                        <c:if test="${listRequestOfRoom.getStatus() eq 'do'}">
                                        <td>
                                            Đang trong quá trình hoàn thành</td>
                                        </c:if>
                                        <c:if test="${listRequestOfRoom.getStatus() eq 'reject'}">
                                        <td>
                                            Từ chối yêu cầu</td>
                                        </c:if>
                                    <td><fmt:formatDate value="${listRequestOfRoom.getCreatedAt()}" pattern="dd/MM/yyyy" /></td>
                                    <td><fmt:formatDate value="${listRequestOfRoom.getUpdateAt()}" pattern="dd/MM/yyyy" /></td>
                                    <td><c:out value="${listRequestOfRoom.getNoteRequest()}"></c:out></td>
                                    <td><c:out value="${listRequestOfRoom.getNoteRequestOfHost()}"></c:out></td>
                                    <td><c:out value="${listRequestOfRoom.getPriceRequest()}"></c:out></td>
                                    <c:if test="${listRequestOfRoom.getStatus() ne 'reject'}">
                                        <td>
                                            <a href="updaterequest?idRequest=${listRequestOfRoom.getRequestID()}" class="edit" title="Xem chi tiết/Sửa/Lưu" data-toggle="tooltip"><i
                                                    class="material-icons">&#xE254;</i></a></td>
                                            </c:if>
                                            <c:if test="${listRequestOfRoom.getStatus() eq 'reject'}">
                                        <td>
                                        </td>
                                    </c:if>
                                </tr>
                            </c:forEach>
                        </tbody>    

                    </table>

                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>