<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý booking</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/host/manageMotel.css">
        <script language="javascript" src="../js/host/manageMotel.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
        
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <link href="<%= request.getContextPath()%>/css/swalert.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp">
            <jsp:param name="nav" value="hostManageBooking"></jsp:param>
        </jsp:include>
        <!-- Body -->
        <div class="container-lg">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Quản lý booking</b></h2>
                            </div>
                            <div class="col-sm-4" style="text-align: center; ">
                                <button style="box-shadow: 3px 2px 10px 4px" onclick="location.href = 'http://localhost:8080/MotelSystem/host/createbooking'" type="button" class="open-button">Thêm Hợp Đồng</button>
                            </div>
                            <div class="col-sm-4">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm booking">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="myTable"  class="table table-striped">
                        <thead>
                            <tr>
                                <td style="width:10% ;">Tên trọ</td>
                                <td style="width:9% ;">Tên phòng</td>
                                <td style="width:12% ;">Người ở hiện tại</td>
                                <td style="width:9% ;">Giá phòng</td>
                                <td style="width:10% ;">Ghi chú</td>
                                <td style="width:10% ;">Ngày bắt đầu</td>
                                <td style="width:10% ;">Ngày kết thúc</td>
                                <td style="width:10% ;">Hoạt động</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listBookingAllInfor}" var="bookingAllInfor">
                                <tr>
                                    <td>${bookingAllInfor.motelName}</td>
                                    <td>${bookingAllInfor.roomName}</td>
                                    <td>
                                        <c:forEach var="fullName" items="${bookingAllInfor.listAccountName}">
                                            ${fullName}<br>
                                        </c:forEach>
                                    </td>                              

                                    <td>${bookingAllInfor.price}</td>
                                    <td>${bookingAllInfor.noteRoom}</td>
                                    
                                    <td>${bookingAllInfor.startAt}</td>
                                    <td>${bookingAllInfor.endAt}</td>
                                    <td>
                                        <a href="BookingDetails?id=${bookingAllInfor.bookingID}" class="view" title="View detail" data-toggle="tooltip"><i 
                                                class="material-icons">visibility</i></a>
                                                <a href="UpdateBooking?bookingID=${bookingAllInfor.bookingID}" class="edit" title="Edit" data-toggle="tooltip"><i 
                                                class="material-icons">edit</i></a>
                                        <a href="DeleteBooking?id=${bookingAllInfor.bookingID}" class="delete" title="Delete" data-toggle="tooltip"><i 
                                                class="material-icons">delete</i></a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <h4 class="${requestScope.bg} text-white text-center rounded w-25 p-2 mb-2 ">${requestScope.error}</h4>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>  
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>
    <script>
        var flag = '${actionDelete}';
        if (flag.localeCompare('success') === 0) {
            swal({
                title: "Dừng đặt phòng thành công!",
                icon: "success",
                buttons: {
                    ok: "OK!"
                }
            });
        } else if (flag.localeCompare('failed') === 0) {
            swal({
                title: "Có lỗi xảy ra!",
                icon: "danger",
                buttons: {
                    ok: "OK!"
                }
            });
        }
    </script>
</html>
