<%-- 
    Document   : manageMotel
    Created on : May 16, 2022, 2:08:53 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quản lý trọ</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

        <!-- link boostrap for datatable -->
        <link rel="stylesheet" href="http://cdn.datatables.net/1.10.2/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="http://cdn.datatables.net/1.10.2/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/host/manageMotel.css">
        <script language="javascript" src="../js/host/manageMotel.js"></script>
        <link href="<%=request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp">
            <jsp:param name="nav" value="manageMotel"></jsp:param>
        </jsp:include>
        <!-- Body -->
        <div class="container-lg">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-4">
                                <h2><b>Quản lý trọ</b></h2>
                            </div>
                            <div class="col-sm-4">
                                <h2 class="add-motel"><a href="AddNewMotel">Thêm trọ</a></h2>
                            </div>
                            <div class="col-sm-4">
                                <div class="search-box">
                                    <div class="input-group">
                                        <input type="text" name="" id="search" class="form-control"
                                               placeholder="Tìm kiếm trọ">
                                        <span class="input-group-addon"><i class="material-icons">&#xE8B6;</i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table id="myTable"  class="table table-striped">
                        <thead>
                            <tr>
                                <td style="width:2% ;">ID</td>
                                <td style="width:10% ;">Tên trọ</td>
                                <td style="width:7% ;">Số phòng</td>
                                <td style="width:7% ;">Số phòng trống</td>
                                <td style="width:8% ;">Số điện thoại</td>
                                <td>Địa chỉ</td>
                                <td style="width:9% ;">Trạng thái</td>
                                <td style="width:10% ;">Hành động</td>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listMotelByAccountID}" var="motel">
                                <tr>
                                    <td>${motel.motelID}</td>
                                    <td>${motel.motelName}</td>
                                    <td>${motel.motelRoomNumber}</td>
                                    <td>${motel.numberEmptyRoom}</td>
                                    <td>${motel.motelPhone}</td>
                                    <td>${motel.motelAddress}</td>                                
                                    <td>
                                        <c:choose>
                                            <c:when test="${motel.isDelete==true}">Đã bị khóa</c:when>
                                            <c:when test="${motel.motelStatus eq 'pending' or motel.motelStatus eq 're-submit'}">Đang chờ duyệt</c:when>
                                            <c:when test="${motel.motelStatus=='publish'}">Đã duyệt</c:when>    
                                            <c:otherwise>Bị từ chối</c:otherwise>
                                        </c:choose>    
                                    </td>
                                    <td>
                                        <c:if test="${motel.motelStatus=='publish' && motel.isDelete==true}">
                                            <a class="disable" href="#" class="view" title="View detail" data-toggle="tooltip"><i 
                                                    class="material-icons">visibility</i></a>
                                            <a class="disable" href="#" class="delete" title="Delete" data-toggle="tooltip"><i
                                                    class="material-icons" class="disable" >&#xE872;</i></a>
                                            <a class="disable" href="#" class="edit" title="Edit" data-toggle="tooltip"><i 
                                                    class="material-icons" >edit</i></a>    
                                        </c:if>
                                        <c:if test="${motel.motelStatus=='publish' && motel.isDelete==false}">
                                            <a href="ViewMotelDetail?id=${motel.getMotelID()}" class="view" title="View detail" data-toggle="tooltip"><i 
                                                    class="material-icons">visibility</i></a>
                                            <a onclick="if (!confirm('Bạn chắc chắn muốn xóa trọ ' + '${motel.motelName}')) {
                                                        return false;
                                                    }"
                                               href="DeleteMotel?id=${motel.getMotelID()}&accept=true" class="delete" title="Delete" data-toggle="tooltip"><i
                                                    class="material-icons">&#xE872;</i></a>
                                            <a href="EditMotel?id=${motel.motelID}" class="edit" title="Edit" data-toggle="tooltip"><i 
                                                    class="material-icons">edit</i></a>    
                                            </c:if>
                                            <c:if test="${motel.motelStatus=='pending' or motel.motelStatus eq 'reject' }">
                                            <a href="ViewRegisterNewMotel?id=${motel.getMotelID()}" class="view" title="View detail" data-toggle="tooltip"><i 
                                                    class="material-icons">visibility</i></a>
                                            <a onclick="if (!confirm('Bạn chắc chắn muốn xóa trọ ' + '${motel.motelName}')) {
                                                        return false;
                                                    }"
                                               href="DeleteMotel?id=${motel.getMotelID()}&accept=false" class="delete" title="Delete" data-toggle="tooltip"><i
                                                    class="material-icons">&#xE872;</i></a>
                                            </c:if>

                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <h4 class="${requestScope.bg} text-white text-center rounded w-25 p-2 mb-2 ">${requestScope.error}</h4>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>  
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    </body>
    <script>
                                                var flag = '${success}';
                                                if (flag === 'true') {
                                                    alert("Thêm trọ thành công vui lòng đợi");
                                                } else if (flag === 'false') {
                                                    alert("Thêm trọ thất bại vui lòng thử lại");
                                                }

    </script>
</html>
