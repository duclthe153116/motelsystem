<%-- 
    Document   : homepage-normal
    Created on : May 14, 2022, 1:43:58 AM
    Author     : royal
--%>

<%@page import="model.AccountAllInfor"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="loginedUser" value="${sessionScope.accountAllInfor}"></c:set>

    <!DOCTYPE html>
    <html lang="en" style="overflow: auto;">
        <head>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
            <meta name="description" content="" />
            <meta name="author" content="" />
            <title>Thay đổi thông tin</title>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
            <!-- Favicon-->
            <!-- Bootstrap icons-->
            <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
            <!-- Core theme CSS (includes Bootstrap)-->
            <link href="<%= request.getContextPath()%>/css/normal-homepage.css" rel="stylesheet" />
        <!-- Library for input img -->
        <link class="jsbin" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
        <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script class="jsbin" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>

        <style>
            img {
                vertical-align: middle;
            }

            /* Position the image container (needed to position the left and right arrows) */
            .container {
                position: relative;
                padding-right: 0px;
                padding-left: 0px;
            }

            /* Hide the images by default */
            .mySlides {
                display: block;
                width: 100%;
                height: 500px;
            }

            /* Add a pointer when hovering over the thumbnail images */
            .cursor {
                cursor: pointer;
            }
            /* Container for image text */
            .caption-container {
                text-align: center;
                background-color: #222;
                padding: 2px 16px;
                color: white;
            }

            .row:after {
                content: "";
                display: table;
                clear: both;
            }

            /* Six columns side by side */
            .column {
                float: left;
                width: 10%;
            }

            /* Add a transparency effect for thumnbail images */
            .demo {
                opacity: 0.6;
                width: 100%;
                height: 100%;
            }

            .active,
            .demo:hover {
                opacity: 1;
            }

            .input-motel{
                width:100%;
            }
            h5{
                margin: 0px!important;
            }

        </style>
    </head>
    <body>
        <!-- Navigation-->
        <jsp:include page="nav-home-host.jsp"/>
        <!-- Header-->
        <!-- Section-->
        
        <section style="padding-bottom: 5em">
            <div class="container mt-5" style="border: 3px solid #8080CC;padding-top: 20px; border-radius: 10px;box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;">
                <div class="d-flex justify-content-between">
                    <div>
                        <h4 style="font-size: 25px;text-decoration: none;color:#8080CC"><b><i>&emsp;&emsp;Chỉnh sửa trọ</i></b></h4>
                    </div>
                </div><br/>
                <form action="EditMotel" method="post" enctype="multipart/form-data">
                    <div class="row px-4" style="padding-bottom: 20px;padding-top: 20px">
                        <div class="col px-4" style="border-right: 2px solid #8080CC">
                            <div class="slideshow" style="width: 100%;height: 80%;position: relative;padding-right: 0px;">
                                <h5 style="text-align: center;">
                                    <label for="motelImage" class="btn btn-dark" style="margin-bottom:10px;">Chọn ảnh</label>
                                    <input type='file' name="image" class="form-control" onchange="readURL(this);" id="motelImage" style="display:none;"/>
                                    <input type="hidden" value="${motel.motelID}" name="motelID">
                                    <input type="hidden" value="0" id="changed" name="change">
                                </h5>
                                <div class="mySlides" >
                                    <img id="motelImg" src="data:image/jpg;base64,${motel.motelImage}" style="width:100%;height: 100%;">
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="row" style="padding-left: 50px;">
                                <div class="row" style="margin-top: 30px">
                                <div class="col-4">
                                    <p><b><i>Chủ trọ</i></b></p>
                                </div>
                                <div class="col-8">
                                    <h5>${motel.ownerName}</h5>
                                </div>
                            </div><br/><br/>
                                <div class="row" style="margin-top: 30px">
                                    <div class="col-3">
                                        <p><b><i>Tên trọ:</i></b></p>
                                    </div>
                                    <div class="col-9">
                                        <h5><input onchange="checkChange()" class="input-motel" type="text" value="${motel.motelName}" name="motelName" placeholder="${motel.motelName}"/></h5>
                                    </div>
                                </div><br/><br/>

                                <div class="row" style="margin-top: 30px">
                                    <div class="col-3">
                                        <p><b><i>Số phòng:</i></b></p>
                                    </div>
                                    <div class="col-9">
                                        <h5>${motel.roomNumber}</h5>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 30px">
                                    <div class="col-3">
                                        <p><b><i>Giá:</i></b></p>
                                    </div>
                                    <div class="col-9">
                                        <h5>${motel.minPrice} - ${motel.maxPrice} (VNĐ)</h5>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 30px">
                                    <div class="col-3">
                                        <p><b><i>Số điện thoại:</i></b></p>
                                    </div>
                                    <div class="col-9">
                                        <h5><input onchange="checkChange()" class="input-motel" type="text" value="${motel.motelPhone}" name="motelPhone" placeholder="${motel.motelPhone}"/></h5>
                                    </div>
                                </div><br/><br/>


                                <div class="row" style="margin-top: 30px">
                                    <div class="col-3">
                                        <p><b><i>Địa chỉ:</i></b></p>
                                    </div>
                                    <div class="col-9">
                                        <input type="hidden" value="${motel.motelStatus}" name="motelStatus">
                                        <c:if test="${motel.motelStatus=='publish'}" >
                                            <h5>${motel.motelAddress}</h5>
                                        </c:if>
                                        <c:if test="${motel.motelStatus=='pending'}" >
                                            <h5><input onchange="checkChange()" class="input-motel" type="text" value="${motel.motelAddress}" name="motelAddress" placeholder="${motel.motelAddress}"/></h5>
                                            </c:if>
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 30px">
                                    <div class="col-3">
                                        <p><b><i>Mô tả chi tiết:</i></b></p>
                                    </div>
                                    <div class="col-9">
                                        <h5><textarea onchange="checkChange()" id="motelDescription" name="motelDescription" rows="4" cols="40" placeholder="${motel.motelDescription}">${motel.motelDescription}</textarea></h5>
                                    </div>
                                </div><br/><br/>
                            </div>
                            <input id="modal-btn" type="submit" class="btn btn-dark" style="height: 40px;margin-top: 30px;margin-left: 250px;" value="Lưu chỉnh sửa"/>
                        </div>        
                    </div>
                </form>
            </div>
        </section>
        <!-- Footer-->
        <jsp:include page="../footer.html"/>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    </body>

    <script>
                                        function readURL(input) {
                                            if (input.files && input.files[0]) {
                                                var reader = new FileReader();

                                                reader.onload = function (e) {
                                                    $('#motelImg')
                                                            .attr('src', e.target.result);
                                                };
                                                document.getElementById('changed').value='1';
                                                reader.readAsDataURL(input.files[0]);
                                            }
                                        }

                                        function checkChange() {
                                            document.getElementById('changed').value='1';
                                        }
    </script>

</html>
