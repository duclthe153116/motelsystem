/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LTC
 */
public class SendEmailDAO extends DBContext{

    public boolean updatePasswordByAccountID(String newPassword, int accID) {

        try {
            String sql = "update Account set [passWord] = ? where AccountID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, newPassword);
            stm.setInt(2, accID);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
