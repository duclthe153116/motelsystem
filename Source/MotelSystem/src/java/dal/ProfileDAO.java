/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.Part;
import model.AccountAllInfor;
import model.Profile;
import model.ProfileOfAccount;

/**
 *
 * @author doson
 */
public class ProfileDAO extends DBContext {

    public void updateProfile(int accountID, String fullName, String phone, String address, String email, String DOB, String gender, Part item) throws IOException {
        String sql = "update Profile\n"
                + "set fullName = ?, phone = ?, [address] = ?, email = ?, DOB = ?, Gender = ?, imageProfile = ? \n"
                + "where AccountID = ?";
        InputStream is = item.getInputStream();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, fullName);
            st.setString(2, phone);
            st.setString(3, address);
            st.setString(4, email);
            st.setString(5, DOB);
            st.setString(6, gender);
            st.setBlob(7, is);
            st.setInt(8, accountID);
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    public String getHostNameByMotelId(int motelId) {

        String query = "select c.fullName "
                + "from Motel as a, Account as b, Profile as c "
                + "where a.AccountID = b.AccountID and b.AccountID = c.AccountID and a.motelID = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setInt(1, motelId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public String getEmailByAccountId(int accountId) {

        String query = "select email\n"
                + "from Account as a, Profile as b\n"
                + "where a.AccountID = b.AccountID and a.AccountID = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setInt(1, accountId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<ProfileOfAccount> getAllListAccountDoNotHaveRoom(int[] accoutnID, int accoutIDOfHost) {
        try {
            String sql1 = "select AccountID from RoleOfAccount\n"
                    + "where RoleID = 1";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            ResultSet rs1 = st1.executeQuery();
            ArrayList<Integer> listAdmin = new ArrayList<>();
            while (rs1.next()) {
                listAdmin.add(rs1.getInt(1));
            }

            int n = listAdmin.size() + accoutnID.length;
            int[] a = new int[n];
            for (int j = 0; j < listAdmin.size(); j++) {
                a[j] = listAdmin.get(j);
            }
            int y = 0;
            for (int k = listAdmin.size(); k < n; k++) {
                a[k] = accoutnID[y];
                y++;
            }
            String sql = "select a.AccountID, a.userName, p.fullName, p.phone, p.[address], p.email, p.DOB, p.Gender from Account a inner join [Profile] p on a.AccountID = p.AccountID\n"
                    + "where a.AccountID != ? ";
            if (accoutnID.length > 0) {
                sql = sql + "and a.AccountID not in (";
                for (int i = 0; i < a.length; i++) {
                    if (i == a.length - 1) {
                        sql = sql + "?)";
                    } else {
                        sql += "?,";
                    }
                }
            }
            System.out.println(sql);
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accoutIDOfHost);
            ArrayList<ProfileOfAccount> listAccount = new ArrayList<>();
            for (int j = 0; j < a.length; j++) {
                st.setInt(j + 2, a[j]);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                ProfileOfAccount acc = new ProfileOfAccount();
                acc.setAccountID(rs.getInt(1));
                acc.setUserName(rs.getString(2));
                acc.setPassWotd("");
                acc.setProfileID(0);
                acc.setFullName(rs.getString(3));
                acc.setPhone(rs.getString(4));
                acc.setAddress(rs.getString(5));
                acc.setEmail(rs.getString(6));
                acc.setDob(rs.getDate(7));
                acc.setGender(rs.getString(8));
                acc.setImageProfile("");
                listAccount.add(acc);
            }
            return listAccount;
        } catch (SQLException e) {
            return null;
        }
    }

    public static void main(String[] args) {
        ProfileDAO pd = new ProfileDAO();
//        String date = "09/20/2001";
//        try {
//            Date d = new SimpleDateFormat("dd/MM/yyyy").parse(date);
//            pd.updateProfile(1, "Do Ngoc Son", "0344025802", "Son La", "doson6411@gmail.com","09/20/2001" , "Female");
//        } catch (ParseException ex) {
//            Logger.getLogger(ProfileDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
        int[] acc = new int[3];
        for (int i = 0; i < acc.length; i++) {
            acc[i] = i + 1;
        }
        System.out.println(pd.getAllListAccountDoNotHaveRoom(acc, 2));

    }
}
