/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.BookingAllInfor;
import model.Room;
import model.RoomAllInfor;

/**
 *
 * @author coder
 */
public class RoomDAO extends DBContext {

    // get all room of motel by list motelID of account
    public ArrayList<Room> getAllRoomOfAccount(ArrayList<Integer> listMotelID) {
        try {
            String sql = "select * from Room where motelID = ? and isDeleteRoom = 0";
            PreparedStatement stm = connection.prepareStatement(sql);
            ArrayList<Room> rooms = new ArrayList<>();
            for (int i = 0; i < listMotelID.size(); i++) {
                stm.setInt(1, listMotelID.get(i));
                ResultSet rs = stm.executeQuery();
                while (rs.next()) {
                    Room room = new Room(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getDouble(6), rs.getDouble(7), rs.getString(8));
                    rooms.add(room);
                }
            }
            return rooms;
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    //Get one room by room ID
    public Room getRoomByRoomID(int roomID) {
        try {
            String sql = "Select * from Room where roomID = ? and isDeleteRoom = 0";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, roomID);
            ResultSet rs = stm.executeQuery();
            Room room = new Room();
            while (rs.next()) {
                room.setRoomID(rs.getInt(1));
                room.setMotelID(rs.getInt(2));
                room.setRoomName(rs.getString(3));
                room.setNumberOfPeopleMax(rs.getInt(4));
                room.setNumberOfPeopleCurrent(rs.getInt(5));
                room.setPrice(rs.getDouble(6));
                room.setArea(rs.getDouble(7));
                room.setNote(rs.getString(8));

            }
            return room;
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public boolean CreateRoom(Room room) {
        try {
            //String sql = "insert into Room values(?,?,?,?,?,?,?)";
            String sql = " insert into Room(motelID, roomName, numberOfPeopleMax, numberOfPeopleCurrent, price, area, noteRoom, isDeleteRoom) \n" +
                        " values (?,?,?,?,?,?,?,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, room.getMotelID());
            stm.setString(2, room.getRoomName());
            stm.setInt(3, room.getNumberOfPeopleMax());
            stm.setInt(4, 0);
            stm.setDouble(5, room.getPrice());
            stm.setDouble(6, room.getArea());
            stm.setString(7, room.getNote());
            stm.setInt(8, 0);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean DeleteRoomByID(int RoomID) {
        try {
            String sql = "update room set isDeleteRoom = 1 where  roomID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, RoomID);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean UpdateRoom(Room room) {
        try {
            String sql = "update room set motelID = ?, roomName = ?, numberOfPeopleMax = ?, numberOfPeopleCurrent = ?, price = ?, area = ?, noteRoom =? where roomID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, room.getMotelID());
            stm.setString(2, room.getRoomName());
            stm.setInt(3, room.getNumberOfPeopleMax());
            stm.setInt(4, room.getNumberOfPeopleCurrent());
            stm.setDouble(5, room.getPrice());
            stm.setDouble(6, room.getArea());
            stm.setString(7, room.getNote());
            stm.setInt(8, room.getRoomID());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    //get roomID by motelID
    public ArrayList<Integer> getRoomidByMotelid(int motelId) {
        try {
            String sql = "select roomID from Room where motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, motelId);
            ResultSet rs = stm.executeQuery();
            ArrayList<Integer> roomIDs = new ArrayList<>();
            while (rs.next()) {
                roomIDs.add(rs.getInt(1));
            }
            return roomIDs;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public int getRoomIdByBookingId(int bookingId) {
        try {
            String sql = " select RoomID\n"
                    + "   from Booking\n"
                    + "   where BookingID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, bookingId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
               return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    // get all room 
    public ArrayList<Room> getAllRooms() {
        try {
            String sql = "select * from Room";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            ArrayList<Room> rooms = new ArrayList<>();

            while (rs.next()) {
                Room room = new Room(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getDouble(6), rs.getDouble(7), rs.getString(8));
                rooms.add(room);
            }

            return rooms;
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    //get room by motelID
    public ArrayList<Room> getAllRoomByMotelid(int motelId) {
        String query = "select * from Room where motelID = ?";

        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, motelId);
            ResultSet rs = stm.executeQuery();
            ArrayList<Room> roomList = new ArrayList<>();

            while (rs.next()) {
                Room r = new Room();
                r.setRoomID(rs.getInt(1));
                r.setMotelID(rs.getInt(2));
                r.setRoomName(rs.getString(3));
                r.setNumberOfPeopleMax(rs.getInt(4));
                r.setNumberOfPeopleCurrent(rs.getInt(5));
                r.setPrice(rs.getDouble(6));
                r.setArea(rs.getDouble(7));
                r.setNote(rs.getString(8));
                r.setIsDelete(rs.getBoolean(9));

                roomList.add(r);
            }
            return roomList;
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<RoomAllInfor> getAllRoomOfMotelFromAccountID(ArrayList<Integer> listMotelID) {
        try {
            String sql = "select Boo.BookingID, Mot.motelID, Mot.motelName, Roo.roomID, Roo.roomName, Ac.AccountID, Pro.profileID, Pro.fullName, Roo.price, Roo.noteRoom, Boo.startAt, Boo.endAt, Roo.numberOfPeopleMax, Roo.numberOfPeopleCurrent, Roo.area from Profile as Pro\n"
                    + "                    full join Account as Ac on Pro.AccountID = Ac.AccountID\n"
                    + "                    full join AccountOfBooking as AcO on Ac.AccountID = AcO.AccountID\n"
                    + "                    full join Booking as Boo on AcO.BookingID = Boo.BookingID\n"
                    + "                    full join Room as Roo on Boo.RoomID = Roo.roomID\n"
                    + "                    full join Motel as Mot on Roo.motelID = Mot.motelID\n"
                    + "					where Mot.motelID = ? and Boo.BookingID in (select BookingID from Booking)";
            System.out.println(sql);
            PreparedStatement st = connection.prepareStatement(sql);
            ArrayList<RoomAllInfor> result = new ArrayList<>();
            for (int i = 0; i < listMotelID.size(); i++) {
                st.setInt(1, listMotelID.get(i));
                ResultSet rs = st.executeQuery();
                int bookingID = -1;
                while (rs.next()) {
                    if (rs.getInt("BookingID") != bookingID) {
                        bookingID = rs.getInt("BookingID");
                        ArrayList<Integer> listAccountID = new ArrayList<>();
                        ArrayList<Integer> listProfileID = new ArrayList<>();
                        ArrayList<String> listAccountName = new ArrayList<>();
                        listAccountID.add(rs.getInt("AccountID"));
                        listProfileID.add(rs.getInt("ProfileID"));
                        listAccountName.add(rs.getString("fullName"));
                        RoomAllInfor bookingAllInfor = new RoomAllInfor(rs.getInt("BookingID"), rs.getInt("MotelID"), rs.getString("motelName"), rs.getInt("roomID"),
                                rs.getString("RoomName"), listAccountID, listProfileID, listAccountName, rs.getInt("price"), rs.getString("noteRoom"), false,
                                rs.getDate("startAt"), rs.getDate("endAt"), rs.getInt("numberOfPeopleMax"), rs.getInt("numberOfPeopleCurrent"), rs.getDouble("area"));

                        result.add(bookingAllInfor);
                    } else {
                        result.get(result.size() - 1).addAccountIDToList(rs.getInt("AccountID"));
                        result.get(result.size() - 1).addProfileIDToList(rs.getInt("ProfileID"));
                        result.get(result.size() - 1).addAccountNameToList(rs.getString("fullName"));
                        if (result.get(result.size() - 1).getStartAt().after(rs.getDate("startAt"))) {
                            result.get(result.size() - 1).setStartAt(rs.getDate("startAt"));
                        }
                        if (result.get(result.size() - 1).getEndAt().before(rs.getDate("endAt"))) {
                            result.get(result.size() - 1).setEndAt(rs.getDate("endAt"));
                        }
                    }
                }
            }
            return result;
        } catch (Exception e) {
            return null;
        }
    }
    
    public ArrayList<Room> getAllListRoomDoNotHaveInBooking(int accountID, int motelID){
        try {
            String sql0 = "select motelID from Motel where AccountID = ?";
            PreparedStatement st0 = connection.prepareStatement(sql0);
            st0.setInt(1, accountID);
            System.out.println(sql0);
            ArrayList<Integer> listMotelID = new ArrayList<>();
            ResultSet rs0 = st0.executeQuery();
            while(rs0.next()){
                listMotelID.add(rs0.getInt(1));
            }
            System.out.println(listMotelID.size());
            String sql1 = "select roomID from Room ";
            if(listMotelID.isEmpty()){
                sql1 = sql1 + "where roomID = 0";
            }
            else{
                sql1 = sql1 + "where motelID in (";
                for(int i=0; i<listMotelID.size(); i++){
                    if(i == listMotelID.size() - 1){
                        sql1 = sql1 + "?)";
                    }
                    else{
                        sql1 += "?,";
                    }
                }
            }
            System.out.println(sql1);
            PreparedStatement st1 = connection.prepareStatement(sql1);
            for(int i = 0 ; i < listMotelID.size(); i++){
                st1.setInt(i+1, listMotelID.get(i));
            }
            ResultSet rs1 = st1.executeQuery();
            ArrayList<Integer> listRoomIDInMotel = new ArrayList<>();
            while(rs1.next()){
                listRoomIDInMotel.add(rs1.getInt(1));
            }
            System.out.println(listRoomIDInMotel.size());
            String sql2 = "select roomID from Room ";
            if(listMotelID.isEmpty()){
                sql2 = sql2 + "where roomID = 0";
            }
            else{
                sql2 = sql2 + "where motelID not in (";
                for(int i=0; i<listMotelID.size(); i++){
                    if(i == listMotelID.size() - 1){
                        sql2 = sql2 + "?)";
                    }
                    else{
                        sql2 += "?,";
                    }
                }
            }
            System.out.println(sql2);
            PreparedStatement st2 = connection.prepareStatement(sql2);
            for(int i = 0 ; i < listMotelID.size(); i++){
                st2.setInt(i+1, listMotelID.get(i));
            }
            ResultSet rs2 = st2.executeQuery();
            ArrayList<Integer> listRoomIDNotInMotel = new ArrayList<>();
            while(rs2.next()){
                listRoomIDNotInMotel.add(rs2.getInt(1));
            }
            System.out.println(listRoomIDNotInMotel.size());
            String sql3 = "select RoomID from Booking ";
            if(listRoomIDInMotel.isEmpty()){
                sql3 = sql3 + "where RoomID = 0";
            }
            else{
                sql3 = sql3 + "where RoomID in (";
                for(int i=0; i<listRoomIDInMotel.size();i++){
                    if(i == listRoomIDInMotel.size() - 1){
                        sql3 = sql3 + "?)";
                    }
                    else{
                        sql3 += "?,";
                    }
                }
            }
            System.out.println(sql3);
            PreparedStatement st3 = connection.prepareStatement(sql3);
            for(int i=0;i<listRoomIDInMotel.size();i++){
                st3.setInt(i+1, listRoomIDInMotel.get(i));
            }
            ResultSet rs3 = st3.executeQuery();
            ArrayList<Integer> listRoomID = new ArrayList<>();
            while(rs3.next()){
                listRoomID.add(rs3.getInt(1));
            }
            for(int i=0 ;i<listRoomIDNotInMotel.size();i++){
                listRoomID.add(listRoomIDNotInMotel.get(i));
            }
            System.out.println(listRoomID.size());
            String sql4 = "select roomID,r.motelID,roomName,numberOfPeopleMax,numberOfPeopleCurrent,price,area,noteRoom,isDeleteRoom from Room r join Motel m on m.motelID = r.motelID "
                    + "where r.motelID = ? ";
            if(listRoomID.isEmpty()){
                sql4 = sql4 + "and roomID = 0";
            }
            else{
                sql4 = sql4 + "and roomID not in (";
                for (int i = 0; i < listRoomID.size(); i++) {
                    if (i == listRoomID.size()-1) {
                        sql4 = sql4 + "?)";
                    } else {
                        sql4 += "?,";
                    }
                }
            }
            System.out.println(sql4);
            PreparedStatement st4 = connection.prepareStatement(sql4);
            st4.setInt(1, motelID);
            for(int i = 0; i<listRoomID.size();i++){
                st4.setInt(i+2, listRoomID.get(i));
            }
            ResultSet rs4 = st4.executeQuery();
            ArrayList<Room> listRoom = new ArrayList<>();
            while(rs4.next()){
                Room room = new Room(rs4.getInt(1), rs4.getInt(2), rs4.getString(3), rs4.getInt(4), rs4.getInt(5), rs4.getDouble(6), rs4.getDouble(7), rs4.getString(8), rs4.getBoolean(9));
                listRoom.add(room);
            }
            System.out.println(listRoom.size());
            return listRoom;
        } catch (SQLException e) {
            System.out.println("Loi ne");
            return null;
        }
        
    }


    public static void main(String[] args) {
        RoomDAO DBR = new RoomDAO();
        ArrayList<RoomAllInfor> listRoom = new ArrayList<>();
        ArrayList<Integer> listMotelID = new ArrayList<>();
        listMotelID.add(1);
//        listMotelID.add(3);
        listRoom = DBR.getAllRoomOfMotelFromAccountID(listMotelID);
        System.out.println(listRoom.get(10).getStartAt());
        //ArrayList<Room> l = new ArrayList<>();
        //l = DBR.getAllListRoomDoNotHaveInBooking(2,1);
        
        
    }
}
