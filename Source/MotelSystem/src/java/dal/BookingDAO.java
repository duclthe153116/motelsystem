/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Booking;
import model.BookingAllInfor;
import model.BookingDetails;
import model.Profile;
import model.Services;

/**
 *
 * @author Admin
 */
public class BookingDAO extends DBContext {

    public boolean createBooking(int roomId) {
        String query = "insert into Booking(RoomID,isDeleteBooking,isDraft) values(?,0,0)";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, roomId);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public int getLastestBookingId() {
        String query = "select MAX(BookingID)\n"
                + "from Booking";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public int getDraftServiceOfBooking() {
        String query = "select MAX(BookingID)\n"
                + "from Booking";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    //get BookingID by roomID
    public ArrayList<Integer> getBookingidByRoomid(int roomid) {
        try {
            String sql = "select BookingID from Booking where RoomID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, roomid);
            ResultSet rs = stm.executeQuery();
            ArrayList<Integer> bookingIDs = new ArrayList<>();
            while (rs.next()) {
                bookingIDs.add(rs.getInt(1));
            }
            return bookingIDs;
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    //get BookingID by roomID

    public ArrayList<BookingAllInfor> getAllBookingAllInforOfUserHost(int accountID) {
        try {

            String sql = "select Boo.BookingID, Mot.motelID, Mot.motelName, Roo.roomID, Roo.roomName, Ac.AccountID, Pro.profileID, Pro.fullName, Roo.price, Roo.noteRoom, Boo.isDeleteBooking, Boo.startAt, Boo.endAt from Profile as Pro\n"
                    + "join Account as Ac on Pro.AccountID = Ac.AccountID\n"
                    + "join AccountOfBooking as AcO on Ac.AccountID = AcO.AccountID\n"
                    + "join Booking as Boo on AcO.BookingID = Boo.BookingID\n"
                    + "join Room as Roo on Boo.RoomID = Roo.roomID\n"
                    + "join Motel as Mot on Roo.motelID = Mot.motelID\n"
                    + "where Mot.motelID in (select MotelID  from Motel where AccountID = ? and motelStatus = 'publish' and isDeleteMotel = 0) and Boo.isDeleteBooking = 0\n"
                    + "order by Boo.BookingID";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, accountID);
            ResultSet rs = stm.executeQuery();
            ArrayList<BookingAllInfor> result = new ArrayList<>();
            int bookingID = -1;
            while (rs.next()) {
                if (rs.getInt("BookingID") != bookingID) {
                    bookingID = rs.getInt("BookingID");
                    ArrayList<Integer> listAccountID = new ArrayList<>();
                    ArrayList<Integer> listProfileID = new ArrayList<>();
                    ArrayList<String> listAccountName = new ArrayList<>();
                    listAccountID.add(rs.getInt("AccountID"));
                    listProfileID.add(rs.getInt("ProfileID"));
                    listAccountName.add(rs.getString("fullName"));
                    BookingAllInfor bookingAllInfor = new BookingAllInfor(rs.getInt("BookingID"), rs.getInt("MotelID"), rs.getString("motelName"), rs.getInt("roomID"),
                            rs.getString("RoomName"), listAccountID, listProfileID, listAccountName, rs.getInt("price"), rs.getString("noteRoom"), rs.getBoolean("isDeleteBooking"),
                            rs.getDate("startAt"), rs.getDate("endAt"));

                    result.add(bookingAllInfor);
                } else {
                    result.get(result.size() - 1).addAccountIDToList(rs.getInt("AccountID"));
                    result.get(result.size() - 1).addProfileIDToList(rs.getInt("ProfileID"));
                    result.get(result.size() - 1).addAccountNameToList(rs.getString("fullName"));
                    if (result.get(result.size() - 1).getStartAt().after(rs.getDate("startAt"))) {
                        result.get(result.size() - 1).setStartAt(rs.getDate("startAt"));
                    }
                    if (result.get(result.size() - 1).getEndAt().before(rs.getDate("endAt"))) {
                        result.get(result.size() - 1).setEndAt(rs.getDate("endAt"));
                    }
                }
            }
            return result;
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public BookingDetails getBookingDetails(int bookingID) {
        try {
            String sql = "select Boo.BookingID, Mot.motelID, Mot.motelName, Roo.roomID, Roo.roomName, Ac.AccountID, Pro.profileID,Pro.imageProfile, Pro.fullName, Pro.DOB, Pro.Gender, Pro.address, pro.phone, Roo.price, Roo.noteRoom, Boo.isDeleteBooking, Se.servicesID, Se.servicesName, Se.price as PriceServices, Boo.startAt, Boo.endAt from Profile as Pro\n"
                    + "full join Account as Ac on Pro.AccountID = Ac.AccountID\n"
                    + "full join AccountOfBooking as AcO on Ac.AccountID = AcO.AccountID\n"
                    + "full join Booking as Boo on AcO.BookingID = Boo.BookingID\n"
                    + "full join Room as Roo on Boo.RoomID = Roo.roomID\n"
                    + "full join Motel as Mot on Roo.motelID = Mot.motelID\n"
                    + "full join ServicesOfBooking as SerO on SerO.BookingID = Boo.BookingID\n"
                    + "full join Services as Se on SerO.servicesID = Se.servicesID where Boo.BookingID = ?";
            System.out.println(sql);
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, bookingID);
            ResultSet rs = stm.executeQuery();
            BookingDetails bookingDetails = new BookingDetails();
            int profileID = -1;
            boolean checkNew = true;
            Map<Integer, Boolean> flag = new HashMap<Integer, Boolean>();
            int firtProfileID = -1;
            while (rs.next()) {
                if (checkNew) {
                    checkNew = false;
                    firtProfileID = rs.getInt("profileID");
                    bookingDetails.setBookingID(rs.getInt("bookingID"));
                    bookingDetails.setMotelID(rs.getInt("motelID"));
                    bookingDetails.setMotelName(rs.getString("motelName"));
                    bookingDetails.setRoomID(rs.getInt("roomID"));
                    bookingDetails.setRoomName(rs.getString("roomName"));
                    bookingDetails.setPrice(rs.getInt("price"));
                    bookingDetails.setNoteRoom(rs.getString("noteRoom"));
                    bookingDetails.setStartAt(rs.getDate("startAt"));
                    bookingDetails.setEndAt(rs.getDate("endAt"));
                }
                if (flag.get(rs.getInt("profileID")) == null) {
                    Profile profile = new Profile();
                    profile.setProfileID(rs.getInt("profileID"));
                    profile.setFullName(rs.getString("fullName"));
                    profile.setdOB(rs.getDate("DOB"));
                    profile.setGender(rs.getString("Gender"));
                    profile.setAddress(rs.getString("address"));
                    profile.setPhone(rs.getString("phone"));
//                    profile.setImageProfile(rs.getString("imageProfile"));
                    Blob b = rs.getBlob("imageProfile");
                    if (b != null) {
                        InputStream is = b.getBinaryStream();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        byte[] buffer = new byte[4096];
                        int bytesRead = -1;
                        try {
                            while ((bytesRead = is.read(buffer)) != -1) {
                                baos.write(buffer, 0, bytesRead);
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        byte[] imageBytes = baos.toByteArray();
                        String img = Base64.getEncoder().encodeToString(imageBytes);
                        profile.setImageProfile(img);
                    }

                    profile.setAccountID(rs.getInt("AccountID"));
                    bookingDetails.addListProfile(profile);
                    flag.put(rs.getInt("profileID"), true);
                    if (bookingDetails.getStartAt().after(rs.getDate("startAt"))) {
                        bookingDetails.setStartAt(rs.getDate("startAt"));
                    }
                    if (bookingDetails.getEndAt().before(rs.getDate("endAt"))) {
                        bookingDetails.setEndAt(rs.getDate("endAt"));
                    }
                }
                if (rs.getInt("profileID") == firtProfileID) {
                    Services services = new Services();
                    services.setServicesID(rs.getInt("servicesID"));
                    services.setServicesName(rs.getString("servicesName"));
                    services.setPrice(rs.getInt("PriceServices"));
                    bookingDetails.addListServices(services);
                }
            }
            return bookingDetails;
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void addManyNewCustomerForRoom(int idRoom, int[] idAccount, String startAT, String endAt) {
        for (int i = 0; i < idAccount.length; i++) {
            try {
                String sql2 = "select top(1) BookingID from Booking\n"
                        + "where RoomID = ?\n"
                        + "order by BookingID desc";
                PreparedStatement st2 = connection.prepareStatement(sql2);
                st2.setInt(1, idRoom);
                ResultSet rs2 = st2.executeQuery();
                int bookingID = 0;
                if (rs2.next()) {
                    bookingID = rs2.getInt("BookingID");
                    System.out.println(bookingID);
                }
                String sql3 = "insert into AccountOfBooking(AccountID,BookingID,startAt,endAt,isDeleteAccountOfBooking) values (?,?,?,?,0)";
                PreparedStatement st3 = connection.prepareStatement(sql3);
                st3.setInt(1, idAccount[i]);
                st3.setInt(2, bookingID);
                st3.setString(3, startAT);
                st3.setString(4, endAt);
                st3.executeUpdate();

                String sql4 = "update Room\n"
                        + "set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = ?)\n"
                        + "where roomID = ?";
                PreparedStatement st4 = connection.prepareStatement(sql4);
                st4.setInt(1, bookingID);
                st4.setInt(2, idRoom);
                st4.executeUpdate();
            } catch (Exception e) {
            }
        }
    }

    public int getNumberOfMaximumUserCanAddToBooking(int bookingID) {
        int num = -1;
        try {
            String sql = "select (numberOfPeopleMax-numberOfPeopleCurrent) as numberAddMax from Room rm left join Booking bk on bk.RoomID = rm.roomID where BookingID = ?;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bookingID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                num = rs.getInt("numberAddMax");
            }
            return num;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public List<Profile> getListFreeCustomerOfBooking(int bookingID) {
        List<Profile> list = new ArrayList<>();
        try {
            String sql = "select pf.AccountID, pf.fullName from Profile pf\n"
                    + "left join RoleOfAccount roa on roa.AccountID = pf.AccountID\n"
                    + "where pf.isDeleteProfile=0 and roa.RoleID = 3 and pf.AccountID not in (select AccountID\n"
                    + "from AccountOfBooking where BookingID = ? and isDeleteAccountOfBooking = 0);";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bookingID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Profile pf = new Profile();
                pf.setAccountID(rs.getInt("AccountID"));
                pf.setFullName(rs.getString("fullName"));
                list.add(pf);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Services> getListServicesOfBooking(int bookingID) {
        List<Services> list = new ArrayList<>();
        try {
            String sql = "select servicesID, servicesName, price from Services\n"
                    + "where servicesID not in (select servicesID from ServicesOfBooking where BookingID = ?)\n"
                    + "and motelID in (select motelID from Room rm left join Booking bk on bk.RoomID = rm.roomID where BookingID = ?)\n"
                    + "and isDeleteServiecs = 0;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bookingID);
            ps.setInt(2, bookingID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Services sv = new Services();
                sv.setServicesID(rs.getInt(1));
                sv.setServicesName(rs.getString(2));
                sv.setPrice(rs.getDouble(3));
                list.add(sv);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getRoomNote(int bookingID) {
        try {
            String sql = "select rm.noteRoom from Room rm left join Booking bk on bk.RoomID = rm.roomID where bk.BookingID = ?;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, bookingID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                sql = rs.getString("noteRoom");
            }
            return sql;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public boolean deleteDraftgBooking() {
        try {
            String sql = "delete Booking where isDraft = 1";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean updateBooking(String listAccDelete, String listServiceDelete, String[] listUser, String[] listService, String bookingID, String start, String end) {
        try {
            connection.setAutoCommit(false);
            PreparedStatement ps = null;
            if (!listAccDelete.equals("nothing")) {
                for (String string : listAccDelete.split("-")) {
                    String deleteAcc = "update AccountOfBooking set isDeleteAccountOfBooking = 1 where AccountID = ? and BookingID = ?;";
                    ps = connection.prepareStatement(deleteAcc);
                    ps.setInt(1, Integer.parseInt(string));
                    ps.setInt(2, Integer.parseInt(bookingID));
                    ps.executeUpdate();
                }
                ps = connection.prepareStatement("update Room set numberOfPeopleCurrent = (select (" + listAccDelete.split("-").length + " - numberOfPeopleCurrent) from Room r left join Booking b on b.RoomID = r.roomID where BookingID = ?)\n"
                        + "where roomID = (select roomID from Booking where BookingID = ?);");
                ps.executeUpdate();
            }
            if (!listServiceDelete.equals("nothing")) {
                for (String string : listServiceDelete.split("-")) {
                    String deleteSv = "update ServicesOfBooking set isDeleteServicesOfBooking = 1 where servicesID = ? and BookingID = ?;";
                    ps = connection.prepareStatement(deleteSv);
                    ps.setInt(1, Integer.parseInt(string));
                    ps.setInt(2, Integer.parseInt(bookingID));
                    ps.executeUpdate();
                }
            }
            if (listUser != null) {
                for (String string : listUser) {
                    String addAccountOfBooking = "insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (?,?,?,?);";
                    ps = connection.prepareStatement(addAccountOfBooking);
                    ps.setInt(1, Integer.parseInt(string));
                    ps.setInt(2, Integer.parseInt(bookingID));
                    ps.setString(3, start);
                    ps.setString(4, end);
                    ps.executeUpdate();
                }
                ps = connection.prepareStatement("update Room set numberOfPeopleCurrent = (select " + listUser.length + " +numberOfPeopleCurrent from Room r left join Booking b on b.RoomID = r.roomID where BookingID = ?)\n"
                        + "where roomID = (select roomID from Booking where BookingID = ?);");
                ps.executeUpdate();
            }
            if (listService != null) {
                for (String string : listService) {
                    String addServiceOfBooking = "insert into ServicesOfBooking(servicesID, BookingID) values(?, ?)";
                    ps = connection.prepareStatement(addServiceOfBooking);
                    ps.setInt(1, Integer.parseInt(string));
                    ps.setInt(2, Integer.parseInt(bookingID));
                    ps.executeUpdate();
                }
            }
            connection.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return false;
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean deleteBookingByBillId(int billId) {
        try {
            String sql = "update Booking set isDeleteBooking = 1 where BookingID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean deleteBooking(int bookingID) {
        try {
            connection.setAutoCommit(false);
            String deleteAccount = "update AccountOfBooking set isDeleteAccountOfBooking = 1 where BookingID = ?;";
            PreparedStatement ps = connection.prepareStatement(deleteAccount);
            ps.setInt(1, bookingID);
            ps.executeUpdate();
            String deleteService = "update ServicesOfBooking set isDeleteServicesOfBooking = 1 where BookingID = ?;";
            ps = connection.prepareStatement(deleteService);
            ps.setInt(1, bookingID);
            ps.executeUpdate();
            String deleteBooking = "update Booking set isDeleteBooking = 1 where BookingID = ?;";
            ps = connection.prepareStatement(deleteBooking);
            ps.setInt(1, bookingID);
            ps.executeUpdate();
            connection.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return false;
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean createDraftBooking(int roomId) {
        String query = "insert into Booking(RoomID,isDeleteBooking,isDraft) values(?,0,1)";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, roomId);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public int getDraftBookingId() {
        String query = "select BookingID from Booking where isDraft = 1";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public boolean updateDraftBooking() {
        try {
            String sql = "update Booking set isDraft = 0 where isDraft = 1";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public List<BookingAllInfor> getListBookingForCustomer(int cusID) {
        List<BookingAllInfor> list = new ArrayList<>();
        try {
            String sql = "select aob.BookingID, r.roomID, r.roomName, m.motelID, m.motelName, aob.startAt, aob.endAt, r.price, m.isDeleteMotel \n"
                    + "from AccountOfBooking aob \n"
                    + "left join Booking b on b.BookingID = aob.BookingID\n"
                    + "left join Room r on r.roomID = b.BookingID\n"
                    + "left join Motel m on m.motelID = r.motelID\n"
                    + "where aob.AccountID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, cusID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                BookingAllInfor b = new BookingAllInfor();
                b.setBookingID(rs.getInt(1));
                b.setRoomID(rs.getInt(2));
                b.setRoomName(rs.getString(3));
                b.setMotelID(rs.getInt(4));
                b.setMotelName(rs.getString(5));
                b.setStartAt(rs.getDate(6));
                b.setEndAt(rs.getDate(7));
                b.setPrice(rs.getInt(8));
                b.setIsDeleteMotel(rs.getBoolean("isDeleteMotel"));
                list.add(b);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean createBooking(int roomID, String startAt, String endAt) {
        try {
            String sql = "insert into Booking (RoomID,isDeleteBooking,isDraft,startAt,endAt) values (?,0,0,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, roomID);
            st.setString(2, startAt);
            st.setString(3, endAt);
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public Booking getBookingByRoomID(int roomID) {
        try {
            String sql = "select BookingID,RoomID,isDeleteBooking,startAt,endAt from Booking\n"
                    + "where RoomID = ? ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, roomID);
            ResultSet rs = st.executeQuery();
            Booking b = null;
            if (rs.next()) {
                b = new Booking(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getDate(4), rs.getDate(5));
            }
            return b;
        } catch (SQLException e) {
            return null;
        }
    }

    public static void main(String[] args) {
        BookingDAO DBBK = new BookingDAO();
        String startAt_raw = "2022-07-01";
//        System.out.println(startAt_raw);
        String endAt_raw = "2022-07-31";
//        System.out.println(endAt_raw);
        int[] id = new int[2];
        id[0] = 5;
        id[1] = 6;
        DBBK.addManyNewCustomerForRoom(26, id, startAt_raw, endAt_raw);
        System.out.println(DBBK.getBookingByRoomID(1));

    }
}
