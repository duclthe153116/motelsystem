/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.Part;
import model.Account;
import model.Motel;
import model.MotelDetail;
import model.MotelHasHost;
import model.Post;

/**
 *
 * @author Admin
 */
public class MotelDAO extends DBContext {

    //get all infor motel and multiple  infor
    public ArrayList<MotelHasHost> getAllMotelHasHostHasStatus(String status) {
        try {
            String sql = "select m.motelID,m.motelName,m.isDeleteMotel,(select p.fullName from "
                        + "[Profile] as p where p.AccountID = m.AccountID) as fullName,"
                        + "m.motelPhone,m.motelAddress,m.motelCreateAt,(select COUNT(*) "
                        + "from Room as ro where ro.motelID = m.motelID) as totalRoom,"
                        + "m.motelStatus,(select a.userName from Account as a where "
                        + "a.AccountID = m.AccountID) as userName from motel as m where m.motelStatus = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, status);
            ResultSet rs = stm.executeQuery();
            ArrayList<MotelHasHost> motelHasHosts = new ArrayList<>();
            while (rs.next()) {
                MotelHasHost motelHasHost = new MotelHasHost();
                motelHasHost.setMotelID(rs.getInt("motelID"));
                motelHasHost.setMotelName(rs.getString("motelName"));
                motelHasHost.setFullName(rs.getString("fullName"));
                motelHasHost.setMotelPhone(rs.getString("motelPhone"));
                motelHasHost.setMotelAddress(rs.getString("motelAddress"));
                motelHasHost.setMotelRoomNumber(rs.getInt("totalRoom"));
                motelHasHost.setMotelStatus(rs.getString("motelStatus"));
                motelHasHost.setUserName(rs.getString("userName"));
                motelHasHost.setMotelCreateAt(rs.getDate("motelCreateAt"));
                motelHasHost.setIsDelete(rs.getBoolean("isDeleteMotel"));
                motelHasHosts.add(motelHasHost);
            }
            return motelHasHosts;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public ArrayList<MotelHasHost> getAllMotelHasHostExpectStatus(String status) {
        AccountDAO DBA = new AccountDAO();
        try {
            String sql = "select m.motelID,m.motelName,m.isDeleteMotel,(select p.fullName from "
                        + "[Profile] as p where p.AccountID = m.AccountID) as fullName,"
                        + "m.motelPhone,m.motelAddress,m.motelCreateAt,(select COUNT(*) "
                        + "from Room as ro where ro.motelID = m.motelID) as totalRoom,"
                        + "m.motelStatus,(select a.userName from Account as a where "
                        + "a.AccountID = m.AccountID) as userName, m.accountID from motel as m where m.motelStatus not like ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, status);
            ResultSet rs = stm.executeQuery();
            ArrayList<MotelHasHost> motelHasHosts = new ArrayList<>();
            while (rs.next()) {
                MotelHasHost motelHasHost = new MotelHasHost();
                motelHasHost.setMotelID(rs.getInt("motelID"));
                motelHasHost.setMotelName(rs.getString("motelName"));
                motelHasHost.setFullName(rs.getString("fullName"));
                motelHasHost.setMotelPhone(rs.getString("motelPhone"));
                motelHasHost.setMotelAddress(rs.getString("motelAddress"));
                motelHasHost.setMotelRoomNumber(rs.getInt("totalRoom"));
                motelHasHost.setMotelStatus(rs.getString("motelStatus"));
                motelHasHost.setUserName(rs.getString("userName"));
                motelHasHost.setMotelCreateAt(rs.getDate("motelCreateAt"));
                motelHasHost.setIsDelete(rs.getBoolean("isDeleteMotel"));
                motelHasHost.setIsRoleOfAccount(DBA.checkIsAccountHasRole("host",rs.getInt("accountID")));
                motelHasHosts.add(motelHasHost);
            }
            return motelHasHosts;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    // get motel by motelID (use in PostDAO)
    public MotelHasHost getMotelHasHostByMotelID(int motelID) {
        try {
            AccountDAO DBA = new AccountDAO();
            String sql = "select * from Motel where motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, motelID);
            ResultSet rs = stm.executeQuery();
            MotelHasHost motelHasHost = new MotelHasHost();
            while (rs.next()) {
                motelHasHost.setMotelID(motelID);
                motelHasHost.setAccountID(rs.getInt(2));
                motelHasHost.setMotelName(rs.getString(3));
                motelHasHost.setMotelAddress(rs.getString(4));
                motelHasHost.setMotelPhone(rs.getString(5));
                motelHasHost.setMotelStatus(rs.getString(6));
                motelHasHost.setMotelDescription(rs.getString(8));
                Account account = DBA.getAccountByAccountID(motelHasHost.getAccountID());
                motelHasHost.setUserName(account.getUsername());
            }
            return motelHasHost;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public MotelDetail getMotelDetailByMotelID(int motelID, boolean flag) throws IOException {
        MotelDAO DBM = new MotelDAO();
        try {
            String sql = "select m.motelID, m.accountID,(select p.fullName from "
                    + "[Profile] as p where p.AccountID = m.AccountID), m.motelName,\n"
                    + "(select count(*) from Room as r where r.motelID = m.motelID),"
                    + "m.motelAddress, m.motelPhone, m.motelStatus,\n"
                    + "(select MAX(r.price) from Room as r where r.motelID = m.motelID),\n"
                    + "(select MIN(r.price) from Room as r where r.motelID = m.motelID),"
                    + " m.motelDescription,m.motelImage, img.imageLink from Motel as m left "
                    + "join MotelImage as img on img.motelID = m.motelID where m.motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, motelID);
            ResultSet rs = stm.executeQuery();
            MotelDetail motelDetail = new MotelDetail();
            List<String> motelImg = new ArrayList<>();
            while (rs.next()) {
                if (motelDetail.getMotelID() == 0) {
                    motelDetail.setMotelID(motelID);
                    motelDetail.setOwnerID(rs.getInt(2));
                    motelDetail.setOwnerName(rs.getString(3));
                    motelDetail.setMotelName(rs.getString(4));
                    motelDetail.setRoomNumber(rs.getInt(5));
                    motelDetail.setNumberEmptyRoom(DBM.getNumberEmptyRoom(motelID));
                    motelDetail.setMotelAddress(rs.getString(6));
                    motelDetail.setMotelPhone(rs.getString(7));
                    motelDetail.setMotelStatus(rs.getString(8));
                    motelDetail.setMaxPrice(rs.getDouble(9));
                    motelDetail.setMinPrice(rs.getDouble(10));
                    motelDetail.setMotelDescription(rs.getString(11));
                    Blob b = rs.getBlob("motelImage");
                    InputStream is = b.getBinaryStream();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[4096];
                    int bytesRead = -1;
                    while ((bytesRead = is.read(buffer)) != -1) {
                        baos.write(buffer, 0, bytesRead);
                    }
                    byte[] imageBytes = baos.toByteArray();
                    String img = Base64.getEncoder().encodeToString(imageBytes);
                    motelDetail.setMotelImage(img);
                    if (flag == true) {
                        motelImg.add(img);
                    }
                    is.close();
                    baos.close();

                    b = rs.getBlob("imageLink");
                    is = b.getBinaryStream();
                    baos = new ByteArrayOutputStream();
                    buffer = new byte[4096];
                    bytesRead = -1;
                    while ((bytesRead = is.read(buffer)) != -1) {
                        baos.write(buffer, 0, bytesRead);
                    }
                    imageBytes = baos.toByteArray();
                    String img_verification = Base64.getEncoder().encodeToString(imageBytes);
                    motelImg.add(img_verification);
                    is.close();
                    baos.close();
                } else {
                    Blob b = rs.getBlob("imageLink");
                    InputStream is = b.getBinaryStream();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[4096];
                    int bytesRead = -1;
                    while ((bytesRead = is.read(buffer)) != -1) {
                        baos.write(buffer, 0, bytesRead);
                    }
                    byte[] imageBytes = baos.toByteArray();
                    String img = Base64.getEncoder().encodeToString(imageBytes);
                    motelImg.add(img);
                    is.close();
                    baos.close();
                }
            }
            motelDetail.setMotelImgList(motelImg);
            return motelDetail;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getMotelIDByAccountIDAndMotelName(int accountID, String motelName) {
        try {
            String sql = "select m.motelID from Motel as m where m.AccountID = ? and m.motelName = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, accountID);
            stm.setString(2, motelName);
            ResultSet rs = stm.executeQuery();
            int motelID = -1;
            while (rs.next()) {
                motelID = rs.getInt(1);
            }
            return motelID;

        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }

    }

    public int getMotelIDByAccountID(int accountID) {
        try {
            String sql = "select m.motelID from Motel as m where m.AccountID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, accountID);
            ResultSet rs = stm.executeQuery();
            int motelID = -1;
            while (rs.next()) {
                motelID = rs.getInt(1);
            }
            return motelID;

        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }

    }

    // get all motel of account
    public ArrayList<Motel> getAllMotelByAccountID(int accountID) {
        try {
            String sql = "select motelID,AccountID,motelName,motelAddress,motelPhone,motelStatus,motelImage,motelDescription,isDeleteMotel from Motel where AccountID = ? and isDeleteMotel = 0 and motelStatus = 'publish'";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, accountID);
            ResultSet rs = stm.executeQuery();
            ArrayList<Motel> motels = new ArrayList<>();
            while (rs.next()) {
                Motel motel = new Motel(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
                motels.add(motel);
            }
            return motels;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    //get motel by motelID
    public Motel getMotelByMotelID(int id) {
        try {
            String sql = "select * from Motel where motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Motel motel = new Motel(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
                return motel;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int getMotelIDByRoomID(int roomId) {
        try {
            String sql = "select motelID\n"
                    + "   from Room\n"
                    + "   where roomID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, roomId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    //delete motel when status = 1
    public boolean deleteMotel(int motelID) {
        try {
            String sql = "update Motel set isDeleteMotel = 1 where motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, motelID);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean OpenLockMotel(int motelID) {
        try {
            String sql = "update Motel set isDeleteMotel = 0 where motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, motelID);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    // update motel status
    public boolean updateMotelStatus(String motelID, String status) {
        try {
            String sql = "update Motel set motelStatus = ? where motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, status);
            stm.setString(2, motelID);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    // delete motel by motelID and status = 0
    public boolean deleteMotelNotAcceptByMotelID(int motelID) {
        try {
            String sql = "delete MotelImage where motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, motelID);
            stm.executeUpdate();
            sql = "delete Motel where motelID = ?";
            stm = connection.prepareStatement(sql);
            stm.setInt(1, motelID);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public String getNameMotelByRoomID(int roomID) {
        try {
            String sql = "select m.motelName from Motel m inner join Room r on m.motelID = r.motelID\n"
                    + "where roomID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            String s = null;
            st.setInt(1, roomID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                s = rs.getString(1);
            }
            return s;
        } catch (Exception e) {
            return null;
        }

    }

    // get all motel of account
    public ArrayList<MotelHasHost> getAllMotelHasHostByAccountID(int accountID) {
        try {
            MotelDAO DBM = new MotelDAO();
            String sql = "select m.motelID,m.motelName,m.motelAddress,m.motelPhone,m.motelStatus,\n"
                    + "(select count(*) from Room as r where r.motelID = m.motelID), \n"
                    + "(select p.fullName from [Profile] as p where p.AccountID = m.AccountID), m.isDeleteMotel\n"
                    + "from Motel as m where m.AccountID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, accountID);
            ResultSet rs = stm.executeQuery();
            ArrayList<MotelHasHost> motelHasHosts = new ArrayList<>();
            while (rs.next()) {
                MotelHasHost motelHasHost = new MotelHasHost();
                motelHasHost.setMotelID(rs.getInt(1));
                motelHasHost.setMotelName(rs.getString(2));
                motelHasHost.setMotelAddress(rs.getString(3));
                motelHasHost.setMotelPhone(rs.getString(4));
                motelHasHost.setMotelStatus(rs.getString(5));
                motelHasHost.setMotelRoomNumber(rs.getInt(6));
                motelHasHost.setNumberEmptyRoom(DBM.getNumberEmptyRoom(rs.getInt(1)));
                motelHasHost.setUserName(rs.getString(7));
                motelHasHost.setIsDelete(rs.getBoolean(8));
                motelHasHosts.add(motelHasHost);
            }
            return motelHasHosts;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public int getNumberEmptyRoom(int motelID) {
        try {
            AccountDAO DBA = new AccountDAO();
            String sql = "select m.motelID,COUNT(r.roomID)- COUNT(CASE WHEN r.numberOfPeopleCurrent>0 THEN 1 END) as emptyRoom \n"
                    + "from Motel as m join Room as r on m.motelID = r.motelID where m.motelID = ? group by m.motelID";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, motelID);
            ResultSet rs = stm.executeQuery();
            int numberEmptyRoom = 0;
            while (rs.next()) {
                numberEmptyRoom = rs.getInt("emptyRoom");
            }
            return numberEmptyRoom;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }
    
    public static void main(String[] args) {
        MotelDAO m = new MotelDAO();
        System.out.println(m.getAllMotelHasHostByAccountID(2).get(0));
        System.out.println(m.getAllMotelHasHostByAccountID(2).get(1));
        System.out.println(m.getAllMotelHasHostByAccountID(2).get(2));
    }

    public boolean updateMotelAccept(String motelName, String motelPhone, String motelDescription, String motelID) {
        try {
            String sql = "update Motel set motelName = ?, motelPhone = ?, motelDescription = ? where motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, motelName);
            stm.setString(2, motelPhone);
            stm.setString(3, motelDescription);
            stm.setString(4, motelID);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean updateMotelNotAccept(String motelName, String motelPhone, String motelAddress, String motelDescription, String motelID) {
        try {
            String sql = "update Motel set motelName = ?, motelPhone = ?,motelAddress = ?, motelDescription = ? where motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, motelName);
            stm.setString(2, motelPhone);
            stm.setString(3, motelAddress);
            stm.setString(4, motelDescription);
            stm.setString(5, motelID);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean checkMotelNull(String motelID) {
        try {
            String sql = "if (select MAX(r.numberOfPeopleCurrent) from Room as r where motelID = " + motelID + ") = 0 \n"
                    + "SELECT 1 as status;\n"
                    + "ELSE IF (select MAX(r.numberOfPeopleCurrent) from Room as r where motelID = " + motelID + ") is null\n"
                    + "SELECT 1 as status;\n"
                    + "ELSE\n"
                    + "SELECT 0 as status;";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            boolean flag = false;
            while (rs.next()) {
                flag = rs.getBoolean(1);
            }
            return flag;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean deleteMotelNullByMotelID(String motelID) {
        try {
            String sql = "delete PostImage where postID in (select p.postID from Post as p where p.motelID = ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, motelID);
            stm.executeUpdate();
            sql = "delete Post where motelID = ?";
            stm = connection.prepareStatement(sql);
            stm.setString(1, motelID);
            sql = "delete MotelImage where motelID = ?";
            stm = connection.prepareStatement(sql);
            stm.setString(1, motelID);
            stm.executeUpdate();
            sql = "delete Motel where motelID = ?";
            stm = connection.prepareStatement(sql);
            stm.setString(1, motelID);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public String getEmailByMotelId(int motelId) {
        String query = "select p.email\n"
                + "from Motel as m, Account as a, Profile as p\n"
                + "where m.AccountID = a.AccountID and a.AccountID = p.AccountID\n"
                + "and m.motelID = ?";
        String email = "";
        try {
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setInt(1, motelId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                email = rs.getString("email");
            }

        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return email;
    }

    public String getMotelNameByMotelId(int motelId) {
        String query = "select motelName\n"
                + "from Motel where motelID = ?";
        String motelName = "";
        try {
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setInt(1, motelId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                motelName = rs.getString("motelName");
            }

        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return motelName;
    }
    
    public boolean insertNewMotel(int accountID, String name, String phone, String address, String description, Part item, String motelCreateAt) {
        try {
            String sql = "insert into Motel(AccountID,motelName,motelAddress,motelPhone,motelStatus,motelDescription,"
                    + "motelCreateAt,motelImage,isDeleteMotel) values(?,?,?,?,'pending',?,'" + motelCreateAt + "',?,0)";
            InputStream is = item.getInputStream();
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, accountID);
            stm.setString(2, name);
            stm.setString(3, address);
            stm.setString(4, phone);
            stm.setString(5, description);
            stm.setBlob(6, is);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } catch (IOException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean insertImgMotelVerification(Part part, int motelID) {
        try {
            PreparedStatement ps = connection.prepareStatement("insert into MotelImage(motelID,imageLink) values(?,?)");
            InputStream is = part.getInputStream();
            ps.setInt(1, motelID);
            ps.setBlob(2, is);
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getMotelIDByCustomerID(int accountID) {
        try {
            String sql = "select m.motelID from Motel as m where m.AccountID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, accountID);
            ResultSet rs = stm.executeQuery();
            int motelID = 0;
            while (rs.next()) {
                motelID = rs.getInt(1);
            }
            return motelID;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public boolean editRequestMotel(int motelID, int ownerID, String name, String phone, String address, String description, Part item, String motelCreateAt, boolean flag) {
        try {
            String sql = "";
            if (flag) {
                sql = "update Motel set motelName=?,motelAddress=?,motelPhone=?,motelDescription=?,motelStatus='pending' where motelID = ?";
            } else {
                sql = "update Motel set motelName=?,motelAddress=?,motelPhone=?,motelDescription=?,motelStatus='pending',motelImage=? where motelID = ?";
            }
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, name);
            stm.setString(2, address);
            stm.setString(3, phone);
            stm.setString(4, description);
            if (!flag) {
                InputStream is = item.getInputStream();
                stm.setBlob(5, is);
                stm.setInt(6, motelID);
            } else {
                stm.setInt(5, motelID);
            }
            stm.executeUpdate();
            return true;
        } catch (SQLException | IOException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean deleteMotelImageVerification(int motelID) {
        try {
            PreparedStatement ps = connection.prepareStatement("delete MotelImage where motelID = ?");
            ps.setInt(1, motelID);
            ps.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    
}
