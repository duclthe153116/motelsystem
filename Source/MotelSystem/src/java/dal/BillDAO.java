/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.BillByHost;
import model.BillDetail;
import model.BillOfCustomer;

/**
 *
 * @author Admin
 */
public class BillDAO extends DBContext {

    public int getTotalBillByBillId(int billId) {
        try {
            String sql = "select totalBill from Bill where billID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public int getTotalBillDTByBillDTId(int billId) {
        try {
            String sql = "select totalBillDetail\n"
                    + "from BillDetail where billDetailID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public ArrayList<BillDetail> getBillDetailsByBillIdU(int billId) {
        ArrayList<BillDetail> listBillDetail = new ArrayList<>();

        String query = "  select a.billID, a.billDetailID, a.servicesOfRoomID, a.requestID, a.quantity,\n"
                + "   a.isDeleteBillDetail, a.totalBillDetail ,b.confirmBillByHost,b.confirmBillByCustomer, a.price\n"
                + "  from BillDetail as a, Bill as b\n"
                + "  where a.billID = b.billID and a.billID  = ?";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, billId);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                if (rs.getBoolean("isDeleteBillDetail") == false) {
                    BillDetail billDT = new BillDetail();
                    billDT.setBillID(billId);
                    billDT.setBillDetailID(rs.getInt("billDetailID"));
                    billDT.setsORID(rs.getInt("servicesOfRoomID"));
                    billDT.setRequestID(rs.getInt("requestID"));
                    billDT.setQuantity(rs.getInt("quantity"));
                    billDT.setIsDelete(rs.getBoolean("isDeleteBillDetail"));
                    billDT.setTotalBillDetail(rs.getInt("totalBillDetail"));
                    billDT.setHostConfirm(rs.getBoolean("confirmBillByHost"));
                    billDT.setCusConfirm(rs.getBoolean("confirmBillByCustomer"));
                    billDT.setServicesPrice(rs.getInt("price"));

                    listBillDetail.add(billDT);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listBillDetail;
    }

    public BillDetail getBillDetailByBillDTIdUU(int billDTId) {
        String query = "select * from BillDetail where billDetailID = ?";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, billDTId);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                BillDetail billDT = new BillDetail();
                billDT.setBillID(rs.getInt("billID"));
                billDT.setBillDetailID(billDTId);
                billDT.setsORID(rs.getInt("servicesOfRoomID"));
                billDT.setRequestID(rs.getInt("requestID"));
                billDT.setQuantity(rs.getInt("quantity"));
                billDT.setIsDelete(rs.getBoolean("isDeleteBillDetail"));
                billDT.setTotalBillDetail(rs.getInt("totalBillDetail"));
                billDT.setServicesPrice(rs.getInt("price"));
                return billDT;
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public BillDetail getBillDetailByBillDTIdU(int billDTId) {
        String query = " select * from BillDetail where billDetailID = ?";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, billDTId);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                BillDetail billDT = new BillDetail();
                billDT.setBillID(billDTId);
                billDT.setBillDetailID(rs.getInt("billDetailID"));
                billDT.setsORID(rs.getInt("servicesOfRoomID"));
                billDT.setRequestID(rs.getInt("requestID"));
                billDT.setQuantity(rs.getInt("quantity"));
                billDT.setTotalBillDetail(rs.getInt("totalBillDetail"));
                billDT.setServicesPrice(rs.getInt("price"));
                billDT.setIsDelete(rs.getBoolean("isDeleteBillDetail"));
                return billDT;
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public boolean createBill(int bookingId, int totalBill) {
        try {
            String sql = "insert into Bill(BookingID,totalBill,confirmBillByHost,confirmBillByCustomer,"
                    + "dateCreateBill,isDraft,isDeleteBill,isSent)\n"
                    + " values(?,?,0,0,GETDATE(),0,0,0)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, bookingId);
            stm.setInt(2, totalBill);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public int getLastestBillId() {
        String query = "select MAX(billID)\n"
                + "from Bill";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public boolean createBillDetailSOB(int billId, int sOR, int quantity, int totalBillDetail, int price) {
        try {
            String sql = "insert into BillDetail(billID, servicesOfRoomID, quantity, "
                    + "totalBillDetail,isDeleteBillDetail,isDraft,price)\n"
                    + "			values(?,?,?,?,0,0,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.setInt(2, sOR);
            stm.setInt(3, quantity);
            stm.setInt(4, totalBillDetail);
            stm.setInt(5, price);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean createBillDetailRq(int billId, int requestId, int quantity, int totalBillDetail, int price) {
        try {
            String sql = "insert into BillDetail(billID, requestID, quantity, totalBillDetail,isDeleteBillDetail,isDraft,price)\n"
                    + "			values(?,?,?,?,0,0,?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.setInt(2, requestId);
            stm.setInt(3, quantity);
            stm.setInt(4, totalBillDetail);
            stm.setInt(5, price);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean reduceTotalBill(int billId, int totalBDT) {
        try {
            String sql = "update Bill set totalBill -= ? where billID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, totalBDT);
            stm.setInt(2, billId);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public int countBillDetail(int billId) {
        try {
            String sql = "   select COUNT(*) from BillDetail where billID = ? and isDeleteBillDetail = 0";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    //get billID by bookingID
    public ArrayList<Integer> getBillidByBookingid(int bookingID) {
        try {
            String sql = "select billID from Bill where BookingID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, bookingID);
            ResultSet rs = stm.executeQuery();
            ArrayList<Integer> billIDs = new ArrayList<>();
            while (rs.next()) {
                billIDs.add(rs.getInt(1));
            }
            return billIDs;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ArrayList<BillByHost> getAllBillsByHost(int accountId) {
        ArrayList<BillByHost> listBill = new ArrayList<>();

        String query = "select bi.billID, m.motelID, m.motelName, r.roomID, r.roomName, \n"
                + " bi.totalBill, bi.confirmBillByHost, bi.confirmBillByCustomer, \n"
                + " bi.dateOfPayment, bi.dateCreateBill, bi.isSent, bi.isDeleteBill\n"
                + " from Account as a, Motel as m, Room as r, Booking as b, Bill as bi\n"
                + " where a.AccountID = ? and a.AccountID = m.AccountID and m.motelID = r.motelID\n"
                + " and r.roomID = b.RoomID and b.BookingID = bi.BookingID "
                + "order by bi.dateCreateBill desc";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, accountId);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                if (rs.getBoolean("isDeleteBill") == false) {
                    BillByHost bill = new BillByHost();
                    bill.setConfirmByCustomer(rs.getBoolean("confirmBillByCustomer"));
                    bill.setConfirmByHost(rs.getBoolean("confirmBillByHost"));
                    bill.setBillID(rs.getInt("billID"));
                    bill.setMotelName(rs.getString("motelName"));
                    bill.setRoomName(rs.getString("roomName"));
                    bill.setTotalPriceBill(rs.getInt("totalBill"));
                    bill.setDateOfPayment(rs.getDate("dateOfPayment"));
                    bill.setDateCreate(rs.getDate("dateCreateBill"));
                    bill.setRoomID(rs.getInt("roomID"));
                    bill.setMotelID(rs.getInt("motelID"));
                    bill.setIsSent(rs.getBoolean("isSent"));
                    bill.setIsDelete(rs.getBoolean("isDeleteBill"));

                    listBill.add(bill);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listBill;
    }

    public ArrayList<BillDetail> getBillDetailsByBillId(int billId) {
        ArrayList<BillDetail> listBillDetail = new ArrayList<>();

        String query = "select bdt.billDetailID, bdt.billID, s.servicesID ,s.servicesName, s.price, bdt.quantity, bdt.totalBillDetail,bi.confirmBillByCustomer,bi.confirmBillByHost, bdt.isDeleteBillDetail\n"
                + "   from Bill as bi, BillDetail as bdt, ServicesOfBooking as sob, [Services] as s\n"
                + "   where bi.billID = ? and bi.billID = bdt.billID and bdt.servicesOfRoomID = sob.servicesOfRoomID and sob.servicesID = s.servicesID ";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, billId);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                if (rs.getBoolean("isDeleteBillDetail") == false) {
                    BillDetail billDT = new BillDetail();
                    billDT.setBillID(billId);
                    billDT.setBillDetailID(rs.getInt("billDetailID"));
                    billDT.setServicesName(rs.getString("servicesName"));
                    billDT.setServicesID(rs.getInt("servicesID"));
                    billDT.setIsDelete(rs.getBoolean("isDeleteBillDetail"));
                    billDT.setServicesPrice(rs.getInt("price"));
                    billDT.setQuantity(rs.getInt("quantity"));
                    billDT.setTotalBillDetail(rs.getInt("totalBillDetail"));
                    billDT.setHostConfirm(rs.getBoolean("confirmBillByHost"));
                    billDT.setCusConfirm(rs.getBoolean("confirmBillByCustomer"));

                    listBillDetail.add(billDT);
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listBillDetail;
    }

    public BillDetail getBillDetailByBillDTId(int billDTId) {
        String query = "select a.billDetailID, a.billID ,c.servicesID, d.servicesName, a.quantity, d.price, b.confirmBillByHost,\n"
                + "		  b.confirmBillByCustomer, a.totalBillDetail, a.isDeleteBillDetail\n"
                + "   from BillDetail as a, Bill as b, ServicesOfBooking as c, [Services] as d\n"
                + "   where a.billID = b.billID and a.servicesOfRoomID = c.servicesOfRoomID\n"
                + "   and c.servicesID = d.servicesID and billDetailID = ?";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, billDTId);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                BillDetail billDT = new BillDetail();
                billDT.setBillID(billDTId);
                billDT.setBillDetailID(rs.getInt("billDetailID"));
                billDT.setServicesName(rs.getString("servicesName"));
                billDT.setServicesID(rs.getInt("servicesID"));
                billDT.setIsDelete(rs.getBoolean("isDeleteBillDetail"));
                billDT.setServicesPrice(rs.getInt("price"));
                billDT.setQuantity(rs.getInt("quantity"));
                billDT.setTotalBillDetail(rs.getInt("totalBillDetail"));
                billDT.setHostConfirm(rs.getBoolean("confirmBillByHost"));
                billDT.setCusConfirm(rs.getBoolean("confirmBillByCustomer"));
                return billDT;
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public ArrayList<Integer> getBillDetailIdByBillId(int billId) {
        ArrayList<Integer> listBillDetailId = new ArrayList<>();

        String query = "select billDetailID from BillDetail where billID = ?";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, billId);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                listBillDetailId.add(rs.getInt(1));
            }
            return listBillDetailId;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Integer> getServiceIdByBillId(int billId) {
        ArrayList<Integer> listServicelId = new ArrayList<>();

        String query = "select servicesOfRoomID from BillDetail where billID = ?";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, billId);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                listServicelId.add(rs.getInt(1));
            }
            return listServicelId;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public int getBookingIdByBillId(int billId) {
        String query = "select BookingID from Bill where billID = ?";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, billId);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return -1;
    }

    public boolean sendBillToCustomer(int billId) {
        try {
            String sql = "update Bill set isSent = 1 where billID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean updateQuantityService(int billDTId, int quantity) {
        try {
            String sql = "update BillDetail set quantity = ? where billDetailID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, quantity);
            stm.setInt(2, billDTId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean deleteBill(int billId) {
        try {
            String sql = "update Bill set isDeleteBill = 1 where billID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean deleteBillDetailByBillId(int billId) {
        try {
            String sql = "update BillDetail set isDeleteBillDetail = 1 where billID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean deleteBillDetailByBillDTId(int billDTId) {
        try {
            String sql = "update BillDetail set isDeleteBillDetail = 1 where billDetailID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billDTId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean updateBill(int billId) {
        try {
            String sql = "update Bill set confirmBillByHost = 'paid', confirmBillByCustomer = 'paid',\n"
                    + "dateOfPayment = GETDATE() "
                    + "where billID = ? ";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean updateTotalBillByChangingQuantity(int billId) {
        try {
            String sql = "UPDATE Bill set totalBill = (select SUM(totalBillDetail) "
                    + "from BillDetail where billID = ?) where billID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.setInt(2, billId);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean updateTotalBillDT(int totalBillDT, int billDTId) {
        try {
            String sql = "update BillDetail set totalBillDetail = ? where billDetailID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, totalBillDT);
            stm.setInt(2, billDTId);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean createDraftBill(int bookingId) {
        try {
            String sql = "insert into Bill(BookingID,totalBill,confirmBillByHost,confirmBillByCustomer,"
                    + "dateCreateBill,isDraft,isDeleteBill,isSent)\n"
                    + " values(?,0,0,0,GETDATE(),1,0,0)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, bookingId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public int getDraftBillId() {
        String query = "select billID from Bill where isDraft = 1";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public int getDraftBillDTId() {
        String query = "select billID from BillDetail where isDraft = 1";
        try {
            PreparedStatement stm = connection.prepareCall(query);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public boolean createDraftBillDetail(int billId, int sOR, int quantity, int totalBillDetail) {
        try {
            String sql = "insert into BillDetail(billID, servicesOfRoomID, quantity, totalBillDetail,isDeleteBillDetail,isDraft)\n"
                    + "	 values(?,?,?,?,0,1)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.setInt(2, sOR);
            stm.setInt(3, quantity);
            stm.setInt(4, totalBillDetail);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean createBillDetail(int billId, int sOR, int quantity, int totalBillDetail) {
        try {
            String sql = "insert into BillDetail(billID, servicesOfRoomID, quantity, totalBillDetail,isDeleteBillDetail,isDraft)\n"
                    + "			values(?,?,?,?,0,0)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.setInt(2, sOR);
            stm.setInt(3, quantity);
            stm.setInt(4, totalBillDetail);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public int getTotalBill(int billId) {
        try {
            String sql = "select SUM(totalBillDetail)\n"
                    + "from BillDetail where billID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
        return -1;
    }

    public int getBillIdByBDTId(int billDTId) {
        try {
            String sql = "select billID\n"
                    + "   from BillDetail\n"
                    + "   where billDetailID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billDTId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
        return -1;
    }

    public boolean updateTotalBill(int totalBill, int billId) {
        try {
            String sql = "update Bill set totalBill += ? where billID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, totalBill);
            stm.setInt(2, billId);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean updateDraftBill() {
        try {
            String sql = "update Bill set isDraft = 0 where isDraft = 1";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean updateDraftBillDetail() {
        try {
            String sql = "update BillDetail set isDraft = 0 where isDraft = 1";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean deleteDraftBill() {
        try {
            String sql = "delete Bill where isDraft = 1";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean deleteDraftBillDetail() {
        try {
            String sql = "delete BillDetail where isDraft = 1";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean updateConfirmHost(int billId) {
        try {
            String sql = "update Bill set confirmBillByHost = 1\n"
                    + " where billID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean updateConfirmHostAndDatePayment(int billId) {
        try {
            String sql = "update Bill set dateOfPayment = GETDATE(), confirmBillByHost = 1"
                    + " where billID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean removeConfirmHost(int billId) {
        try {
            String sql = "update Bill set confirmBillByHost = 0\n"
                    + " where billID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean updateConfirmCus(int billId) {
        try {
            String sql = "update Bill set confirmBillByCustomer = 1, dateOfPayment = GETDATE()\n"
                    + "where billID = ? ";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean updateConfirmCusAndDatePayment(int billId) {
        try {
            String sql = "update Bill set confirmBillByCustomer = 1, dateOfPayment = GETDATE()\n"
                    + "where billID = ? ";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public BillByHost getBillByBillID(int billID) {
        try {
            String query = "select  a.billID, a.confirmBillByCustomer, a.confirmBillByHost, a.dateCreateBill, a.dateOfPayment, a.totalBill,\n"
                    + " c.roomName, d.motelName, c.roomID, d.motelID, a.isDeleteBill, a.isDraft, a.isSent\n"
                    + "from Bill as a, Booking as b, Room as c, Motel as d  \n"
                    + "where billID = ? and a.BookingID = b.BookingID and b.RoomID = c.roomID and c.motelID = d.motelID";
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setInt(1, billID);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                BillByHost bill = new BillByHost();
                bill.setBillID(rs.getInt(1));
                bill.setConfirmByCustomer(rs.getBoolean(2));
                bill.setConfirmByHost(rs.getBoolean(3));
                bill.setDateCreate(rs.getDate(4));
                bill.setDateOfPayment(rs.getDate(5));
                bill.setTotalPriceBill(rs.getInt(6));
                bill.setRoomName(rs.getString(7));
                bill.setMotelName(rs.getString(8));
                bill.setRoomID(rs.getInt(9));
                bill.setMotelID(rs.getInt(10));
                bill.setIsDelete(rs.getBoolean(11));
                bill.setIsDraft(rs.getBoolean(12));
                bill.setIsSent(rs.getBoolean(13));

                return bill;
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<BillOfCustomer> getBillByAccountID(int accountID) {
        ArrayList<BillOfCustomer> listBill = new ArrayList<>();
        try {
            String query = "select a.billID, a.confirmBillByHost,a.dateCreateBill, a.dateOfPayment, a.totalBill,c.roomName, e.motelName, a.isDeleteBill, a.isSent\n"
                    + "from Bill as a join Booking as b on a.BookingID = b.BookingID join Room as c on b.RoomID = c.roomID join AccountOfBooking as d on b.BookingID = d.BookingID join Motel as e on c.motelID = e.motelID\n"
                    + "where d.AccountID = ? and a.isSent = 1 and a.isDeleteBill = 0 order by a.dateCreateBill desc";
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setInt(1, accountID);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                BillOfCustomer bill = new BillOfCustomer();
                bill.setBillID(rs.getInt(1));
                bill.setConfirmByHost(rs.getBoolean(2));
                bill.setDateCreate(rs.getDate(3));
                bill.setDateOfPayment(rs.getDate(4));
                bill.setTotalBill(rs.getInt(5));
                bill.setRoomName(rs.getString(6));
                bill.setMotelName(rs.getString(7));
                bill.setIsDelete(rs.getBoolean(8));
                bill.setIsSent(rs.getBoolean(9));
                listBill.add(bill);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listBill;
    }

    public static void main(String[] args) {
        BillDAO b = new BillDAO();
        System.out.println(b.getBillByAccountID(4).get(0).getDateCreate());
    }
}
