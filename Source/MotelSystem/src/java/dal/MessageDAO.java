/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Message;

/**
 *
 * @author Admin
 */
public class MessageDAO extends DBContext {

//    public ArrayList<Message> getAllMessageReceiveByAccountID(int accountID) {
//        try {
//            ArrayList<Message> messageList = new ArrayList<>();
//            String sql = "select a.messageID, b.fullName, c.contentMessage, a.contentMessage, a.createAt\n"
//                    + "from [Message] as a join [Profile] as b on a.fromUserID = b.AccountID \n"
//                    + "left join [Message] as c on a.messageID = c.repMessage \n"
//                    + "where a.toUserID =?";
//            PreparedStatement stm = connection.prepareStatement(sql);
//            stm.setInt(1, accountID);
//            ResultSet rs = stm.executeQuery();
//            while (rs.next()) {
//                Message mess = new Message();
//                mess.setMessageID(rs.getInt(1));
//                mess.setFrom(rs.getString(2));
//                mess.setRep(rs.getString(3));
//                mess.setContent(rs.getString(4));
//                mess.setCreateAt(rs.getDate(5));
//                messageList.add(mess);
//            }
//            return messageList;
//        } catch (SQLException ex) {
//            Logger.getLogger(MessageDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return null;
//    }
    
    public ArrayList<Message> getAllMessageFromTo(int from, int to) {
        try {
            ArrayList<Message> messageList = new ArrayList<>();
            String sql = "select messageID, fromUserID, toUserID, contentMessage, createAt\n"+
                            " from [Message] where isDeleteMessage != 1 and ((fromUserID = ? and toUserID = ?)\n" +
                            "or (fromUserID = ? and toUserID = ?))\n" +
                            "order by createAt asc";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, from);
            stm.setInt(2, to);
            stm.setInt(3, to);
            stm.setInt(4, from);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Message mess = new Message();
                mess.setMessageID(rs.getInt(1));
                mess.setFrom(rs.getInt(2));
                mess.setTo(rs.getInt(3));
                mess.setContent(rs.getString(4));
                mess.setCreateAt(rs.getDate(5));
                messageList.add(mess);
            }
            return messageList;
        } catch (SQLException ex) {
            Logger.getLogger(MessageDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

//    public ArrayList<Message> getAllMessageSendByAccountID(int accountID) {
//        try {
//            ArrayList<Message> messageList = new ArrayList<>();
//            String sql = "select a.messageID, b.fullName, c.contentMessage, a.contentMessage, a.createAt\n"
//                    + "from [Message] as a inner join [Profile] as b on a.toUserID = b.AccountID\n"
//                    + "left join [Message] as c on a.messageID = c.repMessage\n"
//                    + "where a.fromUserID = ?";
//            PreparedStatement stm = connection.prepareStatement(sql);
//            stm.setInt(1, accountID);
//            ResultSet rs = stm.executeQuery();
//            while (rs.next()) {
//                Message mess = new Message();
//                mess.setMessageID(rs.getInt(1));
//                mess.setTo(rs.getString(2));
//                mess.setRep(rs.getString(3));
//                mess.setContent(rs.getString(4));
//                mess.setCreateAt(rs.getDate(5));
//                messageList.add(mess);
//            }
//            return messageList;
//        } catch (SQLException ex) {
//            Logger.getLogger(MessageDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return null;
//    }
//
    public boolean createMessage(Message mess) {
        try {
            String sql = "insert into Message(fromUserID, toUserID, contentMessage, createAt, isDeleteMessage) values(?,?,?,GETDATE(),0)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, mess.getFrom());
            stm.setInt(2, mess.getTo());
            stm.setString(3, mess.getContent());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MessageDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
//
//    public boolean RepMessage(Message mess, String rep) {
//        try {
//            String sql = "insert into Message(fromUserID, toUserID, contentMessage, createAt, repMessage) values(?,?,?,?,?)";
//            PreparedStatement stm = connection.prepareStatement(sql);
//            stm.setString(1, mess.getTo());
//            stm.setString(2, mess.getFrom());
//            stm.setString(3, rep);
//            LocalDateTime myDateObj = LocalDateTime.now();
//            DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//            String formattedDate = myDateObj.format(myFormatObj);
//            stm.setString(4, formattedDate);
//            stm.setInt(5, mess.getMessageID());
//            stm.executeUpdate();
//        } catch (SQLException ex) {
//            Logger.getLogger(MessageDAO.class.getName()).log(Level.SEVERE, null, ex);
//            return false;
//        }
//        return true;
//    }
//
//    public Message getMessageBymessageID(int messageID) {
//        try {
//            String sql = "select * from [Message] where messageID = ?";
//            PreparedStatement stm = connection.prepareStatement(sql);
//            stm.setInt(1, messageID);
//            ResultSet rs = stm.executeQuery();
//            while (rs.next()) {
//                Message mess = new Message();
//                mess.setMessageID(rs.getInt(1));
//                String from = Integer.toString(rs.getInt(2));
//                mess.setFrom(from);
//                String to = Integer.toString(rs.getInt(3));
//                mess.setTo(to);
//                mess.setContent(rs.getString(4));
//                mess.setCreateAt(rs.getDate(6));
//                return mess;
//            }
//        } catch (SQLException ex) {
//            Logger.getLogger(MessageDAO.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return null;
//    }

    public static void main(String[] args) {
        MessageDAO m = new MessageDAO();
        ArrayList<Message> me = m.getAllMessageFromTo(2, 3);
        System.out.println(me);
    }
}
