/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Motel;
import model.Request;
import model.RequestAllInfor;
import model.RequestOfRoom;
import model.Room;
import model.RoomOfAccount;

/**
 *
 * @author Admin
 */
public class RequestDAO extends DBContext {

    public boolean deleteServiceRq(int requestID) {
        try {
            String sql = "update Request set isDeleteRequest = 1 where requestID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, requestID);
            st.executeUpdate();
            return true;
        } catch (SQLException e) {
        }
        return false;
    }
    
    public ArrayList<RequestOfRoom> getAllRequestOfRoom() {
        RoomDAO DBR = new RoomDAO();
        try {
            String sql = "select * from Request";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            ArrayList<RequestOfRoom> requestOfRooms = new ArrayList<>();
            while (rs.next()) {
                RequestOfRoom requestOfRoom = new RequestOfRoom();
                requestOfRoom.setRequestID(rs.getInt(1));
                requestOfRoom.setRoomID(rs.getInt(2));
                requestOfRoom.setRoomName(DBR.getRoomByRoomID(rs.getInt(2)).getRoomName());
                requestOfRoom.setContent(rs.getString(3));
                requestOfRoom.setStatus(rs.getString(4));
                requestOfRoom.setNoteRequest(rs.getString(5));
                requestOfRoom.setCreatedAt(rs.getDate(6));
                requestOfRoom.setUpdateAt(rs.getDate(7));
                requestOfRooms.add(requestOfRoom);
            }
            return requestOfRooms;
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ArrayList<RequestOfRoom> getAllRequestOfRoomFromMotel(int motelID) {
        try {
            String sql = "select re.requestID,r.roomID,r.roomName,re.content,re.[status],re.noteRequestOfRoom, re.noteRequestOfHost,re.createdAt,re.updateAt from Room r inner join Request re on re.roomID = r.roomID\n"
                    + "where motelID = ? and isDeleteRequest = 0";
            PreparedStatement st = connection.prepareStatement(sql);
            ArrayList<RequestOfRoom> listRequestOfRoom = new ArrayList<>();
            st.setInt(1, motelID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                RequestOfRoom requestOfRoom = new RequestOfRoom();
                requestOfRoom.setRequestID(rs.getInt(1));
                requestOfRoom.setRoomID(rs.getInt(2));
                requestOfRoom.setRoomName(rs.getString(3));
                requestOfRoom.setContent(rs.getString(4));
                requestOfRoom.setStatus(rs.getString(5));
                requestOfRoom.setNoteRequest(rs.getString(6));
                requestOfRoom.setNoteRequestOfHost(rs.getString(7));
                requestOfRoom.setCreatedAt(rs.getDate(8));
                requestOfRoom.setUpdateAt(rs.getDate(9));
                listRequestOfRoom.add(requestOfRoom);
            }
            return listRequestOfRoom;
        } catch (SQLException e) {
            return null;
        }
    }

    public ArrayList<RequestOfRoom> getAllRequestfromAccountByHost(int[] motelID) {
        try {
            String sql = "select re.requestID,r.roomID,r.roomName,re.content,re.[status],re.noteRequestOfRoom, re.noteRequestOfHost,re.createdAt,re.updateAt,re.priceRequest from Room r inner join Request re on re.roomID = r.roomID\n"
                    + "where motelID = ? and re.isDeleteRequest = 0";
            if (motelID.length >= 2) {
                for (int i = 1; i < motelID.length; i++) {
                    sql += "or motelID = ? ";
                }
            }
            System.out.println(sql);
            PreparedStatement st = connection.prepareStatement(sql);
            ArrayList<RequestOfRoom> listRequestOfRoom = new ArrayList<>();
            for (int j = 0; j < motelID.length; j++) {
                st.setInt(j + 1, motelID[j]);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                RequestOfRoom requestOfRoom = new RequestOfRoom();
                requestOfRoom.setRequestID(rs.getInt(1));
                requestOfRoom.setRoomID(rs.getInt(2));
                requestOfRoom.setRoomName(rs.getString(3));
                requestOfRoom.setContent(rs.getString(4));
                requestOfRoom.setStatus(rs.getString(5));
                requestOfRoom.setNoteRequest(rs.getString(6));
                requestOfRoom.setNoteRequestOfHost(rs.getString(7));
                requestOfRoom.setCreatedAt(rs.getDate(8));
                requestOfRoom.setUpdateAt(rs.getDate(9));
                requestOfRoom.setPriceRequest(rs.getDouble(10));
                listRequestOfRoom.add(requestOfRoom);
            }
            return listRequestOfRoom;
        } catch (SQLException e) {
            return null;
        }
    }

    public void updateRequestOfRoomByIdRequest(int requestID, String status, String noteRequest, double price) {
        try {
            String sql = "update Request set [status] = ?, noteRequestOfHost = ?, updateAt = getdate(), priceRequest = ?\n"
                    + "where requestID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, status);
            st.setString(2, noteRequest);st.setDouble(3, price);
            st.setInt(4, requestID);
            
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    public RequestAllInfor getRequestOfRoomByIdRequest(int requestID) {
        try {
            String sql = "select a.AccountID, aob.AccountOfBookingID,b.BookingID,r.roomID,m.motelID,re.requestID,m.motelName,r.roomName,re.content,re.[status],re.createdAt,re.updateAt,re.noteRequestOfRoom,re.noteRequestOfHost,re.priceRequest\n"
                    + "from Account as a join AccountOfBooking aob on a.AccountID = aob.AccountID\n"
                    + "join Booking b on aob.BookingID = b.BookingID\n"
                    + "join Room r on b.RoomID = r.roomID\n"
                    + "join Motel m on r.motelID = m.motelID\n"
                    + "join Request re on r.roomID = re.roomID\n"
                    + "where re.RequestID = ? ";
            PreparedStatement st = connection.prepareStatement(sql);

            st.setInt(1, requestID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                RequestAllInfor re = new RequestAllInfor(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getInt(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getDate(11), rs.getDate(12), rs.getString(13), rs.getString(14),rs.getDouble(15));
                return re;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void deleteRequestByIdRequest(int idRequest) {
        try {
            String sql = "update Request\n"
                    + "set isDeleteRequest = 1\n"
                    + "where requestID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idRequest);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public ArrayList<RequestAllInfor> getAllRequestFromAccountByCustomer(int idAccount) {
        try {
            String sql = "select a.AccountID, aob.AccountOfBookingID,b.BookingID,r.roomID,m.motelID,re.requestID,m.motelName,r.roomName,re.content,re.[status],re.createdAt,re.updateAt,re.noteRequestOfRoom,re.noteRequestOfHost\n"
                    + "from Account as a join AccountOfBooking aob on a.AccountID = aob.AccountID\n"
                    + "join Booking b on aob.BookingID = b.BookingID\n"
                    + "join Room r on b.RoomID = r.roomID\n"
                    + "join Motel m on r.motelID = m.motelID\n"
                    + "join Request re on r.roomID = re.roomID\n"
                    + "where a.AccountID = ? and isDeleteRequest = 0";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idAccount);
            ResultSet rs = st.executeQuery();
            ArrayList<RequestAllInfor> listRequest = new ArrayList<>();
            while (rs.next()) {
                RequestAllInfor re = new RequestAllInfor(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4), rs.getInt(5), rs.getInt(6), rs.getString(7), rs.getString(8), rs.getString(9), rs.getString(10), rs.getDate(11), rs.getDate(12), rs.getString(13), rs.getString(14));
                listRequest.add(re);
            }
            return listRequest;
        } catch (Exception e) {
        }
        return null;
    }

    public ArrayList<Motel> getAllMotelOfAccountByCustomer(int accountID) {
        try {
            String sql = "select distinct m.motelName, m.motelID\n"
                    + "from Account as a join AccountOfBooking aob on a.AccountID = aob.AccountID\n"
                    + "					 join Booking b on aob.BookingID = b.BookingID\n"
                    + "						   join Room r on b.RoomID = r.roomID\n"
                    + "						   join Motel m on r.motelID = m.motelID\n"
                    + "						   join Request re on r.roomID = re.roomID\n"
                    + "						   where a.AccountID = ? ";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accountID);
            ResultSet rs = st.executeQuery();
            ArrayList<Motel> listMotel = new ArrayList<>();
            while(rs.next()){
                Motel m = new Motel(rs.getInt(2), 0, rs.getString(1), "", "", "public");
                listMotel.add(m);
            }
            return listMotel;
        } catch (Exception e) {
        }
        return null;
    }

    public ArrayList<Room> getAllRoomFromMotelOfAccountByCustomer(int accountID, int motelID) {
        try {
            String sql = "select distinct r.roomID, r.motelID, r.roomName, r.numberOfPeopleMax, r.numberOfPeopleCurrent, r.price, r.area, r.noteRoom\n"
                    + "from Account as a join AccountOfBooking aob on a.AccountID = aob.AccountID\n"
                    + "					 join Booking b on aob.BookingID = b.BookingID\n"
                    + "						   join Room r on b.RoomID = r.roomID\n"
                    + "						   join Motel m on r.motelID = m.motelID\n"
                    + "						   join Request re on r.roomID = re.roomID\n"
                    + "						   where a.AccountID = ? and m.motelID = ? ";
            System.out.println(sql);
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(2, motelID);
            st.setInt(1, accountID);
            ResultSet rs = st.executeQuery();
            ArrayList<Room> listRoom = new ArrayList<Room>();
            while (rs.next()) {
                listRoom.add(new Room(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getInt(5), rs.getDouble(6), rs.getDouble(7), rs.getString(8)));
            }
            return listRoom;
        } catch (Exception e) {
        }
        return null;
    }

    public void addRequestByCustomer(int roomID, String content) {
        try {
            String sql = "insert into Request(roomID,content,status,createdAt,isDeleteRequest) values (?,?,'0',getDate(),0)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, roomID);
            st.setString(2, content);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public void updateRequestByCustomer(String content, String note, int requestID) {
        try {
            String sql = "update Request set content = ?, noteRequestOfRoom = ?, updateAt = getdate()\n"
                    + "where requestID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, content);
            st.setString(2, note);
            st.setInt(3, requestID);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public static void main(String[] args) {
        RequestDAO DBR = new RequestDAO();
        MotelDAO md = new MotelDAO();
        System.out.println(DBR.getRequestOfRoomByIdRequest(2));
    }

}
