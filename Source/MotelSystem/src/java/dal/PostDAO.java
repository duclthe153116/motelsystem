/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.Part;
import model.Motel;
import model.MotelHasHost;
import model.Post;
import model.PostDetail;
import model.PostImage;
import model.PostOfMotel;

/**
 *
 * @author Admin
 */
public class PostDAO extends DBContext {

    public List<PostDetail> getListPostDetailForHomepage(String motelName, String motelAddress, String order) {
        List<PostDetail> list = new ArrayList<>();
        try {
            String sql = "select pt.postID, pt.motelID, mt.motelName, pt.postTitle, pt.postDate, \n"
                    + "(select top(1) pti.postImageLink from PostImage pti where pti.postID = pt.postID) as postImage,\n"
                    + "(select max(price) from Room where motelID = pt.motelID) as maxPrice,\n"
                    + "(select min(price) from Room where motelID = pt.motelID) as minPrice\n"
                    + "from Post pt left join Motel mt on pt.motelID = mt.motelID \n"
                    + "where pt.postStatus = 'publish' and isDeletePost = 0 and mt.motelName like N'%" + motelName + "%' \n"
                    + "and upper(mt.motelAddress) like upper(N'%" + motelAddress + "%') order by pt.postDate " + order + ";";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PostDetail pd = new PostDetail();
                pd.setId(rs.getInt("postID"));
                pd.setModelID(rs.getInt("motelID"));
                pd.setMotelName(rs.getString("motelName"));
                pd.setTitle(rs.getString("postTitle"));
                pd.setpDate(rs.getDate("postDate"));
                pd.setMinPrice(rs.getDouble("minPrice"));
                pd.setMaxPrice(rs.getDouble("maxPrice"));
                Blob b = rs.getBlob("postImage");
                InputStream is = b.getBinaryStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;
                while ((bytesRead = is.read(buffer)) != -1) {
                    baos.write(buffer, 0, bytesRead);
                }
                byte[] imageBytes = baos.toByteArray();
                pd.setSpecificImage(Base64.getEncoder().encodeToString(imageBytes));
                is.close();
                baos.close();
                list.add(pd);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public String getEmailByPostID(int postID) {
        String query = "select pr.email\n"
                + "  from Post as p, Motel as m, Account as a, Profile as pr\n"
                + "  where p.motelID = m.motelID and m.AccountID = a.AccountID\n"
                + "  and a.AccountID = pr.AccountID and p.postID = ?";
        String email = "";
        try {
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setInt(1, postID);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                email = rs.getString("email");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return email;
    }

    public String getTitleByPostID(int postID) {
        String query = "select postTitle \n"
                + "from Post where postID = ?";
        String title = "";
        try {
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setInt(1, postID);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                title = rs.getString("postTitle");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return title;
    }

    //get all post
    public ArrayList<PostOfMotel> getAllPostOfMotelHasStatus(String status) {
        try {
            String sql = "select p.postID, p.postDate,p.postTitle,m.motelName,pro.fullName,"
                    + "acc.userName,p.postStatus from Post as p left join Motel as m on p.motelID = m.motelID\n"
                    + "left join [Profile] as pro on pro.AccountID = m.AccountID \n"
                    + "left join Account as acc on acc.AccountID = m.AccountID where p.isDeletePost != 1 and p.postStatus = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, status);
            ResultSet rs = stm.executeQuery();
            ArrayList<PostOfMotel> postOfMotels = new ArrayList<>();
            while (rs.next()) {
                PostOfMotel postOfMotel = new PostOfMotel();
                postOfMotel.setPostID(rs.getInt(1));
                postOfMotel.setPostDate(rs.getDate(2));
                postOfMotel.setPostTitle(rs.getString(3));
                postOfMotel.setMotelName(rs.getString(4));
                postOfMotel.setFullName(rs.getString(5));
                postOfMotel.setUserName(rs.getString(6));
                postOfMotel.setPostStatus(rs.getString(7));
                postOfMotels.add(postOfMotel);
            }
            return postOfMotels;
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ArrayList<PostOfMotel> getAllPostOfMotelExpectStatus(String status) {
        try {
            String sql = "select p.postID, p.postDate,p.postTitle,m.motelName,pro.fullName,"
                    + "acc.userName,p.postStatus from Post as p left join Motel as m on p.motelID = m.motelID\n"
                    + "left join [Profile] as pro on pro.AccountID = m.AccountID \n"
                    + "left join Account as acc on acc.AccountID = m.AccountID where p.isDeletePost != 1 and p.postStatus not like ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, status);
            ResultSet rs = stm.executeQuery();
            ArrayList<PostOfMotel> postOfMotels = new ArrayList<>();
            while (rs.next()) {
                PostOfMotel postOfMotel = new PostOfMotel();
                postOfMotel.setPostID(rs.getInt(1));
                postOfMotel.setPostDate(rs.getDate(2));
                postOfMotel.setPostTitle(rs.getString(3));
                postOfMotel.setMotelName(rs.getString(4));
                postOfMotel.setFullName(rs.getString(5));
                postOfMotel.setUserName(rs.getString(6));
                postOfMotel.setPostStatus(rs.getString(7));
                postOfMotels.add(postOfMotel);
            }
            return postOfMotels;
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ArrayList<PostOfMotel> getAllApprovedPostOfMotel() {
        try {
            MotelDAO DBM = new MotelDAO();
            String sql = "select * from Post where postStatus='publish'";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            ArrayList<PostOfMotel> postOfMotels = new ArrayList<>();
            while (rs.next()) {
                PostOfMotel postOfMotel = new PostOfMotel();
                postOfMotel.setPostID(rs.getInt(1));
                postOfMotel.setMotelID(rs.getInt(2));
                postOfMotel.setPostContent(rs.getString(3));
                postOfMotel.setPostDate(rs.getDate(4));
                postOfMotel.setPostTitle(rs.getString(5));
                postOfMotel.setPostStatus(rs.getString(6));
                MotelHasHost motelHasHost = DBM.getMotelHasHostByMotelID(postOfMotel.getMotelID());
                postOfMotel.setMotelName(motelHasHost.getMotelName());
                postOfMotel.setUserName(motelHasHost.getUserName());
                postOfMotels.add(postOfMotel);
            }
            return postOfMotels;
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ArrayList<PostOfMotel> getAllPostOfMotelByMotelName(String motelName) {
        try {
            MotelDAO DBM = new MotelDAO();
            String sql = "  select pst.postID, pst.motelID, pst.postContent, pst.postDate, pst.postTitle, pst.postStatus, mtl.motelName, pfl.fullName\n"
                    + "  from Post pst \n"
                    + "  left join Motel mtl on mtl.motelID=pst.motelID \n"
                    + "  left join Profile pfl on pfl.AccountID=mtl.AccountID\n"
                    + "  where mtl.motelName like ? and pst.postStatus='publish';";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, "%" + motelName + "%");
            System.out.println(stm);
            ResultSet rs = stm.executeQuery();
            ArrayList<PostOfMotel> postOfMotels = new ArrayList<>();
            while (rs.next()) {
                PostOfMotel postOfMotel = new PostOfMotel();
                postOfMotel.setPostID(rs.getInt(1));
                postOfMotel.setMotelID(rs.getInt(2));
                postOfMotel.setPostContent(rs.getString(3));
                postOfMotel.setPostDate(rs.getDate(4));
                postOfMotel.setPostTitle(rs.getString(5));
                postOfMotel.setPostStatus(rs.getString(6));
                postOfMotel.setMotelName(rs.getString("motelName"));
                postOfMotel.setUserName(rs.getString("fullName"));
                postOfMotels.add(postOfMotel);
            }
            return postOfMotels;
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ArrayList<PostOfMotel> getAllPostOfMotelByMotelAddress(String motelAddress, String order) {
        try {
            MotelDAO DBM = new MotelDAO();
            if (motelAddress.equals("Tất cả")) {
                motelAddress = "";
            }
            String sql = "select pst.postID, pst.motelID, pst.postContent, pst.postDate, pst.postTitle, pst.postStatus, mtl.motelName, pfl.fullName\n"
                    + "from Post pst \n"
                    + "left join Motel mtl on mtl.motelID=pst.motelID \n"
                    + "left join Profile pfl on pfl.AccountID=mtl.AccountID\n"
                    + "where upper(mtl.motelAddress) like upper(N'%" + motelAddress + "%') and pst.postStatus='publish' order by pst.postDate " + order + ";";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            ArrayList<PostOfMotel> postOfMotels = new ArrayList<>();
            while (rs.next()) {
                PostOfMotel postOfMotel = new PostOfMotel();
                postOfMotel.setPostID(rs.getInt(1));
                postOfMotel.setMotelID(rs.getInt(2));
                postOfMotel.setPostContent(rs.getString(3));
                postOfMotel.setPostDate(rs.getDate(4));
                postOfMotel.setPostTitle(rs.getString(5));
                postOfMotel.setPostStatus(rs.getString(6));
                postOfMotel.setMotelName(rs.getString("motelName"));
                postOfMotel.setUserName(rs.getString("fullName"));
                postOfMotels.add(postOfMotel);
            }
            return postOfMotels;
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    // update status post
    public boolean updatePostStatus(String postID, String status) {
        try {
            String sql = "update Post set postStatus = ? where postID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, status);
            stm.setString(2, postID);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    //delete post 
    public boolean deletePostByPostID(String postID) {
        try {
            //delete postIMG
            String sql = "delete PostImage where postID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, postID);
            stm.executeUpdate();
            //delete post
            sql = "delete Post where postID = ?";
            stm = connection.prepareStatement(sql);
            stm.setString(1, postID);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    //get PostID by MotelID
    public ArrayList<Integer> getPostidByMotelid(int motelid) {
        try {
            String sql = "select postID from Post where motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, motelid);
            ResultSet rs = stm.executeQuery();
            ArrayList<Integer> postIDs = new ArrayList<>();
            while (rs.next()) {
                postIDs.add(rs.getInt(1));
            }
            return postIDs;
        } catch (SQLException ex) {
            Logger.getLogger(BillDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public PostOfMotel getPostByID(int postID) {
        try {
            MotelDAO DBM = new MotelDAO();
            String sql = "select * from Post where postID = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, postID);
            ResultSet rs = ps.executeQuery();
            PostOfMotel pom = new PostOfMotel();
            while (rs.next()) {
                pom.setPostID(rs.getInt(1));
                pom.setMotelID(rs.getInt(2));
                pom.setPostContent(rs.getString(3));
                pom.setPostDate(rs.getDate(4));
                pom.setPostTitle(rs.getString(5));
                pom.setPostStatus(rs.getString(6));
                MotelHasHost motelHasHost = DBM.getMotelHasHostByMotelID(pom.getMotelID());
                pom.setMotelName(motelHasHost.getMotelName());
                pom.setUserName(motelHasHost.getUserName());
            }
            return pom;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Integer> getListImageIDOfPost(int postID) {
        List<Integer> list = new ArrayList<>();
        try {
            String sql = "select postImageID from PostImage where postID = ?;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, postID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(Integer.parseInt(rs.getString(1)));
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public int getMaxPriceOfMotelInPost(int motelID) {
        int max = 0;
        try {
            String sql = "select max(price) as max from Room where motelID = ?;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, motelID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt("max");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public int getMinPriceOfMotelInPost(int motelID) {
        int max = 0;
        try {
            String sql = "select min(price) as max from Room where motelID = ?;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, motelID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt("max");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public ArrayList<PostDetail> getAllPostOfHost(int ownerID) {
        try {
            String sql = "select pt.postID,pt.motelID,mt.accountID,pt.postTitle,pt.postContent,pt.postDate, pt.postStatus \n"
                    + "from Post pt left join Motel mt \n"
                    + "on pt.motelID = mt.motelID \n"
                    + "where mt.AccountID=?;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, ownerID);
            ResultSet rs = stm.executeQuery();
            ArrayList<PostDetail> posts = new ArrayList<>();
            while (rs.next()) {
                PostDetail post = new PostDetail();
                post.setId(rs.getInt("postID"));
                post.setModelID(rs.getInt("motelID"));
                post.setOwnerID(rs.getInt("accountID"));
                post.setTitle(rs.getString("postTitle"));
                post.setContent(rs.getString("postContent"));
                post.setpDate(rs.getDate("postDate"));
                post.setpStatus(rs.getString("postStatus"));
                posts.add(post);
            }
            return posts;
        } catch (SQLException ex) {
            Logger.getLogger(PostDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void addNewPost(int motelID, String postTitle, String postContent, Date postDate, int postStatus) {
        String sql = "insert into Post(motelID,postTitle,postContent,postDate,postStatus) values (?,?,?,?,?);";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, motelID);
            ps.setString(2, postTitle);
            ps.setString(3, postContent);
            ps.setString(4, (new SimpleDateFormat("yyyy-MM-dd").format(postDate)));
            ps.setInt(5, postStatus);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getInsertedPostID() {
        int number = 0;
        try {
            String sql = "select max(postID) as highestCurrentID from Post;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                number = rs.getInt("highestCurrentID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return number;
    }

    public boolean deletePostByID(int postID) {
        try {
            PreparedStatement ps = connection.prepareStatement("delete from Post where postID = ?;");
            ps.setInt(1, postID);
            if (ps.executeUpdate() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public PostDetail getPostDetailByPostID(int postID, int motelID) {
        PostDetail pd = null;
        try {
            String sql = "select pt.postID, pt.motelID, mt.AccountID, mt.motelName, pt.postTitle, pt.postContent,\n"
                    + "(select max(price) from Room where motelID = ?) as maxPrice,\n"
                    + "(select min(price) from Room where motelID = ?) as minPrice,\n"
                    + "mt.motelPhone, mt.motelAddress, pt.postDate, pt.postStatus\n"
                    + "from Post pt \n"
                    + "left join Motel mt on pt.motelID = mt.motelID\n"
                    + "where mt.motelID = ? and pt.postID = ?;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, motelID);
            ps.setInt(2, motelID);
            ps.setInt(3, motelID);
            ps.setInt(4, postID);

            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                pd = new PostDetail(rs.getInt("postID"),
                        rs.getInt("motelID"),
                        rs.getInt("accountID"),
                        rs.getString("motelName"),
                        rs.getString("postTitle"),
                        rs.getString("postContent"),
                        rs.getInt("maxPrice"),
                        rs.getInt("minPrice"),
                        rs.getString("motelPhone"),
                        rs.getString("motelAddress"),
                        rs.getDate("postDate"),
                        rs.getString("postStatus"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return pd;
    }

    public boolean updatePostByPostID(String title, String content, int postID) {
        boolean rowUpdated;
        try {
            String sql = "update Post set postTitle = ?, postContent = ?, postStatus = 'pending' where postID = ?;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(3, postID);
            ps.setString(2, content);
            ps.setString(1, title);
            rowUpdated = ps.executeUpdate() > 0;
            return rowUpdated;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public PostDetail getPostDetail(int postID) {
        PostDetail pd = new PostDetail();
        List<PostImage> listImg = new ArrayList<>();
        int pId = -1, mId = -1, aId = -1;
        String title = "", content = "", mName = "", mPhone = "", mAddress = "";
        String pStatus = "";
        double max = -1, min = -1;
        Date pDate = new Date();
        try {
            String sql = "select pt.postID, pt.motelID, mt.AccountID, mt.motelName, pt.postTitle, pt.postContent,\n"
                    + "(select max(price) from Room where motelID = pt.motelID) as maxPrice,\n"
                    + "(select min(price) from Room where motelID = pt.motelID) as minPrice,\n"
                    + "mt.motelPhone, mt.motelAddress, pt.postDate, pt.postStatus, pim.postImageID, pim.postImageLink\n"
                    + "from Post pt \n"
                    + "left join Motel mt on pt.motelID = mt.motelID\n"
                    + "left join PostImage pim on pim.postID = pt.postID\n"
                    + "where pt.postID = ?;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, postID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                pId = rs.getInt(1);
                mId = rs.getInt(2);
                aId = rs.getInt(3);
                mName = rs.getString(4);
                title = rs.getString(5);
                content = rs.getString(6);
                max = rs.getDouble(7);
                min = rs.getDouble(8);
                mPhone = rs.getString(9);
                mAddress = rs.getString(10);
                pStatus = rs.getString(12);
                pDate = rs.getDate(11);
                Blob b = rs.getBlob("postImageLink");
                InputStream is = b.getBinaryStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;
                while ((bytesRead = is.read(buffer)) != -1) {
                    baos.write(buffer, 0, bytesRead);
                }
                byte[] imageBytes = baos.toByteArray();
                String img = Base64.getEncoder().encodeToString(imageBytes);
                listImg.add(new PostImage(rs.getInt("postImageID"), img));
                is.close();
                baos.close();
            }
            pd = new PostDetail(pId, mId, aId, mName, title, content, max, min, mPhone, mAddress, pDate, pStatus);
            pd.setListImages(listImg);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return pd;
    }

    public boolean newPost(int motelID, String postTitle, String postContent, Date postDate, String postStatus, Collection<Part> parts) {
        try {
            connection.setAutoCommit(false);
            PreparedStatement ps = connection.prepareStatement("insert into Post(motelID,postTitle,postContent,postDate,postStatus) values (?,?,?,?,?);", Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, motelID);
            ps.setString(2, postTitle);
            ps.setString(3, postContent);
            ps.setString(4, (new SimpleDateFormat("yyyy-MM-dd").format(postDate)));
            ps.setString(5, postStatus);
            ps.executeUpdate();
            ResultSet tableKeys = ps.getGeneratedKeys();
            tableKeys.next();
            int newPostID = tableKeys.getInt(1);
            for (Part part : parts) {
                if (part.getContentType() != null) {
                    if (part.getContentType().contains("image")) {
                        ps = connection.prepareStatement("insert into PostImage(postID,postImageLink) values (?,?)");
                        InputStream is = part.getInputStream();
                        ps.setInt(1, newPostID);
                        ps.setBlob(2, is);
                        ps.executeUpdate();
                    }
                }
            }
            connection.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return false;
        } catch (IOException ex) {
            ex.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return false;
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean updatePost(int postID, String newTitle, String newContent, String[] listImageIDToDelete, Collection<Part> listImageAddMore) {
        try {
            connection.setAutoCommit(false);
            PreparedStatement ps = null;
            if (newTitle != null) {
                ps = connection.prepareStatement("update Post set postTitle = ? where postID = ?;");
                ps.setString(1, newTitle);
                ps.setInt(2, postID);
                ps.executeUpdate();
            }
            if (newContent != null) {
                ps = connection.prepareStatement("update Post set postContent = ? where postID = ?;");
                ps.setString(1, newContent);
                ps.setInt(2, postID);
                ps.executeUpdate();
            }
            if (!listImageIDToDelete[0].equals("nothing")) {
                for (String string : listImageIDToDelete) {
                    ps = connection.prepareStatement("delete from PostImage where postImageID = ? and postID = ?;");
                    ps.setInt(1, Integer.parseInt(string));
                    ps.setInt(2, postID);
                    ps.executeUpdate();
                }
            }
            for (Part item : listImageAddMore) {
                if (item.getContentType() != null) {
                    if (item.getContentType().contains("image")) {
                        ps = connection.prepareStatement("insert into PostImage(postID,postImageLink) values (?,?)");
                        InputStream is = item.getInputStream();
                        ps.setInt(1, postID);
                        ps.setBlob(2, is);
                        ps.executeUpdate();
                    }
                }
            }
            ps = connection.prepareStatement("update Post set postStatus = 'pending' where postID = ?;");
            ps.setInt(1, postID);
            ps.executeUpdate();
            
            connection.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return false;
        } catch (IOException ex) {
            ex.printStackTrace();
            try {
                connection.rollback();
            } catch (SQLException se) {
                se.printStackTrace();
            }
            return false;
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        PostDAO dao = new PostDAO();
    }
}
