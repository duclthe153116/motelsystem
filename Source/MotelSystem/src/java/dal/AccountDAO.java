/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Account;
import model.AccountAllInfor;
import model.Profile;
import model.Role;
import model.RoleOfAccount;

/**
 *
 * @author coder
 */
public class AccountDAO extends DBContext {

    public int getAccountIDByEmail(String email) {

        String query = "select a.AccountID \n"
                + "from Account as a, Profile as p\n"
                + " where a.AccountID = p.AccountID\n"
                + " and p.email = ?";
        int accID = -1;
        try {
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                accID = rs.getInt("AccountID");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return accID;
    }

    public ArrayList<Integer> getAccountIDByRoomId(int roomId) {

        String query = "select c.AccountID\n"
                + "from Room as a, RoomOfAccount as b, Account c\n"
                + "where a.roomID = b.roomID and b.AccountID = c.AccountID\n"
                + "and a.roomID = ?";
        ArrayList<Integer> accIdList = new ArrayList<>();

        try {
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setInt(1, roomId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                accIdList.add(rs.getInt("AccountID"));
            }
            return accIdList;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    // get all infor of account admin by username and password
    public AccountAllInfor getAccountByUsernamePasswordAndRole(String username, String password, String roleName) throws IOException {
        try {
            // get account
            String sql = "select AccountID, username, password from account where username = ? and password = ? and isDeleteAccount = 0";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, username);
            stm.setString(2, password);
            ResultSet rs = stm.executeQuery();
            Account account = null;
            while (rs.next()) {
                account = new Account();
                account.setAccountID(rs.getInt(1));
                account.setUsername(rs.getString(2));
                account.setPassword(rs.getString(3));
            }
            // if account not exitst return null
            if (account == null) {
                return (null);
            }
            // get profile by accountID
            sql = "select profileID, AccountID, fullName, phone, address, email, DOB, Gender, imageProfile from Profile where AccountID = ?";
            stm = connection.prepareStatement(sql);
            stm.setInt(1, account.getAccountID());
            Profile profile = new Profile();
            rs = stm.executeQuery();
            while (rs.next()) {
                profile.setProfileID(rs.getInt(1));
                profile.setAccountID(rs.getInt(2));
                profile.setFullName(rs.getString(3));
                profile.setPhone(rs.getString(4));
                profile.setAddress(rs.getString(5));
                profile.setEmail(rs.getString(6));
                profile.setdOB(rs.getDate(7));
                profile.setGender(rs.getString(8));
                Blob b = rs.getBlob("imageProfile");
                if (b != null) {
                    InputStream is = b.getBinaryStream();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[4096];
                    int bytesRead = -1;
                    while ((bytesRead = is.read(buffer)) != -1) {
                        baos.write(buffer, 0, bytesRead);
                    }
                    byte[] imageBytes = baos.toByteArray();
                    String img = Base64.getEncoder().encodeToString(imageBytes);
                    profile.setImageProfile(img);
                    is.close();
                    baos.close();
                }
            }
            // get roleOfAccount by where AccountID = ?";
            sql = "select  RoleOfAccountID, AccountID, RoleID  from RoleOfAccount where AccountID = ?";
            stm = connection.prepareStatement(sql);
            stm.setInt(1, account.getAccountID());
            rs = stm.executeQuery();
            ArrayList<RoleOfAccount> listRoleOfAccount = new ArrayList<>();
            ArrayList<Integer> listRoleID = new ArrayList<>();
            while (rs.next()) {
                RoleOfAccount roleOfAccount = new RoleOfAccount();
                roleOfAccount.setRoleOfAccountID(rs.getInt(1));
                roleOfAccount.setAccountID(rs.getInt(2));
                roleOfAccount.setRoleID(rs.getInt(3));
                listRoleID.add(rs.getInt(3));
                listRoleOfAccount.add(roleOfAccount);
            }
            if (listRoleOfAccount.size() == 0) {
                return null;
            }
            // get roleName by roleID
            ArrayList<Role> listRole = new ArrayList<>();
            ArrayList<String> listRoleName = new ArrayList<>();
            boolean checkRole = false;
            for (int i = 0; i < listRoleOfAccount.size(); i++) {
                sql = "select  RoleID, roleName from role where RoleID = ?";
                stm = connection.prepareStatement(sql);
                stm.setInt(1, listRoleOfAccount.get(i).getRoleID());
                rs = stm.executeQuery();

                while (rs.next()) {
                    Role role = new Role();
                    role.setRoleID(rs.getInt(1));
                    role.setRoleName(rs.getString(2));
                    if (rs.getString(2).equalsIgnoreCase(roleName)) {
                        checkRole = true;
                    }
                    listRoleName.add(rs.getString(2));
                    listRole.add(role);
                }
            }
            if (!checkRole) {
                return null;
            }
            AccountAllInfor accountAllInfor = new AccountAllInfor(account.getAccountID(), account.getUsername(), account.getPassword(), profile.getProfileID(), profile.getFullName(),
                    profile.getPhone(), profile.getAddress(), profile.getEmail(), profile.getdOB(), profile.getGender(), profile.getImageProfile(), listRoleID, listRoleName);
            return (accountAllInfor);
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    // get all information of all account
    public ArrayList<AccountAllInfor> getAllInforOfAllAccount() {
        ArrayList<AccountAllInfor> accounts = new ArrayList<>();
        String sql = "select a.AccountID,a.userName,p.phone,p.email,a.isDeleteAccount,roa.RoleID,r.roleName, roa.isDeleteRoleOfAccount from Account as a\n"
                + "left join [Profile] as p on a.AccountID = p.AccountID \n"
                + "left join RoleOfAccount as roa on a.AccountID = roa.AccountID\n"
                + "left join [Role] as r on r.RoleID = roa.RoleID\n"
                + "order by a.AccountID asc";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                AccountAllInfor account = new AccountAllInfor();
                ArrayList<String> roleNames = new ArrayList<>();
                ArrayList<Integer> roleIDs = new ArrayList<>();
                if (accounts.isEmpty() || accounts.get(accounts.size() - 1).getAccountID() != rs.getInt("AccountID")) {
                    account.setAccountID(rs.getInt("AccountID"));
                    account.setUsername(rs.getString("userName"));
                    account.setPhone(rs.getString("phone"));
                    account.setEmail(rs.getString("email"));
                    account.setIsdelete(rs.getBoolean("isDeleteAccount"));
                    boolean isdeleterole = rs.getBoolean("isDeleteRoleOfAccount");
                    if (!isdeleterole) {
                        int roleID = rs.getInt("RoleID");
                        String roleName = rs.getString("roleName");
                        roleNames.add(roleName);
                        roleIDs.add(roleID);
                    }
                    account.setRoleID(roleIDs);
                    account.setRoleName(roleNames);
                    accounts.add(account);
                } else {
                    boolean isdeleterole = rs.getBoolean("isDeleteRoleOfAccount");
                    if (!isdeleterole) {
                        accounts.get(accounts.size() - 1).getRoleID().add(rs.getInt("RoleID"));
                        accounts.get(accounts.size() - 1).getRoleName().add(rs.getString("roleName"));
                    }
                }
            }
            if (accounts.isEmpty()) {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return accounts;
    }

    //get password by email
    public String getPasswordByEmail(String email) {
        String password = "";

        try {
            // get account
            String sql = "select [passWord]\n"
                    + "from Account as a, Profile as p\n"
                    + "where p.email like ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                password = rs.getString("passWord");
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return password;
    }

    //get all email
    public ArrayList<String> getAllEmail() {
        ArrayList<String> emailList = new ArrayList<>();

        try {
            // get account
            String sql = "select email from Profile";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                emailList.add(rs.getString("email"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return emailList;
    }

    // get an account by accountID
    public Account getAccountByAccountID(int accountID) {
        try {
            String sql = "select * from Account where AccountID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, accountID);
            ResultSet rs = stm.executeQuery();
            Account account = new Account();
            while (rs.next()) {
                account.setAccountID(accountID);
                account.setUsername(rs.getString(2));
                account.setPassword(rs.getString(3));
            }
            return account;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    //Create Account
    public boolean CreateAccount(Account acc) {
        try {
            String sql = "INSERT INTO Account(userName,[passWord],isDeleteAccount) VALUES (?,?,0)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, acc.getUsername());
            stm.setString(2, acc.getPassword());
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    //Get Account by username and pass
    public Account GetAccountByUserNameAndPass(Account acc) {
        Account account = new Account();
        try {
            // get account
            String sql = "Select * from Account where username = ? and [passWord] = ? and isDeleteAccount = 0";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, acc.getUsername());
            stm.setString(2, acc.getPassword());
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                account.setAccountID(rs.getInt(1));
                account.setUsername(rs.getString(2));
                account.setPassword(rs.getString(3));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return account;
    }

    public ArrayList<String> GetAllUsername() {
        ArrayList<String> usernameList = new ArrayList<>();

        try {
            // get account
            String sql = "select username from Account where isDeleteAccount = 0";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                usernameList.add(rs.getString("username"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usernameList;
    }

    public boolean CheckUsernameExist(String usern) {
        String username = null;

        try {
            // get account
            String sql = "select username from Account where username = ? and isDeleteAccount = 0";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, usern);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                username = rs.getString("username");
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (username == null) {
            return false;
        } else {
            return true;
        }
    }

    public boolean CheckEmailExist(String email) {
        String newEmail = null;

        try {
            // get account
            String sql = "select email from Profile where email = ? and isDeleteProfile = 0";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                newEmail = rs.getString("email");
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (newEmail == null) {
            return false;
        } else {
            return true;
        }
    }

    //Create profile
    public boolean CreateProfile(Profile profile) {
        try {
            // get account
            String sql = "INSERT INTO [Profile](AccountID, fullName, DOB, Gender, [address], email, phone,isDeleteProfile)\n"
                    + "VALUES (?,?,?,?,?,?,?,0)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, profile.getAccountID());
            stm.setString(2, profile.getFullName());
            java.sql.Date sqlDate = new java.sql.Date(profile.getdOB().getTime());
            stm.setDate(3, sqlDate);
            stm.setString(4, profile.getGender());
            stm.setString(5, profile.getAddress());
            stm.setString(6, profile.getEmail());
            stm.setString(7, profile.getPhone());
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean CreateRoleOfAccount(int accountID) {
        try {
            String sql = "insert into RoleOfAccount values(?,3,0)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, accountID);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public void ChangePassword(int accountID, String newPass) {
        try {
            String sql = "update Account\n"
                    + "set [passWord] = ?\n"
                    + "where AccountID = ?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, newPass);
            st.setInt(2, accountID);
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    // get all information of account by accountId
    public AccountAllInfor getAllInforOfAccount(int accountId) throws IOException {
        AccountAllInfor accountAllInfor = new AccountAllInfor();
        ArrayList<String> roleNames = new ArrayList<>();
        ArrayList<Integer> roleIDs = new ArrayList<>();
        String sql = "select a.AccountID, p.fullName,a.userName,p.DOB,p.Gender,p.phone,p.address,\n"
                + "p.email,p.imageProfile,roa.RoleID,r.roleName from Account as a \n"
                + "left join [Profile] as p on a.AccountID = p.AccountID \n"
                + "left join RoleOfAccount as roa on a.AccountID = roa.AccountID\n"
                + "left join [Role] as r on r.RoleID = roa.RoleID\n"
                + "where roa.isDeleteRoleOfAccount != 1 and a.AccountID = ? order by r.RoleID asc";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, accountId);
            ResultSet rs = stm.executeQuery();
            int count = 0;
            while (rs.next()) {
                if (count == 0) {
                    accountAllInfor.setAccountID(rs.getInt("AccountID"));
                    accountAllInfor.setFullName(rs.getString("fullName"));
                    accountAllInfor.setUsername(rs.getString("userName"));
                    accountAllInfor.setdOB(rs.getDate("DOB"));
                    accountAllInfor.setGender(rs.getString("Gender"));
                    accountAllInfor.setPhone(rs.getString("phone"));
                    accountAllInfor.setAddress(rs.getString("address"));
                    accountAllInfor.setEmail(rs.getString("email"));
                    Blob b = rs.getBlob("imageProfile");
                    InputStream is = b.getBinaryStream();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[4096];
                    int bytesRead = -1;
                    while ((bytesRead = is.read(buffer)) != -1) {
                        baos.write(buffer, 0, bytesRead);
                    }
                    byte[] imageBytes = baos.toByteArray();
                    String img = Base64.getEncoder().encodeToString(imageBytes);
                    accountAllInfor.setImageProfile(img);
                    is.close();
                    baos.close();
                }
                int roleID = rs.getInt("RoleID");
                String roleName = rs.getString("roleName");
                roleNames.add(roleName);
                roleIDs.add(roleID);
                accountAllInfor.setRoleID(roleIDs);
                accountAllInfor.setRoleName(roleNames);
                count++;
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return accountAllInfor;
    }

    public static void main(String[] args) {
        AccountDAO DBA = new AccountDAO();
        ArrayList<AccountAllInfor> a = DBA.getAllInforOfAllAccount();
        System.out.println(a.get(4).getRoleName());
    }

    public boolean deleteAccountByAccountId(int accountId) {
        try {
            String s1 = "update Account set isDeleteAccount = 1 where AccountID = ?";
            PreparedStatement stm = connection.prepareStatement(s1);
            stm.setInt(1, accountId);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean OpenLockAccountByAccountId(int accountId) {
        try {
            String s1 = "update Account set isDeleteAccount = 0 where AccountID = ?";
            PreparedStatement stm = connection.prepareStatement(s1);
            stm.setInt(1, accountId);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean deleteAccountOfBooking(int accountId) {
        try {
            String s1 = "update AccountOfBooking set isDeleteAccountOfBooking = 1 where AccountID = ?";
            PreparedStatement stm = connection.prepareStatement(s1);
            stm.setInt(1, accountId);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean updateRoleOfAccount(int accountId, int roleId) {
        try {
            String sql = "select RoleOfAccountID from RoleOfAccount where AccountID = ? and RoleID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, accountId);
            stm.setInt(2, roleId);
            ResultSet rs = stm.executeQuery();
            int roaID = 0;
            while (rs.next()) {
                roaID = rs.getInt(1);
            }
            if (roaID == 0) {
                String sql1 = "insert into RoleOfAccount values (?,?,0)";
                stm = connection.prepareStatement(sql1);
                stm.setInt(1, accountId);
                stm.setInt(2, roleId);
                stm.executeUpdate();
                return true;
            } else {
                String s1 = "update RoleOfAccount set isDeleteRoleOfAccount = 0 where AccountID = ? and RoleID = ?";
                stm = connection.prepareStatement(s1);
                stm.setInt(1, accountId);
                stm.setInt(2, roleId);
                stm.executeUpdate();
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean deleteRoleOfAccount(int accountId, int roleId) {
        try {

            String s1 = "update RoleOfAccount set isDeleteRoleOfAccount = 1 where AccountID = ? and RoleID = ?";
            PreparedStatement stm = connection.prepareStatement(s1);
            stm.setInt(1, accountId);
            stm.setInt(2, roleId);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(MotelDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public String getFullNameOfAccountByAccountID(int accountID) {
        try {
            String sql = "select fullName from [Profile] where AccountID = ? and isDeleteProfile != 1";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, accountID);
            ResultSet rs = stm.executeQuery();
            String fullName = null;
            while (rs.next()) {
                fullName = rs.getString(1);
            }
            return fullName;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ArrayList<AccountAllInfor> getAllAccountWithProfile() {
        ArrayList<AccountAllInfor> listAccountAllInfor = new ArrayList<>();
        try {
            // get account
            String sql = "select a.AccountID, a.userName, p.fullName from Account as A join Profile as P on a.AccountID = p.AccountID "
                    + "where a.AccountID in (select AccountID from RoleOfAccount where RoleID in (select RoleID from Role where roleName != 'Admin'))";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                AccountAllInfor a = new AccountAllInfor();
                a.setAccountID(rs.getInt(1));
                a.setUsername(rs.getString(2));
                a.setFullName(rs.getString(3));
                listAccountAllInfor.add(a);
            }
            return (listAccountAllInfor);
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public boolean checkIsAccountHasRole(String roleName, int accountID) {
        try {
            String sql = "if (?) in (select r.roleName from RoleOfAccount as RoA join [Role] as r on r.RoleID = RoA.RoleID where RoA.AccountID = ?) \n"
                    + "select 1;\n"
                    + "else\n"
                    + "select 0;";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(2, accountID);
            stm.setString(1, roleName);
            ResultSet rs = stm.executeQuery();
            boolean isTrue = false;
            while (rs.next()) {
                isTrue = rs.getBoolean(1);
            }
            return isTrue;
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public ArrayList<AccountAllInfor> GetAllAccountExcept(int accountID, int accountTop) throws IOException {
        ArrayList<AccountAllInfor> accountList = new ArrayList<>();

        try {
            String sql = "select a.AccountID, a.userName, p.imageProfile from Account as a join [Profile] as p on a.AccountID = p.AccountID where a.AccountID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, accountTop);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                AccountAllInfor acc = new AccountAllInfor();
                acc.setAccountID(rs.getInt(1));
                acc.setUsername(rs.getString(2));
                Blob b = rs.getBlob("imageProfile");
                InputStream is = b.getBinaryStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;
                while ((bytesRead = is.read(buffer)) != -1) {
                    baos.write(buffer, 0, bytesRead);
                }
                byte[] imageBytes = baos.toByteArray();
                String img = Base64.getEncoder().encodeToString(imageBytes);
                acc.setImageProfile(img);
                is.close();
                baos.close();
                if (acc != null) {
                    accountList.add(acc);
                }
            }

            // get account
            String sql1 = "select a.AccountID, a.userName, p.imageProfile \n"
                    + "from Account as a join RoleOfAccount as b on a.AccountID = b.AccountID\n"
                    + "join [Profile] as p on a.AccountID = p.AccountID \n"
                    + "where a.isDeleteAccount = 0 and b.RoleID = 3 and a.AccountID != ? and a.AccountID != ?";
            stm = connection.prepareStatement(sql1);
            stm.setInt(1, accountID);
            stm.setInt(2, accountTop);
            rs = stm.executeQuery();
            while (rs.next()) {
                AccountAllInfor acc = new AccountAllInfor();
                acc.setAccountID(rs.getInt(1));
                acc.setUsername(rs.getString(2));
                Blob b = rs.getBlob("imageProfile");
                InputStream is = b.getBinaryStream();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[4096];
                int bytesRead = -1;
                while ((bytesRead = is.read(buffer)) != -1) {
                    baos.write(buffer, 0, bytesRead);
                }
                byte[] imageBytes = baos.toByteArray();
                String img = Base64.getEncoder().encodeToString(imageBytes);
                acc.setImageProfile(img);
                is.close();
                baos.close();
                accountList.add(acc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return accountList;
    }
    
    public String getEmailByAccountId(int accountId) {

        String query = "select email\n"
                + "from Account as a, Profile as b\n"
                + "where a.AccountID = b.AccountID and a.AccountID = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(query);
            stm.setInt(1, accountId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
