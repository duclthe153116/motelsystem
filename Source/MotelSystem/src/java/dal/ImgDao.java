/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.Part;

/**
 *
 * @author royal
 */
public class ImgDao extends DBContext {

    public int insertImg(Part part, int postID) {
        int result = 0;
        if (part != null) {
            try {
                PreparedStatement ps = connection.prepareStatement("insert into PostImage(postID,postImageLink) values (?,?)");
                InputStream is = part.getInputStream();
                ps.setInt(1, postID);
                ps.setBlob(2, is);
                result = ps.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public int deleteImageByImageID(int imgID) {
        int result = 0;
        try {
            PreparedStatement ps = connection.prepareStatement("delete from PostImage where postImageID = ?;");
            ps.setInt(1, imgID);
            result = ps.executeUpdate();
            return result;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return 0;
        }
    }

    public boolean deleteImageByPostID(int postID) {
        try {
            PreparedStatement ps = connection.prepareStatement("delete from PostImage where postID = ?;");
            ps.setInt(1, postID);
            if (ps.executeUpdate() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Blob getImgByPostImageID(String imgID) {
        Blob b = null;
        try {
            PreparedStatement ps = connection.prepareStatement("select * from PostImage where postImageID=?");
            ps.setString(1, imgID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                b = rs.getBlob("postImageLink");
            } else {
                System.out.println("No image found with this id.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }

    public Blob getImgByPostID(String imgID) {
        Blob b = null;
        try {
            PreparedStatement ps = connection.prepareStatement("select top(1) * from PostImage where postID=?");
            ps.setString(1, imgID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                b = rs.getBlob("postImageLink");
            } else {
                System.out.println("No image found with this id.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }

    public Blob getImgByMotelID(String motelID) {
        Blob b = null;
        try {
            PreparedStatement stm = connection.prepareStatement("select motelImage from Motel where motelID=?");
            stm.setString(1, motelID);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                b = rs.getBlob("motelImage");
            } else {
                System.out.println("No image found with this id.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b;
    }

    public boolean updateImgMotel(Part part, String motelID) {
        if (part != null) {
            try {
                PreparedStatement ps = connection.prepareStatement("update Motel set motelImage = ? where motelID= ?");
                InputStream is = part.getInputStream();
                ps.setBlob(1, is);
                ps.setString(2, motelID);
                ps.executeUpdate();
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
