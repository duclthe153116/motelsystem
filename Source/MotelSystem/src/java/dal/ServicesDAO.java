/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.BillService;
import model.Room;
import model.Services;

/**
 *
 * @author coder
 */
public class ServicesDAO extends DBContext {

    public String getServiceNameBySORId(int billDTId) {
        try {
            String sql = "select c.servicesName \n"
                    + "    from BillDetail as a, ServicesOfBooking as b, [Services] as c\n"
                    + "    where a.servicesOfRoomID = b.servicesOfRoomID and b.servicesID = c.servicesID "
                    + "and a.billDetailID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, billDTId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);

        }
        return null;
    }

    public int getServiceIDBySORId(int sORId) {
        try {
            String sql = "select c.servicesID \n"
                    + "    from BillDetail as a, ServicesOfBooking as b, [Services] as c\n"
                    + "    where a.servicesOfRoomID = b.servicesOfRoomID and b.servicesID = c.servicesID "
                    + "and a.billDetailID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, sORId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);

        }
        return -1;
    }

    public String getServiceNameFromRequest(int requestId) {
        try {
            String sql = "select content from Request where requestID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, requestId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);

        }
        return null;
    }

    public ArrayList<BillService> getAllSOBByMotelAndRoom(int motelId, int roomId) {
        try {
            String sql = "select a.servicesOfRoomID,e.servicesID ,e.servicesName,e.price ,d.motelID, c.roomID \n"
                    + "  from ServicesOfBooking as a, Booking as b, Room as c, Motel as d, [Services] as e\n"
                    + "  where a.BookingID = b.BookingID and b.RoomID = c.roomID and c.motelID = d.motelID\n"
                    + "  and a.servicesID = e.servicesID and c.roomID = ? and d.motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, roomId);
            stm.setInt(2, motelId);
            ArrayList<BillService> services = new ArrayList<>();
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                BillService service = new BillService();
                service.setsOBId(rs.getInt(1));
                service.setServiceId(rs.getInt(2));
                service.setServiceName(rs.getString(3));
                service.setPrice(rs.getInt(4));
                service.setMotelId(rs.getInt(5));
                service.setRoomId(rs.getInt(6));

                services.add(service);
            }
            return services;
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<BillService> getAllRequestByMotelAndRoom(int motelId, int roomId) {
        try {
            String sql = "select a.requestID, b.roomID, c.motelID, a.content\n"
                    + "  from Request as a, Room as b, Motel as c\n"
                    + "  where a.roomID = b.roomID and b.motelID = c.motelID\n"
                    + "  and b.roomID = ? and c.motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, roomId);
            stm.setInt(2, motelId);
            ArrayList<BillService> services = new ArrayList<>();
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                BillService service = new BillService();
                service.setRequestId(rs.getInt(1));
                service.setRoomId(rs.getInt(2));
                service.setMotelId(rs.getInt(3));
                service.setServiceName(rs.getString(4));

                services.add(service);
            }
            return services;
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
     public boolean DeleteSOB(int bdtID) {
        try {
            String sql = "UPDATE ServicesOfBooking set isDeleteServicesOfBooking = 1\n"
                    + "  where servicesOfRoomID = (select servicesOfRoomID from BillDetail where billDetailID = ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, bdtID);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    
    //get service by motelID
    public ArrayList<Services> getAllServiceByMotelid(int motelId) {
        String query = "select * from Services where motelID = ?";

        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, motelId);
            ResultSet rs = stm.executeQuery();
            ArrayList<Services> serviceList = new ArrayList<>();

            while (rs.next()) {
                Services s = new Services();
                s.setServicesID(rs.getInt(1));
                s.setMotelID(rs.getInt(2));
                s.setServicesName(rs.getString(3));
                s.setPrice(rs.getDouble(4));
                s.setIsDelete(rs.getBoolean(5));

                serviceList.add(s);
            }
            return serviceList;
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    // get all Services Of Account by list MotelID
    public ArrayList<Services> getAllServicesOfAccount(ArrayList<Integer> listMotelID) {
        try {
            String sql = "select * from Services where motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            ArrayList<Services> services = new ArrayList<>();
            for (int i = 0; i < listMotelID.size(); i++) {
                stm.setInt(1, listMotelID.get(i));
                ResultSet rs = stm.executeQuery();
                while (rs.next()) {
                    Services service = new Services(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getDouble(4));
                    services.add(service);
                }
            }
            return services;
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public int getServiceIdByBDTId(int BDTId) {
        try {
            String sql = "select servicesOfRoomID\n"
                    + "   from BillDetail\n"
                    + "   where billDetailID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, BDTId);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public boolean UpdateServices(Services services) {
        try {
            String sql = "UPDATE Services set servicesName = ?, price = ? where servicesID = ? and motelID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, services.getServicesName());
            stm.setDouble(2, services.getPrice());
            stm.setInt(3, services.getServicesID());
            stm.setInt(4, services.getMotelID());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return (true);
    }

    public boolean DeleteServicesByID(int servicesID) {
        try {
            String sql = "update [Services] set isDeleteServiecs = 1 where servicesID = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, servicesID);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    public boolean CreateServices(Services services) {
        try {
            String sql = "insert into Services(motelID, servicesName, price) values(?, ?, ?)";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setInt(1, services.getMotelID());
            stm.setString(2, services.getServicesName());
            stm.setDouble(3, services.getPrice());
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    //get service
    public int getPriceByServiceId(int serviceId) {
        String query = "  select price\n"
                + "  from Services\n"
                + "  where servicesID = ?";

        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, serviceId);
            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public boolean createDraftServiceOfBooking(int bookingId, int serviceId) {
        String query = "insert into ServicesOfBooking(BookingID, servicesID, isDeleteServicesOfBooking,isDraft) values(?,?,0,1)";

        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, bookingId);
            stm.setInt(2, serviceId);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    
    public boolean createServiceOfBooking(int bookingId, int serviceId) {
        String query = "insert into ServicesOfBooking(BookingID, servicesID, isDeleteServicesOfBooking,isDraft) values(?,?,0,0)";

        try {
            PreparedStatement stm = connection.prepareCall(query);
            stm.setInt(1, bookingId);
            stm.setInt(2, serviceId);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public int getDraftServiceOfBooking() {
        String query = "select MAX(servicesOfRoomID) from ServicesOfBooking";

        try {
            PreparedStatement stm = connection.prepareCall(query);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    
    public int getLastestServiceOfBookingID() {
        String query = "select MAX(servicesOfRoomID) from ServicesOfBooking";

        try {
            PreparedStatement stm = connection.prepareCall(query);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public boolean updateDraftSOB() {
        try {
            String sql = "update ServicesOfBooking set isDraft = 0 where isDraft = 1";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public boolean deleteDraftgServicesOfBooking() {
        try {
            String sql = "delete ServicesOfBooking where isDraft = 1";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ServicesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static void main(String[] args) {
        ServicesDAO DBS = new ServicesDAO();
        ArrayList<Services> listServices = new ArrayList<>();
        ArrayList<Integer> listMotelID = new ArrayList<>();
        listMotelID.add(1);
        listMotelID.add(2);
        listMotelID.add(3);
        listMotelID.add(4);
        listServices = DBS.getAllServicesOfAccount(listMotelID);
        System.out.println(listServices);
    }
}
