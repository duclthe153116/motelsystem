/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author coder
 */
public class Profile {
    private int profileID;
    private int accountID;
    private String fullName;
    private String phone;
    private String address;
    private String email;
    private Date dOB;
    private String gender;
    private String imageProfile;

    public Profile() {
    }

    public Profile(int profileID, int accountID, String fullName, String phone, String address, String email, Date dOB, String gender, String imageProfile) {
        this.profileID = profileID;
        this.accountID = accountID;
        this.fullName = fullName;
        this.phone = phone;
        this.address = address;
        this.email = email;
        this.dOB = dOB;
        this.gender = gender;
        this.imageProfile = imageProfile;
    }

    public Profile(int accountID, String fullName, String phone, String address, String email, Date dOB, String gender, String imageProfile) {
        this.accountID = accountID;
        this.fullName = fullName;
        this.phone = phone;
        this.address = address;
        this.email = email;
        this.dOB = dOB;
        this.gender = gender;
        this.imageProfile = imageProfile;
    }

    public int getProfileID() {
        return profileID;
    }

    public void setProfileID(int profileID) {
        this.profileID = profileID;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getdOB() {
        return dOB;
    }

    public void setdOB(Date dOB) {
        this.dOB = dOB;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImageProfile() {
        return imageProfile;
    }

    public void setImageProfile(String imageProfile) {
        this.imageProfile = imageProfile;
    }
    
        
    @Override
    public String toString() {
        return "Profile{" + "profileID=" + profileID + ", accountID=" + accountID + ", fullName=" + fullName + ", phone=" + phone + ", address=" + address + ", email=" + email + ", dOB=" + dOB + ", gender=" + gender + ", imageProfile=" + imageProfile + '}';
    }
    
}
