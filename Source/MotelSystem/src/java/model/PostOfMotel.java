/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class PostOfMotel {
    private int postID;
    private String postTitle;
    private int motelID;
    private String motelName;
    private String userName;
    private String postContent;
    private Date postDate;
    private String postStatus;
    private String fullName;

    public PostOfMotel() {
    }

    public PostOfMotel(int postID, String postTitle, int motelID, String motelName, String userName, String postContent, Date postDate, String postStatus) {
        this.postID = postID;
        this.postTitle = postTitle;
        this.motelID = motelID;
        this.motelName = motelName;
        this.userName = userName;
        this.postContent = postContent;
        this.postDate = postDate;
        this.postStatus = postStatus;
    }

    public PostOfMotel(int postID, String postTitle, int motelID, String motelName, String userName, String postContent, Date postDate, String postStatus, String fullName) {
        this.postID = postID;
        this.postTitle = postTitle;
        this.motelID = motelID;
        this.motelName = motelName;
        this.userName = userName;
        this.postContent = postContent;
        this.postDate = postDate;
        this.postStatus = postStatus;
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getPostID() {
        return postID;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public int getMotelID() {
        return motelID;
    }

    public void setMotelID(int motelID) {
        this.motelID = motelID;
    }

    public String getMotelName() {
        return motelName;
    }

    public void setMotelName(String motelName) {
        this.motelName = motelName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }
    
    @Override
    public String toString() {
        return "Post{" + "postID=" + postID + ", postTitle=" + postTitle + ", motelID=" + motelID + ", motelName=" + motelName + ", userName=" + userName + ", postContent=" + postContent + ", postDate=" + postDate + ", postStatus=" + postStatus + '}';
    }
}
