/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author royal
 */
public class PostImage {
    private int id;
    private String base64String;

    public PostImage() {
    }

    public PostImage(int id, String base64String) {
        this.id = id;
        this.base64String = base64String;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBase64String() {
        return base64String;
    }

    public void setBase64String(String base64String) {
        this.base64String = base64String;
    }

    @Override
    public String toString() {
        return "PostImage{" + "id=" + id + ", base64String=" + base64String + '}';
    }
    
    
    
}
