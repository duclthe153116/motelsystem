/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Admin
 */
public class MotelDetail {
    private int motelID;
    private int ownerID;
    private String ownerName;
    private String motelName;
    private int roomNumber;
    private int numberEmptyRoom;
    private String motelAddress;
    private String motelPhone;
    private String motelStatus;
    private double maxPrice;
    private double minPrice;
    private String motelDescription;
    private String motelImage;
    private List<String> motelImgList;

    public MotelDetail() {
    }

    public MotelDetail(int motelID, int ownerID, String ownerName, String motelName, int roomNumber, String motelAddress, String motelPhone, String motelStatus, double maxPrice, double minPrice, String motelDescription, String motelImage, List<String> motelImgList) {
        this.motelID = motelID;
        this.ownerID = ownerID;
        this.ownerName = ownerName;
        this.motelName = motelName;
        this.roomNumber = roomNumber;
        this.motelAddress = motelAddress;
        this.motelPhone = motelPhone;
        this.motelStatus = motelStatus;
        this.maxPrice = maxPrice;
        this.minPrice = minPrice;
        this.motelDescription = motelDescription;
        this.motelImage = motelImage;
        this.motelImgList = motelImgList;
    }

    public MotelDetail(int motelID, int ownerID, String ownerName, String motelName, int roomNumber, int numberEmptyRoom, String motelAddress, String motelPhone, String motelStatus, double maxPrice, double minPrice, String motelDescription, String motelImage, List<String> motelImgList) {
        this.motelID = motelID;
        this.ownerID = ownerID;
        this.ownerName = ownerName;
        this.motelName = motelName;
        this.roomNumber = roomNumber;
        this.numberEmptyRoom = numberEmptyRoom;
        this.motelAddress = motelAddress;
        this.motelPhone = motelPhone;
        this.motelStatus = motelStatus;
        this.maxPrice = maxPrice;
        this.minPrice = minPrice;
        this.motelDescription = motelDescription;
        this.motelImage = motelImage;
        this.motelImgList = motelImgList;
    }

    public int getNumberEmptyRoom() {
        return numberEmptyRoom;
    }

    public void setNumberEmptyRoom(int numberEmptyRoom) {
        this.numberEmptyRoom = numberEmptyRoom;
    }

    public String getMotelImage() {
        return motelImage;
    }

    public void setMotelImage(String motelImage) {
        this.motelImage = motelImage;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public int getMotelID() {
        return motelID;
    }

    public void setMotelID(int motelID) {
        this.motelID = motelID;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public String getMotelName() {
        return motelName;
    }

    public void setMotelName(String motelName) {
        this.motelName = motelName;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getMotelAddress() {
        return motelAddress;
    }

    public void setMotelAddress(String motelAddress) {
        this.motelAddress = motelAddress;
    }

    public String getMotelPhone() {
        return motelPhone;
    }

    public void setMotelPhone(String motelPhone) {
        this.motelPhone = motelPhone;
    }

    public String getMotelStatus() {
        return motelStatus;
    }

    public void setMotelStatus(String motelStatus) {
        this.motelStatus = motelStatus;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public String getMotelDescription() {
        return motelDescription;
    }

    public void setMotelDescription(String motelDescription) {
        this.motelDescription = motelDescription;
    }

    public List<String> getMotelImgList() {
        return motelImgList;
    }

    public void setMotelImgList(List<String> motelImgList) {
        this.motelImgList = motelImgList;
    }
}
