/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


/**
 *
 * @author Admin
 */
public class Motel{
    private int motelID;
    private int accountID;
    private String motelName;
    private String motelAddress;
    private String motelPhone;
    private String motelStatus;
    private String motelImg;
    private String motelDescription;
    private boolean isDelete;

    public Motel() {
    }
    
    public Motel(String motelName, String motelAddress, String motelPhone, String motelDescription) {
        this.motelName = motelName;
        this.motelAddress = motelAddress;
        this.motelPhone = motelPhone;
        this.motelDescription = motelDescription;
    }
    
    public Motel(int motelID, int accountID, String motelName, String motelAddress, String motelPhone, String motelStatus) {
        this.motelID = motelID;
        this.accountID = accountID;
        this.motelName = motelName;
        this.motelAddress = motelAddress;
        this.motelPhone = motelPhone;
        this.motelStatus = motelStatus;
    }

    public Motel(int motelID, int accountID, String motelName, String motelAddress, String motelPhone, String motelStatus, String motelImg, String motelDescription) {
        this.motelID = motelID;
        this.accountID = accountID;
        this.motelName = motelName;
        this.motelAddress = motelAddress;
        this.motelPhone = motelPhone;
        this.motelStatus = motelStatus;
        this.motelImg = motelImg;
        this.motelDescription = motelDescription;
    }
    

    public int getMotelID() {
        return motelID;
    }

    public void setMotelID(int motelID) {
        this.motelID = motelID;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getMotelName() {
        return motelName;
    }

    public void setMotelName(String motelName) {
        this.motelName = motelName;
    }

    public String getMotelAddress() {
        return motelAddress;
    }

    public void setMotelAddress(String motelAddress) {
        this.motelAddress = motelAddress;
    }

    public String getMotelPhone() {
        return motelPhone;
    }

    public void setMotelPhone(String motelPhone) {
        this.motelPhone = motelPhone;
    }

    public String getMotelStatus() {
        return motelStatus;
    }

    public void setMotelStatus(String motelStatus) {
        this.motelStatus = motelStatus;
    }

    public String getMotelImg() {
        return motelImg;
    }

    public void setMotelImg(String motelImg) {
        this.motelImg = motelImg;
    }

    public String getMotelDescription() {
        return motelDescription;
    }

    public void setMotelDescription(String motelDescription) {
        this.motelDescription = motelDescription;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }
   
}
