/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author coder
 */
public class Booking {
    private int bookingID;
    private int roomID;
    private int isDeleteBooking;
    private Date startAt;
    private Date endAt;

    public Booking() {
    }

    public Booking(int bookingID, int roomID, int isDeleteBooking, Date startAt, Date endAt) {
        this.bookingID = bookingID;
        this.roomID = roomID;
        this.isDeleteBooking = isDeleteBooking;
        this.startAt = startAt;
        this.endAt = endAt;
    }
 
    public Booking(int bookingID, int roomID, int isDeleteBooking) {
        this.bookingID = bookingID;
        this.roomID = roomID;
        this.isDeleteBooking = isDeleteBooking;
    }

    public Booking(int roomID, int isDeleteBooking) {
        this.roomID = roomID;
        this.isDeleteBooking = isDeleteBooking;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public int getIsDeleteBooking() {
        return isDeleteBooking;
    }

    public void setIsDeleteBooking(int isDeleteBooking) {
        this.isDeleteBooking = isDeleteBooking;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndAt() {
        return endAt;
    }

    public void setEndAt(Date endAt) {
        this.endAt = endAt;
    }
    
}
