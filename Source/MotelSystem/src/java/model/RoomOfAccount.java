/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author doson
 */
public class RoomOfAccount {
    private int accountId;
    private ArrayList<Room> listRoom;

    public RoomOfAccount() {
    }

    public RoomOfAccount(int accountId, ArrayList<Room> listRoom) {
        this.accountId = accountId;
        this.listRoom = listRoom;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public ArrayList<Room> getListRoom() {
        return listRoom;
    }

    public void setListRoom(ArrayList<Room> listRoom) {
        this.listRoom = listRoom;
    }

    
    
    
}
