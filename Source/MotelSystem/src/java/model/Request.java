/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class Request {
    private int requestID;
    private int roomID;
    private String content;
    private String status;
    private String noteRequest;
    private String noteRequestOfHost;
    private Date createdAt;
    private Date updateAt;

    public Request() {
    }

    public Request(int requestID, int roomID, String content, String status, String noteRequest, String noteRequestOfHost, Date createdAt, Date updateAt) {
        this.requestID = requestID;
        this.roomID = roomID;
        this.content = content;
        this.status = status;
        this.noteRequest = noteRequest;
        this.noteRequestOfHost = noteRequestOfHost;
        this.createdAt = createdAt;
        this.updateAt = updateAt;
    }

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNoteRequest() {
        return noteRequest;
    }

    public void setNoteRequest(String noteRequest) {
        this.noteRequest = noteRequest;
    }

    public String getNoteRequestOfHost() {
        return noteRequestOfHost;
    }

    public void setNoteRequestOfHost(String noteRequestOfHost) {
        this.noteRequestOfHost = noteRequestOfHost;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

   
    
    
}
