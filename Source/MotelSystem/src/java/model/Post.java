/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class Post {
    private int postID;
    private String postTitle;
    private int motelID;
    private String postContent;
    private Date postDate;
    private String postStatus;

    public Post() {
    }

    public Post(int postID, String postTitle, int motelID, String postContent, Date postDate, String postStatus) {
        this.postID = postID;
        this.postTitle = postTitle;
        this.motelID = motelID;
        this.postContent = postContent;
        this.postDate = postDate;
        this.postStatus = postStatus;
    }

    public int getPostID() {
        return postID;
    }

    public void setPostID(int postID) {
        this.postID = postID;
    }

    public int getMotelID() {
        return motelID;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setMotelID(int motelID) {
        this.motelID = motelID;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public String getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(String postStatus) {
        this.postStatus = postStatus;
    }

    @Override
    public String toString() {
        return "Post{" + "postID=" + postID + ", postTitle=" + postTitle + ", motelID=" + motelID + ", postContent=" + postContent + ", postDate=" + postDate + ", postStatus=" + postStatus + '}';
    }

    
    
}
