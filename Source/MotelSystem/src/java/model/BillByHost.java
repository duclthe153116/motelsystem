/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author LTC
 */
public class BillByHost {

    private int billID;
    private Date dateOfPayment;
    private Date dateCreate;
    private int totalPriceBill;
    private boolean confirmByHost;
    private boolean confirmByCustomer;
    private boolean isSent;
    private boolean isDraft;
    private boolean isDelete;
    private int motelID;
    private int roomID;
    private String roomName;
    private String motelName;

    public BillByHost(int billID, Date dateOfPayment, Date dateCreate, int totalPriceBill, boolean confirmByHost, boolean confirmByCustomer, boolean isSent, boolean isDraft, boolean isDelete, int motelID, int roomID, String roomName, String motelName) {
        this.billID = billID;
        this.dateOfPayment = dateOfPayment;
        this.dateCreate = dateCreate;
        this.totalPriceBill = totalPriceBill;
        this.confirmByHost = confirmByHost;
        this.confirmByCustomer = confirmByCustomer;
        this.isSent = isSent;
        this.isDraft = isDraft;
        this.isDelete = isDelete;
        this.motelID = motelID;
        this.roomID = roomID;
        this.roomName = roomName;
        this.motelName = motelName;
    }

    

    public BillByHost() {
    }

    
    
    public int getBillID() {
        return billID;
    }

    public void setBillID(int billID) {
        this.billID = billID;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public Date getDateOfPayment() {
        return dateOfPayment;
    }

    public void setDateOfPayment(Date dateOfPayment) {
        this.dateOfPayment = dateOfPayment;
    }

    public int getTotalPriceBill() {
        return totalPriceBill;
    }

    public void setTotalPriceBill(int totalPriceBill) {
        this.totalPriceBill = totalPriceBill;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public boolean isConfirmByHost() {
        return confirmByHost;
    }

    public void setConfirmByHost(boolean confirmByHost) {
        this.confirmByHost = confirmByHost;
    }

    public boolean isConfirmByCustomer() {
        return confirmByCustomer;
    }

    public void setConfirmByCustomer(boolean confirmByCustomer) {
        this.confirmByCustomer = confirmByCustomer;
    }

    public boolean isIsSent() {
        return isSent;
    }

    public void setIsSent(boolean isSent) {
        this.isSent = isSent;
    }

    public boolean isIsDraft() {
        return isDraft;
    }

    public void setIsDraft(boolean isDraft) {
        this.isDraft = isDraft;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

  

    public int getMotelID() {
        return motelID;
    }

    public void setMotelID(int motelID) {
        this.motelID = motelID;
    }

    public String getMotelName() {
        return motelName;
    }

    public void setMotelName(String motelName) {
        this.motelName = motelName;
    }
    
    


}
