/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author coder
 */
public class BookingDetails {

    private int bookingID;
    private int motelID;
    private String motelName;
    private int roomID;
    private String roomName;
    private ArrayList<Profile> listProfile = new ArrayList<>();
    private int price;
    private String noteRoom;
    private boolean isDeleteBooking;
    private ArrayList<Services> listServices = new ArrayList<>();
    private Date startAt;
    private Date endAt;

    public BookingDetails() {
    }

    public BookingDetails(int bookingID, int motelID, String motelName, int roomID, String roomName, ArrayList<Profile> listProfile, int price, String noteRoom, boolean isDeleteBooking, ArrayList<Services> listServices, Date startAt, Date endAt) {
        this.bookingID = bookingID;
        this.motelID = motelID;
        this.motelName = motelName;
        this.roomID = roomID;
        this.roomName = roomName;
        this.price = price;
        this.noteRoom = noteRoom;
        this.isDeleteBooking = isDeleteBooking;
        this.startAt = startAt;
        this.endAt = endAt;
        this.listProfile = listProfile;
        this.listServices = listServices;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public int getMotelID() {
        return motelID;
    }

    public void setMotelID(int motelID) {
        this.motelID = motelID;
    }

    public String getMotelName() {
        return motelName;
    }

    public void setMotelName(String motelName) {
        this.motelName = motelName;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }


    public ArrayList<Profile> getListProfile() {
        return listProfile;
    }

    public void setListProfile(ArrayList<Profile> listProfile) {
        this.listProfile = listProfile;
    }

    public void addListProfile(Profile profile) {
        this.listProfile.add(profile);
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getNoteRoom() {
        return noteRoom;
    }

    public void setNoteRoom(String noteRoom) {
        this.noteRoom = noteRoom;
    }

    public boolean isIsDeleteBooking() {
        return isDeleteBooking;
    }

    public void setIsDeleteBooking(boolean isDeleteBooking) {
        this.isDeleteBooking = isDeleteBooking;
    }

    public ArrayList<Services> getListServices() {
        return listServices;
    }

    public void setListServices(ArrayList<Services> listServices) {
        this.listServices = listServices;
    }
    public void addListServices(Services services) {
        this.listServices.add(services);
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndAt() {
        return endAt;
    }

    public void setEndAt(Date endAt) {
        this.endAt = endAt;
    }

}
