/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author royal
 */
public class PostDetail {

    private int id;
    private int modelID;
    private int ownerID;
    private String motelName;
    private String title;
    private String content;
    private double maxPrice;
    private double minPrice;
    private String phone;
    private String address;
    private Date pDate;
    private String pStatus;
    private String specificImage;
    private List<PostImage> listImages;

    public String getSpecificImage() {
        return specificImage;
    }

    public void setSpecificImage(String specificImage) {
        this.specificImage = specificImage;
    }

    public List<PostImage> getListImages() {
        return listImages;
    }

    public void setListImages(List<PostImage> listImages) {
        this.listImages = listImages;
    }

    
    
    

    public PostDetail() {
    }

    public PostDetail(int id, int modelID, int ownerID, String motelName, String title, String content, double maxPrice, double minPrice, String phone, String address, Date pDate, String postStatus) {
        this.id = id;
        this.modelID = modelID;
        this.ownerID = ownerID;
        this.motelName = motelName;
        this.title = title;
        this.content = content;
        this.maxPrice = maxPrice;
        this.minPrice = minPrice;
        this.phone = phone;
        this.address = address;
        this.pDate = pDate;
        this.pStatus = postStatus;
    }

    public String getpStatus() {
        return pStatus;
    }

    public void setpStatus(String pStatus) {
        this.pStatus = pStatus;
    }
    
    

    public int getId() {
        return id;
    }

    public int getModelID() {
        return modelID;
    }

    public int getOwnerID() {
        return ownerID;
    }

    public String getMotelName() {
        return motelName;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public double getMaxPrice() {
        return maxPrice;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public Date getpDate() {
        return pDate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setModelID(int modelID) {
        this.modelID = modelID;
    }

    public void setOwnerID(int ownerID) {
        this.ownerID = ownerID;
    }

    public void setMotelName(String motelName) {
        this.motelName = motelName;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setMaxPrice(double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setpDate(Date pDate) {
        this.pDate = pDate;
    }

    @Override
    public String toString() {
        return "PostDetail{" + "id=" + id + ", modelID=" + modelID + ", ownerID=" + ownerID + ", motelName=" + motelName + ", title=" + title + ", content=" + content + ", maxPrice=" + maxPrice + ", minPrice=" + minPrice + ", phone=" + phone + ", address=" + address + ", pDate=" + pDate + ", pStatus=" + pStatus + ", specificImage=" + specificImage + '}';
    }

    
    

}
