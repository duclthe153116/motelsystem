/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author Admin
 */
public class MotelImage {
    private int motelImgID;
    private int motelID;
    private List<String> motelImgs;

    public MotelImage() {
    }

    public MotelImage(int motelImgID, int motelID, List<String> motelImgs) {
        this.motelImgID = motelImgID;
        this.motelID = motelID;
        this.motelImgs = motelImgs;
    }

    public int getMotelImgID() {
        return motelImgID;
    }

    public void setMotelImgID(int motelImgID) {
        this.motelImgID = motelImgID;
    }

    public int getMotelID() {
        return motelID;
    }

    public void setMotelID(int motelID) {
        this.motelID = motelID;
    }

    public List<String> getMotelImgs() {
        return motelImgs;
    }

    public void setMotelImgs(List<String> motelImgs) {
        this.motelImgs = motelImgs;
    }
    
    
}
