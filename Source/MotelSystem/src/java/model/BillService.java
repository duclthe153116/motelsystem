
package model;

/**
 *
 * @author LTC
 */
public class BillService {
    private int sOBId;
    private int requestId;
    private int motelId;
    private int roomId;
    private int serviceId;
    private String serviceName;
    private int price;
    private int quantity;
    private int total;

    public BillService() {
    }

    public BillService(int sOBId, int requestId, int motelId, int roomId, int serviceId, String serviceName, int price, int quantity, int total) {
        this.sOBId = sOBId;
        this.requestId = requestId;
        this.motelId = motelId;
        this.roomId = roomId;
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.price = price;
        this.quantity = quantity;
        this.total = total;
    }

    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    

    public int getsOBId() {
        return sOBId;
    }

    public void setsOBId(int sOBId) {
        this.sOBId = sOBId;
    }

    public int getMotelId() {
        return motelId;
    }

    public void setMotelId(int motelId) {
        this.motelId = motelId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
    
}