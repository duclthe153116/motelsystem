/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author doson
 */
public class ProfileOfAccount {
    private int accountID;
    private String userName;
    private String passWotd;
    private int profileID;
    private String fullName;
    private String phone;
    private String address;
    private String email;
    private Date dob;
    private String gender;
    private String imageProfile;

    public ProfileOfAccount() {
    }

    public ProfileOfAccount(int accountID, String userName, String passWotd, int profileID, String fullName, String phone, String address, String email, Date dob, String gender, String imageProfile) {
        this.accountID = accountID;
        this.userName = userName;
        this.passWotd = passWotd;
        this.profileID = profileID;
        this.fullName = fullName;
        this.phone = phone;
        this.address = address;
        this.email = email;
        this.dob = dob;
        this.gender = gender;
        this.imageProfile = imageProfile;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWotd() {
        return passWotd;
    }

    public void setPassWotd(String passWotd) {
        this.passWotd = passWotd;
    }

    public int getProfileID() {
        return profileID;
    }

    public void setProfileID(int profileID) {
        this.profileID = profileID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getImageProfile() {
        return imageProfile;
    }

    public void setImageProfile(String imageProfile) {
        this.imageProfile = imageProfile;
    }
    
}
