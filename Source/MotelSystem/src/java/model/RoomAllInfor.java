/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author doson
 */
public class RoomAllInfor {
    private int bookingID;
    private int motelID;
    private String motelName;
    private int roomID;
    private String roomName;
    private ArrayList<Integer> listAccountID = new ArrayList<>();
    private ArrayList<Integer> listProfileID = new ArrayList<>();
    private ArrayList<String> listAccountName = new ArrayList<>();
    private int price;
    private String noteRoom;
    private boolean isDeleteBooking;
    private Date startAt;
    private Date endAt;
    private int numberOfMax;
    private int numberOfCurrent;
    private double area;

    public RoomAllInfor() {
    }

    public RoomAllInfor(int bookingID, int motelID, String motelName, int roomID, String roomName,ArrayList<Integer> listAccountID, ArrayList<Integer> listProfileID, ArrayList<String> listAccountName, int price, String noteRoom, boolean isDeleteBooking, Date startAt, Date endAt, int numberOfMax, int numberOfCurrent, double area) {
        this.bookingID = bookingID;
        this.motelID = motelID;
        this.motelName = motelName;
        this.roomID = roomID;
        this.roomName = roomName;
        this.price = price;
        this.noteRoom = noteRoom;
        this.isDeleteBooking = isDeleteBooking;
        this.listAccountID = listAccountID;
        this.listProfileID = listProfileID;
        this.listAccountName = listAccountName;
        this.startAt = startAt;
        this.endAt = endAt;
        this.numberOfMax = numberOfMax;
        this.numberOfCurrent = numberOfCurrent;
        this.area = area;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public int getMotelID() {
        return motelID;
    }

    public void setMotelID(int motelID) {
        this.motelID = motelID;
    }

    public String getMotelName() {
        return motelName;
    }

    public void setMotelName(String motelName) {
        this.motelName = motelName;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public ArrayList<Integer> getListAccountID() {
        return listAccountID;
    }

    public void setListAccountID(ArrayList<Integer> listAccountID) {
        this.listAccountID = listAccountID;
    }

    public ArrayList<Integer> getListProfileID() {
        return listProfileID;
    }

    public void setListProfileID(ArrayList<Integer> listProfileID) {
        this.listProfileID = listProfileID;
    }

    public ArrayList<String> getListAccountName() {
        return listAccountName;
    }

    public void setListAccountName(ArrayList<String> listAccountName) {
        this.listAccountName = listAccountName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getNoteRoom() {
        return noteRoom;
    }

    public void setNoteRoom(String noteRoom) {
        this.noteRoom = noteRoom;
    }

    public boolean isIsDeleteBooking() {
        return isDeleteBooking;
    }

    public void setIsDeleteBooking(boolean isDeleteBooking) {
        this.isDeleteBooking = isDeleteBooking;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndAt() {
        return endAt;
    }

    public void setEndAt(Date endAt) {
        this.endAt = endAt;
    }

    public int getNumberOfMax() {
        return numberOfMax;
    }

    public void setNumberOfMax(int numberOfMax) {
        this.numberOfMax = numberOfMax;
    }

    public int getNumberOfCurrent() {
        return numberOfCurrent;
    }

    public void setNumberOfCurrent(int numberOfCurrent) {
        this.numberOfCurrent = numberOfCurrent;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }
    public void addAccountIDToList(int AccountID){
        this.listAccountID.add(AccountID);
    }
    public void addProfileIDToList(int profileID){
        this.listProfileID.add(profileID);
    }
     public void addAccountNameToList(String accountName){
        this.listAccountName.add(accountName);
    }
    
    
}
