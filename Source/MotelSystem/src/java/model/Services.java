/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author coder
 */
public class Services {
    private int servicesID;
    private int motelID;
    private String servicesName;
    private double price;
    private boolean isDelete;

    public Services() {
    }

    public Services(int servicesID, int motelID, String servicesName, double price, boolean isDelete) {
        this.servicesID = servicesID;
        this.motelID = motelID;
        this.servicesName = servicesName;
        this.price = price;
        this.isDelete = isDelete;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }
    
    

    public Services(int motelID, String servicesName, double price) {
        this.motelID = motelID;
        this.servicesName = servicesName;
        this.price = price;
    }

    public Services(int servicesID, int motelID, String servicesName, double price) {
        this.servicesID = servicesID;
        this.motelID = motelID;
        this.servicesName = servicesName;
        this.price = price;
    }

    public int getServicesID() {
        return servicesID;
    }

    public void setServicesID(int servicesID) {
        this.servicesID = servicesID;
    }

    public int getMotelID() {
        return motelID;
    }

    public void setMotelID(int motelID) {
        this.motelID = motelID;
    }

    public String getServicesName() {
        return servicesName;
    }

    public void setServicesName(String servicesName) {
        this.servicesName = servicesName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
}
