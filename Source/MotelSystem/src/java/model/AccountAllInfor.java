/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author coder
 */
public class AccountAllInfor {
    private int accountID;
    private String username;
    private String password;
    private int profileID;
    private String fullName;
    private String phone;
    private String address;
    private String email;
    private Date dOB;
    private String gender;
    private String imageProfile;
    private ArrayList<Integer> roleID;
    private ArrayList<String> roleName;
    private boolean isdelete;

    public AccountAllInfor() {
        this.roleID = new ArrayList<>();
        this.roleName = new ArrayList<>();
    }
    
    public AccountAllInfor(int accountID, String username, String password, int profileID, String fullName, String phone, String address, String email, Date dOB, String gender, String imageProfile, ArrayList<Integer> roleID, ArrayList<String> roleName) {
        this.accountID = accountID;
        this.username = username;
        this.password = password;
        this.profileID = profileID;
        this.fullName = fullName;
        this.phone = phone;
        this.address = address;
        this.email = email;
        this.dOB = dOB;
        this.gender = gender;
        this.imageProfile = imageProfile;
        this.roleID = roleID;
        this.roleName = roleName;
    }
    

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getProfileID() {
        return profileID;
    }

    public void setProfileID(int profileID) {
        this.profileID = profileID;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getdOB() {
        return dOB;
    }

    public void setdOB(Date dOB) {
        this.dOB = dOB;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public ArrayList<Integer> getRoleID() {
        return roleID;
    }

    public void setRoleID(ArrayList<Integer> roleID) {
        this.roleID = roleID;
    }

    public ArrayList<String> getRoleName() {
        return roleName;
    }

    public void setRoleName(ArrayList<String> roleName) {
        this.roleName = roleName;
    }

    public String getImageProfile() {
        return imageProfile;
    }

    public void setImageProfile(String imageProfile) {
        this.imageProfile = imageProfile;
    }

    public boolean isIsdelete() {
        return isdelete;
    }

    public void setIsdelete(boolean isdelete) {
        this.isdelete = isdelete;
    }
        

    public String toStringRoleID(){
        String result = "";
        for (int i = 0; i < roleID.size()-1; i++){
            result = result + roleID.get(i) + " ";
        }
        result = result + roleID.get(roleID.size()-1);
        return (result);
    }
    public String toStringRoleName(){
        String result = "";
        for (int i = 0; i < roleName.size()-1; i++){
            result = result + roleName.get(i) + " ";
        }
        result = result + roleName.get(roleName.size()-1);
        return (result);
    }
    @Override
    public String toString() {
        return "AccountAllInfor{" + "accountID=" + accountID + ", username=" + username + ", password=" + password + ", profileID=" + profileID + ", fullName=" + fullName + ", phone=" + phone + ", address=" + address + ", email=" + email + ", dOB=" + dOB + ", gender=" + gender + ", roleID=" + toStringRoleID() + ", roleName=" + toStringRoleName() + '}';
    }
    
    
    
}
