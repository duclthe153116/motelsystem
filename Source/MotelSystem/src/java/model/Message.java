/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class Message {
    private int MessageID;
    private int From;
    private int To;
    private String Content;
    private Date CreateAt;

    public Message() {
    }

    public Message(int MessageID, int From, int To, String Content, Date CreateAt) {
        this.MessageID = MessageID;
        this.From = From;
        this.To = To;
        this.Content = Content;
        this.CreateAt = CreateAt;
    }

    public Message(int From, int To, String Content, Date CreateAt) {
        this.From = From;
        this.To = To;
        this.Content = Content;
        this.CreateAt = CreateAt;
    }

    public Message(int From, int To, String Content) {
        this.From = From;
        this.To = To;
        this.Content = Content;
    }

    
    public int getMessageID() {
        return MessageID;
    }

    public void setMessageID(int MessageID) {
        this.MessageID = MessageID;
    }

    public int getFrom() {
        return From;
    }

    public void setFrom(int From) {
        this.From = From;
    }

    public int getTo() {
        return To;
    }

    public void setTo(int To) {
        this.To = To;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }

    public Date getCreateAt() {
        return CreateAt;
    }

    public void setCreateAt(Date CreateAt) {
        this.CreateAt = CreateAt;
    }
    
    
}
