/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author LTC
 */
public class BillDetail {

    private int billDetailID;
    private int billID;
    private int servicesID;
    private String servicesName;
    private int servicesPrice;
    private int quantity;
    private int totalBillDetail;
    private boolean hostConfirm;
    private boolean cusConfirm;
    private boolean isDelete;
    private int requestID;
    private int sORID;

    public BillDetail(int billDetailID, int billID, int servicesID, String servicesName, int servicesPrice, int quantity, int totalBillDetail, boolean hostConfirm, boolean cusConfirm, boolean isDelete) {
        this.billDetailID = billDetailID;
        this.billID = billID;
        this.servicesID = servicesID;
        this.servicesName = servicesName;
        this.servicesPrice = servicesPrice;
        this.quantity = quantity;
        this.totalBillDetail = totalBillDetail;
        this.hostConfirm = hostConfirm;
        this.cusConfirm = cusConfirm;
        this.isDelete = isDelete;
    }

    public BillDetail(int billDetailID, int billID, int servicesID, String servicesName, int servicesPrice, int quantity, int totalBillDetail, boolean hostConfirm, boolean cusConfirm, boolean isDelete, int requestID, int sORID) {
        this.billDetailID = billDetailID;
        this.billID = billID;
        this.servicesID = servicesID;
        this.servicesName = servicesName;
        this.servicesPrice = servicesPrice;
        this.quantity = quantity;
        this.totalBillDetail = totalBillDetail;
        this.hostConfirm = hostConfirm;
        this.cusConfirm = cusConfirm;
        this.isDelete = isDelete;
        this.requestID = requestID;
        this.sORID = sORID;
    }

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public int getsORID() {
        return sORID;
    }

    public void setsORID(int sORID) {
        this.sORID = sORID;
    }

    

    public BillDetail() {
    }

    public int getBillDetailID() {
        return billDetailID;
    }

    public void setBillDetailID(int billDetailID) {
        this.billDetailID = billDetailID;
    }

    public int getBillID() {
        return billID;
    }

    public void setBillID(int billID) {
        this.billID = billID;
    }

    public int getServicesID() {
        return servicesID;
    }

    public void setServicesID(int servicesID) {
        this.servicesID = servicesID;
    }

    public String getServicesName() {
        return servicesName;
    }

    public void setServicesName(String servicesName) {
        this.servicesName = servicesName;
    }

    public int getServicesPrice() {
        return servicesPrice;
    }

    public void setServicesPrice(int servicesPrice) {
        this.servicesPrice = servicesPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getTotalBillDetail() {
        return totalBillDetail;
    }

    public void setTotalBillDetail(int totalBillDetail) {
        this.totalBillDetail = totalBillDetail;
    }

    public boolean isHostConfirm() {
        return hostConfirm;
    }

    public void setHostConfirm(boolean hostConfirm) {
        this.hostConfirm = hostConfirm;
    }

    public boolean isCusConfirm() {
        return cusConfirm;
    }

    public void setCusConfirm(boolean cusConfirm) {
        this.cusConfirm = cusConfirm;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    

   

}
