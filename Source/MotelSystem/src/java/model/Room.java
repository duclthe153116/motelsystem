/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author coder
 */
public class Room {
    private int roomID;
    private int motelID;
    private String roomName;
    private int numberOfPeopleMax;
    private int numberOfPeopleCurrent;
    private double price;
    private double area;
    private String note;
    private boolean isDelete;

    public Room() {
    }

    public Room(int motelID, String roomName, int numberOfPeopleMax, int numberOfPeopleCurrent, double price, double area, String note) {
        this.motelID = motelID;
        this.roomName = roomName;
        this.numberOfPeopleMax = numberOfPeopleMax;
        this.numberOfPeopleCurrent = numberOfPeopleCurrent;
        this.price = price;
        this.area = area;
        this.note = note;
    }

    public Room(int roomID, int motelID, String roomName, int numberOfPeopleMax, int numberOfPeopleCurrent, double price, double area, String note, boolean isDelete) {
        this.roomID = roomID;
        this.motelID = motelID;
        this.roomName = roomName;
        this.numberOfPeopleMax = numberOfPeopleMax;
        this.numberOfPeopleCurrent = numberOfPeopleCurrent;
        this.price = price;
        this.area = area;
        this.note = note;
        this.isDelete = isDelete;
    }
    
    

    public Room(int roomID, int motelID, String roomName, int numberOfPeopleMax, int numberOfPeopleCurrent, double price, double area, String note) {
        this.roomID = roomID;
        this.motelID = motelID;
        this.roomName = roomName;
        this.numberOfPeopleMax = numberOfPeopleMax;
        this.numberOfPeopleCurrent = numberOfPeopleCurrent;
        this.price = price;
        this.area = area;
        this.note = note;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }
    

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public int getMotelID() {
        return motelID;
    }

    public void setMotelID(int motelID) {
        this.motelID = motelID;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public int getNumberOfPeopleMax() {
        return numberOfPeopleMax;
    }

    public void setNumberOfPeopleMax(int numberOfPeopleMax) {
        this.numberOfPeopleMax = numberOfPeopleMax;
    }

    public int getNumberOfPeopleCurrent() {
        return numberOfPeopleCurrent;
    }

    public void setNumberOfPeopleCurrent(int numberOfPeopleCurrent) {
        this.numberOfPeopleCurrent = numberOfPeopleCurrent;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
    
}
