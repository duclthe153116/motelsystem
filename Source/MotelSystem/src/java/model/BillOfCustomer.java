/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class BillOfCustomer {
    private int billID;
    private int bookingID;
    private Date dateCreate;
    private int totalBill;
    private boolean confirmByHost;
    private boolean confirmByCus;
    private String roomName;
    private String motelName;
    private Date dateOfPayment;
    private boolean isDelete;
    private boolean isSent;

    public BillOfCustomer() {
    }

    public BillOfCustomer(int billID, int bookingID, Date dateCreate, int totalBill, boolean confirmByHost, boolean confirmByCus, String roomName, String motelName, Date dateOfPayment, boolean isDelete, boolean isSent) {
        this.billID = billID;
        this.bookingID = bookingID;
        this.dateCreate = dateCreate;
        this.totalBill = totalBill;
        this.confirmByHost = confirmByHost;
        this.confirmByCus = confirmByCus;
        this.roomName = roomName;
        this.motelName = motelName;
        this.dateOfPayment = dateOfPayment;
        this.isDelete = isDelete;
        this.isSent = isSent;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean isIsSent() {
        return isSent;
    }

    public void setIsSent(boolean isSent) {
        this.isSent = isSent;
    }

    

    public int getBillID() {
        return billID;
    }

    public void setBillID(int billID) {
        this.billID = billID;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public Date getDateOfPayment() {
        return dateOfPayment;
    }

    public void setDateOfPayment(Date dateOfPayment) {
        this.dateOfPayment = dateOfPayment;
    }

    public int getTotalBill() {
        return totalBill;
    }

    public void setTotalBill(int totalBill) {
        this.totalBill = totalBill;
    }

    public boolean isConfirmByHost() {
        return confirmByHost;
    }

    public void setConfirmByHost(boolean confirmByHost) {
        this.confirmByHost = confirmByHost;
    }

    public boolean isConfirmByCus() {
        return confirmByCus;
    }

    public void setConfirmByCus(boolean confirmByCus) {
        this.confirmByCus = confirmByCus;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getMotelName() {
        return motelName;
    }

    public void setMotelName(String motelName) {
        this.motelName = motelName;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }
    
    
}
