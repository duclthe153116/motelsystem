/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author doson
 */
public class RequestAllInfor {
    private int accountID;
    private int accountOfBookingID;
    private int bookingID;
    private int roomID;
    private int motelID;
    private int requestID;
    private String motelName;
    private String roomName;
    private String content;
    private String status;
    private Date createAt;
    private Date updateAt;
    private String noteRequestOfRoom;
    private String noteRequestOfHost;
    private double priceRequest;

    public RequestAllInfor() {
    }

    public RequestAllInfor(int accountID, int accountOfBookingID, int bookingID, int roomID, int motelID, int requestID, String motelName, String roomName, String content, String status, Date createAt, Date updateAt, String noteRequestOfRoom, String noteRequestOfHost) {
        this.accountID = accountID;
        this.accountOfBookingID = accountOfBookingID;
        this.bookingID = bookingID;
        this.roomID = roomID;
        this.motelID = motelID;
        this.requestID = requestID;
        this.motelName = motelName;
        this.roomName = roomName;
        this.content = content;
        this.status = status;
        this.createAt = createAt;
        this.updateAt = updateAt;
        this.noteRequestOfRoom = noteRequestOfRoom;
        this.noteRequestOfHost = noteRequestOfHost;
    }

    public RequestAllInfor(int accountID, int accountOfBookingID, int bookingID, int roomID, int motelID, int requestID, String motelName, String roomName, String content, String status, Date createAt, Date updateAt, String noteRequestOfRoom, String noteRequestOfHost, double priceRequest) {
        this.accountID = accountID;
        this.accountOfBookingID = accountOfBookingID;
        this.bookingID = bookingID;
        this.roomID = roomID;
        this.motelID = motelID;
        this.requestID = requestID;
        this.motelName = motelName;
        this.roomName = roomName;
        this.content = content;
        this.status = status;
        this.createAt = createAt;
        this.updateAt = updateAt;
        this.noteRequestOfRoom = noteRequestOfRoom;
        this.noteRequestOfHost = noteRequestOfHost;
        this.priceRequest = priceRequest;
    }
    

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public int getAccountOfBookingID() {
        return accountOfBookingID;
    }

    public void setAccountOfBookingID(int accountOfBookingID) {
        this.accountOfBookingID = accountOfBookingID;
    }

    public int getBookingID() {
        return bookingID;
    }

    public void setBookingID(int bookingID) {
        this.bookingID = bookingID;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public int getMotelID() {
        return motelID;
    }

    public void setMotelID(int motelID) {
        this.motelID = motelID;
    }

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public String getMotelName() {
        return motelName;
    }

    public void setMotelName(String motelName) {
        this.motelName = motelName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Date updateAt) {
        this.updateAt = updateAt;
    }

    public String getNoteRequestOfRoom() {
        return noteRequestOfRoom;
    }

    public void setNoteRequestOfRoom(String noteRequestOfRoom) {
        this.noteRequestOfRoom = noteRequestOfRoom;
    }

    public String getNoteRequestOfHost() {
        return noteRequestOfHost;
    }

    public void setNoteRequestOfHost(String noteRequestOfHost) {
        this.noteRequestOfHost = noteRequestOfHost;
    }

    public double getPriceRequest() {
        return priceRequest;
    }

    public void setPriceRequest(double priceRequest) {
        this.priceRequest = priceRequest;
    }
    
    
}
