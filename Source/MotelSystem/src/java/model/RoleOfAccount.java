/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author coder
 */
public class RoleOfAccount {
    private int roleOfAccountID;
    private int accountID;
    private int roleID;

    public RoleOfAccount() {
    }

    public RoleOfAccount(int roleOfAccountID, int accountID, int roleID) {
        this.roleOfAccountID = roleOfAccountID;
        this.accountID = accountID;
        this.roleID = roleID;
    }

    public int getRoleOfAccountID() {
        return roleOfAccountID;
    }

    public void setRoleOfAccountID(int roleOfAccountID) {
        this.roleOfAccountID = roleOfAccountID;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    @Override
    public String toString() {
        return "RoleOfAccount{" + "roleOfAccountID=" + roleOfAccountID + ", accountID=" + accountID + ", roleID=" + roleID + '}';
    }
    
}
