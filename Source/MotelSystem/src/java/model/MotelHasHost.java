/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.Date;

/**
 *
 * @author Admin
 */
public class MotelHasHost {
    private int motelID;
    private int accountID;
    private String userName;
    private String motelName;
    private String motelAddress;
    private String motelPhone;
    private int motelRoomNumber;
    private int numberEmptyRoom;
    private String motelStatus;
    private String motelDescription;
    private String fullName;
    private Date motelCreateAt;
    private boolean isDelete;
    private boolean isRoleOfAccount;

    public MotelHasHost() {
    }

    public MotelHasHost(int motelID, int accountID, String userName, String motelName, String motelAddress, String motelPhone, int motelRoomNumber, String motelStatus) {
        this.motelID = motelID;
        this.accountID = accountID;
        this.userName = userName;
        this.motelName = motelName;
        this.motelAddress = motelAddress;
        this.motelPhone = motelPhone;
        this.motelRoomNumber = motelRoomNumber;
        this.motelStatus = motelStatus;
    }

    public MotelHasHost(int motelID, int accountID, String userName, String motelName, String motelAddress, String motelPhone, int motelRoomNumber, String motelStatus, String motelDescription, String fullName, Date motelCreateAt) {
        this.motelID = motelID;
        this.accountID = accountID;
        this.userName = userName;
        this.motelName = motelName;
        this.motelAddress = motelAddress;
        this.motelPhone = motelPhone;
        this.motelRoomNumber = motelRoomNumber;
        this.motelStatus = motelStatus;
        this.motelDescription = motelDescription;
        this.fullName = fullName;
        this.motelCreateAt = motelCreateAt;
    }

    public MotelHasHost(int motelID, int accountID, String userName, String motelName, String motelAddress, String motelPhone, int motelRoomNumber, int numberEmptyRoom, String motelStatus, String motelDescription, String fullName, Date motelCreateAt, boolean isDelete) {
        this.motelID = motelID;
        this.accountID = accountID;
        this.userName = userName;
        this.motelName = motelName;
        this.motelAddress = motelAddress;
        this.motelPhone = motelPhone;
        this.motelRoomNumber = motelRoomNumber;
        this.numberEmptyRoom = numberEmptyRoom;
        this.motelStatus = motelStatus;
        this.motelDescription = motelDescription;
        this.fullName = fullName;
        this.motelCreateAt = motelCreateAt;
        this.isDelete = isDelete;
    }

    public void setIsRoleOfAccount(boolean isRoleOfAccount) {
        this.isRoleOfAccount = isRoleOfAccount;
    }

    public boolean isIsRoleOfAccount() {
        return isRoleOfAccount;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }
    
    public Date getMotelCreateAt() {
        return motelCreateAt;
    }

    public void setMotelCreateAt(Date motelCreateAt) {
        this.motelCreateAt = motelCreateAt;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    

    public int getMotelID() {
        return motelID;
    }

    public void setMotelID(int motelID) {
        this.motelID = motelID;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMotelName() {
        return motelName;
    }

    public void setMotelName(String motelName) {
        this.motelName = motelName;
    }

    public String getMotelAddress() {
        return motelAddress;
    }

    public void setMotelAddress(String motelAddress) {
        this.motelAddress = motelAddress;
    }

    public String getMotelPhone() {
        return motelPhone;
    }

    public void setMotelPhone(String motelPhone) {
        this.motelPhone = motelPhone;
    }

    public int getMotelRoomNumber() {
        return motelRoomNumber;
    }

    public void setMotelRoomNumber(int motelRoomNumber) {
        this.motelRoomNumber = motelRoomNumber;
    }

    public String getMotelStatus() {
        return motelStatus;
    }

    public void setMotelStatus(String motelStatus) {
        this.motelStatus = motelStatus;
    }

    public String getMotelDescription() {
        return motelDescription;
    }

    public void setMotelDescription(String motelDescription) {
        this.motelDescription = motelDescription;
    }

    public int getNumberEmptyRoom() {
        return numberEmptyRoom;
    }

    public void setNumberEmptyRoom(int numberEmptyRoom) {
        this.numberEmptyRoom = numberEmptyRoom;
    }

    @Override
    public String toString() {
        return this.fullName + "-" + this.motelAddress + "-" + this.motelDescription + "-" +
                this.motelName + "-" + this.motelPhone + "-" + this.motelStatus + "-" + this.userName  + "-" + this.numberEmptyRoom;
    }
    
    
    
    
    
    
}
