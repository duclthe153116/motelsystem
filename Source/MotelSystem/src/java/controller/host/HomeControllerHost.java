/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.PostDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccountAllInfor;

/**
 *
 * @author royal
 */
@WebServlet(name = "HomeControllerHost", urlPatterns = {"/host/home", "/host/search", "/host/filter"})
public class HomeControllerHost extends BaseAuthenticationControllerHost {

    public PostDAO pDao;

    @Override
    public void init() {
        pDao = new PostDAO();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HomeControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HomeControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String action = request.getServletPath();
        switch (action) {
            case "/host/home":
                request.setAttribute("listPost", pDao.getListPostDetailForHomepage("", "", "desc"));
                request.getRequestDispatcher("/page/host/home-host.jsp").forward(request, response);
                break;
            case "/host/search":
                request.setAttribute("listPost", pDao.getListPostDetailForHomepage(request.getParameter("motel_name"), "", "desc"));
                request.getRequestDispatcher("/page/host/home-host.jsp").forward(request, response);
                break;
            case "/host/filter":
                String order = "asc";
                if (request.getParameter("order") != null && request.getParameter("order").equals("desc")) {
                    order = "desc";
                }
                request.setAttribute("listPost", pDao.getListPostDetailForHomepage("", request.getParameter("city"), order));
                request.getRequestDispatcher("/page/host/home-host.jsp").forward(request, response);
                break;
            default:
                break;
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
