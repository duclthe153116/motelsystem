/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.MotelDAO;
import dal.RequestDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccountAllInfor;
import model.Motel;
import model.RequestOfRoom;

/**
 *
 * @author doson
 */
public class ManageRequestControllerHost extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManageRequestControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Alo " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        MotelDAO md = new MotelDAO();
        HttpSession session = request.getSession();
        AccountAllInfor aai = (AccountAllInfor) session.getAttribute("accountAllInfor");
        ArrayList<Motel> listMotelByAccountID = md.getAllMotelByAccountID(aai.getAccountID());
        request.setAttribute("listMotelByAccountID", listMotelByAccountID);
        RequestDAO rd = new RequestDAO();
        int n = md.getAllMotelByAccountID(aai.getAccountID()).size();
        int[] motelID = new int[n];
        for (int i = 0; i < n; i++) {
            motelID[i] = md.getAllMotelByAccountID(aai.getAccountID()).get(i).getMotelID();
        }
        ArrayList<RequestOfRoom> listRequestOfRoom = rd.getAllRequestfromAccountByHost(motelID);
        request.setAttribute("listRequestOfRoom", listRequestOfRoom);
        request.getRequestDispatcher("../page/host/manageRequest.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        String motelID_raw = request.getParameter("motel");
        MotelDAO md = new MotelDAO();
        HttpSession session = request.getSession();
        AccountAllInfor aai = (AccountAllInfor) session.getAttribute("accountAllInfor");
        ArrayList<Motel> listMotelByAccountID = md.getAllMotelByAccountID(aai.getAccountID());
        ArrayList<RequestOfRoom> listRequestOfRoom = new ArrayList<RequestOfRoom>();
        RequestDAO rd = new RequestDAO();
        if (motelID_raw == null) {
            motelID_raw = "0";
        }
        if (motelID_raw.equals("0")) {
            int n = md.getAllMotelByAccountID(aai.getAccountID()).size();
            int[] motelID = new int[n];
            for (int i = 0; i < n; i++) {
                motelID[i] = md.getAllMotelByAccountID(aai.getAccountID()).get(i).getMotelID();
            }
            listRequestOfRoom = rd.getAllRequestfromAccountByHost(motelID);
            request.setAttribute("listRequestOfRoom", listRequestOfRoom);
            request.setAttribute("listMotelByAccountID", listMotelByAccountID);
            request.getRequestDispatcher("../page/host/manageRequest.jsp").forward(request, response);
        } else {
            try {
                int motelID = Integer.parseInt(motelID_raw);
                listRequestOfRoom = rd.getAllRequestOfRoomFromMotel(motelID);
                request.setAttribute("motelID", motelID);
                request.setAttribute("listRequestOfRoom", listRequestOfRoom);
                request.setAttribute("listMotelByAccountID", listMotelByAccountID);
                request.getRequestDispatcher("../page/host/manageRequest.jsp").forward(request, response);
            } catch (NumberFormatException e) {
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
