/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.MotelDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.Part;
import model.AccountAllInfor;
import model.Motel;

/**
 *
 * @author Admin
 */
@MultipartConfig(maxFileSize = 524288000, maxRequestSize = 524288000)
public class AddNewMotelControllerHost extends BaseAuthenticationControllerHost {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddNewMotelControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddNewMotelControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("../page/host/register-motel-host.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MotelDAO DBM = new MotelDAO();

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        HttpSession ses = request.getSession();
        AccountAllInfor loginedUser = (AccountAllInfor) ses.getAttribute("accountAllInfor");
        int ownerID = loginedUser.getAccountID();

        String name = request.getParameter("motelName");
        String phone = request.getParameter("motelPhone");
        String address = request.getParameter("motelAddress");
        String description = request.getParameter("motelDescription");
        int motelID = DBM.getMotelIDByAccountIDAndMotelName(ownerID, name);

        LocalDateTime now = LocalDateTime.now();
        String motelCreateAt = now.toString();
        boolean isCreateSuccess = false;
        Part item = request.getPart("motel_Image");
        isCreateSuccess = DBM.insertNewMotel(ownerID, name, phone, address, description, item, motelCreateAt);
        int count = 0;
        motelID = DBM.getMotelIDByAccountIDAndMotelName(ownerID, name);
        for (Part part : request.getParts()) {
            if (count < 4) {
                isCreateSuccess = DBM.insertImgMotelVerification(part, motelID);
                count++;
            }
        }
        response.sendRedirect("ManageMotel?success=" + isCreateSuccess);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
