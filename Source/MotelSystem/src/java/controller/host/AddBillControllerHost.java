
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.RequestDAO;
import model.BillService;
import dal.BillDAO;
import dal.BookingDAO;
import dal.MotelDAO;
import dal.RoomDAO;
import dal.ServicesDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccountAllInfor;
import model.Motel;
import model.Services;

/**
 *
 * @author LTC
 */
public class AddBillControllerHost extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddBillControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddBillControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int elecQ = Integer.parseInt(request.getParameter("elecQ"));
        int electT = Integer.parseInt(request.getParameter("elecT"));

        int motelId = Integer.parseInt(request.getParameter("motelId"));
        int roomId = Integer.parseInt(request.getParameter("roomId"));

        ServicesDAO sd = new ServicesDAO();

        BillDAO bd = new BillDAO();
        BookingDAO bkd = new BookingDAO();

        ArrayList<BillService> sobServiceList = sd.getAllSOBByMotelAndRoom(motelId, roomId);
        ArrayList<Services> serviceList = sd.getAllServiceByMotelid(motelId);

        int electIdSOB = -1;
        for (int i = 0; i < sobServiceList.size(); i++) {
            if ("Điện".equalsIgnoreCase(sobServiceList.get(i).getServiceName())) {
                electIdSOB = sobServiceList.get(i).getsOBId();
                break;
            }
        }

        int electId = -1;
        if (electIdSOB == -1) {
            for (int i = 0; i < serviceList.size(); i++) {
                if ("Điện".equalsIgnoreCase(serviceList.get(i).getServicesName())) {
                    electId = serviceList.get(i).getServicesID();
                }
            }
        }

        ArrayList<BillService> rqList = sd.getAllRequestByMotelAndRoom(motelId, roomId);
        ArrayList<BillService> rqServiceList = new ArrayList<>();

        for (int i = 0; i < rqList.size(); i++) {
            int count = 0;
            // count
            for (int j = i; j < rqList.size(); j++) {
                if (rqList.get(i).getServiceName().equalsIgnoreCase(rqList.get(j).getServiceName())) {
                    count++;
                }
            }

            int dupplicate = 0;
            // check dupplicate
            for (int k = i; k >= 0; k--) {
                if (rqList.get(i).getServiceName().equalsIgnoreCase(rqList.get(k).getServiceName())) {
                    dupplicate++;
                }
            }
            if (dupplicate <= 1) {
                rqList.get(i).setQuantity(count);
                rqServiceList.add(rqList.get(i));
            }
        }

        ArrayList<Integer> rqQuanList = new ArrayList<>();
        ArrayList<Integer> rqTotalList = new ArrayList<>();
        ArrayList<Integer> rqPriceList = new ArrayList<>();

        for (int i = 0; i < rqServiceList.size(); i++) {
            String indexq = rqServiceList.get(i).getRequestId() + "q";
            String indext = rqServiceList.get(i).getRequestId() + "t";
            String indexp = rqServiceList.get(i).getRequestId() + "p";

            rqQuanList.add(Integer.parseInt(request.getParameter(indexq)));
            rqTotalList.add(Integer.parseInt(request.getParameter(indext)));
            rqPriceList.add(Integer.parseInt(request.getParameter(indexp)));
        }

         int totalBill = Integer.parseInt(request.getParameter("totalBill"));
        int elecPrice = Integer.parseInt(request.getParameter("elecP"));

        // ---------------- insert dich vu ben bang SOB ------------------------
        boolean bkdT = bkd.createBooking(roomId);
        boolean bT = bd.createBill(bkd.getLastestBookingId(), totalBill);

        // insert tien dien
        if(electIdSOB == -1){
            boolean sobT = sd.createServiceOfBooking(bkd.getLastestBookingId(), electId);
            electIdSOB = sd.getLastestServiceOfBookingID();
        }
        boolean bdtT = bd.createBillDetailSOB(bd.getLastestBillId(), electIdSOB, 
                elecQ, electT, elecPrice);

        // insert dich vu hang thang
        for (int i = 0; i < sobServiceList.size(); i++) {
            if (!"Điện".equalsIgnoreCase(sobServiceList.get(i).getServiceName())) {
                bdtT = bd.createBillDetailSOB(bd.getLastestBillId(), sobServiceList.get(i).getsOBId(),
                        1, sobServiceList.get(i).getPrice(), sobServiceList.get(i).getPrice());
            }
        }

        // --------------- insert dich vu ben bang request ---------------------
        for (int i = 0; i < rqServiceList.size(); i++) {
            bdtT = bd.createBillDetailRq(bd.getLastestBillId(), rqServiceList.get(i).getRequestId(),
                    rqQuanList.get(i), rqTotalList.get(i), rqPriceList.get(i));
        }

        request.setAttribute("message", "Tạo hóa đơn thành công!");
        request.getRequestDispatcher("manageBill").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
