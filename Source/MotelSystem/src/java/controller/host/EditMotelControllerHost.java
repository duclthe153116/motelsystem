/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.AccountDAO;
import dal.ImgDao;
import dal.MotelDAO;
import dal.PostDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.MultipartConfig;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.Part;
import model.MotelDetail;
import model.MotelHasHost;

/**
 *
 * @author Admin
 */
@MultipartConfig(maxFileSize = 524288000, maxRequestSize = 524288000)
public class EditMotelControllerHost extends BaseAuthenticationControllerHost {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditMotelControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditMotelControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MotelDAO DBM = new MotelDAO();
        int motelID = Integer.parseInt(request.getParameter("id"));
        MotelDetail motelDetail = DBM.getMotelDetailByMotelID(motelID,true);
        request.setAttribute("motel", motelDetail);
        request.getRequestDispatcher("../page/host/motel-edit-host.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        String motelID = request.getParameter("motelID");
        ImgDao DBI = new ImgDao();
        String change = request.getParameter("change");
        if (change.equals("0")) {
            response.sendRedirect("ViewMotelDetail?id=" + motelID + "&action=notchange");
        } else {
            String motelName = request.getParameter("motelName");
            String motelPhone = request.getParameter("motelPhone");
            String motelStatus = request.getParameter("motelStatus");
            String motelDescription = request.getParameter("motelDescription");
            MotelDAO DBM = new MotelDAO();
            boolean isUpdateSuccess = false;
            if (motelStatus.equals("publish")) {
                isUpdateSuccess = DBM.updateMotelAccept(motelName, motelPhone, motelDescription, motelID);
            }
            Part item = request.getPart("image");
            if (item.getContentType() != null) {
                if (item.getContentType().contains("image")) {
                    isUpdateSuccess = DBI.updateImgMotel(item, motelID);
                }
            }
            if (isUpdateSuccess) {
                response.sendRedirect("ViewMotelDetail?id=" + motelID + "&action=success");
            } else {
                response.sendRedirect("ViewMotelDetail?id=" + motelID + "&action=fail");
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
