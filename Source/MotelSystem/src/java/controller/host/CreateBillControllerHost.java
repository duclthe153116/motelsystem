/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.MotelDAO;
import dal.RoomDAO;
import dal.ServicesDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccountAllInfor;
import model.BillService;
import model.Motel;
import model.Room;
import model.Services;

/**
 *
 * @author LTC
 */
public class CreateBillControllerHost extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateBillControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateBillControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String motelIdS = request.getParameter("motelId");
        String roomIdS = request.getParameter("roomId");

        HttpSession session = request.getSession();
        AccountAllInfor accAllInf = (AccountAllInfor) session.getAttribute("accountAllInfor");

        RoomDAO rd = new RoomDAO();
        MotelDAO md = new MotelDAO();
        ServicesDAO sd = new ServicesDAO();
        ArrayList<Motel> motelList = md.getAllMotelByAccountID(accAllInf.getAccountID());

        if (motelIdS == null) {
            request.setAttribute("motelList", motelList);
            request.getRequestDispatcher("../page/host/createBill.jsp").forward(request, response);
        } else if (motelIdS != null && roomIdS == null) {

            ArrayList<Room> roomList = rd.getAllRoomByMotelid(Integer.parseInt(motelIdS));

            request.setAttribute("motelList", motelList);
            request.setAttribute("motelId", Integer.parseInt(motelIdS));
            request.setAttribute("roomList", roomList);

            request.getRequestDispatcher("../page/host/createBill.jsp").forward(request, response);
        } else if (motelIdS != null && roomIdS != null) {

            ArrayList<Room> roomList = rd.getAllRoomByMotelid(Integer.parseInt(motelIdS));
            ArrayList<Services> serviceList = sd.getAllServiceByMotelid(Integer.parseInt(motelIdS));
            ArrayList<BillService> sobServices = sd.getAllSOBByMotelAndRoom(Integer.parseInt(motelIdS),
                    Integer.parseInt(roomIdS));
            ArrayList<BillService> sobServiceList = new ArrayList<>();
            for (int i = 0; i < sobServices.size(); i++) {
                if (!"Điện".equalsIgnoreCase(sobServices.get(i).getServiceName())) {
                    sobServiceList.add(sobServices.get(i));
                }
            }

            ArrayList<BillService> rqList = sd.getAllRequestByMotelAndRoom(Integer.parseInt(motelIdS),
                    Integer.parseInt(roomIdS));
            ArrayList<BillService> rqServiceList = new ArrayList<>();
            for (int i = 0; i < rqList.size(); i++) {
                int count = 0;
                // count
                for (int j = i; j < rqList.size(); j++) {
                    if (rqList.get(i).getServiceName().equalsIgnoreCase(rqList.get(j).getServiceName())) {
                        count++;
                    }
                }

                int dupplicate = 0;
                // check dupplicate
                for (int k = i; k >= 0; k--) {
                    if (rqList.get(i).getServiceName().equalsIgnoreCase(rqList.get(k).getServiceName())) {
                        dupplicate++;
                    }
                }
                if (dupplicate <= 1) {
                    rqList.get(i).setQuantity(count);
                    rqServiceList.add(rqList.get(i));
                }
            }

            for (int i = 0; i < rqServiceList.size(); i++) {
                rqServiceList.get(i).setTotal(rqServiceList.get(i).getPrice() * rqServiceList.get(i).getQuantity());
            }

            request.setAttribute("motelList", motelList);
            request.setAttribute("motelId", Integer.parseInt(motelIdS));
            request.setAttribute("roomId", Integer.parseInt(roomIdS));
            request.setAttribute("roomList", roomList);
            request.setAttribute("serviceList", serviceList);
            request.setAttribute("sobServiceList", sobServiceList);
            request.setAttribute("rqServiceList", rqServiceList);
            request.setAttribute("displayBtn", "on");

            request.getRequestDispatcher("../page/host/createBill.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
