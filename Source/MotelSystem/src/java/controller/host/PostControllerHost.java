/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.ImgDao;
import dal.MotelDAO;
import dal.PostDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.AccountAllInfor;
import model.PostDetail;

/**
 *
 * @author royal
 */
@WebServlet(name = "PostControllerHost", urlPatterns = {"/host/PostManagement", "/host/PostEditHost", "/host/PostDeleteHost", "/host/PostAddNew",})
@MultipartConfig(maxFileSize = 524288000, maxRequestSize = 524288000)//1.5mb
public class PostControllerHost extends BaseAuthenticationControllerHost {

    public PostDAO pDao;
    public MotelDAO mDao;
    public ImgDao iDao;

    @Override
    public void init() {
        pDao = new PostDAO();
        mDao = new MotelDAO();
        iDao = new ImgDao();
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PostControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PostControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();
        HttpSession ses = request.getSession();
        AccountAllInfor loginedUser = (AccountAllInfor) ses.getAttribute("accountAllInfor");
        int ownerID = loginedUser.getAccountID();
        switch (action) {
            case "/host/PostManagement":
                request.setAttribute("listPost", pDao.getAllPostOfHost(ownerID));
                request.setAttribute("listMotel", mDao.getAllMotelByAccountID(ownerID));
                request.getRequestDispatcher("/page/host/post-management.jsp").forward(request, response);
                break;
            case "/host/PostEditHost":
                int postID = Integer.parseInt(request.getParameter("postID"));
                request.setAttribute("post", pDao.getPostDetail(postID));
                request.getRequestDispatcher("/page/host/post-edit-form.jsp").forward(request, response);
                break;
            case "/host/PostAddNew":
                int motelID = Integer.parseInt(request.getParameter("motelID"));
                PostDetail p = new PostDetail();
                p.setModelID(motelID);
                p.setMaxPrice(pDao.getMaxPriceOfMotelInPost(motelID));
                p.setMinPrice(pDao.getMinPriceOfMotelInPost(motelID));
                p.setMotelName(mDao.getMotelByMotelID(motelID).getMotelName());
                p.setPhone(mDao.getMotelByMotelID(motelID).getMotelPhone());
                p.setAddress(mDao.getMotelByMotelID(motelID).getMotelAddress());
                p.setpDate(new Date());
                request.setAttribute("post", p);
                request.getRequestDispatcher("/page/host/post-edit-form.jsp").forward(request, response);
                break;
            case "/host/PostDeleteHost":
                int postIDToDelete = Integer.parseInt(request.getParameter("postID"));
                if (iDao.deleteImageByPostID(postIDToDelete)) {
                    if (pDao.deletePostByID(postIDToDelete)) {
                        request.setAttribute("action", "delete");
                        request.setAttribute("listPost", pDao.getAllPostOfHost(ownerID));
                        request.setAttribute("listMotel", mDao.getAllMotelByAccountID(ownerID));
                        request.getRequestDispatcher("/page/host/post-management.jsp").forward(request, response);
                    }
                }
                break;
            default:
                break;
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String action = request.getServletPath();
        HttpSession ses = request.getSession();
        AccountAllInfor loginedUser = (AccountAllInfor) ses.getAttribute("accountAllInfor");
        int ownerID = loginedUser.getAccountID();
        switch (action) {
            case "/host/PostAddNew":
                int motelID = Integer.parseInt(request.getParameter("motelID"));
                String pTitle = request.getParameter("title");
                String pContent = request.getParameter("content");
                if (pDao.newPost(motelID, pTitle, pContent, (new Date()), "pending", request.getParts())) {
                    request.setAttribute("listPost", pDao.getAllPostOfHost(ownerID));
                    request.setAttribute("listMotel", mDao.getAllMotelByAccountID(ownerID));
                    request.getRequestDispatcher("/page/host/post-management.jsp").forward(request, response);
                } else {
                    request.setAttribute("listPost", pDao.getAllPostOfHost(ownerID));
                    request.setAttribute("listMotel", mDao.getAllMotelByAccountID(ownerID));
                    request.setAttribute("action", "failtoadd");
                    request.getRequestDispatcher("/page/host/post-management.jsp").forward(request, response);
                }
                break;
            case "/host/PostEditHost":
                int postIDUpdate = Integer.parseInt(request.getParameter("postID"));
                String titleToUpdate = request.getParameter("title");
                String contentToUpdate = request.getParameter("content");
                if (pDao.updatePost(postIDUpdate, titleToUpdate, contentToUpdate, request.getParameter("listImageIDToDelete").split("-"), request.getParts())) {
                    request.setAttribute("post", pDao.getPostDetail(postIDUpdate));
                    request.setAttribute("action", "update");
                    request.getRequestDispatcher("/page/host/post-detail-host.jsp").forward(request, response);
                } else {
                    request.setAttribute("post", pDao.getPostDetail(postIDUpdate));
                    request.setAttribute("action", "failtoupdate");
                    request.getRequestDispatcher("/page/host/post-detail-host.jsp").forward(request, response);
                }
            default:
                break;
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
