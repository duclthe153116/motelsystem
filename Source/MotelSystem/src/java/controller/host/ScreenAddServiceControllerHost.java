
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.ServicesDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Services;

/**
 *
 * @author LTC
 */
public class ScreenAddServiceControllerHost extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ScreenAddServiceControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ScreenAddServiceControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
//        String s = request.getParameter("billId");
        
        int billId = Integer.parseInt(request.getParameter("billId"));
        int motelId = Integer.parseInt(request.getParameter("motelId"));
        int roomId = Integer.parseInt(request.getParameter("roomId"));
        String isFirst = request.getParameter("isFirst");
        ServicesDAO sd = new ServicesDAO();

        // cho nguoi dung chon phong tro va dich vu
        if (isFirst != null) {
            ArrayList<Services> serviceList = sd.getAllServiceByMotelid(motelId);

            request.setAttribute("motelId", motelId);
            request.setAttribute("roomId", roomId);
            request.setAttribute("billId", billId);
            request.setAttribute("serviceList", serviceList);
        } // cho nguoi dung xem gia dich vu
        else {
            int serviceId = Integer.parseInt(request.getParameter("serviceId"));
            int quantity = Integer.parseInt(request.getParameter("quantity"));

            int price = sd.getPriceByServiceId(serviceId);
            int total = price * quantity;

            ArrayList<Services> serviceList = sd.getAllServiceByMotelid(motelId);

            request.setAttribute("motelId", motelId);
            request.setAttribute("serviceId", serviceId);
            request.setAttribute("roomId", roomId);
            request.setAttribute("quantity", quantity);
            request.setAttribute("total", total);
            request.setAttribute("billId", billId);

            request.setAttribute("serviceList", serviceList);
        }
        request.getRequestDispatcher("../page/host/addService.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}