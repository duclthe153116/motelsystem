/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.AccountDAO;
import dal.MessageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccountAllInfor;
import model.Message;

/**
 *
 * @author coder
 */
public class CreateMessageControllerHost extends BaseAuthenticationControllerHost {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateMessageControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateMessageControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        MessageDAO DBM = new MessageDAO();
        HttpSession session = request.getSession();
        AccountAllInfor accountAllInfor = (AccountAllInfor) session.getAttribute("accountAllInfor");
        
        int idFrom = accountAllInfor.getAccountID();
        int idTo = Integer.parseInt(request.getParameter("idT"));
        String content = request.getParameter("content");
        Message mess = new Message(idFrom, idTo, content);
        boolean statusCreate = DBM.createMessage(mess);
        if(!statusCreate){
            request.setAttribute("error", "Gửi tin nhắn không thành công");
        }
        
        request.getRequestDispatcher("/host/ManageMessage?Top="+idTo).forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        MessageDAO DBM = new MessageDAO();
        HttpSession session = request.getSession();
        AccountAllInfor accountAllInfor = (AccountAllInfor) session.getAttribute("accountAllInfor");
        
        int idFrom = accountAllInfor.getAccountID();
        int idTo = Integer.parseInt(request.getParameter("idT"));
        String content = request.getParameter("content");
        Message mess = new Message(idFrom, idTo, content);
        boolean statusCreate = DBM.createMessage(mess);
        
        if(statusCreate){
            request.getRequestDispatcher("/host/ManageMessage?Top="+idTo).forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
