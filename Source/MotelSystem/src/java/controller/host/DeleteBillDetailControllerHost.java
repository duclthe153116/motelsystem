/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.BillDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.BillDetail;
import dal.BookingDAO;
import dal.ProfileDAO;
import dal.RequestDAO;
import dal.ServicesDAO;
import model.BillByHost;

/**
 *
 * @author LTC
 */
public class DeleteBillDetailControllerHost extends BaseAuthenticationControllerHost {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DeleteBillDetailControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DeleteBillDetailControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int billId = Integer.parseInt(request.getParameter("billId"));
        int billDetailId = Integer.parseInt(request.getParameter("billDetailId"));

        int motelId = Integer.parseInt(request.getParameter("motelId"));
        int roomId = Integer.parseInt(request.getParameter("roomId"));

        BillDAO bd = new BillDAO();
        ServicesDAO sd = new ServicesDAO();

        int sORID = sd.getServiceIdByBDTId(billId);
        BillDetail bdt = bd.getBillDetailByBillDTIdU(billDetailId);
        boolean reduceTotalBill = bd.reduceTotalBill(billId, bdt.getTotalBillDetail());

        boolean bdtT = bd.deleteBillDetailByBillDTId(billDetailId);

        if (bdt.getsORID() == 0) {
            RequestDAO rd = new RequestDAO();
            rd.deleteServiceRq(bdt.getRequestID());
        } else {
            boolean sOBT = sd.DeleteSOB(billDetailId);
        }

        int countBillDT = bd.countBillDetail(billId);

        if (countBillDT == 0) {
            BookingDAO bkd = new BookingDAO();
            int bookingId = bd.getBookingIdByBillId(billId);

            bd.deleteBill(billId);
            bkd.deleteBookingByBillId(billId);

            request.setAttribute("message", "Đã xóa thành công dịch vụ và hóa đơn!");
            request.getRequestDispatcher("manageBill").forward(request, response);
            return;
        }

        ProfileDAO pd = new ProfileDAO();

        String hostName = pd.getHostNameByMotelId(motelId);
        int total = bd.getTotalBillByBillId(billId);
        ArrayList<BillDetail> billDetailList = bd.getBillDetailsByBillIdU(billId);

        for (int i = 0; i < billDetailList.size(); i++) {
            // dich vu tu ben request
            if (billDetailList.get(i).getsORID() == 0) {
                billDetailList.get(i).setServicesName(sd.getServiceNameFromRequest(
                        billDetailList.get(i).getRequestID()));
                billDetailList.get(i).setServicesID(0);
            } else {
                billDetailList.get(i).setServicesName(sd.getServiceNameBySORId(
                        billDetailList.get(i).getBillDetailID()));
                billDetailList.get(i).setServicesID(sd.getServiceIDBySORId(
                        billDetailList.get(i).getBillDetailID()));
            }
        }

        BillByHost bill = bd.getBillByBillID(billId);

        request.setAttribute("billDetailList", billDetailList);
        request.setAttribute("motelId", motelId);
        request.setAttribute("roomId", roomId);
        request.setAttribute("billId", billId);
        request.setAttribute("total", total);
        request.setAttribute("hostName", hostName);
        request.setAttribute("success", "Xóa dịch vụ thành công!");
        request.getRequestDispatcher("../page/host/viewBillDetail.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
