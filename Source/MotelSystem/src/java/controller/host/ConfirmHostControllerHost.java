
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.BillDAO;
import dal.BookingDAO;
import dal.MotelDAO;
import dal.ProfileDAO;
import dal.RoomDAO;
import dal.ServicesDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.BillByHost;
import model.BillDetail;

/**
 *
 * @author LTC
 */
public class ConfirmHostControllerHost extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ConfirmHostControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ConfirmHostControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int billID = Integer.parseInt(request.getParameter("billID"));
        int motelId = Integer.parseInt(request.getParameter("motelID"));
        int roomId = Integer.parseInt(request.getParameter("roomID"));

        String confirmCus = request.getParameter("confirmCus");
        confirmCus = confirmCus.trim();

        BillDAO bd = new BillDAO();
        ServicesDAO sd = new ServicesDAO();

        if ("true".equalsIgnoreCase(confirmCus)) {
            boolean t1 = bd.updateConfirmHostAndDatePayment(billID);
            request.setAttribute("success", "Xác nhận thanh toán thành công! Hóa đơn đã được thanh toán.");
        } else {
            boolean t2 = bd.updateConfirmHost(billID);
            request.setAttribute("success", "Xác nhận thanh toán thành công!");
        }

        ProfileDAO pd = new ProfileDAO();

        String hostName = pd.getHostNameByMotelId(motelId);
        int total = bd.getTotalBillByBillId(billID);
        ArrayList<BillDetail> billDetailList = bd.getBillDetailsByBillIdU(billID);
        for (int i = 0; i < billDetailList.size(); i++) {
            // dich vu tu ben request
            if (billDetailList.get(i).getsORID() == 0) {
                billDetailList.get(i).setServicesName(sd.getServiceNameFromRequest(
                        billDetailList.get(i).getRequestID()));
                billDetailList.get(i).setServicesID(0);
            } else {
                billDetailList.get(i).setServicesName(sd.getServiceNameBySORId(
                        billDetailList.get(i).getBillDetailID()));
                billDetailList.get(i).setServicesID(sd.getServiceIDBySORId(
                        billDetailList.get(i).getBillDetailID()));
            }
        }

        BillByHost bill = bd.getBillByBillID(billID);

        request.setAttribute("billDetailList", billDetailList);
        request.setAttribute("motelId", motelId);
        request.setAttribute("roomId", roomId);
        request.setAttribute("billId", billID);
        request.setAttribute("bill", bill);
        request.setAttribute("total", total);
        request.setAttribute("hostName", hostName);

        request.getRequestDispatcher("../page/host/viewBillDetail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
