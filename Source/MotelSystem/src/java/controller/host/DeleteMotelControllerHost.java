/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.MotelDAO;
import dal.RoomDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class DeleteMotelControllerHost extends BaseAuthenticationControllerHost {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DeleteMotelControllerHost</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DeleteMotelControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String motelID = request.getParameter("id");
        String isAccept = request.getParameter("accept");
        MotelDAO DBM = new MotelDAO();
        boolean isDeleteSuccess = false;
        if(isAccept.equals("false")){
            isDeleteSuccess = DBM.deleteMotelNotAcceptByMotelID(Integer.parseInt(motelID));
        }else if (DBM.checkMotelNull(motelID)){
            isDeleteSuccess = DBM.deleteMotelNullByMotelID(motelID);
        }else {
            request.setAttribute("error", "Trọ có người thuê, xóa trọ thất bại");
            request.setAttribute("bg", "bg-danger");
            request.getRequestDispatcher("ManageMotel").forward(request, response);
        }
        if(isDeleteSuccess){
            request.setAttribute("error", "Xóa trọ thành công");
            request.setAttribute("bg", "bg-success");
        }else {
            request.setAttribute("error", "Xóa trọ thất bại");
            request.setAttribute("bg", "bg-danger");
        }
        
        request.getRequestDispatcher("ManageMotel").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
