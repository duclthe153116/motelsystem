/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.MotelDAO;
import dal.RoomDAO;
import dal.ServicesDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccountAllInfor;
import model.Motel;
import model.Room;
import model.Services;

/**
 *
 * @author coder
 */
public class CreateRoomControllerHost extends BaseAuthenticationControllerHost {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateRoomControllerHost</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateRoomControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        HttpSession session = request.getSession();
        AccountAllInfor accountAllInfor =  (AccountAllInfor) session.getAttribute("accountAllInfor");
        int accountID = accountAllInfor.getAccountID();
        MotelDAO DBMotel = new MotelDAO();
        ArrayList<Motel> listMotel = DBMotel.getAllMotelByAccountID(accountID);
        request.setAttribute("listMotel", listMotel);
        RoomDAO DBRoom = new RoomDAO();
        ArrayList<Integer> listMotelID = new ArrayList<>();
        for (int i = 0; i < listMotel.size(); i++){
            listMotelID.add(listMotel.get(i).getMotelID());
        }
        ArrayList<Room> listRoom = DBRoom.getAllRoomOfAccount(listMotelID);
        request.setAttribute("listRoom", listRoom);
        request.getRequestDispatcher("../page/host/createRoom.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        int motelID = Integer.parseInt(request.getParameter("motelID"));
        String motelName = request.getParameter("roomName");
        int numberMax = Integer.parseInt(request.getParameter("numberMax"));
        //int numberCurrent = Integer.parseInt(request.getParameter("numberCurrent"));
        double area = Double.parseDouble(request.getParameter("area"));
        double price = Double.parseDouble(request.getParameter("price"));
        String note = "";
        if (request.getParameter("note") != null){
            note = request.getParameter("note");
        }
        RoomDAO DBRoom = new RoomDAO();
        boolean statusCreate = DBRoom.CreateRoom(new Room(motelID, motelName, numberMax, 0, price, area, note));
        HttpSession session = request.getSession();
        session.setAttribute("statusCreate", statusCreate);
//        request.getRequestDispatcher("ManageSevices").forward(request, response);
        response.sendRedirect("ManageRoom");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
