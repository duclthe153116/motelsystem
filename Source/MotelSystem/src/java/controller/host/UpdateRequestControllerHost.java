/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.MotelDAO;
import dal.RequestDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccountAllInfor;
import model.Motel;
import model.RequestAllInfor;
import model.RequestOfRoom;

/**
 *
 * @author doson
 */
public class UpdateRequestControllerHost extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateRequestControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateRequestControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        String idRequest_raw = request.getParameter("idRequest");

            int idRequest = Integer.parseInt(idRequest_raw);
            RequestDAO rd = new RequestDAO();
            RequestAllInfor requestOfRoom = rd.getRequestOfRoomByIdRequest(idRequest);
            rd.updateRequestOfRoomByIdRequest(idRequest, "seen", requestOfRoom.getNoteRequestOfHost(),requestOfRoom.getPriceRequest());
            request.setAttribute("requestOfRoomById", requestOfRoom);
           request.getRequestDispatcher("../page/host/updateRequest.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        String idRequest_raw = request.getParameter("idRequest");
        String note = request.getParameter("note");
        String status = request.getParameter("status");
        String price_raw = request.getParameter("price");
        MotelDAO md = new MotelDAO();
        HttpSession session = request.getSession();
            AccountAllInfor aai = (AccountAllInfor) session.getAttribute("accountAllInfor");
        try {
            int idRequest = Integer.parseInt(idRequest_raw);
            double price = Double.parseDouble(price_raw);
            RequestDAO rd = new RequestDAO();
            if (status.equals("reject") && (note.equals("") || note.isEmpty() || note == null)) {
                request.setAttribute("err", "Nếu bạn muốn từ chối yêu cầu bạn cần nhập rõ lý do vào ô Ghi chú từ chủ trọ");
                RequestAllInfor requestOfRoom = rd.getRequestOfRoomByIdRequest(idRequest);
                rd.updateRequestOfRoomByIdRequest(idRequest, "seen", requestOfRoom.getNoteRequestOfHost(),requestOfRoom.getPriceRequest());
                String nameMotel = md.getNameMotelByRoomID(idRequest);
                request.setAttribute("nameMotel", nameMotel);
                request.setAttribute("idRequest", idRequest);
                request.setAttribute("requestOfRoomById", requestOfRoom);
                request.getRequestDispatcher("../page/host/updateRequest.jsp").forward(request, response);
            }
            else{
            rd.updateRequestOfRoomByIdRequest(idRequest, status, note,price);

            
            ArrayList<Motel> listMotelByAccountID = md.getAllMotelByAccountID(aai.getAccountID());
            request.setAttribute("listMotelByAccountID", listMotelByAccountID);
            /*        RequestDAO rd = new RequestDAO();*/
            int n = md.getAllMotelByAccountID(aai.getAccountID()).size();
            int[] motelID = new int[n];
            for (int i = 0; i < n; i++) {
                motelID[i] = md.getAllMotelByAccountID(aai.getAccountID()).get(i).getMotelID();
            }
            ArrayList<RequestOfRoom> listRequestOfRoom = rd.getAllRequestfromAccountByHost(motelID);
            request.setAttribute("listRequestOfRoom", listRequestOfRoom);
            request.getRequestDispatcher("../page/host/manageRequest.jsp").forward(request, response);
            }
        } catch (Exception e) {
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
