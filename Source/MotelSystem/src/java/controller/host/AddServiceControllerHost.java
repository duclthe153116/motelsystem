
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.BillDAO;
import dal.BookingDAO;
import dal.ServicesDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Services;

/**
 *
 * @author LTC
 */
public class AddServiceControllerHost extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddServiceControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddServiceControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int motelId = Integer.parseInt(request.getParameter("motelId"));
        int serviceId = Integer.parseInt(request.getParameter("serviceId"));
        int roomId = Integer.parseInt(request.getParameter("roomId"));
        int quantity = Integer.parseInt(request.getParameter("quantity"));
        int total = Integer.parseInt(request.getParameter("total"));
        int billId = Integer.parseInt(request.getParameter("billId"));

        BookingDAO bd = new BookingDAO();
        ServicesDAO sd = new ServicesDAO();
        BillDAO bid = new BillDAO();

        int bookingId = bid.getBookingIdByBillId(billId);

        boolean tSOB = sd.createServiceOfBooking(bookingId, serviceId);
        int sOR = sd.getLastestServiceOfBookingID();

        boolean tBillDetail = bid.createBillDetail(billId, sOR, quantity, total);
        bid.updateTotalBill(total, billId);

        ArrayList<Services> serviceList = sd.getAllServiceByMotelid(motelId);

        request.setAttribute("serviceId", serviceId);
        request.setAttribute("roomId", roomId);
        request.setAttribute("motelId", motelId);
        request.setAttribute("quantity", quantity);
        request.setAttribute("total", total);
        request.setAttribute("billId", billId);

        request.setAttribute("serviceList", serviceList);

        request.setAttribute("success", "Đã thêm dịch vụ.");
        request.getRequestDispatcher("../page/host/addService.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
