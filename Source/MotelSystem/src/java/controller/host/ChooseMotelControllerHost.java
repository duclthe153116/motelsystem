/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.MotelDAO;
import dal.RoomDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccountAllInfor;
import model.Motel;
import model.Room;
import model.RoomAllInfor;

/**
 *
 * @author doson
 */
public class ChooseMotelControllerHost extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChooseMotelControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChooseMotelControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String motelID_raw = request.getParameter("chooseMotel");
        int motelID = Integer.parseInt(motelID_raw);
        MotelDAO md = new MotelDAO();
        RoomDAO rd = new RoomDAO();
        HttpSession session = request.getSession();
        AccountAllInfor aai = (AccountAllInfor) session.getAttribute("accountAllInfor");
        ArrayList<Motel> listMotelByAccountID = md.getAllMotelByAccountID(aai.getAccountID());
        ArrayList<Integer> listMotelID = new ArrayList<>();
        for (int i = 0; i < listMotelByAccountID.size(); i++) {
            listMotelID.add(listMotelByAccountID.get(i).getMotelID());
        }
        ArrayList<RoomAllInfor> listRoomOfAccount = rd.getAllRoomOfMotelFromAccountID(listMotelID);
        ArrayList<Room> listRoomInMotel = rd.getAllListRoomDoNotHaveInBooking(aai.getAccountID(), motelID);
        request.setAttribute("listRoomInMotel", listRoomInMotel);
        request.setAttribute("listRoomOfAccount", listRoomOfAccount);
        request.setAttribute("listMotel", listMotelByAccountID);
        request.setAttribute("motelID", motelID);
        request.getRequestDispatcher("../page/host/createBooking.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
