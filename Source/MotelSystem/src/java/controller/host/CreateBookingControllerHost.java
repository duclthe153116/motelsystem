/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.BookingDAO;
import dal.MotelDAO;
import dal.RoomDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccountAllInfor;
import model.Booking;
import model.BookingAllInfor;
import model.Motel;
import model.Room;
import model.RoomAllInfor;

/**
 *
 * @author doson
 */
public class CreateBookingControllerHost extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateBookingControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateBookingControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        MotelDAO md = new MotelDAO();
        RoomDAO rd = new RoomDAO();
        HttpSession session = request.getSession();
        AccountAllInfor aai = (AccountAllInfor) session.getAttribute("accountAllInfor");
        ArrayList<Motel> listMotelByAccountID = md.getAllMotelByAccountID(aai.getAccountID());
        ArrayList<Integer> listMotelID = new ArrayList<>();
        for (int i = 0; i < listMotelByAccountID.size(); i++) {
            listMotelID.add(listMotelByAccountID.get(i).getMotelID());
        }
        ArrayList<RoomAllInfor> listRoomOfAccount = rd.getAllRoomOfMotelFromAccountID(listMotelID);
        request.setAttribute("listRoomOfAccount", listRoomOfAccount);
        request.setAttribute("listMotel", listMotelByAccountID);
        request.getRequestDispatcher("../page/host/createBooking.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        PrintWriter out = response.getWriter();
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        String roomID_raw = request.getParameter("chooseRoom");
//        out.println(roomID_raw);
        String startAt_raw = request.getParameter("startAt");
//        out.println(startAt_raw);
        String endAt_raw = request.getParameter("endAt");
//        out.println(endAt_raw);
        try {
            DateFormat dateStart = new SimpleDateFormat("yyyy-MM-dd");
            Date startAt = dateStart.parse(startAt_raw);
            Date endAt = dateStart.parse(endAt_raw);
//            out.println(startAt);
//            out.println(endAt);
            if (roomID_raw == null || roomID_raw.isEmpty() || (startAt_raw == null || startAt_raw.isEmpty()) || (endAt_raw == null || endAt_raw.isEmpty()) || endAt.before(startAt) == true) {
                MotelDAO md = new MotelDAO();
                RoomDAO rd = new RoomDAO();
                //BookingDAO bd = new BookingDAO();
                HttpSession session = request.getSession();
                AccountAllInfor aai = (AccountAllInfor) session.getAttribute("accountAllInfor");
//                out.println(aai.getFullName());
                ArrayList<Motel> listMotelByAccountID = md.getAllMotelByAccountID(aai.getAccountID());
//                out.println(listMotelByAccountID.size());
                ArrayList<Integer> listMotelID = new ArrayList<>();
                for (int i = 0; i < listMotelByAccountID.size(); i++) {
                    listMotelID.add(listMotelByAccountID.get(i).getMotelID());
                }
                ArrayList<RoomAllInfor> listRoomOfAccount = rd.getAllRoomOfMotelFromAccountID(listMotelID);
//                out.println(listRoomOfAccount.size());
                request.setAttribute("listRoomOfAccount", listRoomOfAccount);
                request.setAttribute("listMotel", listMotelByAccountID);
                request.setAttribute("err", "Thêm booking thất bại");
                request.getRequestDispatcher("../page/host/createBooking.jsp").forward(request, response);
            } else {
                int roomID = Integer.parseInt(roomID_raw);
//                out.println(roomID);
                MotelDAO md = new MotelDAO();
                RoomDAO rd = new RoomDAO();
                BookingDAO bd = new BookingDAO();
                HttpSession session = request.getSession();
                AccountAllInfor aai = (AccountAllInfor) session.getAttribute("accountAllInfor");
//                out.println(aai.getFullName());
                ArrayList<Motel> listMotelByAccountID = md.getAllMotelByAccountID(aai.getAccountID());
//                out.println(listMotelByAccountID.size());
                ArrayList<Integer> listMotelID = new ArrayList<>();
                for (int i = 0; i < listMotelByAccountID.size(); i++) {
                    listMotelID.add(listMotelByAccountID.get(i).getMotelID());
                }
                bd.createBooking(roomID, startAt_raw, endAt_raw);
                ArrayList<RoomAllInfor> listRoomOfAccount = rd.getAllRoomOfMotelFromAccountID(listMotelID);
//                out.println(listRoomOfAccount.size());
                
                request.setAttribute("listRoomOfAccount", listRoomOfAccount);
                request.setAttribute("listMotel", listMotelByAccountID);
                request.setAttribute("err", "Thêm booking thành công");

                request.getRequestDispatcher("../page/host/createBooking.jsp").forward(request, response);
            }
        } catch (Exception e) {
            MotelDAO md = new MotelDAO();
                RoomDAO rd = new RoomDAO();
                //BookingDAO bd = new BookingDAO();
                HttpSession session = request.getSession();
                AccountAllInfor aai = (AccountAllInfor) session.getAttribute("accountAllInfor");
//                out.println(aai.getFullName());
                ArrayList<Motel> listMotelByAccountID = md.getAllMotelByAccountID(aai.getAccountID());
//                out.println(listMotelByAccountID.size());
                ArrayList<Integer> listMotelID = new ArrayList<>();
                for (int i = 0; i < listMotelByAccountID.size(); i++) {
                    listMotelID.add(listMotelByAccountID.get(i).getMotelID());
                }
                ArrayList<RoomAllInfor> listRoomOfAccount = rd.getAllRoomOfMotelFromAccountID(listMotelID);
//                out.println(listRoomOfAccount.size());
                request.setAttribute("listRoomOfAccount", listRoomOfAccount);
                request.setAttribute("listMotel", listMotelByAccountID);
                request.setAttribute("err", "Thêm booking thất bại");
                request.getRequestDispatcher("../page/host/createBooking.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
