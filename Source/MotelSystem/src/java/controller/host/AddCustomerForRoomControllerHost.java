/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.BookingDAO;
import dal.MotelDAO;
import dal.ProfileDAO;
import dal.RoomDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccountAllInfor;
import model.Booking;
import model.BookingAllInfor;
import model.Motel;
import model.ProfileOfAccount;
import model.Room;
import model.RoomAllInfor;

/**
 *
 * @author doson
 */
public class AddCustomerForRoomControllerHost extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddCustomerForRoomControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddCustomerForRoomControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        HttpSession session = request.getSession();
        AccountAllInfor aai = (AccountAllInfor) session.getAttribute("accountAllInfor");
        //PrintWriter out = response.getWriter();
        String[] accID_raw = request.getParameterValues("accID");
        String roomID_raw = request.getParameter("roomID");
        int[] accID = new int[accID_raw.length];
        int roomID;
        ProfileDAO pd = new ProfileDAO();
        RoomDAO rd = new RoomDAO();
        MotelDAO mt = new MotelDAO();
        ArrayList<Integer> listAcc = new ArrayList<>();
        for (int i = 0; i < accID_raw.length; i++) {
            //out.println(accID_raw[i]);
        }
        //out.println(roomID_raw);
        for (int i = 0; i < accID_raw.length; i++) {
            accID[i] = Integer.parseInt(accID_raw[i]);
        }
        for (int i = 0; i < accID_raw.length; i++) {
            listAcc.add(accID[i]);
            //out.println(accID[i]);
        }
        roomID = Integer.parseInt(roomID_raw);
        //out.println(roomID);
        ArrayList<ProfileOfAccount> listProfile = pd.getAllListAccountDoNotHaveRoom(accID, aai.getAccountID());
        //out.println(listProfile.get(0).getFullName());
        Room r = rd.getRoomByRoomID(roomID);
        //out.println(r.getRoomName());
        String nameMotel = mt.getNameMotelByRoomID(roomID);
        //out.println(nameMotel);
        request.setAttribute("listAcc", listAcc);
        request.setAttribute("listProfile", listProfile);
        request.setAttribute("room", r);
        request.setAttribute("nameMotel", nameMotel);
        request.getRequestDispatcher("../page/host/addCustomerForRoom.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        String[] idAccount_raw = request.getParameterValues("idAccount");
        String roomID_raw = request.getParameter("roomID");
        String startAt_raw = request.getParameter("startAt");
        String endAt_raw = request.getParameter("endAt");
        RoomDAO rd = new RoomDAO();
        BookingDAO bd = new BookingDAO();
        String[] accID_raw = request.getParameterValues("accID");
        int[] accID2 = new int[accID_raw.length];
        ProfileDAO pd = new ProfileDAO();
        MotelDAO md = new MotelDAO();
        ArrayList<Integer> listAcc = new ArrayList<>();
        HttpSession session = request.getSession();
        AccountAllInfor aai = (AccountAllInfor) session.getAttribute("accountAllInfor");
        int roomID = Integer.parseInt(roomID_raw);
        int[] accID = new int[idAccount_raw.length];
        try {
            DateFormat dateStart = new SimpleDateFormat("yyyy-MM-dd");
            Date startAt = dateStart.parse(startAt_raw);
            Date endAt = dateStart.parse(endAt_raw);
            
        int numberOfCurrent = rd.getRoomByRoomID(roomID).getNumberOfPeopleCurrent();
        int numberOfMax = rd.getRoomByRoomID(roomID).getNumberOfPeopleMax();
            Booking b = bd.getBookingByRoomID(roomID);
            if ((numberOfCurrent + idAccount_raw.length <= numberOfMax) && (startAt.compareTo(b.getStartAt()) >= 0) && (startAt.compareTo(b.getEndAt()) <= 0) && (endAt.compareTo(b.getEndAt()) <= 0) && (endAt.compareTo(startAt) >= 0) && (idAccount_raw.length != 0)
                    && (!startAt_raw.isEmpty()) && (!endAt_raw.isEmpty())) {
                for (int i = 0; i < idAccount_raw.length; i++) {
                    accID[i] = Integer.parseInt(idAccount_raw[i]);
                }
                bd.addManyNewCustomerForRoom(roomID, accID, startAt_raw, endAt_raw);
                ArrayList<Motel> listMotelByAccountID = md.getAllMotelByAccountID(aai.getAccountID());
                ArrayList<Integer> listMotelID = new ArrayList<>();
                for (int i = 0; i < listMotelByAccountID.size(); i++) {
                    listMotelID.add(listMotelByAccountID.get(i).getMotelID());
                }
                ArrayList<RoomAllInfor> listRoomOfAccount = rd.getAllRoomOfMotelFromAccountID(listMotelID);
                request.setAttribute("listRoomOfAccount", listRoomOfAccount);
                request.getRequestDispatcher("../page/host/createBooking.jsp").forward(request, response);
            }
            else{
                for (int i = 0; i < accID_raw.length; i++) {
                    accID2[i] = Integer.parseInt(accID_raw[i]);
                }
                for (int i = 0; i < accID_raw.length; i++) {
                    listAcc.add(accID2[i]);
                }
                roomID = Integer.parseInt(roomID_raw);
                ArrayList<ProfileOfAccount> listProfile = pd.getAllListAccountDoNotHaveRoom(accID2, aai.getAccountID());
                Room r = rd.getRoomByRoomID(roomID);
                String nameMotel = md.getNameMotelByRoomID(roomID);
                request.setAttribute("listAcc", listAcc);
                request.setAttribute("listProfile", listProfile);
                request.setAttribute("room", r);
                request.setAttribute("nameMotel", nameMotel);
                request.setAttribute("err", "Thêm người vào phòng thất bại");
                request.getRequestDispatcher("../page/host/addCustomerForRoom.jsp").forward(request, response);
            }
        } catch (IOException | NumberFormatException | ParseException | ServletException e) {
            for (int i = 0; i < accID_raw.length; i++) {
                    accID2[i] = Integer.parseInt(accID_raw[i]);
                }
                for (int i = 0; i < accID_raw.length; i++) {
                    listAcc.add(accID2[i]);
                }
                roomID = Integer.parseInt(roomID_raw);
                ArrayList<ProfileOfAccount> listProfile = pd.getAllListAccountDoNotHaveRoom(accID2, aai.getAccountID());
                Room r = rd.getRoomByRoomID(roomID);
                String nameMotel = md.getNameMotelByRoomID(roomID);
                request.setAttribute("listAcc", listAcc);
                request.setAttribute("listProfile", listProfile);
                request.setAttribute("room", r);
                request.setAttribute("nameMotel", nameMotel);
                request.setAttribute("err", "Thêm người vào phòng thất bại");
                request.getRequestDispatcher("../page/host/addCustomerForRoom.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
