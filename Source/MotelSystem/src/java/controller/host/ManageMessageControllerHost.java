/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.AccountDAO;
import dal.MessageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Account;
import model.AccountAllInfor;
import model.Message;

/**
 *
 * @author Admin
 */
public class ManageMessageControllerHost extends BaseAuthenticationControllerHost {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MessageDAO DBM = new MessageDAO();
        AccountDAO DBA = new AccountDAO();
        HttpSession session = request.getSession();
        AccountAllInfor accountAllInfor = (AccountAllInfor) session.getAttribute("accountAllInfor");
        int accountTop = Integer.parseInt(request.getParameter("Top"));
        ArrayList<AccountAllInfor> accountList = DBA.GetAllAccountExcept(accountAllInfor.getAccountID(),accountTop);
        
        ArrayList<Message> messList = DBM.getAllMessageFromTo(accountAllInfor.getAccountID(), accountList.get(0).getAccountID());
        
        request.setAttribute("accountList", accountList);
        request.setAttribute("messList", messList);
        request.setAttribute("accountAllInfor", accountAllInfor);
        request.setAttribute("idAccountTo", accountList.get(0).getAccountID());
        request.getRequestDispatcher("../page/host/manageMessage.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
