/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.MotelDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import model.AccountAllInfor;
import model.MotelDetail;

/**
 *
 * @author Admin
 */
@MultipartConfig(maxFileSize = 524288000, maxRequestSize = 524288000)
public class ResubmitRequestMotelControllerhost extends BaseAuthenticationControllerHost {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ResubmitRequestMotelControllerhost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ResubmitRequestMotelControllerhost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        
        MotelDAO DBM = new MotelDAO();
        
        HttpSession ses = request.getSession();
        AccountAllInfor loginedUser = (AccountAllInfor) ses.getAttribute("accountAllInfor");
        int ownerID = loginedUser.getAccountID();

        String name = request.getParameter("motelName");
        String phone = request.getParameter("motelPhone");
        String address = request.getParameter("motelAddress");
        String description = request.getParameter("motelDescription");

        LocalDateTime now = LocalDateTime.now();
        String motelCreateAt = now.toString();
        boolean isCreateSuccess = false;
        boolean isDeleteSuccess = true;
        Part item = request.getPart("motel_Image");
        int motelID = Integer.parseInt(request.getParameter("motelID"));
        String flag1 = request.getParameter("input-resubmit-motelImage-verification");
        String flag2 = request.getParameter("input-resubmit-motelImage");
        if (flag2.equals("0")) {//do not edit motel image
            isCreateSuccess = DBM.editRequestMotel(motelID, ownerID, name, phone, address, description, item, motelCreateAt, true);
        } else {
            isCreateSuccess = DBM.editRequestMotel(motelID, ownerID, name, phone, address, description, item, motelCreateAt, false);
        }
        if (flag1.equals("1")) {// edit motel image verification 
            isDeleteSuccess = DBM.deleteMotelImageVerification(motelID);
            int count = 0;
            for (Part part : request.getParts()) {
                if (count < 4) {
                    isCreateSuccess = DBM.insertImgMotelVerification(part, motelID);
                    count++;
                }
            }
        }
        if (isCreateSuccess && isDeleteSuccess) {
            response.sendRedirect("ManageMotel?success=true");
        } else {
            response.sendRedirect("ManageMotel?success=false");
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
