/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.host;

import dal.MotelDAO;
import dal.ServicesDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccountAllInfor;
import model.Motel;
import model.Services;

/**
 *
 * @author coder
 */
public class ManageSevicesControllerHost extends BaseAuthenticationControllerHost {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ManageSeviceControllerHost</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ManageSeviceControllerHost at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        if (session.getAttribute("statusDelete") != null) {
            boolean statusDelete = (Boolean) session.getAttribute("statusDelete");
            session.setAttribute("statusDelete", null);
            request.setAttribute("statusDelete", statusDelete);
        }
        if (session.getAttribute("statusCreate") != null) {
            boolean statusDelete = (Boolean) session.getAttribute("statusCreate");
            session.setAttribute("statusCreate", null);
            request.setAttribute("statusCreate", statusDelete);
        }
        AccountAllInfor accountAllInfor = (AccountAllInfor) session.getAttribute("accountAllInfor");
        //get accountID in session
        int accountID = accountAllInfor.getAccountID();
        MotelDAO DBM = new MotelDAO();
        // get all motel of account
        ArrayList<Motel> listMotel = DBM.getAllMotelByAccountID(accountID);
        request.setAttribute("listMotel", listMotel);
        // init list motelID
        ArrayList<Integer> listMotelID = new ArrayList<>();
        for (int i = 0; i < listMotel.size(); i++) {
            listMotelID.add(listMotel.get(i).getMotelID());
        }
        // get all services of account
        ServicesDAO DBServices = new ServicesDAO();
        ArrayList<Services> listServices = new ArrayList<>();
        listServices = DBServices.getAllServicesOfAccount(listMotelID);
        request.setAttribute("listServices", listServices);
        request.getRequestDispatcher("../page/host/manageServices.jsp").forward(request, response);
//        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
