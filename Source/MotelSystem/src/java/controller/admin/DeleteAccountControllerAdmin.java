/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import commnon.SendEmail;
import dal.AccountDAO;
import dal.MotelDAO;
import dal.RoomDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.AccountAllInfor;
import model.Motel;
import model.Room;

/**
 *
 * @author Admin
 */
public class DeleteAccountControllerAdmin extends BaseAuthenticationControllerAdmin {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DeleteAccountControllerAdmin</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DeleteAccountControllerAdmin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AccountDAO DBA = new AccountDAO();
        int accountId = Integer.parseInt(request.getParameter("id"));
        AccountAllInfor account = DBA.getAllInforOfAccount(accountId);
        SendEmail sm = new SendEmail();
        
        boolean success1 = true;
        for(int i=0; i<account.getRoleName().size(); i++){
            if(account.getRoleName().get(i).equals("admin")){
                request.setAttribute("error", "Không thể khóa tài khoản admin");
                request.setAttribute("bg", "bg-danger");
                request.getRequestDispatcher("ManageAccount").forward(request, response);
                return;
            }else{
                success1 = DBA.deleteAccountByAccountId(account.getAccountID());                
                break;
            }
        }
        
        if(success1){
            try {
                sm.lockAccount(account.getEmail());
            } catch (MessagingException ex) {
                Logger.getLogger(DeleteAccountControllerAdmin.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("error", "Khóa tài khoản thành công");
            request.setAttribute("bg", "bg-success");
            //send email để vảo đây r đặt tên là lockAccount nhé, trong accountallinfor có mail r đấy nên đừng gọi thêm method nữa nhớ t vẽ sequence r
                
        }else{
            request.setAttribute("error", "Khóa tài khoản thất bại");
            request.setAttribute("bg", "bg-danger");
        }
        request.getRequestDispatcher("ManageAccount").forward(request, response);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
