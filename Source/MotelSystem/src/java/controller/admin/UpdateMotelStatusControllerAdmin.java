/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import commnon.SendEmail;
import dal.AccountDAO;
import dal.MotelDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.AccountAllInfor;
import model.Motel;

/**
 *
 * @author Admin
 */
public class UpdateMotelStatusControllerAdmin extends BaseAuthenticationControllerAdmin {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateRequestHostControllerAdmin</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateRequestHostControllerAdmin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        MotelDAO DBM = new MotelDAO();
        AccountDAO DBA = new AccountDAO();
        String status = request.getParameter("status");
        String motelID = request.getParameter("id");
        Motel motel = DBM.getMotelByMotelID(Integer.parseInt(motelID));
        String statusUpdate = "";
        String message = "";
        boolean isRejectSuccess = false;
        String email = DBM.getEmailByMotelId(motel.getMotelID());
        SendEmail sm = new SendEmail();
        //từ chối cấp quyền hoạt động
        if (status.equals("reject")) {
            isRejectSuccess = DBM.updateMotelStatus(motelID, "reject");
            if(isRejectSuccess){
                try {
                    sm.rejectRequestMotel(email, motel.getMotelName());
                } catch (MessagingException ex) {
                    Logger.getLogger(UpdateMotelStatusControllerAdmin.class.getName()).log(Level.SEVERE, null, ex);
                }
                statusUpdate = "Success";
                message = "Trọ đã bị từ chối cấp quyền hoạt động.";
            }else{
                statusUpdate = "Fail";
            }
        } else {
            boolean isHost = DBA.checkIsAccountHasRole("host", motel.getAccountID()); 
            boolean isUpdateMotelSuccess = DBM.updateMotelStatus(motelID, "publish");
            boolean isUpdateRoleSuccess = true;
            if (!isHost) {
                isUpdateRoleSuccess = DBA.updateRoleOfAccount(motel.getAccountID(), 2);
            }
            if (isUpdateMotelSuccess && isUpdateRoleSuccess) {
                try {
                    sm.updateMotelStatus(email, motel.getMotelName());
                } catch (MessagingException ex) {
                    Logger.getLogger(UpdateMotelStatusControllerAdmin.class.getName()).log(Level.SEVERE, null, ex);
                }
                statusUpdate = "Success";
               message = "Trọ đã được cấp quyền hoạt động.";
            } else {
                statusUpdate = "Fail";
            }
        }
        
        request.setAttribute("error", statusUpdate);
        request.setAttribute("message", message);

        request.getRequestDispatcher("ViewMotel?id="+motelID).forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
