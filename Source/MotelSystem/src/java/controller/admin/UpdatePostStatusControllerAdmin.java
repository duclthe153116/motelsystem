/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import commnon.SendEmail;
import dal.PostDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class UpdatePostStatusControllerAdmin extends BaseAuthenticationControllerAdmin {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdatePostControllerAdmin</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdatePostControllerAdmin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PostDAO DBP = new PostDAO();
        
        String status = request.getParameter("status");
        String postID = request.getParameter("id");
        
        String email = DBP.getEmailByPostID(Integer.parseInt(postID));
        String title = DBP.getTitleByPostID(Integer.parseInt(postID));
        
        SendEmail sm = new SendEmail();
        String statusUpdate = "";
        String message = "";
        if (status.equals("reject")) {
            boolean isRejectSuccess = DBP.updatePostStatus(postID, "reject");
            if(isRejectSuccess){
                statusUpdate = "Success";
                message = "Đã từ chối bài viết";
                try {
                    sm.rejectPost(email, title);
                } catch (MessagingException ex) {
                    Logger.getLogger(UpdatePostStatusControllerAdmin.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else{
                statusUpdate = "Fail";
                message = "Cập nhật thất bại";
            }
        } else {
            boolean success = DBP.updatePostStatus(postID, "publish");
            if (success) {
                try {
                    sm.updatePostStatus(email, title);
                } catch (MessagingException ex) {
                    Logger.getLogger(UpdatePostStatusControllerAdmin.class.getName()).log(Level.SEVERE, null, ex);
                }
                statusUpdate = "Success";
                message = "Đã chấp nhận bài viết";
            } else {
                statusUpdate = "Fail";
                message = "Cập nhật thất bại";
            }
        }
        
        request.setAttribute("error", statusUpdate);
        request.setAttribute("message", message);
        String flag = request.getParameter("request");
        request.getRequestDispatcher("ViewPost?id="+postID).forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
