/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.AccountAllInfor;

/**
 *
 * @author coder
 */
public abstract class BaseAuthenticationControllerAdmin extends HttpServlet {

    private boolean isAuthenticated(HttpServletRequest request) {
//        return request.getSession().getAttribute("account") != null;
        AccountAllInfor accountAllInfor = (AccountAllInfor) request.getSession().getAttribute("accountAllInfor");
        if (accountAllInfor == null) {
            return (false);
        }
        ArrayList<String> listRoleName = accountAllInfor.getRoleName();
        for (int i = 0; i < listRoleName.size(); i++) {
            if (listRoleName.get(i).equalsIgnoreCase("admin")) {
                return (true);
            }
        }
        return false;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (isAuthenticated(request)) {
            //business
            processGet(request, response);
        } else {
            response.setContentType("text/html;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {
                /* TODO output your page here. You may use following sample code. */
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet LoginController</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Chưa đăng nhập hoặc đăng nhập với vai trò chưa hợp lệ!</h1>");
                out.println("</body>");
                out.println("</html>");
            }
        }
    }

    protected abstract void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException;

    protected abstract void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException;

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (isAuthenticated(request)) {
            //business
            processPost(request, response);
        } else {
            response.setContentType("text/html;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {
                /* TODO output your page here. You may use following sample code. */
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet LoginController</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Chưa đăng nhập hoặc đăng nhập với vai trò chưa hợp lệ!</h1>");
                out.println("</body>");
                out.println("</html>");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
