/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import commnon.SendEmail;
import dal.AccountDAO;
import dal.MotelDAO;
import dal.ProfileDAO;
import dal.RoomDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.AccountAllInfor;
import model.Motel;

/**
 *
 * @author Admin
 */
public class UpdateRoleOfAccountControllerAdmin extends BaseAuthenticationControllerAdmin {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateRoleOfAccountControllerAdmin</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateRoleOfAccountControllerAdmin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AccountDAO DBA = new AccountDAO();
        SendEmail sm = new SendEmail();        
        
        String isAdmin = request.getParameter("isAdmin");
        String isHost = request.getParameter("isHost");
        String isCustomer = request.getParameter("isCustomer");
        int accountId = Integer.parseInt(request.getParameter("accountId"));

        AccountAllInfor accountAllInfor = DBA.getAllInforOfAccount(accountId);
        ArrayList<String> rolenames = accountAllInfor.getRoleName();
        boolean success1 = true;
        boolean success2 = true;
        boolean success3 = true;
        String emailTo = accountAllInfor.getEmail();

        for (String rolename : rolenames) {
        //update thanh admin 
            if (!rolename.equals("admin") && isAdmin.equals("true")) {                
                success1 = DBA.deleteRoleOfAccount(accountId, 2);
                success2 = DBA.deleteRoleOfAccount(accountId, 3);
                success3 = DBA.updateRoleOfAccount(accountId, 1);

                if (success1 & success2 & success3) {
                    // gui email
                    try {
                        sm.updateRole(emailTo, "admin");
                    } catch (MessagingException ex) {
                        Logger.getLogger(UpdateRoleOfAccountControllerAdmin.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                break;
            } // xoa role admin 
            else if (rolename.equals("admin") && isAdmin.equals("false")) {
                success1 = DBA.deleteRoleOfAccount(accountId, 1);
                success2 = DBA.updateRoleOfAccount(accountId, 3);

                if (success1 && success2) {
                    // gui email
                    try {
                        sm.deleteRole(emailTo, "admin");
                    } catch (MessagingException ex) {
                        Logger.getLogger(UpdateRoleOfAccountControllerAdmin.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                break;
            } //update thanh host
            else if (!rolename.equals("host") && isHost.equals("true")) {
                success1 = DBA.updateRoleOfAccount(accountId, 2);

                if (success1) {
                    // gui email
                    try {
                        sm.updateRole(emailTo, "host");
                    } catch (MessagingException ex) {
                        Logger.getLogger(UpdateRoleOfAccountControllerAdmin.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                break;
            }
            //bo vai tro host cua account 
            else if (rolename.equals("host") && isHost.equals("false")) {
                success1 = DBA.deleteRoleOfAccount(accountId, 2);

                if (success1) {
                    // gui email
                    try {
                        sm.deleteRole(emailTo, "host");
                    } catch (MessagingException ex) {
                        Logger.getLogger(UpdateRoleOfAccountControllerAdmin.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                break;
            }// khong co thay doi
            else {
                request.setAttribute("error", "Không có sự thay đổi");
                request.setAttribute("bg", "bg-danger");
                request.getRequestDispatcher("ManageAccount").forward(request, response);
                return;
            }
        }
        if (success1 && success2 && success3 ) {
            request.setAttribute("error", "Cập nhật khoản thành công");
            request.setAttribute("bg", "bg-success");
        } else {
            request.setAttribute("error", "Cập nhật khoản bài thất bại");
            request.setAttribute("bg", "bg-danger");
        }
        request.getRequestDispatcher("ManageAccount").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
