/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import commnon.SendEmail;
import dal.AccountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;

/**
 *
 * @author Admin
 */
public class OpenLockAccountControllerAdmin extends BaseAuthenticationControllerAdmin {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OpenLockAccountControllerAdmin</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OpenLockAccountControllerAdmin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AccountDAO DBA = new AccountDAO();
        SendEmail sm = new SendEmail();
        int accountID = Integer.parseInt(request.getParameter("id"));
        boolean statusupdate = DBA.OpenLockAccountByAccountId(accountID);
        String email = DBA.getEmailByAccountId(accountID);
        String username =DBA.getAccountByAccountID(accountID).getUsername();
        
        if(statusupdate){
            try {
                sm.openLockAccount(email);//OpenLockAccount
            } catch (MessagingException ex) {
                Logger.getLogger(DeletePostControllerAdmin.class.getName()).log(Level.SEVERE, null, ex);
            }
            request.setAttribute("error", "Mở khóa tài khoản thành công");
            request.setAttribute("bg", "bg-success");
        }else{
            request.setAttribute("error", "Mở khóa tài khoản thất bại");
            request.setAttribute("bg", "bg-danger");
        }
        request.getRequestDispatcher("ManageAccount").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
