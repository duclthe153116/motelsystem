/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.all;

import commnon.HashPassword;
import controller.customer.*;
import dal.AccountDAO;
import dal.DBContext;
import dal.SendEmailDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LTC
 */
public class ForgetController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ForgetControllerCustomer</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ForgetControllerCustomer at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/page/forget.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String emailTo = request.getParameter("email");

        AccountDAO accDAO = new AccountDAO();
        ArrayList<String> emailList = accDAO.getAllEmail();
        for (String email : emailList) {
            if (email.equals(emailTo)) {
                int accID = accDAO.getAccountIDByEmail(emailTo);
                commnon.SendEmail sm = new commnon.SendEmail();

                try {
                    String newPassword = sm.resetPassword(emailTo);
                    SendEmailDAO smd = new SendEmailDAO();
                    HashPassword hashPass = new HashPassword();
                    boolean isSent = smd.updatePasswordByAccountID(hashPass.encryptPassword(newPassword), accID);
                    request.setAttribute("emailTo", emailTo);
                    request.setAttribute("success", "Mật khẩu mới đã được gửi đến email của bạn.");
                    request.getRequestDispatcher("/page/forget.jsp").forward(request, response);
                    } catch (MessagingException ex) {
                    Logger.getLogger(ForgetController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        request.setAttribute("emailTo", emailTo);
        request.setAttribute("error", "Email này chưa đăng ký!");
        request.getRequestDispatcher("/page/forget.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
