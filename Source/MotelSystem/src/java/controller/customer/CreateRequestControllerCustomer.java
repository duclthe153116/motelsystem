/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.customer;

import dal.RequestDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.AccountAllInfor;
import model.Motel;
import model.Request;
import model.RequestAllInfor;
import model.RequestOfRoom;
import model.Room;
import model.RoomOfAccount;

/**
 *
 * @author doson
 */
public class CreateRequestControllerCustomer extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CreateRequestControllerCustomer</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CreateRequestControllerCustomer at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        HttpSession session = request.getSession();
        AccountAllInfor account = (AccountAllInfor)session.getAttribute("accountAllInfor");
        RequestDAO rd = new RequestDAO();
        ArrayList<Motel> listMotel = rd.getAllMotelOfAccountByCustomer(account.getAccountID());
        String idMotel_raw = request.getParameter("motel");
        try {
            int idMotel = Integer.parseInt(idMotel_raw);
            ArrayList<Room> listRoom = rd.getAllRoomFromMotelOfAccountByCustomer(account.getAccountID(), idMotel);
            request.setAttribute("motelID", idMotel);
            request.setAttribute("listMotelByAccountID", listMotel);
            request.setAttribute("listRoom", listRoom);
            request.getRequestDispatcher("../page/customer/createRequest.jsp").forward(request, response);
        } catch (Exception e) {
        }
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        //PrintWriter out = response.getWriter();
        RequestDAO rd = new RequestDAO();
        String content = request.getParameter("content");
        String idRoom_raw = request.getParameter("room");
        try {
            int idRoom = Integer.parseInt(idRoom_raw);
            if(content.equals("") || content.isEmpty() || content == null){
                request.setAttribute("err", "Thêm yêu cầu thất bại (Bạn cần nhập nội dung yêu cầu)");
                request.getRequestDispatcher("../page/customer/createRequest.jsp").forward(request, response);
            }
            else{
        rd.addRequestByCustomer(idRoom, content);
        HttpSession session = request.getSession();
        AccountAllInfor account = (AccountAllInfor)session.getAttribute("accountAllInfor");
        ArrayList<RequestAllInfor> listRequest = rd.getAllRequestFromAccountByCustomer(account.getAccountID());
        request.setAttribute("listRequest", listRequest);
            request.getRequestDispatcher("../page/customer/request.jsp").forward(request, response);
            }
        } catch (Exception e) {
            request.setAttribute("err", "Thêm yêu cầu không thành công!!!");
            request.getRequestDispatcher("../page/customer/createRequest.jsp").forward(request, response);
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
