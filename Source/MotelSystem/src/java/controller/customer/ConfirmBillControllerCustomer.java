/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.customer;

import dal.BillDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class ConfirmBillControllerCustomer extends BaseAuthenticationControllerCustomer {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ConfirmBillControllerCustomer</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ConfirmBillControllerCustomer at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BillDAO BDB = new BillDAO();
        int billId = Integer.parseInt(request.getParameter("billId"));
        String confirmHost = request.getParameter("confirmHost");
        confirmHost = confirmHost.trim();

        boolean success = false;
        if ("true".equalsIgnoreCase(confirmHost)) {
            success = BDB.updateConfirmCusAndDatePayment(billId);
            request.setAttribute("error", "Xác nhận thành công. Hóa đơn đã được thanh toán!");
        } else {
            success = BDB.updateConfirmCus(billId);
            request.setAttribute("error", "Xác nhận thanh toán thành công!");
        }
        
        if(success == false) {
            request.setAttribute("error", "Xác nhận thanh toán thất bại");
            request.setAttribute("bg", "bg-danger");
        }
        
        request.getRequestDispatcher("managebill").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void processPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
