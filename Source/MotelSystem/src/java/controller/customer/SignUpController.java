/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.customer;

import commnon.HashPassword;
import dal.AccountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Account;
import model.Profile;

/**
 *
 * @author Admin
 */
public class SignUpController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        request.getRequestDispatcher("../page/SignUpAccount.jsp").forward(request, response);
        System.out.println("test dang ky");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");
        AccountDAO AD = new AccountDAO();
        HashPassword hashPass = new HashPassword();
        String username = request.getParameter("user_name");
        boolean existUsername = AD.CheckUsernameExist(username);  
        String email = request.getParameter("email");
        boolean existEmail = AD.CheckEmailExist(email);
        if(existUsername || existEmail){
            String error = "Tên tài khoản hoặc email đã được sử dụng";
            request.setAttribute("error", error);
            request.getRequestDispatcher("../page/SignUpAccount.jsp").forward(request, response);
            return;
        }
        String password = request.getParameter("user_password");
        String fullname = request.getParameter("name");
        String phone = request.getParameter("contact_no");
        String address = request.getParameter("address");        
        String date = request.getParameter("DOB");
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
        Date dob = null;
        try {
            dob = dt.parse(date);
        } catch (ParseException ex) {
            Logger.getLogger(SignUpController.class.getName()).log(Level.SEVERE, null, ex);
        }
        String gender = request.getParameter("gender");
        String imageProfile = request.getParameter("image");
        Account account = new Account(username, hashPass.encryptPassword(password));
        boolean createstatus = AD.CreateAccount(account);
        if (!createstatus) {
            String error = "Tạo tài khoản không thành công";
            request.setAttribute("error", error);
            request.getRequestDispatcher("../page/SignUpAccount.jsp").forward(request, response);
        } else {
            Account getA = AD.GetAccountByUserNameAndPass(account);
            Profile profile = new Profile(getA.getAccountID(), fullname, phone, address, email, dob, gender, imageProfile);
            boolean profilestatus = AD.CreateProfile(profile);
            boolean rolestatus = AD.CreateRoleOfAccount(getA.getAccountID());
            if(profilestatus && rolestatus){
                response.sendRedirect("login");
            }else{
                String error = "Tạo tài khoản không thành công";
                request.setAttribute("error", error);
                request.getRequestDispatcher("../page/SignUpAccount.jsp").forward(request, response);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
