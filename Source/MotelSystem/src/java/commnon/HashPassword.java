/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package commnon;
import java.security.NoSuchAlgorithmException;  
import java.security.MessageDigest;  
/**
 *
 * @author coder
 */
public class HashPassword {

    public String encryptPassword(String password) {
        String result = null;
        try {
            /* MessageDigest instance for MD5. */
            MessageDigest m = MessageDigest.getInstance("MD5");

            /* Add plain-text password bytes to digest using MD5 update() method. */
            m.update(password.getBytes());

            /* Convert the hash value into bytes */
            byte[] bytes = m.digest();

            /* The bytes array has bytes in decimal form. Converting it into hexadecimal format. */
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                s.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }

            /* Complete hashed password in hexadecimal format */
            result = s.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return result;
    }
    public boolean comparePassword(String input, String base){
        if (encryptPassword(input).equals(base)) return true;
        return false;
    }
    public static void main(String[] args) {
        // Duc@260201 -> c008e60f8ce743f03667ec83b59ae0cb
        HashPassword p = new HashPassword();
        System.out.println(p.comparePassword("hieupm", "107852fa8092d8141bb1a8a8913b6373"));
        System.out.println(p.encryptPassword("admin"));
        System.out.println(p.encryptPassword("duclt"));
        System.out.println(p.encryptPassword("tuanhd"));
        System.out.println(p.encryptPassword("tuanvq"));
        System.out.println(p.encryptPassword("luongtth"));
        System.out.println(p.encryptPassword("hieupm"));
        System.out.println(p.comparePassword("duclt","27506f07208ccf02e29689ab0627d402"));
//        System.out.println(p.comparePassword("Duc@260201", "c008e60f8ce743f03667ec83b59ae0cb"));
//        System.out.println(p.encryptPassword("Duc@26020"));
    }
}
