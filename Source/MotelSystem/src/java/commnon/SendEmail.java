/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commnon;

import model.*;
import java.util.Properties;
import java.util.Random;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author LTC
 */
public class SendEmail {

    public void updatePostStatus(String toAddress, String title) throws MessagingException {

        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        final String userName = "swp.motel.12@outlook.com";
        final String password = "Motel123456789";

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        });

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);

//        msg.setSubject("Update New Post");
//
//        String htmlContent = "Your post is approved by admin. Noew everyone can see it.";
        msg.setSubject("Cập Nhật Bài Đăng Mới", "UTF-8");

        String htmlContent = "Bài đăng của bạn: '" + title + "' đã được phê duyệt. Từ bây giờ, mọi người có thể nhìn thấy bài đăng của bạn.";
        msg.setText(htmlContent, "UTF-8");

        // sends the e-mail
        Transport.send(msg);

    }

    public void deletePost(String toAddress, String title) throws MessagingException {

        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        final String userName = "swp.motel.12@outlook.com";
        final String password = "Motel123456789";

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        });

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);

        msg.setSubject("Thông Báo Xóa Bài Đăng","UTF-8");

        String htmlContent = "Bài đăng của bạn có tiêu đề: '" + title + "' đã bị xóa vì vi phạm tiêu chuẩn "
                + "cộng đồng.";

//        msg.setSubject("Xóa Bài Đăng Vi Phạm Tiêu Chuẩn");
//
//        String htmlContent = "Bài đăng của bạn: " + title + "đã bị xóa khỏi hệ thống vì vi phạm tiêu chuẩn cộng đồng.";
        msg.setText(htmlContent,"UTF-8");

        // sends the e-mail
        Transport.send(msg);

    }
    
    public void lockAccount(String toAddress) throws MessagingException {

        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        final String userName = "swp.motel.12@outlook.com";
        final String password = "Motel123456789";

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        });

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);

        msg.setSubject("Thông Báo Khóa Tài Khoản", "UTF-8");

        String htmlContent = "Tài khoản của bạn đã bị khóa vì vi phạm tiêu chuẩn của hệ thống nhà trọ.";

//        msg.setSubject("Xóa Trọ Vi Phạm Tiêu Chuẩn");
//
//        String htmlContent = "Trọ của bạn: " + motelName + "đã bị xóa khỏi hệ thống vì vi phạm tiêu chuẩn cộng đồng.";
        msg.setText(htmlContent, "UTF-8");

        // sends the e-mail
        Transport.send(msg);

    }
    
    public void openLockAccount(String toAddress) throws MessagingException {

        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        final String userName = "swp.motel.12@outlook.com";
        final String password = "Motel123456789";

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        });

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);

        msg.setSubject("Thông Báo Mở Tài Khoản", "UTF-8");

        String htmlContent = "Tài khoản của bạn đã được mở.";

//        msg.setSubject("Xóa Trọ Vi Phạm Tiêu Chuẩn");
//
//        String htmlContent = "Trọ của bạn: " + motelName + "đã bị xóa khỏi hệ thống vì vi phạm tiêu chuẩn cộng đồng.";
        msg.setText(htmlContent, "UTF-8");

        // sends the e-mail
        Transport.send(msg);

    }
    
    public void deleteMotel(String toAddress, String motelName) throws MessagingException {

        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        final String userName = "swp.motel.12@outlook.com";
        final String password = "Motel123456789";

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        });

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);

        msg.setSubject("Thông Báo Xóa Trọ", "UTF-8");

        String htmlContent = "Trọ của bạn: '" + motelName + "' đã bị xóa khỏi hệ thống vì vi phạm tiêu"
                + " chuẩn cộng đồng";

//        msg.setSubject("Xóa Trọ Vi Phạm Tiêu Chuẩn");
//
//        String htmlContent = "Trọ của bạn: " + motelName + "đã bị xóa khỏi hệ thống vì vi phạm tiêu chuẩn cộng đồng.";
        msg.setText(htmlContent, "UTF-8");

        // sends the e-mail
        Transport.send(msg);

    }

    public void lockMotel(String toAddress, String motelName) throws MessagingException {

        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        final String userName = "swp.motel.12@outlook.com";
        final String password = "Motel123456789";

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        });

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);

        msg.setSubject("Thông Báo Khóa Trọ", "UTF-8");

        String htmlContent = "Trọ của bạn: '" + motelName + "' đã bị khóa vì vi phạm tiêu"
                + " chuẩn cộng đồng";

//        msg.setSubject("Xóa Trọ Vi Phạm Tiêu Chuẩn");
//
//        String htmlContent = "Trọ của bạn: " + motelName + "đã bị xóa khỏi hệ thống vì vi phạm tiêu chuẩn cộng đồng.";
        msg.setText(htmlContent, "UTF-8");

        // sends the e-mail
        Transport.send(msg);

    }
    
    public void openLockMotel(String toAddress, String motelName) throws MessagingException {

        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        final String userName = "swp.motel.12@outlook.com";
        final String password = "Motel123456789";

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        });

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);

        msg.setSubject("Thông Báo Mở Khóa Trọ", "UTF-8");

        String htmlContent = "Trọ của bạn: '" + motelName + "' đã được mở trở lại.";

//        msg.setSubject("Xóa Trọ Vi Phạm Tiêu Chuẩn");
//
//        String htmlContent = "Trọ của bạn: " + motelName + "đã bị xóa khỏi hệ thống vì vi phạm tiêu chuẩn cộng đồng.";
        msg.setText(htmlContent, "UTF-8");

        // sends the e-mail
        Transport.send(msg);

    }

    public void updateMotelStatus(String toAddress, String motelName) throws MessagingException {

        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        final String userName = "swp.motel.12@outlook.com";
        final String password = "Motel123456789";

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        });

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);

        msg.setSubject("Thông Báo Đăng Ký Trọ", "UTF-8");

        String htmlContent = "Yêu cầu đăng ký trọ của bạn: '" + motelName + "' đã được quản trị viên phê duyệt"
                + " thành công.";

//        msg.setSubject("Cập Nhật Phê Duyệt Đăng Ký Trọ");
//
//        String htmlContent = "Trọ của bạn: " + motelName + "đã được phê duyệt. Từ bây giờ bạn có thể sử dụng để kinh doanh";
        msg.setText(htmlContent,"UTF-8");

        // sends the e-mail
        Transport.send(msg);

    }

    public void updateRole(String toAddress, String role) throws MessagingException {

        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        final String userName = "swp.motel.12@outlook.com";
        final String password = "Motel123456789";

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        });

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);

        msg.setSubject("Cập Nhật Vai Trò Tài Khoản", "UTF-8");
        String htmlContent = "";

        if (role.equals("host")) {
            htmlContent = "Tài khoản của bạn đã đăng ký thành công làm chủ trọ.";
        }

        if (role.equals("admin")) {
            htmlContent = "Tài khoản của bạn được đăng nhập với tư cách quản trị viên.";
        }
        msg.setText(htmlContent, "UTF-8");

        // sends the e-mail
        Transport.send(msg);

    }

    public void deleteRole(String toAddress, String role) throws MessagingException {

        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        final String userName = "swp.motel.12@outlook.com";
        final String password = "Motel123456789";

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        });

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);

        msg.setSubject("Cập Nhật Vai Trò Tài Khoản", "UTF-8");
        String htmlContent = "";

        if (role.equals("admin")) {
            htmlContent = "Tài khoản của bạn đã bị xóa quyền quản trị viên.";
        }

        if (role.equals("customer")) {
            htmlContent = "Tài khoản của bạn hiện chỉ có thể đăng nhập với tư cách khách hàng.";
        }

        if (role.equals("host")) {
            htmlContent = "Tài khoản của bạn đã bị xóa tư cách chủ trọ.";
        }
        msg.setText(htmlContent, "UTF-8");

        // sends the e-mail
        Transport.send(msg);

    }

    public String resetPassword(String toAddress) throws MessagingException {

        String newPassword = randomPassword();

        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        final String userName = "swp.motel.12@outlook.com";
        final String password = "Motel123456789";

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        });

        // creates a new e-mail message
        Message msg = new MimeMessage(session);

        msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
        msg.addHeader("format", "flowed");
        msg.addHeader("Content-Transfer-Encoding", "8bit");

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(toAddress)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);
//        msg.setSubject("Quên Mật Khẩu");
        msg.setSubject("Forgot Password");

//        String htmlContent = "<p> Mật Khẩu mới của bạn là: </p>" + "<h1>" + newPassword + "</h1>" +
//                "." + "<p>" + "Dùng mật khẩu mới để đăng nhập." + "</p>";
        String htmlContent = "Your new password is " + newPassword;
        msg.setText(htmlContent);

        // sends the e-mail
        Transport.send(msg);

        return newPassword;
    }

    public String randomPassword() {
        String s = "qwertyuiopasdfghjklzxcvbnm";
        s = s + s.toUpperCase() + "0123456789";
        int length = 8;
        String password = "";
        Random random = new Random();
        int index = 0;

        for (int i = 0; i < length; i++) {
            index = random.nextInt(s.length());
            password += s.charAt(index);
        }
        return password;
    }

    public void rejectRequestMotel(String email, String motelName) throws MessagingException {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        final String userName = "swp.motel.12@outlook.com";
        final String password = "Motel123456789";

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        });

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(email)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);

        msg.setSubject("Thông Báo Đăng Ký Trọ", "UTF-8");

        String htmlContent = "Yêu cầu đăng ký trọ của bạn: '" + motelName + "' đã bị từ chối. Vui lòng kiểm tra lại.";

//        msg.setSubject("Cập Nhật Phê Duyệt Đăng Ký Trọ");
//
//        String htmlContent = "Trọ của bạn: " + motelName + "đã được phê duyệt. Từ bây giờ bạn có thể sử dụng để kinh doanh";
        msg.setText(htmlContent,"UTF-8");

        // sends the e-mail
        Transport.send(msg);
    }

    public void rejectPost(String email, String title) throws MessagingException{
        Properties properties = new Properties();
        properties.put("mail.smtp.host", "smtp.office365.com");
        properties.put("mail.smtp.port", 587);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");

        // creates a new session with an authenticator
        final String userName = "swp.motel.12@outlook.com";
        final String password = "Motel123456789";

        Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication(userName, password);
            }
        });

        // creates a new e-mail message
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress(userName));
        InternetAddress[] toAddresses = {new InternetAddress(email)};
        msg.setRecipients(Message.RecipientType.TO, toAddresses);

        msg.setSubject("Thông Báo Đăng Ký Trọ", "UTF-8");

        String htmlContent = "Yêu cầu đăng bài của bạn: '" + title + "' đã bị từ chối. Vui lòng kiểm tra lại.";

//        msg.setSubject("Cập Nhật Phê Duyệt Đăng Ký Trọ");
//
//        String htmlContent = "Trọ của bạn: " + motelName + "đã được phê duyệt. Từ bây giờ bạn có thể sử dụng để kinh doanh";
        msg.setText(htmlContent,"UTF-8");

        // sends the e-mail
        Transport.send(msg);
    }

}
