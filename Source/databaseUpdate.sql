drop database Motel_System
go
create database Motel_System
go
Use Motel_System
go
Create table Account(
	AccountID int IDENTITY(1,1) primary key,
	userName nvarchar(50),
	[passWord] nvarchar(50),
	isDeleteAccount bit,
)
go
Create table [Role](
	RoleID int IDENTITY(1,1) primary key,
	roleName nvarchar(50),
	isDeleteRole bit,
)
go
Create table RoleOfAccount(
	RoleOfAccountID int IDENTITY(1,1) primary key,
	AccountID int references Account(AccountID),
	RoleID int references [Role](RoleID),
	isDeleteRoleOfAccount bit,
)
go
Create table [Profile](
	profileID int IDENTITY(1,1) primary key,
	AccountID int unique references Account(AccountID),
	fullName nvarchar(50),
	phone varchar(10) check(phone like '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	[address] nvarchar(100),
	email varchar(100),
	DOB date,
	Gender nvarchar(20),
	imageProfile Image,
	isDeleteProfile bit,
)
go
Create table Motel(
	motelID int IDENTITY(1,1) primary key,
	AccountID int references Account(AccountID),
	motelName nvarchar(50),
	motelAddress nvarchar(100),
	motelPhone varchar(10) check(motelPhone like '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
	motelStatus nvarchar(20),
	motelDescription nvarchar(1000),
	motelCreateAt date,
	motelImage Image,
	isDeleteMotel bit,
)
go
Create table MotelImage(
	motelImageID int IDENTITY(1,1) primary key,
	motelID int references Motel(motelID),
	imageLink Image,
	isDeleteMotelImage bit,
)
go
Create table Post(
	postID int IDENTITY(1,1) primary key,
	motelID int references Motel(motelID),
	postContent nvarchar(1000),
	postDate date,
	postTitle nvarchar(1000),
	postStatus nvarchar(20),
	isDeletePost bit,
)
go
Create table PostImage(
	postImageID int IDENTITY(1,1) primary key,
	postID int references Post(postID),
	postImageLink Image,
	isDeletePostImage bit,
)
go
Create table [Services](
	servicesID int IDENTITY(1,1) primary key,
	motelID int references Motel(motelID),
	servicesName nvarchar(100),
	price float,
	isDeleteServiecs bit,
)
go
Create table Room (
	roomID int IDENTITY(1,1) primary key,
	motelID int references Motel(motelID),
	roomName nvarchar(50),
	numberOfPeopleMax int,
	numberOfPeopleCurrent int,
	price float,
	area float,
	noteRoom nvarchar(1000),
	isDeleteRoom bit,
)
go
Create table Request (
	requestID int IDENTITY(1,1) primary key,
	roomID int references Room(roomID),
	content nvarchar(1000),
	[status] varchar(10),
	noteRequestOfRoom nvarchar(1000),
	noteRequestOfHost nvarchar(1000),
	createdAt datetime,
	updateAt datetime,
	isDeleteRequest bit,
	priceRequest float,
)
go
Create table Booking(
	BookingID int IDENTITY(1,1) primary key,
	RoomID int references room(RoomID),
	isDeleteBooking bit,
	isDraft bit,
	startAt date,
	endAt date,
)
go
Create table AccountOfBooking(
	AccountOfBookingID int IDENTITY(1,1) primary key,
	AccountID int references Account(AccountID),
	BookingID int references Booking(BookingID),
	startAt date,
	endAt date,
	isDeleteAccountOfBooking bit,
)
go
Create table ServicesOfBooking(
	servicesOfRoomID int IDENTITY(1,1) primary key,
	BookingID int references Booking(BookingID),
	servicesID int references [Services](servicesID),
	isDeleteServicesOfBooking bit,
	isDraft bit,	
)
go
Create table Bill(
	billID int IDENTITY(1,1) primary key,
	BookingID int references Booking(BookingID),
	dateOfPayment datetime,
	totalBill float,
	confirmBillByHost bit,
	confirmBillByCustomer bit,
	dateCreateBill datetime,
	isDraft bit,
	isDeleteBill bit,
	isSent bit
)
go
Create table BillDetail(
	billDetailID int IDENTITY(1,1) primary key,
	billID int references Bill(billID),
	servicesOfRoomID int references ServicesOfBooking(servicesOfRoomID),
	requestID int references Request(requestID),
	dateCreateBillDetail datetime,
	quantity int,
	totalBillDetail float,
	isDeleteBillDetail bit,
	isDraft bit,
	price int
)
go
Create table [Message](
	messageID int IDENTITY(1,1) primary key,
	fromUserID int references Account(AccountID),
	toUserID int references Account(AccountID),
	contentMessage nvarchar(1000),
	repMessage int references [Message](messageID),
	createAt datetime,
	isDeleteMessage bit,
)

