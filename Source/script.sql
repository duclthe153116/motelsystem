﻿Use Motel_System
select * from Account
insert into Account values('admin','21232f297a57a5a743894a0e4a801fc3',0) -- password = admin
insert into Account values('duclt','27506f07208ccf02e29689ab0627d402',0) -- password = duclt
insert into Account values('tuanhd','dcf12e97182876629f620dc0be2aacf1',0) -- password = tuanhd
insert into Account values('tuanvq','90422f4692a41bf5b43e6a76ae4be14b',0) -- password = tuanvq
insert into Account values('luongtth','8d49247299d37af908cb2dfcd6261788',0) -- password = luongtth
insert into Account values('hieupm','107852fa8092d8141bb1a8a8913b6373',0) -- password = hieupm
insert into Account values('sondt','e0f1670f4f07f5f7a44bb3191554e528',0) -- password sondt

select * from [Role]
insert into [Role] values('admin',0)
insert into [Role] values('host',0)
insert into [Role] values('customer',0)

select * from RoleOfAccount
insert into RoleOfAccount values(1,1,0)
insert into RoleOfAccount values(2,2,0)
insert into RoleOfAccount values(2,3,0)
insert into RoleOfAccount values(3,3,0)
insert into RoleOfAccount values(4,3,0)
insert into RoleOfAccount values(5,3,0)
insert into RoleOfAccount values(6,2,0)
insert into RoleOfAccount values(6,3,0)
insert into RoleOfAccount values(7,3,0)

select * from [Profile]
insert into [Profile] values(1,N'nguyen van admin','0123456789',N'Hà nội','admin@gmail.com','05/16/2001','Male',(select * from OPENROWSET(BULK N'E:\image\profile\1.jpg', SINGLE_BLOB) as T1),0)
insert into [Profile] values(2,N'Lê thiện đức','0123456789',N'Hà nội','duclt@gmail.com','05/16/2001','Male',(select * from OPENROWSET(BULK N'E:\image\profile\2.jpg', SINGLE_BLOB) as T1),0)
insert into [Profile] values(3,N'Hoàng danh tuấn','0123456789',N'Bắc Giang','tuanhd@gmail.com','05/16/2001','Male',(select * from OPENROWSET(BULK N'E:\image\profile\3.jpg', SINGLE_BLOB) as T1),0)
insert into [Profile] values(4,N'Vương quốc tuấn','0123456789',N'Hà nội','tuanvq@gmail.com','05/16/2001','Male',(select * from OPENROWSET(BULK N'E:\image\profile\4.jpg', SINGLE_BLOB) as T1),0)
insert into [Profile] values(5,N'Trương thị hiền lương','0123456789',N'Hưng Yên','luongtth@gmail.com','05/16/2001','FeMale',(select * from OPENROWSET(BULK N'E:\image\profile\5.jpg', SINGLE_BLOB) as T1),0)
insert into [Profile] values(6,N'Phạm minh hiếu','0123456789',N'Hà nội','hieupm@gmail.com','05/16/2001','Male',(select * from OPENROWSET(BULK N'E:\image\profile\6.jpg', SINGLE_BLOB) as T1),0)
insert into [Profile] values(7,N'Đỗ Thanh Sơn','0123456789','Sơn La','sondt@gmail.com','05/16/2001','Male',(select * from OPENROWSET(BULK N'E:\image\profile\7.jpg', SINGLE_BLOB) as T1),0)

select * from Motel
insert into Motel(AccountID,motelName,motelAddress,motelPhone,motelStatus,motelDescription,motelCreateAt,motelImage,isDeleteMotel) 
values(2,N'Nhà trọ Thiện Đức 1',N'Hà nội','0989868686','publish',N'Nhà trọ giá cả phải chăng, phù hợp với học sinh, sinh viên','2022-01-01',(select * from OPENROWSET(BULK N'E:\image\motel\1.jpg', SINGLE_BLOB) as T1),0)
insert into Motel(AccountID,motelName,motelAddress,motelPhone,motelStatus,motelDescription,motelCreateAt,motelImage,isDeleteMotel)
 values(2,N'Nhà trọ Thiện Đức 2',N'Hà nội','0989868686','pending',N'Nhà trọ giá cả phải chăng, phù hợp với học sinh, sinh viên','2022-01-02',(select * from OPENROWSET(BULK N'E:\image\motel\2.jpg', SINGLE_BLOB) as T1),0)
insert into Motel(AccountID,motelName,motelAddress,motelPhone,motelStatus,motelDescription,motelCreateAt,motelImage,isDeleteMotel) 
values(2,N'Nhà trọ Thiện Đức 3',N'Hà nội','0989868686','publish',N'Nhà trọ giá cả phải chăng, phù hợp với học sinh, sinh viên','2022-01-02',(select * from OPENROWSET(BULK N'E:\image\motel\3.jpg', SINGLE_BLOB) as T1),0)
insert into Motel(AccountID,motelName,motelAddress,motelPhone,motelStatus,motelDescription,motelCreateAt,motelImage,isDeleteMotel) 
values(6,N'Nhà trọ Minh Hiếu 1',N'Hà nội','0989868686','publish',N'Nhà trọ giá cả phải chăng, phù hợp với học sinh, sinh viên','2022-01-03',(select * from OPENROWSET(BULK N'E:\image\motel\4.jpg', SINGLE_BLOB) as T1),0)
insert into Motel(AccountID,motelName,motelAddress,motelPhone,motelStatus,motelDescription,motelCreateAt,motelImage,isDeleteMotel) 
values(6,N'Nhà trọ Minh Hiếu 2',N'Hà nội','0989868686','pending',N'Nhà trọ giá cả phải chăng, phù hợp với học sinh, sinh viên','2022-01-04',(select * from OPENROWSET(BULK N'E:\image\motel\5.jpg', SINGLE_BLOB) as T1),0)
insert into Motel(AccountID,motelName,motelAddress,motelPhone,motelStatus,motelDescription,motelCreateAt,motelImage,isDeleteMotel)
 values(6,N'Nhà trọ Minh Hiếu 3',N'Hà nội','0989868686','pending',N'Nhà trọ giá cả phải chăng, phù hợp với học sinh, sinh viên','2022-01-05',(select * from OPENROWSET(BULK N'E:\image\motel\6.jpg', SINGLE_BLOB) as T1),0)


select * from MotelImage
insert into MotelImage(motelID,imageLink) values (1,(select * from OPENROWSET(BULK N'E:\image\motel\1.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (1,(select * from OPENROWSET(BULK N'E:\image\motel\2.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (1,(select * from OPENROWSET(BULK N'E:\image\motel\2.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (1,(select * from OPENROWSET(BULK N'E:\image\motel\4.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (2,(select * from OPENROWSET(BULK N'E:\image\motel\6.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (2,(select * from OPENROWSET(BULK N'E:\image\motel\7.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (2,(select * from OPENROWSET(BULK N'E:\image\motel\8.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (2,(select * from OPENROWSET(BULK N'E:\image\motel\9.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (3,(select * from OPENROWSET(BULK N'E:\image\motel\11.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (3,(select * from OPENROWSET(BULK N'E:\image\motel\12.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (3,(select * from OPENROWSET(BULK N'E:\image\motel\14.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (3,(select * from OPENROWSET(BULK N'E:\image\motel\15.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (4,(select * from OPENROWSET(BULK N'E:\image\motel\16.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (4,(select * from OPENROWSET(BULK N'E:\image\motel\17.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (4,(select * from OPENROWSET(BULK N'E:\image\motel\18.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (4,(select * from OPENROWSET(BULK N'E:\image\motel\20.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (5,(select * from OPENROWSET(BULK N'E:\image\motel\16.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (5,(select * from OPENROWSET(BULK N'E:\image\motel\17.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (5,(select * from OPENROWSET(BULK N'E:\image\motel\18.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (5,(select * from OPENROWSET(BULK N'E:\image\motel\20.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (6,(select * from OPENROWSET(BULK N'E:\image\motel\16.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (6,(select * from OPENROWSET(BULK N'E:\image\motel\17.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (6,(select * from OPENROWSET(BULK N'E:\image\motel\18.jpg', SINGLE_BLOB) as T1))
insert into MotelImage(motelID,imageLink) values (6,(select * from OPENROWSET(BULK N'E:\image\motel\20.jpg', SINGLE_BLOB) as T1))

select * from Post
insert into Post(motelID,postContent,postDate,postTitle,postStatus, isDeletePost) values(1,N'Chúng tôi có mọi trọ mà bạn cần','05/15/2022',N'Phòng trọ giá rẻ','publish',0)
insert into Post(motelID,postContent,postDate,postTitle,postStatus, isDeletePost) values(1,N'Trọ bạn thuê, chúng tôi có','05/15/2022',N'Phòng trọ sinh viên','publish',0)
insert into Post(motelID,postContent,postDate,postTitle,postStatus, isDeletePost) values(1,N'Trọ hot hot hot','05/15/2022',N'Phòng trọ tiên','pending',0)
insert into Post(motelID,postContent,postDate,postTitle,postStatus, isDeletePost) values(3,N'Trọ ngay cạnh cổng trường FPT','05/15/2022',N'Cho thuê trọ','publish',0)
insert into Post(motelID,postContent,postDate,postTitle,postStatus, isDeletePost) values(3,N'Trọ chúng tôi có mọi dịch vụ bạn cần','05/15/2022',N'Cho mướn trọ','publish',0)
insert into Post(motelID,postContent,postDate,postTitle,postStatus, isDeletePost) values(3,N'Siêu hạ giá, giá học sinh phòng phụ huynh','05/15/2022',N'Bán phòng trọ','pending',0)
insert into Post(motelID,postContent,postDate,postTitle,postStatus, isDeletePost) values(4,N'Không xịn không lấy tiền','05/15/2022',N'Phòng trọ xin sò','publish',0)
insert into Post(motelID,postContent,postDate,postTitle,postStatus, isDeletePost) values(4,N'Đến với chúng tôi là đến với thiên đường','05/15/2022',N'Phòng trọ lung linh','publish',0)
insert into Post(motelID,postContent,postDate,postTitle,postStatus, isDeletePost) values(4,N'Hàng xóm yên tĩnh,thân thiện đặc biệt có nhiều .... ','05/15/2022',N'Nhà trọ bồng lai','publish',0)
insert into Post(motelID,postContent,postDate,postTitle,postStatus, isDeletePost) values(4,N'Tiên cảnh bồng lai','05/15/2022',N'Nhà trọ tiên cảnh','pending',0)

select * from Room
insert into Room values(1,'100',4,0,10000,100,null,0)
insert into Room values(1,'101',4,1,10000,100,null,0)
insert into Room values(1,'102',4,2,10000,100,null,0)
insert into Room values(1,'103',4,3,10000,100,null,0)
insert into Room values(1,'104',4,2,10000,100,null,0)
insert into Room values(1,'105',4,3,10000,100,null,0)
insert into Room values(1,'106',4,2,10000,100,null,0)
insert into Room values(1,'107',4,1,10000,100,null,0)
insert into Room values(1,'108',4,4,10000,100,null,0)
insert into Room values(1,'109',4,4,10000,100,null,0)
insert into Room values(3,'101',4,2,10000,100,null,0)
insert into Room values(3,'102',4,3,10000,100,null,0)
insert into Room values(3,'103',4,3,10000,100,null,0)
insert into Room values(3,'104',4,2,10000,100,null,0)
insert into Room values(3,'105',4,1,10000,100,null,0)
insert into Room values(3,'106',4,1,10000,100,null,0)
insert into Room values(3,'107',4,2,10000,100,null,0)
insert into Room values(3,'108',4,0,10000,100,null,0)
insert into Room values(3,'109',4,0,10000,100,null,0)
insert into Room values(4,'101',4,3,10000,100,null,0)
insert into Room values(4,'102',4,2,10000,100,null,0)
insert into Room values(4,'103',4,1,10000,100,null,0)
insert into Room values(4,'104',4,2,10000,100,null,0)
insert into Room values(4,'105',4,2,10000,100,null,0)
insert into Room values(4,'106',4,1,10000,100,null,0)
insert into Room values(4,'107',4,2,10000,100,null,0)
insert into Room values(4,'108',4,1,10000,100,null,0)
insert into Room values(4,'109',4,1,10000,100,null,0)

select * from Services
insert into Services(motelID, servicesName, price, isDeleteServiecs) values(1, N'Điện', 3500,0)
insert into Services(motelID, servicesName, price, isDeleteServiecs) values(1, N'Nước', 50000,0)
insert into Services(motelID, servicesName, price, isDeleteServiecs) values(1, N'Mạng', 50000,0)
insert into Services(motelID, servicesName, price, isDeleteServiecs) values(1, N'Gửi xe', 50000,0)
insert into Services(motelID, servicesName, price, isDeleteServiecs) values(3, N'Điện', 2800,0)
insert into Services(motelID, servicesName, price, isDeleteServiecs) values(3, N'Nước', 50000,0)
insert into Services(motelID, servicesName, price, isDeleteServiecs) values(3, N'Mạng', 50000,0)
insert into Services(motelID, servicesName, price, isDeleteServiecs) values(3, N'Gửi xe', 30000,0)
insert into Services(motelID, servicesName, price, isDeleteServiecs) values(4, N'Điện', 4000,0)
insert into Services(motelID, servicesName, price, isDeleteServiecs) values(4, N'Nước', 50000,0)
insert into Services(motelID, servicesName, price, isDeleteServiecs) values(4, N'Mạng', 75000,0)
insert into Services(motelID, servicesName, price, isDeleteServiecs) values(4, N'Điện', 2000,0)

select * from Request
insert into Request (roomID,content,[status],createdAt) values (1,N'Sua quat may','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (2,N'Sua dieu hoa ','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (3,N'Sua ong nuoc','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (4,N'Sua dien ','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (5,N'Sua quat may','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (1,N'Sua dieu hoa','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (2,N'Sua ong nuoc','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (3,N'Sua dien','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (4,N'Sua ong nuoc','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (5,N'Sua ong nuoc','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (1,N'Sua ong nuoc','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (2,N'Sua ong nuoc','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (3,N'Sua ong nuoc','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (4,N'Kiem tra phong','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (5,N'Sua dien','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (1,N'Sua bep','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (2,N'Sua quat may','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (3,N'Sua dien','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (4,N'Bi ro nuoc','0',getDate())
insert into Request (roomID,content,[status],createdAt) values (5,N'Sua dieu hoa','0',getDate())
update Request set isDeleteRequest = 0
update Request set priceRequest = 0

select * from PostImage
insert into PostImage(postID,postImageLink,isDeletePostImage) values (5,(select * from OPENROWSET(BULK N'E:\image\post\1.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (5,(select * from OPENROWSET(BULK N'E:\image\post\2.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (5,(select * from OPENROWSET(BULK N'E:\image\post\3.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (5,(select * from OPENROWSET(BULK N'E:\image\post\4.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (6,(select * from OPENROWSET(BULK N'E:\image\post\5.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (6,(select * from OPENROWSET(BULK N'E:\image\post\6.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (6,(select * from OPENROWSET(BULK N'E:\image\post\7.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (6,(select * from OPENROWSET(BULK N'E:\image\post\8.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (7,(select * from OPENROWSET(BULK N'E:\image\post\9.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (7,(select * from OPENROWSET(BULK N'E:\image\post\10.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (7,(select * from OPENROWSET(BULK N'E:\image\post\11.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (7,(select * from OPENROWSET(BULK N'E:\image\post\12.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (8,(select * from OPENROWSET(BULK N'E:\image\post\13.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (8,(select * from OPENROWSET(BULK N'E:\image\post\14.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (8,(select * from OPENROWSET(BULK N'E:\image\post\15.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (8,(select * from OPENROWSET(BULK N'E:\image\post\16.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (1,(select * from OPENROWSET(BULK N'E:\image\post\17.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (1,(select * from OPENROWSET(BULK N'E:\image\post\18.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (1,(select * from OPENROWSET(BULK N'E:\image\post\19.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (1,(select * from OPENROWSET(BULK N'E:\image\post\20.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (2,(select * from OPENROWSET(BULK N'E:\image\post\1.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (2,(select * from OPENROWSET(BULK N'E:\image\post\2.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (2,(select * from OPENROWSET(BULK N'E:\image\post\3.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (2,(select * from OPENROWSET(BULK N'E:\image\post\4.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (3,(select * from OPENROWSET(BULK N'E:\image\post\5.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (3,(select * from OPENROWSET(BULK N'E:\image\post\6.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (3,(select * from OPENROWSET(BULK N'E:\image\post\7.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (3,(select * from OPENROWSET(BULK N'E:\image\post\8.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (4,(select * from OPENROWSET(BULK N'E:\image\post\9.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (4,(select * from OPENROWSET(BULK N'E:\image\post\10.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (4,(select * from OPENROWSET(BULK N'E:\image\post\11.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (4,(select * from OPENROWSET(BULK N'E:\image\post\12.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (9,(select * from OPENROWSET(BULK N'E:\image\post\13.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (9,(select * from OPENROWSET(BULK N'E:\image\post\14.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (9,(select * from OPENROWSET(BULK N'E:\image\post\15.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (9,(select * from OPENROWSET(BULK N'E:\image\post\16.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (10,(select * from OPENROWSET(BULK N'E:\image\post\17.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (10,(select * from OPENROWSET(BULK N'E:\image\post\18.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (10,(select * from OPENROWSET(BULK N'E:\image\post\19.jpg', SINGLE_BLOB) as T1),0);
insert into PostImage(postID,postImageLink,isDeletePostImage) values (10,(select * from OPENROWSET(BULK N'E:\image\post\20.jpg', SINGLE_BLOB) as T1),0);

select * from Message;
insert into Message(fromUserID, toUserID, contentMessage, createAt, isDeleteMessage)values(3, 2, N'Tôi muốn thuê trọ', '05/15/2022', 0)
insert into Message(fromUserID, toUserID, contentMessage, createAt, isDeleteMessage)values(4, 2, N'Ok', '05/16/2022',0)
insert into Message(fromUserID, toUserID, contentMessage, createAt, isDeleteMessage)values(5, 2, N'Trọ này giá ra sao', '05/17/2022',0)
insert into Message(fromUserID, toUserID, contentMessage, createAt, isDeleteMessage)values(6, 2, N'Trọ này không thuê chỉ cho', '05/18/2022',0)
insert into Message(fromUserID, toUserID, contentMessage, createAt, isDeleteMessage)values(2, 3, N'Trọ đẹp bạn có muốn thuê không', '05/19/2022',0)

select * from Booking
insert into Booking (RoomID,isDeleteBooking,isDraft) values(1,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(2,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(3,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(4,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(5,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(6,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(7,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(8,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(9,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(10,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(11,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(12,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(13,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(14,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(15,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(16,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(17,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(18,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(19,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(20,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(21,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(22,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(23,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(24,0,0)
insert into Booking (RoomID,isDeleteBooking,isDraft) values(25,0,0)

update Booking set startAt = getDate(), endAt = GETDATE() + 30
 select * from AccountOfBooking
insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (4,21,'2021-05-01','2021-06-01');
insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (3,21,'2021-05-01','2021-06-01');
insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (5,22,'2021-05-01','2021-06-01');
insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (6,22,'2021-05-01','2021-06-01');

insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (4,23,'2021-05-01','2021-06-01');
insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (6,24,'2021-05-01','2021-06-01');
insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (5,24,'2021-05-01','2021-06-01');

insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (4,10,'2021-05-01','2021-06-01');
insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (6,10,'2021-05-01','2021-06-01');
insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (4,15,'2021-05-01','2021-06-01');

insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (6,1,'2021-05-01','2021-06-01');
insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (6,2,'2021-05-01','2021-06-01');
insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (6,3,'2021-05-01','2021-06-01');
insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (6,4,'2021-05-01','2021-06-01');

insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (4,20,'2021-05-01','2021-06-01');
insert into AccountOfBooking(AccountID,BookingID,startAt,endAt) values (4,25,'2021-05-01','2021-06-01');

insert into ServicesOfBooking(servicesID, BookingID, isDeleteServicesOfBooking, isDraft) values(5, 1,0,0)
insert into ServicesOfBooking(servicesID, BookingID, isDeleteServicesOfBooking, isDraft) values(6, 1,0,0)
insert into ServicesOfBooking(servicesID, BookingID, isDeleteServicesOfBooking, isDraft) values(7, 1,0,0)

insert into ServicesOfBooking(servicesID, BookingID, isDeleteServicesOfBooking, isDraft) values(5, 2,0,0)
insert into ServicesOfBooking(servicesID, BookingID, isDeleteServicesOfBooking, isDraft) values(6, 2,0,0)
insert into ServicesOfBooking(servicesID, BookingID, isDeleteServicesOfBooking, isDraft) values(10, 3,0,0)
insert into ServicesOfBooking(servicesID, BookingID, isDeleteServicesOfBooking, isDraft) values(11, 3,0,0)
insert into ServicesOfBooking(servicesID, BookingID, isDeleteServicesOfBooking, isDraft) values(9, 3,0,0)

insert into ServicesOfBooking(servicesID, BookingID, isDeleteServicesOfBooking, isDraft) values(10, 4,0,0)
insert into ServicesOfBooking(servicesID, BookingID, isDeleteServicesOfBooking, isDraft) values(9, 4,0,0)

insert into ServicesOfBooking(servicesID, BookingID, isDeleteServicesOfBooking, isDraft) values(11, 5,0,0)
select * from Services



update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 1))
where roomID = 1
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 2))
where roomID = 2
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 3))
where roomID = 3
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 4))
where roomID = 4
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 5))
where roomID = 5
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 6))
where roomID = 6
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 7))
where roomID = 7
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 8))
where roomID = 8
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 9))
where roomID = 9
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 10))
where roomID = 10
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 11))
where roomID = 11
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 12))
where roomID = 12
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 13))
where roomID = 13
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 14))
where roomID = 14
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 15))
where roomID = 15
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 16))
where roomID = 16
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 17))
where roomID = 17
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 18))
where roomID = 18
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 19))
where roomID = 19
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 20))
where roomID = 20
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 21))
where roomID = 21
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 22))
where roomID = 22
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 23))
where roomID = 23
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 24))
where roomID = 24
update Room
set numberOfPeopleCurrent = (select count(AccountID) from AccountOfBooking where BookingID = (select BookingID from Booking where RoomID = 25))
where roomID = 25

insert into Bill values(1,null,100000,0,0,'2022-01-06',0,0,0)
insert into Bill values(2,null,200000,0,0,'2022-01-01',0,0,0)
insert into Bill values(3,null,300000,0,0,'2021-12-31',0,0,0)
insert into Bill values(4,null,400000,0,0,'2022-07-12',0,0,0)
insert into Bill values(5,null,150000,0,0,'2022-06-30',0,0,0)

insert into BillDetail values(1,2,null,null,1,50000,0,0,50000)
insert into BillDetail values(1,2,null,null,1,50000,0,0,50000)

insert into BillDetail values(2,5,null,null,4,200000,0,0,50000)

insert into BillDetail values(3,6,null,null,3,150000,0,0,50000)
insert into BillDetail values(3,7,null,null,4,200000,0,0,50000)

insert into BillDetail values(4,9,null,null,8,400000,0,0,50000)

insert into BillDetail values(5,11,null,null,2,150000,0,0,75000)


go
create Trigger TG_DeleteAccountByAdmin
on Account
after update
as
begin
	declare @id int 
	declare @isdelete bit
    select @id = AccountID, @isdelete = isDeleteAccount from inserted
    if @isdelete = 1
		begin
			update Motel set isDeleteMotel = 1 where AccountID = @id
			update Booking set isDeleteBooking = 1 where BookingID in (select BookingID from AccountOfBooking where AccountID = @id)
			update AccountOfBooking set isDeleteAccountOfBooking = 1 where AccountID = @id
			update [Message] set isDeleteMessage = 1 where fromUserID = @id or toUserID = @id
			update RoleOfAccount set isDeleteRoleOfAccount = 1 where AccountID = @id
			update [Profile] set isDeleteProfile = 1 where AccountID = @id
		end
	else if @isdelete = 0
		begin
			update Motel set isDeleteMotel = 0 where AccountID = @id
			update Booking set isDeleteBooking = 0 where BookingID in (select BookingID from AccountOfBooking where AccountID = @id)
			update AccountOfBooking set isDeleteAccountOfBooking = 0 where AccountID = @id
			update [Message] set isDeleteMessage = 0 where fromUserID = @id or toUserID = @id
			update RoleOfAccount set isDeleteRoleOfAccount = 0 where AccountID = @id  and (RoleID = 2 or RoleID = 3)
			update [Profile] set isDeleteProfile = 0 where AccountID = @id
		end
end
go
create trigger TG_DeleteMotelByAdmin
on Motel
after update
as
begin
	declare @id int
	declare @isdelete bit
	select @id = motelID,@isdelete = isDeleteMotel from inserted
	if @isdelete = 1
		begin
			update Booking set isDeleteBooking = 1 where RoomID in ( select roomID from Room where motelID = @id)
			update AccountOfBooking set isDeleteAccountOfBooking = 1 where BookingID in (select BookingID from Booking where RoomID in (select RoomID from Room where motelID = @id))
			update ServicesOfBooking set isDeleteServicesOfBooking = 1 where servicesID in (select servicesID from Services where motelID = @id)
			update Services set isDeleteServiecs = 1 where  motelID = @id
			update Request set isDeleteRequest = 1 where roomID in (select RoomID from Room where motelID = @id)
			update Room set isDeleteRoom = 1 where motelID = @id
			update PostImage set isDeletePostImage = 1 where postID in ( select postID from Post where motelID = @id)
			update Post set isDeletePost = 1 where motelID = @id			
		end
	if @isdelete = 0
		begin
			update Booking set isDeleteBooking = 0 where RoomID in ( select roomID from Room where motelID = @id)
			update AccountOfBooking set isDeleteAccountOfBooking = 0 where BookingID in (select BookingID from Booking where RoomID in (select RoomID from Room where motelID = @id))
			update ServicesOfBooking set isDeleteServicesOfBooking = 0 where servicesID in (select servicesID from Services where motelID = @id)
			update Services set isDeleteServiecs = 0 where  motelID = @id
			update Request set isDeleteRequest = 0 where roomID in (select RoomID from Room where motelID = @id)
			update Room set isDeleteRoom = 0 where motelID = @id
			update PostImage set isDeletePostImage = 0 where postID in ( select postID from Post where motelID = @id)
			update Post set isDeletePost = 0 where motelID = @id			
		end
end
go
create trigger TG_DeleteRoleOfAccount
on RoleOfAccount
after update
as
begin
	declare @accountid int
	declare @isdelete bit
	declare @roleid int
	select @accountid=AccountID, @isdelete=isDeleteRoleOfAccount, @roleid=RoleID from inserted
	if @isdelete = 1 and @roleid = 2
		begin
		update Motel set isDeleteMotel = 1 where AccountID = @accountid
		end
	else if @isdelete = 1 and @roleid = 3
		begin
		update AccountOfBooking set isDeleteAccountOfBooking = 1 where AccountID = @accountid
		end
end
go
create trigger TG_UpdateRoleOfAccount
on RoleOfAccount
after update
as
begin
	declare @accountid int
	declare @isdelete bit
	declare @roleid int
	select @accountid=AccountID, @isdelete=isDeleteRoleOfAccount, @roleid=RoleID from inserted
	if @isdelete = 0 and @roleid = 2
		begin
		update Motel set isDeleteMotel = 0 where AccountID = @accountid
		end
	else if @isdelete = 0 and @roleid = 3
		begin
		update AccountOfBooking set isDeleteAccountOfBooking = 0 where AccountID = @accountid
		end
end

